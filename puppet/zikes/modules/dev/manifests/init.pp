#postgres installs with the root linux user 'postgres'
$db_postgres_pass 	= '12345'

# PostgreSQL
#----------------------
class { 'postgresql::server':
  postgres_password => $db_postgres_pass,
}

postgresql::server::pg_hba_rule { 'allow application network to access app database':
  description => "Allow local access to osm db for user postgres",
  type        => 'local',
  database    => 'osm',
  user        => 'postgres',
  auth_method => 'trust',
}

class zxpackages {
	package { 'python' : ensure => installed }
	package { 'python-lxml' : ensure => installed }
	package { 'python-pip' : ensure => installed }
	package { 'python-dev' : ensure => installed }
	package { 'python-requests' : ensure => installed }
	package { 'python-numpy' : ensure => installed }

	#google protocol buffers
	package { 'libprotobuf-dev' : ensure => installed }
	package { 'python-protobuf' : ensure => installed }
	package { 'protobuf-compiler' : ensure => installed }

	#python->postgresql module
	package { 'python-psycopg2' : ensure => installed }
	package { 'libffi-dev' : ensure => installed }
	package { 'python-paramiko' : ensure => installed }


	#GIS extensions for postgreSQL (see: http://trac.osgeo.org/postgis/wiki/UsersWikiPostGIS21UbuntuPGSQL93Apt)
	package { 'postgis' : ensure => installed }

	#C++
	#Boost
	package { 'libboost-all-dev' : ensure => installed }
	#pqxx (C++ library for postgresql)
	package { 'libpqxx-dev' : ensure => installed }
	#pqxx (C++ library for postgresql)
	package { 'libxml++2.6-dev' : ensure => installed }
}


class dev {
  	class { 'zxpackages': }
}
