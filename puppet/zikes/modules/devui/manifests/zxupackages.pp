class zxupackages {

	#python GTK bindings
	package { 'python-tk' : ensure => installed }

	#GUI console to postgreSQL
	package { 'pgadmin3' : ensure => installed }
}
