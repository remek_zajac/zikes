#!/usr/bin/env bash
set -e

THISFOLDER="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
export PYTHONPATH="$PYTHONPATH:/$THISFOLDER"
for TESTFOLDER in "lib/osmsql/test"
do
	pushd "$THISFOLDER/$TESTFOLDER"
	python -m unittest discover
	popd
done
