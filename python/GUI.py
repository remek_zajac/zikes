# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# (C) 2012 Remek Zajac
#!/usr/bin/python

import gtk
import cairo
import sys

#local modules
from lib import pyGpxMapWidget
from lib import pyConfig


class GpxGUI(gtk.Window):

    def __init__(self, dbName):
        super(GpxGUI, self).__init__()

        self.set_title(dbName)
        self.resize(1000, 800)
        self.set_position(gtk.WIN_POS_CENTER)
        self.connect("destroy", gtk.main_quit)

        #Load config file
        self.config = pyConfig.Config()
        self.config.parse()

        #Load the main map widget
        self.mapWidget = pyGpxMapWidget.GPXMapWidget(self, dbName)

        #add menu
        menu_bar = gtk.MenuBar()
        menu_bar.show()

        #View Menu item
        view_menubar_item = gtk.MenuItem("View")
        view_menubar_item.show()
        view_menubar_item.set_submenu(self.mapWidget.viewMenu())
        menu_bar.append(view_menubar_item)

        #Tools Menu item
        tools_menu_item = gtk.MenuItem("Tools")
        tools_menu_item.show()
        tools_menu_item.set_submenu(self.mapWidget.toolsMenu())
        menu_bar.append(tools_menu_item)


        #status bar
        status_bar = gtk.Statusbar()
        status_bar.set_size_request(200, 20)
        status_bar.show()
        self.mapWidget.setStatusBar(status_bar)

        #pack display
        vbox = gtk.VBox(False, 0)
        self.add(vbox)
        vbox.pack_start(menu_bar, False, False, 2)
        vbox.pack_start(self.mapWidget, True, True, 2)
        vbox.pack_end(status_bar, False, False, 0)

        #finalise
        self.show_all()



    def clear_map(self, event):
        self.mapWidget.clear_map()

    def show_locally_rendered_tiles(self, event):
        self.mapWidget.show_locally_rendered_tiles()

def main():
    dbName = "osm"
    if len(sys.argv) == 2:
        dbName = sys.argv[1]
    GpxGUI(dbName)
    gtk.main()

if __name__ == "__main__":
    main()

