# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# (C) 2012 Remek Zajac
#!/usr/bin/python

import math
import random


class Point(object):
    def __init__(self, xy):
        self.set(xy)

    def clone(self):
        return Point(self)

    def __getitem__(self, idx):
        if idx == 0:
            return self.x
        elif idx == 1:
            return self.y
        assert False, "illegal index on a two-ple point %d" % idx

    def set(self, xy):
        if isinstance(xy, list):
            xy = tuple(xy)
        if isinstance(xy, tuple):
            self.x = xy[0]
            self.y = xy[1]
        elif isinstance(xy, Point):
            self.x = xy.x
            self.y = xy.y
        elif type(xy) is str:
            xy = GeoPoint.xyFromString(xy)
        else:
            assert False, "Point ctor given unsupported initialiser: "+str(xy)
        return self

    @staticmethod
    def xyFromString(latlon):
        return map(lambda elem: float(elem), latlon.strip(')(').split(","))

    @staticmethod
    def toPoint(arg):
        if isinstance(arg, Point):
            return arg
        return Point(arg)

    def equals(self, other):
        return self.x == other.x and self.y == other.y

    def distanceSquared(self, other):
        return (other.x - self.x) ** 2 + (other.y - self.y) ** 2

    def distance(self, other):
        return math.sqrt(self.distanceSquared(other))


class Vector(Point):
    def __init__(self, xy):
        if isinstance(xy, tuple):
            if len(xy) == 4:
                xy = (xy[2]-xy[0], xy[3]-xy[1])
            elif len(xy) == 2:
                if isinstance(xy[0], Point):
                    xy = ((xy[0].x,xy[0].y), xy[1])
                if isinstance(xy[1], Point):
                    xy = (xy[0], (xy[1].x,xy[1].y))
                if isinstance(xy[0], tuple) and isinstance(xy[1], tuple):
                    xy = (xy[1][0]-xy[0][0], xy[1][1]-xy[0][1])
                assert isinstance(xy, tuple)
            else:
                assert False
        elif isinstance(xy, Point):
            xy = (xy.x, xy.y)
        super(Vector, self).__init__(xy)

    def draw(self, image, origin, fill):
        dst = (origin[0]+self.x, origin[1]+self.y)
        draw = ImageDraw.Draw(image)
        draw.line((origin, dst), fill)

    def reverse(self):
        return Vector((-self.x,-self.y))

    def orthogonal(self):
        return Vector((-self.y, self.x))

    def setLength(self, length):
        if self.x == 0:
            return Vector((0, length))
        y2x = (1.0*self.y/self.x)
        newX = 1.0*math.copysign(1, self.x)*math.sqrt((length**2)/(1+y2x**2))
        newY = 1.0*math.copysign(1, self.y)*math.sqrt(length**2-newX**2)
        return Vector((newX, newY))

    def lengthSquared(self):
        return self.x**2 + self.y**2

    def length(self):
        return math.sqrt(self.lengthSquared())

    def addToPoint(self, point):
        return (point[0]+self.x, point[1]+self.y)

    def add(self, otherVector):
        return Vector((self.x+otherVector.x, self.y+otherVector.y))

    def dotProduct(self, otherVector):
        return self.x * otherVector.x + self.y * otherVector.y

    def angleRad(self, otherVector):
        multipliedLenths = self.length()*otherVector.length()
        if multipliedLenths:
            cos = self.dotProduct(otherVector)/multipliedLenths
            return math.acos(cos)
        else:
            return 0.0

    def rotate(self, angle):
        if angle != 0:
            radians = math.radians(360 - angle)
            sinAngle = math.sin(radians)
            cosAngle = math.cos(radians)
            return Vector(
                (cosAngle * self.x - sinAngle * self.y,
                 sinAngle * self.x + cosAngle * self.y)
            )

class GeoPoint(Point):

    KEarthRadiusKm  = 6371.0
    KEarthRadiusMts = KEarthRadiusKm*1000

    @staticmethod
    def toRad(deg):
        return deg * math.pi / 180

    @staticmethod
    def toDeg(rad):
        return rad * 180 / math.pi

    @staticmethod
    def latlondelta2km(latlonstart, latlonend):
        dLat = math.radians(latlonend[0]-latlonstart[0])
        dLon = math.radians(latlonend[1]-latlonstart[1])
        latstart = math.radians(latlonstart[0])
        latend = math.radians(latlonend[0])
        a = math.sin(dLat/2) * math.sin(dLat/2) + math.sin(dLon/2) * math.sin(dLon/2) * math.cos(latstart) * math.cos(latend);
        c = 2 * math.atan2(math.sqrt(a), math.sqrt(1-a));
        return GeoPoint.KEarthRadiusKm * c

    def __init__(self, latlon, eleMts=None):
        super(GeoPoint, self).__init__(latlon)
        self.mEleMts = eleMts

    def latlon(self):
        return (self.x, self.y)

    def lat(self):
        return self.x

    def lon(self):
        return self.y

    def eleMts(self):
        return self.mEleMts

    def clone(self):
        return GeoPoint(self)

    def distanceKms(self, otherGeoPoint):
        if isinstance(otherGeoPoint, tuple):
            otherGeoPoint = GeoPoint(otherGeoPoint)
        assert isinstance(otherGeoPoint, GeoPoint), "Expecting a GeoPoint, got: "+str(otherGeoPoint)
        return GeoPoint.latlondelta2km(self.latlon(), otherGeoPoint.latlon())

    def distanceMts(self, otherGeoPoint):
        return 1000*self.distanceKms(otherGeoPoint)

    def __str__(self):
        return str(self.lat())+","+str(self.lon())

    def bearing(self, other):
        frm = Point((GeoPoint.toRad(self.x),  GeoPoint.toRad(self.y)))
        to  = Point((GeoPoint.toRad(other.x), GeoPoint.toRad(other.y)))

        y = math.sin(to.y - frm.y) *math.cos(to.x)
        x = math.cos(frm.x)*math.sin(to.x) - math.sin(frm.x)*math.cos(to.x) * math.cos(to.y - frm.y)
        return GeoPoint.toDeg(math.atan2(y, x))

    def offsetCourse(self, distanceMts, aCourseRad):
        earthPerimeter = 2 * math.pi * GeoPoint.KEarthRadiusMts
        distance       = 2 * math.pi * distanceMts / earthPerimeter
        srcLat         = GeoPoint.toRad(self.x)
        srctLon        = GeoPoint.toRad(self.y)

        # http://williams.best.vwh.net/avform.htm#LL
        dstLat = math.asin(math.sin(srcLat)*math.cos(distance)+math.cos(srcLat)*math.sin(distance)*math.cos(aCourseRad))
        dlon   = math.atan2(math.sin(aCourseRad)*math.sin(distance)*math.cos(srcLat),math.cos(distance)-math.sin(srcLat)*math.sin(dstLat))
        dstLon = srctLon - dlon

        return GeoPoint((GeoPoint.toDeg(dstLat), GeoPoint.toDeg(dstLon)))

class Line(object):

    def __init__(self, fromPoint, toPoint):
        if isinstance(fromPoint, tuple):
            fromPoint = Point(fromPoint)
        if isinstance(toPoint, tuple):
            toPoint = Point(toPoint)
        assert isinstance(fromPoint, Point) and isinstance(toPoint, Point)

        self.mBegining  = fromPoint
        self.mEnd       = toPoint
        self.mVector    = Vector((fromPoint, toPoint))

    def distanceFromProjection(self, point):
        point = Point.toPoint(point)
        t = self.__projectionRatio(point);
        if t < 0:
            #Beyond the mEnd end of the segment
            return None
        elif t > 1:
            #beyond the mBegining end of the segment
            return None
        projection = self.__projection(t)
        return math.sqrt((projection.x-point.x)**2+(projection.y-point.y)**2)

    def __projectionRatio(self, point):
        return ((point.x-self.mBegining.x)*(self.mEnd.x-self.mBegining.x)+(point.y-self.mBegining.y)*(self.mEnd.y-self.mBegining.y))/float(self.mVector.lengthSquared())

    def __projection(self, projectionRatio):
        return self.mBegining.clone().set(
            (self.mBegining.x + projectionRatio*(self.mEnd.x-self.mBegining.x),
             self.mBegining.y + projectionRatio*(self.mEnd.y-self.mBegining.y))
            )

    def projection(self, point, onLine=True):
        point = Point.toPoint(point)
        t = self.__projectionRatio(point)
        if onLine:
            if t < 0:
                #Beyond the mEnd end of the segment
                return None
            elif t > 1:
                #beyond the mBegining end of the segment
                return None
        return self.__projection(t)

    def middle(self):
        return (self.mBegining.x+(self.mEnd.x-self.mBegining.x)/2, self.mBegining.y+(self.mEnd.y-self.mBegining.y)/2)

    def start(self):
        return self.mBegining

    def end(self):
        return self.mEnd

    def begin(self):
        return self.mBegining

    def vector(self):
        return Vector(
            (self.mEnd.x-self.mBegining.x, self.mEnd.y-self.mBegining.y)
        )


class GeoLine(Line):
    def __init__(self, fromPoint, toPoint):
        if isinstance(fromPoint, tuple):
            fromPoint = GeoPoint(fromPoint)
        if isinstance(toPoint, tuple):
            toPoint = GeoPoint(toPoint)
        assert isinstance(fromPoint, GeoPoint) and isinstance(toPoint, GeoPoint)
        super(GeoLine, self).__init__(fromPoint, toPoint)

    def lengthMts(self):
        return self.mBegining.distanceMts(self.mEnd)


class GeoRegion(object):
    nullNW = (-361,+361) #illegal nw (se extremum)
    nullSE = (361,-361)  #illegal sw (nw extremum)
    maxNW = (90,-180)  #max nw
    maxSE = (-90,180)  #max se

    @staticmethod
    def _nw(latlon1, latlon2):
        if (latlon1 == GeoRegion.nullSE): #check illegal values, if latlon1 is nullNW, it will organically loose with latlon2, otherwise it might win, so prevent it
            return latlon2
        if (latlon2 == GeoRegion.nullSE):
            return latlon1
        return GeoPoint(
            (max(latlon1[0], latlon2[0]), min(latlon1[1], latlon2[1]))
        )

    @staticmethod
    def _se(latlon1, latlon2):
        if (latlon1 == GeoRegion.nullNW): #check illegal values, if latlon1 is nullSE, it will organically loose with latlon2, otherwise it might win, so prevent it
            return latlon2
        if (latlon2 == GeoRegion.nullNW):
            return latlon1
        return GeoPoint(
            (min(latlon1[0], latlon2[0]), max(latlon1[1], latlon2[1]))
        )

    @staticmethod
    def world():
        return GeoRegion((GeoRegion.maxNW[0], GeoRegion.maxNW[1], GeoRegion.maxSE[0], GeoRegion.maxSE[1]))

    def randomPoint(self):
        return (random.random() * self.height() + self.se().lat(),
                random.random() * self.width() + self.nw().lon())

    def __init__(self, latlonRegion=(nullNW[0], nullNW[1], nullSE[0], nullSE[1])):
        self.m_nw = self._nw((latlonRegion[0], latlonRegion[1]), (latlonRegion[2], latlonRegion[3]))
        self.m_se = self._se((latlonRegion[0], latlonRegion[1]), (latlonRegion[2], latlonRegion[3]))

    def nw(self):
        return self.m_nw

    def se(self):
        return self.m_se

    def ne(self):
        return (self.nw()[0], self.se()[1])

    def sw(self):
        return (self.se()[0], self.nw()[1])

    #resize by given percent in all directions
    def percentResize(self, percent):
        halfNewWidthDelta = 1.0*self.width()*(percent-100)/200
        halfNewHeightDelta = 1.0*self.height()*(percent-100)/200
        nw = (self.m_nw[0] + halfNewHeightDelta, self.m_nw[1] - halfNewWidthDelta)
        se = (self.m_se[0] - halfNewHeightDelta, self.m_se[1] + halfNewWidthDelta)
        return GeoRegion((nw[0], nw[1], se[0], se[1]))

    #resise by the number of units (de/increment by n) in all directions
    def unitResize(self, units):
        halfNewWidthDelta = 1.0*units/2
        halfNewHeightDelta = 1.0*units/2
        nw = (self.m_nw[0] + halfNewHeightDelta, self.m_nw[1] - halfNewWidthDelta)
        se = (self.m_se[0] - halfNewHeightDelta, self.m_se[1] + halfNewWidthDelta)
        return GeoRegion((nw[0], nw[1], se[0], se[1]))

    def isNull(self):
        return self.m_nw == self.nullNW or self.m_se == self.nullSE

    def width(self):
        if not self.isNull():
            return math.fabs(self.m_nw[1]-self.m_se[1])
        return 0

    def height(self):
        if not self.isNull():
            return math.fabs(self.m_nw[0]-self.m_se[0])
        return 0

    def tuple(self):
        return (self.m_nw[0], self.m_nw[1], self.m_se[0], self.m_se[1])

    def to_WKT_Geom(self):
        return "POLYGON(("+str(self.m_nw[0])+" "+str(self.m_nw[1])+","+str(self.m_se[0])+" "+str(self.m_nw[1])+","+str(self.m_se[0])+" "+str(self.m_se[1])+","+str(self.m_nw[0])+" "+str(self.m_se[1])+","+str(self.m_nw[0])+" "+str(self.m_nw[1])+"))"

    def add(self, geoRegionOrPoint):
        if isinstance(geoRegionOrPoint, GeoRegion):
            self.m_nw = self._nw(self.m_nw, geoRegionOrPoint.m_nw)
            self.m_se = self._se(self.m_se, geoRegionOrPoint.m_se)
        else:
            self.m_nw = self._nw(self.m_nw, geoRegionOrPoint)
            self.m_se = self._se(self.m_se, geoRegionOrPoint)

    def isNull(self):
        if self.m_nw == self.nullNW or self.m_se == self.nullSE:
            return True

    def contains(self, latlon):
        return ((latlon[0] < self.m_nw[0]) and (latlon[0] > self.m_se[0]) and
            (latlon[1] > self.m_nw[1]) and (latlon[1] < self.m_se[1]))

    def intersects(self, lineTuple):
        lineBegin = lineTuple[0]
        lineEnd = lineTuple[1]
        if ((lineBegin[0] > self.nw()[0] and lineEnd[0] > self.nw()[0]) or #line north of region
            (lineBegin[0] < self.se()[0] and lineEnd[0] < self.se()[0]) or #line south of region
            (lineBegin[1] < self.nw()[1] and lineEnd[1] < self.nw()[1]) or #line west of region
            (lineBegin[1] > self.se()[1] and lineEnd[1] > self.se()[1])):  #line east of region
            return False
        else:
            return True

    def centre(self):
        return (self.m_nw[0]-self.height()/2, self.m_nw[1]+self.width()/2)

    def nw(self):
        return self.m_nw

    def se(self):
        return self.m_se

    def __str__(self):
        return "["+str(self.nw())+","+str(self.se())+"]"

    def equals(self, other):
        return self.nw().equals(other.nw()) and self.se().equals(other.se())



class LinearSmoother(object):

    # The function will analyse the dataset by finding how far its individual points
    # are from the ideal line segment projected between its start and end. If all
    # points are within the tolerance range (no further than tolerance of the
    # line segment length), the dataset is approximatelly linear.
    # The alogrithm will allow a discardFraction number of extreme dataset points to fall
    # beyond the tolerance before giving in
    @staticmethod
    def isLinear(dataset, tolerance, discardFraction=0.2):
        referenceLine   = Line(
            (dataset[0][0],  dataset[0][1]),
            (dataset[-1][0], dataset[-1][1])
        )

        allowNoOfExtremes   = int(len(dataset)*discardFraction)

        for x, y in dataset:
            distanceFromProjection = referenceLine.distanceFromProjection((x, y))

            if distanceFromProjection > tolerance:
                if allowNoOfExtremes > 0:
                    allowNoOfExtremes -= 1
                else:
                    return False
        return True

    @staticmethod
    def linearSmoothen(dataset, distanceWindow):
        result = []
        for idx, point in enumerate(dataset):
            influencers = []
            influencers += LinearSmoother.__scan(dataset, idx, 1, distanceWindow)
            influencers += LinearSmoother.__scan(dataset, idx, -1, distanceWindow)
            totalWeigth = 1
            smoothenValue  = point[1]
            for influencer in influencers:
                influecingPoint     = influencer[0]
                influencingValue    = influecingPoint[1]
                influecingWeight    = influencer[1]
                smoothenValue      += influencingValue*influecingWeight
                totalWeigth        += influecingWeight
            smoothenValue /= totalWeigth
            result.append((point[0], smoothenValue))
        return result

    @staticmethod
    def __scan(dataset, idx, increment, distanceWindow):
        referencePoint = dataset[idx]
        result = []
        idx += increment
        while idx > 0 and idx < len(dataset):
            influencingPoing = dataset[idx]
            distance = math.fabs(influencingPoing[0] - referencePoint[0])
            if distance > distanceWindow:
                break
            result.append((influencingPoing, (distanceWindow-distance)/distanceWindow))
            idx += increment
        return result


class Polygon(object):

    def __init__(self, listOfTuples):
        self.mPoints  = listOfTuples
        polygonClosed = ((listOfTuples[0][0] == listOfTuples[-1][0]) and
                         (listOfTuples[0][1] == listOfTuples[-1][1]))
        if not polygonClosed:
            self.mPoints.append(self.mPoints[0])


    def isClockwise(self):
        signedArea = 0
        prev = None
        for point in self.mPoints:
            if prev:
                signedArea += (prev[0] * point[1] - point[0] * prev[1])
            prev = point
        return signedArea > 0