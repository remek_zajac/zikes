# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# (C) 2016 Remek Zajac
#!/usr/bin/python

import os
import re
import psycopg2

#local modules
import sys
import traceback

import pyUtils
import pyPolyline
from osmsql.pyDb import OSMGisDatabase

class Country(object):

    def __init__(self, id, name, leftHandSideTraffic, polyline):
        self.mId = id
        self.mName = name
        self.mLeftHandSideTraffic = leftHandSideTraffic
        self.mPolyline = Country.parseGeomText(polyline)

    def leftHandSideTraffic(self):
        return self.mLeftHandSideTraffic

    def name(self):
        return self.mName

    def polyline(self):
        return self.mPolyline

    def isInside(self, latlon):
        n = len(self.mPolyline)
        inside = False
        p1x, p1y = self.mPolyline[0]
        for i in range(1, n + 1):
            p2x, p2y = self.mPolyline[i % n]
            if latlon[1] > min(p1y, p2y):
                if latlon[1] <= max(p1y, p2y):
                    if latlon[0] <= max(p1x, p2x):
                        if p1y != p2y:
                            xinters = (latlon[1] - p1y) * (p2x - p1x) / (p2y - p1y) + p1x
                        if p1x == p2x or latlon[0] <= xinters:
                            inside = not inside
            p1x, p1y = p2x, p2y
        return inside

    def id(self):
        return self.mId

    @staticmethod
    def parseGeomText(polygonGeomText):
        tuplesRegEx = re.compile("POLYGON\(\((.+)\)\)")
        res = re.findall(tuplesRegEx, polygonGeomText)
        poly = map(lambda tupleStr: eval("["+",".join(tupleStr.split(" "))+"]"), res[0].split(","))
        return poly

    @staticmethod
    def fromRecord(record):
        return Country(*record)

class Countries(object):

    class CantWorkOutRegion(Exception):
        pass

    KDownloadURL = "http://download.geofabrik.de/"
    KContinents  = ["europe", "north-america", "south-america", "australia-oceania", "asia", "central-america"]
    KLeftHandSideTrafficCountries = ["europe/great-britain", "europe/ireland-and-northern-ireland", "europe/british-isles", "europe/isle-of-man", "australia-oceania/new-zealand"]

    def __init__(self, dbConnection, debug=None, cacheRoot=pyUtils.KCacheFolder):
        self.mCacheFolder = "%s/Countries/" % cacheRoot
        if not os.path.exists(self.mCacheFolder):
            os.makedirs(self.mCacheFolder)
        if not debug:
            debug = self.mDebug = pyUtils.Debug(pyUtils.Debug.normal)
        self.mDebug = debug
        self.dbconn = dbConnection
        dbCursor = self.dbconn.cursor()
        dbCursor.execute("SELECT COUNT(*) FROM Countries")
        if dbCursor.fetchone()[0] == 0:
            self.insertCountries(dbCursor)
        dbCursor.execute("SELECT COUNT(*) FROM Countries")
        assert dbCursor.fetchone()[0] > 0, "Unable to insert countries"
        dbCursor.close()
        self.mLastCheckedCountry = None

    def getAt(self, latlon):
        dbCursor = self.dbconn.cursor()
        try:
            if self.mLastCheckedCountry and self.mLastCheckedCountry.isInside(latlon):
                return self.mLastCheckedCountry
            sql = "SELECT id, name, leftHandSideTraffic, ST_AsText(polyline) FROM Countries WHERE ST_Within(ST_GeomFromText('POINT(%s %s)', 4326), polyline) order by ST_Area(polyline) ASC limit 1" % (latlon[0], latlon[1])
            dbCursor.execute(sql)
            country = dbCursor.fetchone()
            if country:
                self.mLastCheckedCountry = Country.fromRecord(country)
                return self.mLastCheckedCountry
            raise Countries.CantWorkOutRegion("Cannot work out country for location: %s" % str(latlon))
        finally:
            dbCursor.close()

    def fromName(self, name):
        dbCursor = self.dbconn.cursor()
        try:
            sql = "SELECT id, name, leftHandSideTraffic, ST_AsText(polyline) FROM Countries WHERE name='%s'" % name
            dbCursor.execute(sql)
            country = dbCursor.fetchone()
            if country:
                return Country.fromRecord(country)
        finally:
            dbCursor.close()

    def insertCountries(self, dbCursor):
        countries = reduce(lambda cum, continent: cum + self.getContinent(continent), Countries.KContinents, [])
        for country in countries:
            if os.path.isfile(country) and country.endswith(".poly"):
                def toGeometryString(poly):
                    res = []
                    for point in poly:
                        res.append("%s %s" % (point[0], point[1]))
                    res.append(res[0])
                    return ",".join(res)
                poly = toGeometryString(pyPolyline.Polyline.parse(country))
                countryName = os.path.relpath(country, self.mCacheFolder)[:-len(".poly")]
                sql = "INSERT INTO Countries (name, leftHandSideTraffic, polyline) VALUES ('%s',%s,ST_GeomFromText('POLYGON((%s))', 4326))" % (countryName,countryName in Countries.KLeftHandSideTrafficCountries, poly)
                try:
                    dbCursor.execute(sql)
                except psycopg2.IntegrityError:
                    pass

    def getContinent(self, continent):
        try:
            self.downloadContintent(continent)
        except:
            self.mDebug.exception("Error downloading continent %s" % continent)
        countries = map(lambda country: os.path.join(self.mCacheFolder, country), pyUtils.FileUtils.listFiles(self.mCacheFolder, recursive=True))
        return countries


    def downloadContintent(self, continent):
        #download index
        result = []
        index = pyUtils.FileDownload.download(Countries.KDownloadURL+continent+".html", None, self.mDebug)
        countryRegEx = re.compile("<a href=\"%s/(.+).html\".+" % continent)
        for m in re.finditer(countryRegEx, index):
            dstFile = self.mCacheFolder+"%s/%s.poly" % (continent, m.group(1))
            result.append(dstFile)
            if os.path.exists(dstFile):
                continue
            dstFolder = os.path.dirname(dstFile)
            if not os.path.exists(dstFolder):
                os.makedirs(dstFolder)
            self.mDebug.logUpdated("Downloading poly %s" % dstFile)
            pyUtils.FileDownload.download(Countries.KDownloadURL+continent+"/%s.poly" % m.group(1), dstFile, self.mDebug)
        return result


def usage():
    print "Usage:"
    print sys.argv[0]+" <db_name>"
    print "Will saturate the given database with Country info (left/right -hand side traffic and contours)"

def main():
    try:
        with OSMGisDatabase(sys.argv[1]) as db:
            Countries(db.conn, pyUtils.Debug(pyUtils.Debug.normal))
    except:
        traceback.print_exc()
        usage()
        sys.exit(2)

if __name__ == "__main__":
    main()