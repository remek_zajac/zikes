# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# (C) 2017 Remek Zajac
#!/usr/bin/python

import math
import pyGeometry
import pyUtils


class SectionView(object):

    KArrowArmLen = 10
    KArrowAngle  = 30
    KWidth = 3

    def __init__(self, referenceMap, points, colour=(0.0,0.0,0.8,0.5)):
        self.mMap = referenceMap
        self.mPoints = SectionView.toPoints(points)
        self.mRegion = pyGeometry.GeoRegion()
        for point in self.mPoints:
            self.mRegion.add(point)
        self.mColour = colour

    def region(self):
        return self.mRegion

    @staticmethod
    def toPoints(points):
        if not isinstance(points, list):
            return points["nodes"]
        if isinstance(points[0], dict):
            return reduce(
                lambda cum, pts: cum + pts,
                map(SectionView.toPoints, points)
            )
        return points

    def draw(self, cr):
        pixPoints = []
        cr.set_source_rgba(*self.mColour)
        for point in self.mPoints:
            point = self.mMap.latlon2pix(point)
            if len(pixPoints) == 0:
                cr.move_to(point[0], point[1])
            else:
                cr.line_to(point[0], point[1])
            pixPoints.append(point)

        #arrow
        tip = pixPoints[-1]
        ref = pixPoints[-2]
        direction = pyGeometry.Vector((ref[0]-tip[0], ref[1]-tip[1])).setLength(SectionView.KArrowArmLen)
        leftArm = direction.rotate(SectionView.KArrowAngle)
        rightArm = direction.rotate(-SectionView.KArrowAngle)
        left  = leftArm.addToPoint(tip)
        right = rightArm.addToPoint(tip)
        cr.line_to(left[0], left[1])
        cr.move_to(tip[0], tip[1])
        cr.line_to(right[0], right[1])
        cr.stroke()

        for idx, point in enumerate(pixPoints[:-1]):
            cr.set_line_width(SectionView.KWidth)
            cr.arc(point[0], point[1], SectionView.KWidth+1 if idx != 0 else SectionView.KWidth+3, 0, 2 * math.pi)
            cr.fill()

    def invalidate(self, who):
        pass

    def clear(self):
        pass


# Unlike WayView (which shows an OSM way as fetched from db), this accepts a JSON like so:
# [
#  {
#   'magnitude': <some number>,
#   'section'  :[[51.5465537, -0.2044156], ..., [51.5466925, -0.2041464]]
#  },
#  ...
#  And it will run the content finding the min and max for the number and then plot
#  the ways colouring them to match the maginitude
# ]
# or a file with each line being this (without commas):
# {
#  'magnitude': <some number>,
#  'section'      :[[51.5465537, -0.2044156], ..., [51.5466925, -0.2041464]]
# }
# Alternativelly, a section can be an array of objects that each feature
# { ... nodes: [[51.5465537, -0.2044156], ..., [51.5466925, -0.2041464]], ... }
#
class SectionsView(object):
    KNarrowDownTo   = 10000
    KBytesPerPoint  = 33337/1345

    def __init__(self, referenceMap, sections):
        self.mMap      = referenceMap
        self.mInput    = sections
        self.mSections = []
        self.mRegion   = pyGeometry.GeoRegion()
        self.mMinMag   = float("inf")
        self.mMaxMag   = float("-inf")
        self.mBuckets  = {}

        def checkSection(section):
            mag = section["magnitude"]
            if mag not in self.mBuckets:
                self.mBuckets[mag] = 0
            self.mBuckets[mag] += 1
            self.mMinMag = min(mag, self.mMinMag)
            self.mMaxMag = max(mag, self.mMaxMag)

        def calcColour(section):
            d = self.mMaxMag-self.mMinMag
            return (
                1.0*(section["magnitude"]-self.mMinMag)/d if d > 0 else 1.0,
                0.0, 0.0, 0.8
            )

        def addSection(section):
            section = SectionView(
                referenceMap,
                section["section"],
                calcColour(section)
            )
            self.mRegion.add(section.region())
            self.mSections.append(section)

        if isinstance(self.mInput, list):
            for section in self.mInput:
                checkSection(section)
            for section in self.mInput:
                addSection(section)
        elif isinstance(self.mInput, str):
            for line in pyUtils.linesInFile(self.mInput):
                checkSection(eval(line))
            for line in pyUtils.linesInFile(self.mInput):
                addSection(eval(line))
        print "%d segments dropped, buckets:" % len(self.mSections)
        sortedBuckets = self.mBuckets.keys()
        sortedBuckets.sort()
        for key in sortedBuckets:
            print "  %s : %s " % (key, self.mBuckets[key])




    def region(self):
        return self.mRegion

    def draw(self, cr):
        for section in self.mSections:
            section.draw(cr)

    def invalidate(self, who):
        pass

    def clear(self):
        pass