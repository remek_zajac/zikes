# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# (C) 2012 Remek Zajac
#!/usr/bin/python

import threading
import time
import sys
import os
import traceback
import requests
import shutil

KLibFolder = os.path.dirname(os.path.abspath(__file__))
KRootFolder = os.path.dirname(KLibFolder)
KCacheFolder = KRootFolder+"/.localCache/"
KLogFolder = KRootFolder+"/.logs/"
KConfigFolder = KRootFolder+"/configs/"

class Singleton:
    threadLock = threading.Lock()
    """
    A non-thread-safe helper class to ease implementing singletons.
    This should be used as a decorator -- not a metaclass -- to the
    class that should be a singleton.

    The decorated class can define one `__init__` function that
    takes only the `self` argument. Other than that, there are
    no restrictions that apply to the decorated class.

    To get the singleton instance, use the `Instance` method. Trying
    to use `__call__` will result in a `TypeError` being raised.

    Limitations: The decorated class cannot be inherited from.

    """

    def __init__(self, decorated):
        self._decorated = decorated
        self._instance = None

    def Instance(self):
        """
        Returns the singleton instance. Upon its first call, it creates a
        new instance of the decorated class and calls its `__init__` method.
        On all subsequent calls, the already created instance is returned.
        """
        self.threadLock.acquire()
        if not self._instance:
            self._instance = self._decorated()
        self.threadLock.release()
        return self._instance

    def __call__(self):
        raise TypeError('Singletons must be accessed through `Instance()`.')

    def __instancecheck__(self, inst):
        return isinstance(inst, self._decorated)


class DebugTimeReporter:
    def __init__(self, name, parentOrDebug):
        self.nestedTimeReporters = {}
        self.name = name
        assert parentOrDebug
        if isinstance(parentOrDebug, DebugTimeReporter) :
            self.debug  = parentOrDebug.debug
            self.parent = parentOrDebug
            self.parent.nestedTimeReporters[name] = self
        else:
            self.debug = parentOrDebug
            self.parent = None
        self.accumulatedTime = 0
        self.maxTime = None
        self.minTime = None
        self.startTime = None
        self.noOfStarts = 0

    def start(self):
        if not self.startTime:
            self.noOfStarts += 1
            self.startTime = time.time()
            return True
        return False

    def reporter(self, name, create=True):
        if create and name not in self.nestedTimeReporters:
            self.nestedTimeReporters[name] = DebugTimeReporter(name, self)
        return self.nestedTimeReporters.get(name)

    def getStartTime(self):
        return self.startTime

    def stop(self):
        if self.startTime:
            stopTime = time.time()
            assert(self.startTime)
            td = stopTime - self.startTime
            self.accumulatedTime += td
            if self.maxTime:
                self.maxTime = max(self.maxTime, td)
                self.minTime = min(self.minTime, td)
            else:
                self.maxTime = td
                self.minTime = td
            self.startTime = None
            return True
        return False

    def measure(self, op):
        self.start()
        result = op()
        self.stop()
        return result

    def timeSpent(self):
        result = self.accumulatedTime
        if self.startTime:
            result += time.time() - self.startTime
        return result

    def averageTimeSpent(self):
        if self.noOfStarts:
            return self.timeSpent()/self.noOfStarts
        return 0

    def maxTimeSpent(self):
        return self.maxTime

    def minTimeSpent(self):
        return self.minTime

    def avgReportStr(self):
        return "average:"+("%0.3f"%self.averageTimeSpent())+" min..max: ["+("%0.3f"%self.minTimeSpent())+".."+("%0.3f"%self.maxTimeSpent())+"]"

    def timeSpentHMS(self):
        return time.gmtime(self.timeSpent())

    def isIdle(self):
        return self.startTime == None

    def isTicking(self):
        return not self.isIdle()

    def reportIndentation(self):
        if self.parent:
            return self.parent.reportIndentation()+'  '
        return ''

    def report(self, output = sys.stdout):
        assert(not self.startTime)
        if not self.parent:
            self.debug.log("TIME REPORT *******************************************")
        timeSpent = self.timeSpent()
        avgTimeSpent = self.averageTimeSpent()
        reportStr = self.reportIndentation()+"Timer:"+self.name+" total:"+("%0.2f"%timeSpent)+"s "
        if avgTimeSpent != timeSpent:
            reportStr += self.avgReportStr()
        self.debug.log(reportStr)
        contributions = 0
        for reportName, report in self.nestedTimeReporters.iteritems():
            proportion = report.timeSpent()/self.timeSpent()
            contributions += report.timeSpent()
            self.debug.log(self.reportIndentation()+' *'+str(int(proportion*100))+'%')
            report.report(output)
        if len(self.nestedTimeReporters) > 0:
            unacountedFor = self.timeSpent()-contributions
            proportion = unacountedFor/self.timeSpent()
            self.debug.log(self.reportIndentation()+ ' *Unaccounted for:'+('%0.2f'%unacountedFor)+'s, representing:'+str(int(proportion*100))+'%')
        if not self.parent:
            self.debug.log("*******************************************************")


class Debug(object):
    stty_size = os.popen('stty size', 'r').read().split()
    KTermHeight, KTermWidth = (80,80)
    if stty_size and len(stty_size) == 2:
        KTermHeight, KTermWidth = (int(stty_size[0]), int(stty_size[1]))
    none        = 0
    sparse      = 1
    normal      = 2
    fine        = 3
    superfine   = 4

    def __init__(self, level=normal, logFilePath=None):
        self.mLogFile = logFilePath
        if logFilePath and os.path.exists(logFilePath):
            os.remove(logFilePath)
        self.mLevel     = level
        self.mLastUpdatedMessageConsole = None
        self.mLastUpdatedMessageFile    = None

    def _log(self, message, level, output):
        if self.mLastUpdatedMessageFile:
            if self.mLogFile:
                with open(self.mLogFile, "a") as logFile:
                    logFile.write(self.mLastUpdatedMessageFile+"\n")
            self.mLastUpdatedMessageFile = None

        if self.mLevel >= level:
            if self.mLastUpdatedMessageConsole:
                output.write(self.mLastUpdatedMessageConsole+"\n")
                self.mLastUpdatedMessageConsole = None
            if message:
                output.write(message+"\n")

        if message and self.mLogFile:
            root = os.path.dirname(self.mLogFile)
            if not os.path.exists(root):
                os.makedirs(root)
            with open(self.mLogFile, "a") as logFile:
                logFile.write(message+"\n")


    def log(self, message, level=normal):
        self._log(message, level, sys.stdout)

    def logUpdated(self, message=None, level=normal, finish=False):
        if not message:
            if self.mLastUpdatedMessageConsole:
                message = ' '*len(self.mLastUpdatedMessageConsole)
            else:
                return
        self.mLastUpdatedMessageFile = message
        if self.mLevel >= level:
            msgLen = len(message)
            self.mLastUpdatedMessageConsole = message
            if msgLen >= Debug.KTermWidth:
                message = message[:(Debug.KTermWidth/2)-2]+"..."+message[(msgLen-Debug.KTermWidth/2)+2:]
            message = message.ljust(Debug.KTermWidth)+"\r"
            sys.stdout.write(message)
            sys.stdout.flush()
            if finish:
                self.log("")

    def err(self, message, level=none):
        self._log("Error     :"+message, level, sys.stderr)

    def exception(self, message, level=none):
        ex_type, ex, tb = sys.exc_info()
        if ex_type:
            message = "Exception :"+message
            message += "\n"+traceback.format_exc()
        else:
            message = "Error     :"+message
        self._log(message, level, sys.stderr)


    def warn(self, message, level=none):
        self._log("Warning  :"+message, level, sys.stderr)

    def lineWidth(self):
        return Debug.KTermWidth




class FileUtils(object):

    @staticmethod
    def listFiles(path, recursive=False):
        assert os.path.exists(path)
        if os.path.isfile(path):
            return [path]
        result = []
        for elem in os.listdir(path):
            fullpath = os.path.join(path, elem)
            if os.path.isdir(fullpath) and recursive:
                result += map(lambda child: os.path.join(elem,child), FileUtils.listFiles(fullpath, recursive))
            else:
                result.append(elem)
        return result

    @staticmethod
    def cpFile(src,dst,retries=3):
        nesting = os.path.dirname(dst)
        if not os.path.exists(nesting):
            os.makedirs(nesting)
        if os.path.isdir(dst):
            dst = os.path.join(dst, os.path.basename(src))
        startedSec = time.time()
        try:
            shutil.copyfile(src,dst)
        except IOError:
            if retries > 0 and time.time() - startedSec > 2:
                time.sleep(5*60)
                FileUtils.cpFile(src,dst,retries-1)
            else:
                raise()

    @staticmethod
    def cp(src, dst):
        if os.path.isdir(src):
            for item in os.listdir(src):
                s = os.path.join(src, item)
                d = os.path.join(dst, item)
                if os.path.isdir(s):
                    FileUtils.cp(s, d)
                else:
                    FileUtils.cpFile(s,d)
        else:
            FileUtils.cpFile(src,dst)

    @staticmethod
    def mv(src, dst):
        FileUtils.cp(src, dst)
        FileUtils.rm(src)

    @staticmethod
    def rm(path):
        if os.path.exists(path):
            if os.path.isdir(path):
                shutil.rmtree(path)
            else:
                os.remove(path)



class FileDownload(threading.Thread):

    class PersistentError(Exception):
        pass

    def __init__(self, url, dstFile=None, debug=None, retryForSec=60*10):
        threading.Thread.__init__(self)
        self.mUrl = url
        self.mDstFile = dstFile
        self.mRetryForSec = retryForSec

    def background(self):
        self.start()

    def run(self):
        FileDownload.download(url=self.mUrl, dstFile=self.mDstFile, retryForSec=self.mRetryForSec)

    @staticmethod
    def download(url, dstFile=None, debug=None, retryForSec=60*10, progressCb=None):
        sleepSec = 2
        finish = False
        finishBefore = time.time() + retryForSec
        while True:
            try:
                response = requests.get(url, stream=True)
            except:
                if finish:
                    break
                sleepSec = min(sleepSec*2, 60)
                timeToFinish = int(finishBefore - time.time())
                if timeToFinish < sleepSec:
                    sleepSec = timeToFinish
                    finish = True
                if debug:
                    ex_type, ex, tb = sys.exc_info()
                    message = "[%s, %s]:\n when downloading file %s. Retrying in %ssec and will keep trying for another %dsec" % (ex_type, ex, url, sleepSec, timeToFinish)
                    debug.err(message)
                time.sleep(sleepSec)

                continue
            if response.status_code == 200:
                content_length = response.headers.get("content-length")
                if isinstance(dstFile, str):
                    with open(dstFile, 'wb') as fd:
                        FileDownload._downloadFile(response, fd, content_length, progressCb)
                else:
                    return FileDownload._downloadFile(response, dstFile, content_length, progressCb)
                return
            raise FileDownload.PersistentError("Unable to download %s because of status code: %s" % (url, response.status_code))
        raise Exception("Unable to download, max retry limit reached :(")

    @staticmethod
    def _downloadFile(httpResponse, fd=None, contentLength=None, progressCB=None):
        chunksNo = 0
        content  = ""
        for chunk in httpResponse.iter_content(chunk_size=1024):
            if chunk: # filter out keep-alive new chunks
                if fd:
                    fd.write(chunk)
                    fd.flush()
                else:
                    content += chunk
                chunksNo+=1
                if progressCB:
                    if (contentLength):
                        percent = 100*chunksNo*1024/int(contentLength)
                        progressCB("%s%%" % percent)
                    else:
                        progressCB(str(chunksNo)+"kB")
        return content


def currentTimeMillis():
    return int(round(time.time() * 1000))

def objectsEqual(a,b):
    for key_a, value_a in a.iteritems():
        if type(value_a) is dict:
            if objectsEqual(value_a, b[key_a]):
                return False
        elif type(value_a) is list:
            assert False, "implement me"
        else:
            if not b or value_a != b[key_a]:
                return False
    return True

def linesInFile(path):
    with open(path, "r") as fd:
        while True:
            line = fd.readline()
            if line == "":
                break
            yield line
