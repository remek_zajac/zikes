# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# (C) 2016 Remek Zajac
#!/usr/bin/python

import sys
import os
import pyGeometry

class PointView(object):
    KCrossSizePix   = 5
    KCrossColour    = (0.0,0.0,0.0,0.5)
    KNarrowDownTo   = 10000
    KBytesPerPoint  = 33337/1345

    def __init__(self, referenceMap, points):
        self.mMap   = referenceMap
        if isinstance(points, list):
            self.mPoints = points
        elif isinstance(points, str):
            self.mPoints = self.loadFromFile(points)
        self.mRegion = pyGeometry.GeoRegion()
        self.mMinMaxElevation = (sys.maxint, -sys.maxint)
        for point in self.mPoints:
            self.mRegion.add(point)
            if len(point) == 3:
                self.mMinMaxElevation = (min(self.mMinMaxElevation[0], point[2]), max(self.mMinMaxElevation[1], point[2]))

    def loadFromFile(self, path):
        estimatedPointsInFile = os.path.getsize(path) / PointView.KBytesPerPoint
        pickEvery = max(1,int(estimatedPointsInFile/PointView.KNarrowDownTo))
        with open(path, "r") as fd:
            points = []
            count = 0
            while True:
                line = fd.readline()[0:-1]
                if line == "":
                    break
                if count % pickEvery == 0:
                    points.append(eval(line))
                count += 1
            return points

    def region(self):
        return self.mRegion

    def draw(self, cr):
        dEle = self.mMinMaxElevation[1] - self.mMinMaxElevation[0]
        for point in self.mPoints:
            if len(point) == 2:
                cr.set_line_width(3)
                cr.set_source_rgba(*PointView.KCrossColour)
            elif len(point) == 3:
                cr.set_line_width(3)
                delta = point[2] - self.mMinMaxElevation[0]
                cr.set_source_rgba(
                    PointView.KCrossColour[0] + 1.0*delta/dEle,
                    PointView.KCrossColour[1] + 1.0*(dEle-delta)/dEle,
                    PointView.KCrossColour[2],
                    PointView.KCrossColour[3]
                )
            centre = self.mMap.latlon2pix(point)
            cr.move_to(centre[0]-PointView.KCrossSizePix,centre[1]-PointView.KCrossSizePix)
            cr.line_to(centre[0]+PointView.KCrossSizePix,centre[1]+PointView.KCrossSizePix)
            cr.move_to(centre[0]-PointView.KCrossSizePix,centre[1]+PointView.KCrossSizePix)
            cr.line_to(centre[0]+PointView.KCrossSizePix,centre[1]-PointView.KCrossSizePix)
            cr.stroke()

    def invalidate(self, who):
        pass

    def clear(self):
        pass