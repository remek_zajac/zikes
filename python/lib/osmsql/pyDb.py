# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# (C) 2016 Remek Zajac
#!/usr/bin/env python

import psycopg2

class OSMGisDatabase(object):

    KClientConnParams = "host=localhost user=postgres"

    def __init__(self, dbName):
        self.mName = dbName
        connect_params = OSMGisDatabase.KClientConnParams + " dbname=%s" % dbName
        self.conn = psycopg2.connect(connect_params)
        self.conn.autocommit = True #This makes it extra slow, if SAVEPOINTS aren't used, but they are

    def __del__(self):
        self.close()

    def name(self):
        return self.mName

    def __enter__(self):
        return self

    def __exit__(self, type, value, tb):
        self.close()

    def close(self):
        if self.conn:
            self.conn.close()

    KTemplateDB = "zikes_template"

    @staticmethod
    def createFromTemplate(dbName, template=KTemplateDB, dropExisting=False):
        connect_params = OSMGisDatabase.KClientConnParams
        with psycopg2.connect(connect_params) as conn:
            with conn.cursor() as cur:
                conn.autocommit = True
                if dropExisting:
                    OSMGisDatabase.dropDatabase(dbName, cur)
                cur.execute("CREATE DATABASE %s TEMPLATE %s;" % (dbName, template))
        return OSMGisDatabase(dbName)

    @staticmethod
    def dropDatabase(dbName, cursor=None):
        if not cursor:
            connect_params = OSMGisDatabase.KClientConnParams
            with psycopg2.connect(connect_params) as conn:
                with conn.cursor() as cur:
                    conn.autocommit = True
                    OSMGisDatabase.doDropDatabase(dbName, cur)
        else:
            OSMGisDatabase.doDropDatabase(dbName, cursor)

    @staticmethod
    def doDropDatabase(dbName, cursor):
        cursor.execute("DROP DATABASE IF EXISTS %s;" % dbName)
