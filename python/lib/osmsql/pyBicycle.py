# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# (C) 2016 Remek Zajac
#!/usr/bin/env python

from enum import Enum

from lib import pyUtils

def flatten(*l):
    return (e for a in l for e in (flatten(*a) if isinstance(a, (tuple, list)) else (a,)))

KAccess = "access"
KBicycle = "bicycle"
KMtb = "mtb"
KBicyclePrefix = KBicycle + ":"
KYes = ["yes", "si"]
KOfficial="official"
KAllowed="allowed"
KDestination="destination"
KNo = "no"
KPrivate = "private"
KDesignated = "designated"
KDedicated = "dedicated"
KShared = "shared"
KPermissive = "permissive"
KOneway = "oneway"
KBackward = "backward"
KForward  = "forward"
KCycleway = "cycleway"
KLeft = "left"
KRight = "right"
KOneside = "oneside"
KOtherside = "otherside"
KMotorway = "motorway"
KMotorwayLink = KMotorway + "_link"
KFootway = "footway"
KHighway = "highway"
KRoute = "route"
KDismount = ["dismount", "dismounted"]
KWalk = "walk"
KTrack = "track"
KLane = "lane"
KSharedLane = "shared_"+KLane
KShareBusway = "share_busway"
KOpposite = "opposite"
KCrossing = "crossing"
KSegregated = "segregated"
KAllDesignatedCyclewayValues = [
    KTrack,
    KLane,
    KSharedLane,
    KShareBusway,
    KShared,
    KCrossing,
    KYes,
    KDesignated,
    KSegregated
]
KSurface="surface"
KAllPavedSurfaces=["asphalt",
                    "paved",
                    "cobblestone",
                    "cobblestone:flattened",
                    "sett",
                    "concrete",
                    "concrete:lanes",
                    "concrete:plates",
                    "paving_stones",
                    "paving_stones:30",
                    "paving_stones:20"]


#http://wiki.openstreetmap.org/wiki/Cycle_routes - legacy way of marking LCN bike routes
#Cardington Street is like this
KAllCycleNetworks = ["lcn", "rcn", "ncn", "icn"]
KAllOppositeDesignatedCyclewayValues = map(
    lambda marker: KOpposite + "_" + marker, flatten(KAllDesignatedCyclewayValues)
)

class Bicycle(object):

    class Access(Enum):
        EForbidden  = -1
        EAllowed    = 0
        EDesignated = 1


    class Attrs(object):

        KAnyVal = 666
        def __init__(self, attrs):
            self.mAttrs = {}
            self.mVisits = {}
            for k,v in attrs.items():
                self.mAttrs[k.lower()] = v.lower()

        def _key(self, k):
            res = k
            if type(k) is tuple:
                res = ":".join(k)
            return res

        def _values(self, v):
            assert v is not None
            v = list(flatten(v))
            return v

        def _has(self, k, v):
            if k not in self.mVisits:
                self.mVisits[k] = {}
            self.mVisits[k][v] = True
            if k in self.mAttrs:
                if v == Bicycle.Attrs.KAnyVal or v == self.mAttrs[k]:
                    return True
            return False

        def has(self, key, values):
            key = self._key(key)
            values = self._values(values)
            return reduce(lambda cum, v: self._has(key, v) or cum, values, False)

        def get(self, k, default=None):
            k = self._key(k)
            if k in self.mAttrs:
                return self.mAttrs[k]
            return default

        def items(self):
            return self.mAttrs.items()

        def reportUnvisited(self, wayId, debug):
            for k,v in self.items():
                if (KBicycle in k or KCycleway in k) and (k not in self.mVisits or v not in self.mVisits[k]):
                    debug.warn(
                        "Could have ignored an important bicycle attribution '%s'='%s'\n ..for wayid %s with attrs %s" %
                        (k,v,wayId,self.mAttrs),
                        pyUtils.Debug.fine
                    )


    class AccessAccum(object):
        def __init__(self, oneway):
            self.along = Bicycle.Access.EForbidden if oneway == -1 else Bicycle.Access.EAllowed
            self.opposite = Bicycle.Access.EForbidden if oneway == 1 else Bicycle.Access.EAllowed

        def softOverride(self, newAccess):
            if not type(newAccess) is tuple:
                newAccess = (newAccess, newAccess)
            if self.along != Bicycle.Access.EForbidden:
                self.override((newAccess[0], None))
            if self.opposite != Bicycle.Access.EForbidden:
                self.override((None, newAccess[1]))

        def override(self, newAccess):
            if not type(newAccess) is tuple:
                newAccess = (newAccess, newAccess)
            if newAccess[0] is not None:
                self.along = newAccess[0]
            if newAccess[1] is not None:
                self.opposite = newAccess[1]


    @staticmethod
    def _access(wayAttrs):
        # Lichtenstein
        # Warning  :Could have skipped an important bicycle attribution 'bicycle'='shared' for wayid 39327393 with attrs
        # {'maxspeed': '50', 'bicycle': 'shared', 'name': 'steinb\xc3\xb6sstrasse', 'highway': 'residential'}
        # Luxemburg:
        # Warning  :Could have skipped an important bicycle attribution 'bicycle'='official' for wayid 405788013 with attrs
        KAllBicycleAccessSofteners = (KYes, KOfficial, KPermissive, KDesignated, KShared, KAllowed)

        KAllBicycleAccessNo = (KNo, KPrivate)
        noAccess = wayAttrs.has(KAccess, KAllBicycleAccessNo)
        noAccess = wayAttrs.has(KBicycle, KAllBicycleAccessNo) or noAccess
        noAccess = wayAttrs.has(KHighway, (KMotorway, KMotorwayLink, KFootway)) or noAccess
        noAccess = (not (wayAttrs.has(KBicycle, KAllBicycleAccessSofteners) or
                         wayAttrs.has((KAccess, KBicycle), KAllBicycleAccessSofteners))) and noAccess
        wayAttrs.has(KBicycle, (KDismount, KWalk, KDestination)) #we will ignore this for now in terms of access
        return not noAccess

    @staticmethod
    def _oneway(wayAttrs, oneway):
        AllOnewaySofteners = KAllOppositeDesignatedCyclewayValues + [KOpposite]
        onewayRelaxedForBicycles = wayAttrs.has((KOneway,KBicycle), KNo) and not wayAttrs.has((KOneway,KBicycle), KYes)
        onewayRelaxedForBicycles = wayAttrs.has((KBicycle,KOneway), KNo) or onewayRelaxedForBicycles
        onewayRelaxedForBicycles = wayAttrs.has((KBicycle,KBackward), KYes) or onewayRelaxedForBicycles
        onewayRelaxedForBicycles = wayAttrs.has(KCycleway, AllOnewaySofteners) or onewayRelaxedForBicycles
        onewayRelaxedForBicycles = wayAttrs.has((KCycleway, KLeft), AllOnewaySofteners) or onewayRelaxedForBicycles
        onewayRelaxedForBicycles = wayAttrs.has((KCycleway, KRight), AllOnewaySofteners) or onewayRelaxedForBicycles
        return Bicycle.AccessAccum(0 if onewayRelaxedForBicycles else oneway)


    @staticmethod
    def forRawWay(wayAttrs, oneway, leftHandSideTraffic, wayId, highwayName2IdxMapper, debug=None):
        #see http://wiki.openstreetmap.org/wiki/Key:cycleway

        wayAttrs = Bicycle.Attrs(wayAttrs)
        if not Bicycle._access(wayAttrs):
            return Bicycle.Access.EForbidden, Bicycle.Access.EForbidden

        access = Bicycle._oneway(wayAttrs, oneway)
        highway = wayAttrs.get("highway")
        isGeneralCycleway = wayAttrs.has(KCycleway, KAllDesignatedCyclewayValues)
        isGeneralCycleway = wayAttrs.has((KRoute, KBicycle), KYes) or isGeneralCycleway
        isGeneralCycleway = wayAttrs.has(KBicycle, (KOfficial, KDesignated, KDedicated)) or isGeneralCycleway
        isGeneralCycleway = (wayAttrs.has((KRoute, KMtb), KYes) and
                 (wayAttrs.has(KSurface, KAllPavedSurfaces) or
                  wayAttrs.has((KCycleway, KSurface), KAllPavedSurfaces) or
                  highwayName2IdxMapper[highway] < highwayName2IdxMapper["unclassified"])) or isGeneralCycleway

        #This may be controversial, but looking Tiergarten in Berlin, there is plenty of paths that are exceptional
        #for cycling and are marked 'footway'+'bicycle=yes' or 'path'+'bicycle=yes'. Now a footways and paths
        #are normally fine to cycle on and deserve no extra effort of adding 'bicycle=yes'. And so that effort
        #is taken as an encouragment from whoever tagged this that the way is particularly good for cycling
        #and this deserves promotion similar to being designated
        isGeneralCycleway = (wayAttrs.has(KBicycle, KYes) and
                             (highway in {"footway", "path", "track"}) and
                             not wayAttrs.has(KCycleway, KNo)) or isGeneralCycleway
        for cycleNetwork in KAllCycleNetworks:
            isGeneralCycleway = wayAttrs.has(cycleNetwork+"_ref", Bicycle.Attrs.KAnyVal) or isGeneralCycleway
            isGeneralCycleway = wayAttrs.has(cycleNetwork, KYes) or isGeneralCycleway

        cyclwayNo = wayAttrs.has(KCycleway, KNo)
        if highway == KCycleway or isGeneralCycleway:
            access.softOverride(Bicycle.Access.EDesignated)
            if cyclwayNo and debug:
                debug.warn(
                    "Conflicting bicycle attribution for way \'%s\': %s " %
                    (wayId, wayAttrs.items()),
                    pyUtils.Debug.fine
                )

        def resolveRightAndLeft(customaryFlow, access):
            illegalFlow = KLeft
            if customaryFlow == KLeft:
                illegalFlow = KRight
            if wayAttrs.has((KCycleway, customaryFlow), KAllDesignatedCyclewayValues):
                access.override((Bicycle.Access.EDesignated, None))
            if wayAttrs.has((KCycleway, illegalFlow), KAllDesignatedCyclewayValues):
                '''
                If, in a left-hand side traffic place, there is an attribute:
                bicycle:right="lane" it means different things on whether this
                is or isn't a one-way street. From: http://wiki.openstreetmap.org/wiki/Key:cycleway:
                  "(use) cycleway:left = lane and / or cycleway:right = lane tags for a cycle lane which is on the
                   left and / or right side, relative to the direction in which the way was drawn in the editor, as
                   this describes on which side the cycle lane is. It should then be assumed that cycle traffic is
                   allowed to flow in the customary direction for traffic on that side of the road"
                '''
                if access.opposite == Bicycle.Access.EForbidden:
                    access.override((Bicycle.Access.EDesignated, None))
                else:
                    access.override((None, Bicycle.Access.EDesignated))
            if wayAttrs.has((KCycleway, illegalFlow), KAllOppositeDesignatedCyclewayValues):
                access.softOverride((None, Bicycle.Access.EDesignated))

        resolveRightAndLeft(KLeft if leftHandSideTraffic else KRight, access)

        if wayAttrs.has((KCycleway, KOneside), KAllDesignatedCyclewayValues):
            access.override((Bicycle.Access.EDesignated, None))
        if wayAttrs.has((KCycleway, KOtherside), KAllDesignatedCyclewayValues):
            access.override((None, Bicycle.Access.EDesignated))

        if wayAttrs.has((KRoute, KBicycle,KForward), KYes):
            access.softOverride((Bicycle.Access.EDesignated, None))
        if wayAttrs.has((KRoute, KBicycle,KBackward), KYes):
            access.softOverride((None, Bicycle.Access.EDesignated))
        if wayAttrs.has(KBicycle, KDismount):
            #we are not that harsh for the moment - but we downgrade from designated
            #if we see this
            access.softOverride((Bicycle.Access.EAllowed, Bicycle.Access.EAllowed))
        if debug:
            wayAttrs.reportUnvisited(wayId, debug)
        return access.along, access.opposite
