import unittest

from baseTest import DBTools
from lib.osmsql.pyBicycle import Bicycle


class ImplicitRoundabouts(unittest.TestCase):

    DB = DBTools("testimplicitroundabouts")

    @classmethod
    def setUpClass(cls):
        ImplicitRoundabouts.DB.importOsmFile("%s/test_caniloRoundabout.osm" % DBTools.KThisFolder)

    @classmethod
    def tearDownClass(cls):
        ImplicitRoundabouts.DB.dropDb()

    def testImplicitOneWayForRoundabouts(self):
        db = ImplicitRoundabouts.DB
        self.assertEquals(
            db.dbBrowser.getWaySectionsForWayId("181921266")[0].oneway(), 1
        )
        self.assertEquals(
            db.dbBrowser.getWaySectionsForWayId("4161179")[0].oneway(), 1
        )
        self.assertEquals(
            db.dbBrowser.getWaySectionsForWayId("75746415")[0].oneway(), 1
        )




