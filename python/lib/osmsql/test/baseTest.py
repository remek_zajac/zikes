import os

from lib.osmsql.pyBrowse import WayNodes
from lib.osmsql.pyDb import OSMGisDatabase
from lib.osmsql.pyImport import OSMDatabaseImporter

class DBTools(object):

    KThisFolder = os.path.dirname(os.path.abspath(__file__))

    def __init__(self, dbName):
        self.mDbName = dbName
        self.db = None

    def connectToDb(self):
        self.db = OSMGisDatabase.createFromTemplate(self.mDbName, dropExisting=True)
        self.dbBrowser = WayNodes(self.db)

    def importOsmFile(self, filePath):
        if not self.db:
            self.connectToDb()
        importer = OSMDatabaseImporter(self.db)
        importer.parseOsm(filePath)

    def dropDb(self):
        self.db.close()
        OSMGisDatabase.dropDatabase(self.mDbName)