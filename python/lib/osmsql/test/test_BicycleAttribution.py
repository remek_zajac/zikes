import unittest

from baseTest import DBTools
from lib.osmsql.pyBicycle import Bicycle


class BicycleAttribution(unittest.TestCase):

    DB = DBTools("testbicycleattribution")

    @classmethod
    def setUpClass(cls):
        BicycleAttribution.DB.importOsmFile("%s/test_bicycleAttribution.osm" % DBTools.KThisFolder)

    @classmethod
    def tearDownClass(cls):
        BicycleAttribution.DB.dropDb()

    def testBasic(self):
        db = BicycleAttribution.DB
        #a vanilla way
        self.assertEquals(
            (Bicycle.Access.EAllowed, Bicycle.Access.EAllowed),
            db.dbBrowser.getWaySectionsForWayId("1")[0].cycleway()
        )
        #a bidirectional way with a cycleway on the lefthand side (with lefthand side traffic)
        self.assertEquals(
            (Bicycle.Access.EDesignated, Bicycle.Access.EAllowed),
            db.dbBrowser.getWaySectionsForWayId("2")[0].cycleway())
        #a bidirectional way with a cycleway on the righthand side (with righthand side traffic)
        self.assertEquals(
            (Bicycle.Access.EDesignated, Bicycle.Access.EAllowed),
            db.dbBrowser.getWaySectionsForWayId("200")[0].cycleway())
        #a one way street
        self.assertEquals(
            (Bicycle.Access.EAllowed, Bicycle.Access.EForbidden),
            db.dbBrowser.getWaySectionsForWayId("3")[0].cycleway()
        )
        #a one way street flowing oposite direction to how the way was drawn
        self.assertEquals(
            (Bicycle.Access.EForbidden, Bicycle.Access.EAllowed),
            db.dbBrowser.getWaySectionsForWayId("4")[0].cycleway()
        )
        #a bidirectional way with a cycleway on the righthand side (with lefthand side traffic)
        self.assertEquals(
            (Bicycle.Access.EAllowed, Bicycle.Access.EDesignated),
            db.dbBrowser.getWaySectionsForWayId("5")[0].cycleway()
        )
        #a one way street with counter flow dedicated cycleway
        self.assertEquals(
            (Bicycle.Access.EAllowed, Bicycle.Access.EDesignated),
            db.dbBrowser.getWaySectionsForWayId("6")[0].cycleway()
        )
        # a one way street with allowing bicycles counter traffic
        self.assertEquals(
            (Bicycle.Access.EAllowed, Bicycle.Access.EAllowed),
            db.dbBrowser.getWaySectionsForWayId("7")[0].cycleway()
        )
        # a one way street with oneway:bicycle=no
        self.assertEquals(
            (Bicycle.Access.EAllowed, Bicycle.Access.EAllowed),
            db.dbBrowser.getWaySectionsForWayId("8")[0].cycleway()
        )

    def testDistance(self):
        db = BicycleAttribution.DB
        self.assertEquals(db.dbBrowser.getWaySectionsForWayId("200")[0].distanceMts(), 248)

    def testGordonSquare(self):
        db = BicycleAttribution.DB
        '''
        Gordon Square in London has an interesting attribution:
        <tag k="cycleway:right" v="opposite_lane"/>
        <tag k="oneway" v="yes"/>
        Namely, that it is one way with a counter lane for bikes.
        Additionally, it belongs to LCN going backwards, consistently
        with the "cycleway:right" v="opposite_lane"
        '''
        gordonSquare = db.dbBrowser.getWaySectionsForWayId("100")[0]
        self.assertEquals((Bicycle.Access.EAllowed, Bicycle.Access.EDesignated), gordonSquare.cycleway())

    def testTottenhamCrtRoad(self):
        db = BicycleAttribution.DB
        '''
        Tottenham Court Road in London has an interesting attribution:
        <tag k="cycleway" v="lane"/>
        <tag k="cycleway:right" v="track"/>
        <tag k="lcn" v="yes"/>
        <tag k="oneway" v="yes"/>
        Namely, that it is strictly one way and belongs to lcn
        as well as it has two cycle lanes, left and right
        '''
        self.assertEquals((Bicycle.Access.EDesignated, Bicycle.Access.EForbidden),
                          db.dbBrowser.getWaySectionsForWayId("101")[0].cycleway())
        #and now the same test, but w/o any cycle networks, just dedicated bicycle
        #lane on the right hand side (with left-hand side traffic)
        self.assertEquals((Bicycle.Access.EDesignated, Bicycle.Access.EForbidden),
                          db.dbBrowser.getWaySectionsForWayId("102")[0].cycleway())
        #repeat for a righthand side country
        self.assertEquals((Bicycle.Access.EDesignated, Bicycle.Access.EForbidden),
                          db.dbBrowser.getWaySectionsForWayId("201")[0].cycleway())

    def testLeverStreet(self):
        db = BicycleAttribution.DB
        '''
        Lever Street in London has an interesting attribution:
        <tag k="name" v="Lever Street"/>
        <tag k="oneway" v="yes"/>
        <tag k="highway" v="secondary"/>
        <tag k="surface" v="asphalt"/>
        <tag k="maxspeed" v="20 mph"/>
        <tag k="sidewalk" v="both"/>
        <tag k="cycleway:right" v="opposite_lane"/> //TODO, discussion started on talk-london to work out which is correct
                                                    //in the meantime, changed data as assumed to <tag k="cycleway:right" v="lane"/>
        <tag k="cyclestreets_id" v="47560"/>
        <tag k="cycleway:oneside" v="lane"/>
        <tag k="cycleway:oneside:width" v="1.25"/>
        Namely, that it is strictly one way and belongs to lcn
        as well as it has two cycle lanes, left and right
        '''
        self.assertEquals((Bicycle.Access.EDesignated, Bicycle.Access.EForbidden),
                          db.dbBrowser.getWaySectionsForWayId("405")[0].cycleway())


    def testSurface(self):
        db = BicycleAttribution.DB
        self.assertEquals((Bicycle.Access.EDesignated, Bicycle.Access.EDesignated),
                          db.dbBrowser.getWaySectionsForWayId("300")[0].cycleway())
        self.assertEquals((Bicycle.Access.EAllowed, Bicycle.Access.EAllowed),
                          db.dbBrowser.getWaySectionsForWayId("301")[0].cycleway())
        self.assertEquals((Bicycle.Access.EDesignated, Bicycle.Access.EForbidden),
                          db.dbBrowser.getWaySectionsForWayId("302")[0].cycleway())

    def testBicycleYes(self):
        db = BicycleAttribution.DB
        self.assertEquals((Bicycle.Access.EAllowed, Bicycle.Access.EAllowed),
                          db.dbBrowser.getWaySectionsForWayId("400")[0].cycleway())
        self.assertEquals((Bicycle.Access.EDesignated, Bicycle.Access.EDesignated),
                          db.dbBrowser.getWaySectionsForWayId("401")[0].cycleway())
        self.assertEquals((Bicycle.Access.EAllowed, Bicycle.Access.EAllowed),
                          db.dbBrowser.getWaySectionsForWayId("402")[0].cycleway())
        self.assertEquals((Bicycle.Access.EForbidden, Bicycle.Access.EForbidden),
                          db.dbBrowser.getWaySectionsForWayId("403")[0].cycleway())
        self.assertEquals((Bicycle.Access.EDesignated, Bicycle.Access.EDesignated),
                          db.dbBrowser.getWaySectionsForWayId("404")[0].cycleway())