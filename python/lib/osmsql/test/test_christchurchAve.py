import unittest

from baseTest import DBTools
from lib.pyElevation import ClimbingWay


class ChristchurchAveTest(unittest.TestCase):

    DB = DBTools("christchurchave")

    @classmethod
    def setUpClass(cls):
        ChristchurchAveTest.DB.importOsmFile("%s/test_christchurchAve.osm" % DBTools.KThisFolder)

    @classmethod
    def tearDownClass(cls):
        ChristchurchAveTest.DB.dropDb()

    # a stretch of the road just under the bridge reported once 4% climb - because it was
    # very short. Short segments are now artficially expanded to dilute their potential climb
    def testElevation(self):
        db = ChristchurchAveTest.DB
        christchurchAvenue_nextToStation = db.dbBrowser.getWaySectionsForWayId("273032213")[0]
        self.assertEquals(
            christchurchAvenue_nextToStation.nodes()[0].eleMts(), 48
        )
        self.assertEquals(
            christchurchAvenue_nextToStation.nodes()[1].eleMts(), 49
        )
        self.assertEquals(
            christchurchAvenue_nextToStation.distanceMts(), 24
        )
        self.assertEquals(
            christchurchAvenue_nextToStation.climb(), 1
        )

    def testTrafficLights(self):
        db = ChristchurchAveTest.DB
        christchurchAvenue_xssing = db.dbBrowser.getWaySectionsForWayId("273032214")[1].nodes()[1]
        self.assertEquals(
            christchurchAvenue_xssing.attrs()["highway"], "traffic_signals"
        )