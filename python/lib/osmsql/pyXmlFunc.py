import os
import xml.sax
import sys

from lib import pyGeometry


class NodeParser(object, xml.sax.handler.ContentHandler):
    def __init__(self, name, attrs, functions=None):
        xml.sax.handler.ContentHandler.__init__(self)
        self.name = name
        self.attrs = attrs
        self.mNext = None
        if not functions:
            functions = Function(lambdaFunction=None)
        self.mFunctions = functions
        self.mStack = []

    '''
    Content Handler functions
    '''
    def startElement(self, name, attrs):
        self.mStack.append(name)
        if self.mNext:
            return self.mNext.startElement(name, attrs)
        else:
            self.mNext = self.mFunctions.startElement(NodeParser(name, attrs))
            return self.mNext

    def endElement(self, name):
        self.mStack.pop(-1)
        if len(self.mStack) == 0:
            self.mNext.finished()
            self.mFunctions.endElement(self.mNext)
            res = self.mNext
            self.mNext = None
            return res
        else:
            return self.mNext.endElement(name)

    def characters(self, chars):
        pass

    def finished(self):
        pass

    def map(self, mapper):
        self.mFunctions.append(Mapper(mapper))
        return self

    def filter(self, filter):
        self.mFunctions.append(Filter(filter))
        return self

    def reduce(self, reductor, zeroValue):
        reductor = Reductor(reductor, zeroValue)
        self.mFunctions.append(reductor)
        return reductor

    def write(self, writer):
        self.mFunctions.append(writer)
        return self


class XMLEvent(object):
    KStart = 0
    KEnd = 1
    KChars = 2

    def __init__(self, type, node):
        self.type = type
        self.node = node



class StashNode(NodeParser):

    def __init__(self, decorated):
        super(StashNode, self).__init__(decorated.name, decorated.attrs)
        self.mBuffer = []
        self.mDecorated = decorated

    def startElement(self, name, attrs):
        result = self.mDecorated.startElement(name, attrs)
        self.mBuffer.append(XMLEvent(XMLEvent.KStart, result))
        return result

    def endElement(self, name):
        result = self.mDecorated.endElement(name)
        self.mBuffer.append(XMLEvent(XMLEvent.KEnd, result))
        return result

    def finished(self):
        return self.mDecorated.finished()

    def replay(self, dst):
        for event in self.mBuffer:
            if event.type == XMLEvent.KStart:
                dst.startElement(event.node.name, event.node.attrs)
            elif event.type == XMLEvent.KEnd:
                dst.endElement(event.node.name)



class Function(object):
    def __init__(self, lambdaFunction):
        self.mNextFunction = None
        self.mLambda = lambdaFunction

    def next(self):
        return self.mNext

    def tail(self):
        if self.mNextFunction:
            return self.mNextFunction.tail()
        return self

    def append(self, function):
        self.tail().mNextFunction = function

    def result(self):
        raise Exception("Undefined")

    def startElement(self, node):
        if self.mNextFunction:
            return self.mNextFunction.startElement(node)
        return node

    def endElement(self, node):
        if self.mNextFunction:
            return self.mNextFunction.endElement(node)
        return node


class Mapper(Function):
    def __init__(self, lambdaFunction):
        super(Mapper, self).__init__(lambdaFunction)

    def startElement(self, node):
        node = self.mLambda(node)
        return super(Mapper, self).startElement(node)


class Filter(Function):
    def __init__(self, lambdaFunction):
        super(Filter, self).__init__(lambdaFunction)
        self.mStash = None
        self.mResult = None

    def startElement(self, node):
        self.mResult = self.mLambda(node)
        if self.mResult is True:
            return super(Filter, self).startElement(node)
        elif isinstance(self.mResult, Function):
            self.mStash = StashNode(node)
            return self.mStash
        return node

    def endElement(self, node):
        if self.mResult is True:
            return super(Filter, self).endElement(node)
        if self.mResult is False:
            return node
        if self.mStash and self.mResult.result() and self.mNextFunction:
            self.mStash.replay(
                super(Filter, self).startElement(
                    NodeParser(self.mStash.name, self.mStash.attrs)
                )
            )
            return super(Filter, self).endElement(node)
        return node #filtered out




class Reductor(Function):
    def __init__(self, lambdaFunction, zeroValue):
        super(Reductor, self).__init__(lambdaFunction)
        self.mCum = zeroValue

    def startElement(self, node):
        self.mCum = self.mLambda(self.mCum, node)
        return super(Reductor, self).startElement(node)

    def result(self):
        return self.mCum


class FileWriter(object):

    def __init__(self, dstFile):
        self.mDstFile = dstFile
        self.mSameNodeBuf = None

    def startElement(self, indent, tagContentString, forNode):
        if self.mSameNodeBuf:
            self.mDstFile.write("%s<%s>\n" % (" "*self.mSameNodeBuf[1], self.mSameNodeBuf[2]))
        self.mSameNodeBuf = (forNode,
                             indent,
                             tagContentString)

    def endElement(self, indent, name, forNode):
        if self.mSameNodeBuf and self.mSameNodeBuf[0] == forNode:
            #start and stop in quick succession, single liner
            self.mDstFile.write("%s<%s/>\n" % (" "*self.mSameNodeBuf[1], self.mSameNodeBuf[2]))
            self.mSameNodeBuf = None
        else:
            self.mDstFile.write("%s</%s>\n" % (" "*indent, name))


class NodeWriter(Function):
    def __init__(self, fileWriter, indent=0):
        super(NodeWriter, self).__init__(None)
        self.mFileWriter = fileWriter
        self.mIndent     = indent


    def startElement(self, node):
        def escapeForXML(v):
            v = v.replace("&", "&amp;")  # escape ampersands for xml
            v = v.replace("\"", "&quot;")
            v = v.replace("<", "&lt;")
            v = v.replace(">", "&gt;")
            return v

        attrs = " ".join(
            map(lambda kv: "%s=\"%s\"" % (kv[0], escapeForXML(kv[1])), node.attrs.items())
        )
        self.mFileWriter.startElement(
            self.mIndent,
            "%s %s".encode('utf-8') % (node.name.encode('utf-8'), attrs.encode('utf-8')),
            self
        )
        node.write(
            NodeWriter(self.mFileWriter, self.mIndent + 2)
        )
        return node

    def endElement(self, node):
        self.mFileWriter.endElement(self.mIndent, node.name, self)
        return node



class XMLPipe(NodeParser):

    def __init__(self, file):
        NodeParser.__init__(self, None, None)
        self.mFile = file

    def startDocument(self):
        pass

    def endDocument(self):
        pass

    def write(self, dstFile):
        if os.path.exists(dstFile):
            os.remove(dstFile)
        dir = os.path.dirname(dstFile)
        if not os.path.exists(dir):
            os.makedirs(dir)
        with open(dstFile, "a") as fd:
            NodeParser.write(
                self, NodeWriter(
                    FileWriter(fd)
                )
            )
            parser = xml.sax.make_parser()
            parser.setContentHandler(self)
            parser.parse(self.mFile)




def main():
    coords = map(lambda elem: float(elem), eval(sys.argv[3]))
    region = pyGeometry.GeoRegion(coords)
    acceptedNodes = {}
    acceptedWays  = {}

    def osmWayMapper(child):
        if child.name == "way":
            acceptedWays[child.attrs["id"]] = True
        return child

    def osmWayReducer(cum, wayChild):
        # we don't want railways purely to remove clutter/conserve space - they are very long
        return (not (wayChild.name == "tag") or "railway" not in wayChild.attrs.values()) and (cum or
         (wayChild.name == "nd" and wayChild.attrs["ref"] in acceptedNodes))

    def osmChildFilter(child):
        if child.name == "node":
            point = pyGeometry.GeoPoint(
                (float(child.attrs["lat"]), float(child.attrs["lon"]))
            )
            if region.contains(point):
                acceptedNodes[child.attrs["id"]] = True
                return True
        elif child.name == "way":
            return child.reduce(
                osmWayReducer,
                False
            )
        elif child.name == "relation":
            return child.reduce(
                lambda cum, nd: cum or
                                (nd.name == "member" and nd.attrs["type"] == "way" and nd.attrs["ref"] in acceptedWays),
                False
            )
        return False


    XMLPipe(sys.argv[1]).map(
        lambda osm: osm.filter(
            osmChildFilter
        ).map(osmWayMapper)
    ).write(sys.argv[2])


if __name__ == "__main__":
    main()
