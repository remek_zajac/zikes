# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# (C) 2012 Remek Zajac
#!/usr/bin/env python

import cairo
import sets
import math
import sys
import threading

#local modules
from lib.osmsql.pyBicycle import Bicycle
from .  import pyBase


class WaySection(pyBase.RawWaySection):

    def __init__(self, rawWay, nodes, distanceMts, climb, attrs):
        pyBase.RawWaySection.__init__(
            self,
            rawWay=rawWay,
            nodes=nodes,
            distanceMts=distanceMts,
            climb=climb,
            attrs=attrs
        )

    def cycleway(self):
        return self.mRawWay.cycleway()

    @staticmethod
    def fromRecord(parent, waySectionRecord, includeNode=None):
        waySection = pyBase.RawWaySection.fromRecord(waySectionRecord, parent.db.conn)
        nodes = waySection.nodes()
        for i in range(len(nodes)):
            if includeNode and nodes[i].id() == includeNode.id():
                nodes[i] = includeNode
            else:
                nodes[i] = WayNode(parent, nodes[i].id(), nodes[i].latlon(), nodes[i].eleMts(), nodes[i].attrs(), nodes[i].isVertex())
            if i == 0 or i == len(nodes)-1:
                nodes[i] = parent.append(nodes[i]) #if edge nodes, append to the WayNodes hashmap

        waySection = WaySection(
            rawWay=waySection.mRawWay,
            nodes=nodes,
            distanceMts=waySection.distanceMts(),
            climb=waySection.climb(),
            attrs=waySection.attrs()
        )

        nodes[0].addEdge(waySection)
        nodes[-1].addEdge(waySection)
        if includeNode and includeNode != nodes[0] and includeNode != nodes[-1]:
            #we're evaluating a node that is in a middle of a WaySection
            #we're making it navigable from, but not navigable to (as it's not a proper vertex)
            fromEdge, toEdge = waySection.split(includeNode)
            includeNode.addEdge(fromEdge)
            includeNode.addEdge(toEdge)
            includeNode.mAritificial = True
        return waySection

    def split(self, onWayNode):
        #first calculate distance
        splitDistanceMts = 0
        splitNodeIdx = 0
        for i in range(len(self.mNodes)):
            rawNode = self.mNodes[i]
            if i > 0:
                previousRawNode = self.mNodes[i-1]
                splitDistanceMts += previousRawNode.distanceMts(rawNode)
            if rawNode.id() == onWayNode.id():
                splitNodeIdx = i
                break

        before = WaySection(
            rawWay=self.mRawWay,
            nodes=self.mNodes[:splitNodeIdx+1],
            distanceMts=splitDistanceMts,
            climb=self.mClimb,
            attrs=self.attrs()
        )

        after = WaySection(
            rawWay=self.mRawWay,
            nodes=self.mNodes[splitNodeIdx:],
            distanceMts=self.mDistanceMts-splitDistanceMts,
            climb=self.mClimb,
            attrs=self.attrs()
        )

        return (before, after)

    def highway(self):
        return self.mRawWay.highway()



class DirectedWaySection(object):

    KNavigableFoot      = 0
    KNavigableBicycle   = 1
    KNavigableMotirised = 2

    def __init__(self, waySection, reversed):
        self.mWaySection = waySection
        self.mReversed = reversed
        self.mCycleway = self.mWaySection.cycleway()[int(self.mReversed)]
        self.mHighwayIndex = pyBase.RawWay.KHighwayType2IdxDictionary[waySection.highway()]
        car = True
        if self.mReversed:
            if self.mWaySection.oneway() == 1:
                car = False
        else:
            if self.mWaySection.oneway() == -1:
                car = False
        bicycle     = (self.mCycleway != Bicycle.Access.EForbidden)
        pedestrain  = True
        self.mNavigable = (
            pedestrain,
            bicycle,
            car
        )

    @staticmethod
    def relativeToNode(waySection, node):
        if node == waySection.mNodes[-1]:
            return DirectedWaySection(waySection, False)
        elif node == waySection.mNodes[0]:
            return DirectedWaySection(waySection, True)
        else:
            assert False, "waySection (way id: "+str(waySection.wayid())+") doesn't start or end with "+str(node.id())

    @staticmethod
    def relativeFromNode(waySection, node):
        if node == waySection.mNodes[0]:
            return DirectedWaySection(waySection, False)
        elif node == waySection.mNodes[-1]:
            return DirectedWaySection(waySection, True)
        else:
            nodesStr = map(lambda elem: str(elem.id()), waySection.mNodes)
            assert False, "waySection (way id: "+str(waySection.wayid())+", with nodes: "+str(nodesStr)+") doesn't start or end with "+str(node.id())
            #AssertionError: waySection (way id: 22734776) doesn't contain relativeFromNode 638770515 (Poland)

    def fromNode(self):
        if self.mReversed:
            return self.mWaySection.mNodes[-1]
        else:
            return self.mWaySection.mNodes[0]

    def toNode(self):
        if self.mReversed:
            return self.mWaySection.mNodes[0]
        else:
            return self.mWaySection.mNodes[-1]

    def climb(self):
        if self.mWaySection.mClimb and self.mReversed:
            return -self.mWaySection.mClimb
        else:
            return self.mWaySection.mClimb

    def nodes(self, reversed = False):
        if self.mReversed != reversed:
            reversed = list(self.mWaySection.mNodes)
            reversed.reverse()
            return reversed
        else:
            return self.mWaySection.mNodes

    def distanceMts(self):
        return self.mWaySection.mDistanceMts

    def wayid(self):
        return self.mWaySection.wayid()

    def highway(self):
        return self.mWaySection.highway()

    def highwayIndex(self):
        return self.mHighwayIndex

    def cycleway(self):
        return self.mCycleway

    def navigable(self, means):
        return self.mNavigable[means]

    def reversed(self):
        return DirectedWaySection(self.mWaySection, not self.mReversed)

    def equals(self, otherDirectedSection):
        return bool(
            self.mWaySection.wayid() == otherDirectedSection.mWaySection.wayid() and
            self.mWaySection.mNodes[0].id() == otherDirectedSection.mWaySection.mNodes[0].id() and
            self.mWaySection.mNodes[1].id() == otherDirectedSection.mWaySection.mNodes[1].id()
        )


class WayNode(pyBase.RawNode):
    def __init__(self, parent, id, latlon, eleMts, attrs, isVertex):
        pyBase.RawNode.__init__(self, id, latlon[0], latlon[1], eleMts, attrs, isVertex)
        self.mParent        = parent
        self.mEdges         = sets.Set() #WaySections out of this
        self.mEvaluated     = False      #if this node (and all its 'next' references) can be trusted
        self.mAritificial   = False
        self.lock = threading.RLock()

    @staticmethod
    def fromRecord(parent, record):
        return WayNode(parent, record[0], (float(record[1]), float(record[2])), record[3], record[4], record[5])

    def _evaluate(self):
        assert(not self.mEvaluated)
        cursor = self.mParent.db.conn.cursor()
        ##get all WaySections that include 'this'
        #The following works with conjuntion with the waySectionsNodeIdIdx INDEX
        waySectionsSql = ("SELECT WaySections.*, Ways.attrs FROM WaySections JOIN Ways ON Ways.id = WaySections.wayid WHERE WaySections.nodes @> ARRAY["+str(self.mId)+"::bigint]")
        cursor.execute(waySectionsSql)
        waySectionRows = cursor.fetchall()
        for waySectionRow in waySectionRows:
            WaySection.fromRecord(self.mParent, waySectionRow, self)

        cursor.close()
        self.mEvaluated = True

    def addEdge(self, waySection):
        with self.lock:
            newEdge = DirectedWaySection.relativeFromNode(waySection, self)
            for existingEdge in self.mEdges:
                if existingEdge.equals(newEdge):
                    return existingEdge.mWaySection
            self.mEdges.add(newEdge)

    def edges(self):
        if not self.mEvaluated:
            self._evaluate()
        return self.mEdges

    #returns tuple (cost, edge)
    def edgeFor(self, node, costFun):
        result = (sys.maxint, None)
        for edge in self.edges():
            if edge.toNode().id() == node.id():
                cost = costFun(edge)
                if cost < result[0]:
                    result = (cost, edge)
        return result

    def nearestJunctions(self):
        if not self.mEvaluated:
            self._evaluate()
        if self.mAritificial:
            junctions = []
            for edge in self.mEdges:
                junctions.append(edge.toNode())
            return (junctions[0], junctions[1])
        #'this' itself is a junction
        return (self,self)


class WayNodes:

    def __init__(self, db):
        self.db = db
        self.waynodes = {} #node.id hashed waynodes
        self.waysections = {} #wayid, first node.id

    def getNearest(self, latlon, cutOffDistanceMts=100):
        nearestNode = None
        dbcursor = self.db.conn.cursor()
        sqlWhatwhat = "Nodes.id, ST_X(Nodes.latlon), ST_Y(Nodes.latlon), Nodes.eleMts, Nodes.attrs, Nodes.isVertex"
        sqlJoinNodes = "JOIN Ways ON Nodes.id=ANY(Ways.nodes)" #No need for that if non-way nodes are pruned (which they are, see dropUnrefed)
        sqlGetNearest = ("SELECT "+sqlWhatwhat+" FROM Nodes ORDER BY Nodes.latlon <-> ST_GeomFromText('POINT("+str(latlon.lat())+" "+str(latlon.lon())+")', 4326) LIMIT 1")
        dbcursor.execute(sqlGetNearest)
        nearestNodeRow = dbcursor.fetchone()
        if nearestNodeRow:
            nearestNode = WayNode.fromRecord(self, nearestNodeRow)
            if nearestNode.distanceMts(latlon) > cutOffDistanceMts:
                nearestNode = None
            elif self.waynodes.has_key(nearestNode.id()):
                nearestNode = self.waynodes[nearestNode.id()]
            else:
                #now, we need to evaluate the node here and now
                #because if it's an artificial (mArtificial) node
                #then it must not be in the waynodes hashmap
                #edges() evaluates the node and inserts it to
                #the hashmap if it is not artificial. Non aritifical
                #nodes must be put to the hasmap because the graph would
                #gain alternative dimensions
                nearestNode.edges()

        dbcursor.close()
        return nearestNode

    def get(self, id):
        nearestNode = None
        dbcursor = self.db.conn.cursor()
        getNearestSql = ('SELECT Nodes.id, ST_X(Nodes.latlon), ST_Y(Nodes.latlon), Nodes.eleMts, Nodes.attrs, Nodes.refCount FROM Nodes JOIN Ways ON Nodes.id=ANY(Ways.nodes) WHERE Nodes.id='+str(id))
        dbcursor.execute(getNearestSql)
        nearestNodeRow = dbcursor.fetchone()
        if nearestNodeRow:
            nearestNode = WayNode.fromRecord(self, nearestNodeRow)
            if self.waynodes.has_key(nearestNode.id()):
                nearestNode = self.waynodes[nearestNode.id()]
            else:
                #now, we need to evaluate the node here and now
                #because if it's an artificial (mArtificial) node
                #then it must not be in the waynodes hashmap
                #edges() evaluates the node and inserts it to
                #the hashmap if it is not artificial. Non aritifical
                #nodes must be put to the hasmap because the graph would
                #gain alternative dimensions
                nearestNode.edges()

        dbcursor.close()
        return nearestNode

    def append(self, waynode):
        if self.waynodes.has_key(waynode.id()):
            waynode = self.waynodes[waynode.id()]
        else:
            self.waynodes[waynode.id()] = waynode
        return waynode

    def remove(self, waynode):
        if self.waynodes.has_key(waynode.id()):
            del self.waynodes[waynode.id()]

    def _cachedWaySections(self, waySections):
        result = []
        for waySection in waySections:
            key = (waySection.wayid(), waySection.nodes()[0].id())
            if not key in self.waysections:
                self.waysections[key] = waySection
            result.append(self.waysections[key])
        return result

    def getWaySectionsInRegion(self, region):
        with self.db.conn.cursor() as cursor:
            pyBase.RawWaySection.inRegion(cursor, region)
            waySections = self._cachedWaySections(
                map(lambda record:WaySection.fromRecord(self, record), cursor.fetchall())
            )
        return waySections

    def getWaySectionsForWayId(self, wayid):
        dbcursor = self.db.conn.cursor()
        sql = ('SELECT WaySections.*, Ways.attrs FROM WaySections JOIN Ways ON Ways.id = WaySections.wayid WHERE WaySections.wayid='+str(wayid))
        dbcursor.execute(sql)
        waySections = self._cachedWaySections(
            map(lambda record:WaySection.fromRecord(self, record), dbcursor.fetchall())
        )
        dbcursor.close()
        return self.sortWaySections(waySections)

    def getWaysIntersectingNode(self, nodeid):
        with self.db.conn.cursor() as cursor:
            sql = "SELECT id FROM Ways WHERE %s=ANY(Ways.nodes)" % nodeid
            cursor.execute(sql)
            waySections = map(lambda id: pyBase.ClimbingWay.byId(cursor, id[0]), cursor.fetchall())
        return waySections

    def getWay(self, id):
        with self.db.conn.cursor() as cursor:
            return pyBase.ClimbingWay.byId(cursor, id)

    def nodesForWay(self, way):
        with self.db.conn.cursor() as cursor:
            nodes = pyBase.RawNode.nodesForWay(cursor, way.id())
        return nodes

    def sortWaySections(self, waySections):
        result = []
        maxIterations = 100
        if len(waySections) == 1:
            return waySections
        while len(waySections) > 0 and maxIterations > 0:
            maxIterations-=1
            waySection = waySections.pop(0)
            if len(result) == 0:
                result.append(waySection)
            else:
                for idx, storedWaySection in enumerate(result):
                    if waySection.mNodes[0].id() == storedWaySection.mNodes[-1].id():
                        #behind
                        result.insert(idx+1, waySection)
                        waySection = None
                        break
                    elif waySection.mNodes[-1].id() == storedWaySection.mNodes[0].id():
                        #before
                        result.insert(idx, waySection)
                        waySection = None
                        break
                if waySection:
                    waySections.append(waySection)
        if maxIterations == 0:
            print "Unable to sort way sections for way: '%s'" % waySections[0].id
            for waySection in waySections:
                print "%s->%s" % (waySection.mNodes[0].id(), waySection.mNodes[-1].id())
            return waySections
        return result


class WayNodesRAMCacheRenderer:
    ERepaintEveryGrowBy = 50

    def __init__(self, parent, wayNodes):
        self.mParent = parent
        self.surface = None
        self.lastDrawingPassFor = 0 #cached nodes
        self.repaint = False
        self.cache = wayNodes
        self.lastZoom = None

    def latlon2pix(self, latlon):
        x = int((latlon[1]-self.cache.waynodesRAMCache.region.nw()[1])*self.xfactor)
        y = int((self.cache.waynodesRAMCache.region.nw()[0]-latlon[0])*self.yfactor)
        return (x,y)

    def draw(self, cr):
        if self.repaint:
            assert(self.surface)
            localcr = cairo.Context(self.surface)
            nodeDrawingTool = pyCairoDrawToolkit.NodeTool(localcr)
            cursor = self.wayNodes.cursor()   #grab the database cursor for the duration of the
                                                    #iteration. Bit of a hack, but prevents the
                                                    #dictionary from updates
            for waynode in self.cache.waynodesRAMCache.waynodes.itervalues():
                if len(waynode.mEdges) > 2: #paint junctions only and don't expand (don't call next(),
                                            #as it raises dictionary changed during iteration
                    nodeDrawingTool.draw(self.mLatLon2pix(waynode.latlon()))
            self.lastZoom = self.mParent.zoom
            self.lastDrawingPassFor = len(self.cache.waynodesRAMCache.waynodes)
            self.repaint = False

        if self.surface:
            pix = self.mParent.latlon2pix(self.cache.waynodesRAMCache.region.nw())
            cr.set_source_surface(self.surface,pix[0], pix[1])
            cr.paint()

    def invalidate(self, calledBy):
        cursor = self.wayNodes.cursor()   #grab the database cursor for the duration of the checks, prevents the RAM cache from changing from other threads
        if not self.surface or self.lastDrawingPassFor+self.ERepaintEveryGrowBy < len(self.cache.waynodesRAMCache.waynodes) or self.lastZoom != self.mParent.zoom:
            georegion = self.cache.waynodesRAMCache.region
            if not georegion.isNull():
                nw = self.mParent.latlon2pix(georegion.nw())
                se = self.mParent.latlon2pix(georegion.se())
                pixwidth = int(math.fabs(nw[0]-se[0]))
                pixheight = int(math.fabs(nw[1]-se[1]))
                self.surface = cairo.ImageSurface(cairo.FORMAT_ARGB32, pixwidth, pixheight)
                self.repaint = True
                self.yfactor = 1.0* pixheight / georegion.height()
                self.xfactor = 1.0* pixwidth / georegion.width()
        else:
            self.repaint = False




