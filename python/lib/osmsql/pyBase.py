# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# (C) 2014 Remek Zajac
#!/usr/bin/env python

from lib.osmsql.pyBicycle import Bicycle
from .. import pyElevation
from .. import pyGeometry


class RawDBRow(object):
    def __init__(self, id, attrs):
        if not attrs:
            attrs = {}
        self.mAttributes = attrs
        self.mId = id
        if type(attrs) is list:
            self.mAttributes = RawDBRow.attrsFromSql(attrs)

    def id(self):
        return self.mId

    @staticmethod
    def attrsFromSql(arrayOfKV):
        result = {}
        for kv in arrayOfKV:
            k,v = kv.split('=', 1)
            assert(not result.has_key(k))
            result[k] = v
        return result

    def attrsToSql(self):
        escaped = ""
        attrsStr = ""
        for k,v in self.attrs().iteritems():
            v = v.replace("\'", "\'\'") #escape single quotes in sql
            if "\"" in v:
                #escape double quotes
                v = v.replace("\"", "\\\\\"")
                escaped = "E" #see http://www.postgresql.org/docs/9.1/static/arrays.html
            elem = "\""+k+"="+v+"\""
            if len(attrsStr) > 0:
                attrsStr += ','
            attrsStr += elem
        attrsStr = escaped+"'{"+attrsStr+"}'" if len(attrsStr) else "NULL"
        return attrsStr

    def attrs(self):
        return self.mAttributes

    #sadly, Python, when stringifying an array of longs appends the dredded 'L' and
    #the end of each entry. Use this instead.
    @staticmethod
    def arrayLong2arrayStr(array):
        return ",".join(map(lambda elem: str(elem), array))

    @staticmethod
    def inRegionCondition(region):
        assert region
        return "ST_Within(Nodes.latlon, ST_GeomFromText('"+region.to_WKT_Geom()+"',4326))"



class RawNode(RawDBRow, pyGeometry.GeoPoint):
    KSqlColumns = "id, ST_X(Nodes.latlon) AS lat, ST_Y(Nodes.latlon) AS lon, eleMts, attrs, isVertex"

    def __init__(self, id, lat, lon, eleMts, attrs=None, isVertex=None):
        RawDBRow.__init__(self, id, attrs)
        pyGeometry.GeoPoint.__init__(self, (lat, lon), eleMts)
        self.mIsVertex = isVertex

    def isVertex(self):
        return self.mIsVertex

    def promiseSetVertex(self):
        result = [self.id()] if not self.mIsVertex else []
        self.mIsVertex = True
        return result

    @staticmethod
    def fulfilSetVertex(nodeIds, cursor):
        if len(nodeIds) > 0:
            cursor.execute(
                "UPDATE Nodes SET isVertex=True WHERE id=ANY(ARRAY[%s])" % RawDBRow.arrayLong2arrayStr(nodeIds)
            )

    @staticmethod
    def fromRecord(record):
        return RawNode(*record)

    @staticmethod
    def addRefCountColumn(cursor):
        cursor.execute("ALTER TABLE Nodes ADD COLUMN refCount int NOT NULL DEFAULT 0") #if this blows, your db is botched, reset it"

    @staticmethod
    def consumeRefCountColumn(cursor):
        cursor.execute("DELETE FROM Nodes WHERE Nodes.refCount=0")
        dropped = cursor.rowcount
        cursor.execute("UPDATE Nodes SET isVertex=True WHERE refCount>1")
        cursor.execute("ALTER TABLE Nodes DROP COLUMN refCount")
        return dropped

    @staticmethod
    def inRegion(cursor, region):
        nodesSql = "SELECT %s FROM Nodes" % RawNode.KSqlColumns
        if region:
            nodesSql += " WHERE %s" % RawDBRow.inRegionCondition(region)
        cursor.execute(nodesSql)
        return cursor

    @staticmethod
    def byId(cursor, id):
        nodesSql = "SELECT %s FROM Nodes WHERE id=%s" % (RawNode.KSqlColumns, id)
        cursor.execute(nodesSql)
        record = cursor.fetchone()
        if record:
            return RawNode.fromRecord(record)

    @staticmethod
    def count(cursor, region=None, condition=None):
        if condition:
            condition = [condition]
        else:
            condition = []
        if region:
            condition.append(RawDBRow.inRegionCondition(region))
        sql = "SELECT COUNT(*) FROM Nodes"
        if len(condition):
            sql += " WHERE %s" % " AND ".join(condition)
        cursor.execute(sql)
        count = cursor.fetchone()[0]
        return count

    @staticmethod
    def concaveHull(cursor, region=None, alpha=0.9, sampleFraction=0.01):
        nodeSql = "SELECT latlon from nodes where random() < %s" % sampleFraction
        if region:
            nodeSql += "and %s" % RawDBRow.inRegionCondition(region)
        nodeSql = ("SELECT (ST_DumpPoints(ST_ConcaveHull(ST_Collect(nodes.latlon), %s))).* FROM (%s) As nodes" % (alpha, nodeSql))
        nodeSql = ("SELECT ST_X(geom) as lat, ST_Y(geom) as lon FROM (%s) As geom" % nodeSql)
        cursor.execute(nodeSql)
        return [cursor.fetchall()]


    # There are ways and waysections with nodeIds containing cycles (e.g.: roundabouts, ramps, driveways)
    @staticmethod
    def orderedNodes(cursor, nodeIds):
        strNodesIds = RawNode.arrayLong2arrayStr(nodeIds)
        nodesSql = ("SELECT "+RawNode.KSqlColumns+" FROM Nodes WHERE Nodes.id IN ("+strNodesIds+")")
        cursor.execute(nodesSql)
        fetchedNodes = cursor.fetchall()
        fetchedNodes = map(lambda elem: RawNode(*elem), fetchedNodes)
        #put them to dictionary as nodesIds may contain cycles whereas fetchall will return distinct rows
        fetchedNodes = {node.id():node for node in fetchedNodes}
        resultNodes  = []
        for nodeid in nodeIds:
            #now create order
            if fetchedNodes.has_key(nodeid):
                #if the sequence is missing some nodes (which can be the case)
                #for truncated maps, create partial sequences.
                resultNodes.append(fetchedNodes[nodeid])
        return resultNodes

    def insert(self, cursor):
        #see zikes.sql for the explantation of the magic number: 4326
        eleMts = "NULL"
        if self.mEleMts is not None:
            eleMts = str(self.eleMts())
        sqlInsertCommand = "INSERT INTO Nodes VALUES (%s,ST_GeomFromText('POINT(%s %s)', 4326), %s, %s)" % (self.id(), self.latlon()[0], self.latlon()[1], eleMts, self.attrsToSql())
        cursor.execute(sqlInsertCommand)

    def toString(self):
        return "[id:"+str(self.id())+" "+str(self.latlon())+"]"




class RawWay(RawDBRow):

    KSqlColumns = "Ways.id, Ways.nodes, Ways.attrs, Countries.leftHandSideTraffic"
    KHighwayTypesArray = [
        "motorway",            #0
        "motorway_link",       #1
        "trunk",               #2
        "trunk_link",          #3
        "primary",             #4
        "primary_link",        #5
        "secondary",           #6
        "secondary_link",      #7
        "tertiary",            #8
        "tertiary_link",       #9
        "road",                #10
        "unclassified",        #11
        "service",             #12
        "bus_guideway",        #13
        "raceway",             #14
        "living_street",       #15
        "residential",         #16
        "cycleway",            #17
        "footway",             #18
        "pedestrian",          #19
        "steps",               #20
        "bridleway",           #21
        "track",               #22
        "path",                #23
        "ferry"                #24
    ]
    KHighwayType2IdxDictionary = {x:i for i,x in enumerate(KHighwayTypesArray)}
    KHighwayIdx2TypeDictionary = {i:x for i,x in enumerate(KHighwayTypesArray)}

    def __init__(self, id, nodes, attrs, leftHandSideTraffic):
        super(RawWay, self).__init__(id, attrs)
        if not nodes:
            nodes = []
        self.mNodes = nodes
        self.mLeftHandSideTraffic = leftHandSideTraffic

    def appendWayNode(self, waynodeid):
        if not len(self.mNodes) or self.mNodes[-1] != waynodeid: #it does happen in the osm files
            self.mNodes.append(long(waynodeid))                  #it's fine for ways to have cycles, but two identical
                                                                 #nodeids in direct succession is not a cycle, it's a bug
                                                                 #in the input data
    def nodes(self):
        return self.mNodes

    def refCountNodes(self, cursor):
        #Because of potential cycles, we must refCount++ individually per node (in a loop)
        for i in reversed(range(len(self.mNodes))):
            sql = "UPDATE Nodes SET refCount = refCount + 1 WHERE Nodes.id = %s" % self.mNodes[i]
            cursor.execute(sql)
            if cursor.rowcount() == 0:
                #We didn't find this node in the db. This means that the way refers to
                #nodes that this db cannot substantiate. This can happen if part of the way is beyond the region
                #we're intrested in. In this case we simply cherrypick the nodes in region and end up
                #with a slightly crippled, but navigable way.
                self.mNodes.pop(i)
        cursor.execute(
            "UPDATE Nodes SET isVertex = true WHERE Nodes.id = %s or Nodes.id = %s" % (self.mNodes[0], self.mNodes[-1])
        )

    def insert(self, cursor, country):
        self.refCountNodes(cursor)
        if len(self.mNodes) > 1:
            assert len(self.attrs())>0,  "No attributes for RawWay with id="+self.mId

            nodeIdsStr = "["+RawWay.arrayLong2arrayStr(self.nodes())+"]"
            sqlInsertCommand = "INSERT INTO Ways VALUES (%s,ARRAY%s,%s,%s)" % (self.id(), nodeIdsStr, self.attrsToSql(), country.id())
            try:
                cursor.execute(sqlInsertCommand)
            except Exception as e:
                e.args = (e.args[0] + " sql: '%s'" % sqlInsertCommand,)
                raise
            return True
        else:
            return False

    def cycleway(self, debug=None):
        return Bicycle.forRawWay(
            self.attrs(),
            self.oneway(),
            self.mLeftHandSideTraffic,
            self.id(),
            RawWay.KHighwayType2IdxDictionary,
            debug
        )

    def highway(self):
        return self.attrs()["highway"]

    def oneway(self):
        if self.attrs().has_key("oneway"):
            #see http://wiki.openstreetmap.org/wiki/Key:oneway
            oneWay = self.attrs()["oneway"]
            if oneWay == "yes" or oneWay == "true" or oneWay == "1":
                return 1
            elif oneWay == "-1" or oneWay == "reverse":
                return -1
        if self.attrs().has_key("junction") and self.attrs()["junction"] == "roundabout":
            isClockwise = pyGeometry.Polygon(
                map(lambda node: node.latlon(), self.mNodes)
            ).isClockwise()
            if isClockwise != self.mLeftHandSideTraffic:
                return -1
            else:
                return 1
        return 0

    @staticmethod
    def fromRecord(record, cursor):
        return RawWay(record[0], record[1], record[2], record[3])

    @staticmethod
    def fromRecordWithNodes(record, cursor):
        return RawWay(
            record[0],
            RawNode.orderedNodes(cursor, record[1]),
            record[2],
            record[3]
        )

    KJoinOnCountry = "JOIN Countries ON (Ways.country=Countries.id)"
    @staticmethod
    def inRegion(cursor, region):
        sql = "SELECT DISTINCT ON (Ways.id) %s FROM Ways %s" % (RawWay.KSqlColumns, RawWay.KJoinOnCountry)
        if region:
            sql += " WHERE Ways.nodes && ARRAY(SELECT id FROM Nodes WHERE %s)" % RawDBRow.inRegionCondition(region)
        cursor.execute(sql)
        return cursor

    @staticmethod
    def byId(cursor, id, factory=None):
        if not factory:
            factory = RawWay.fromRecord
        waysSql = "SELECT %s FROM Ways %s WHERE Ways.id=%s" % (RawWay.KSqlColumns, RawWay.KJoinOnCountry, id)
        cursor.execute(waysSql)
        record = cursor.fetchone()
        if record:
            return factory(record, cursor)






class ClimbingWay(RawWay, pyElevation.ClimbingWay):

    def __init__(self, id, nodes, attrs, lefhandSideTraffic, eleProvider):
        RawWay.__init__(self, id, nodes, attrs, lefhandSideTraffic)
        pyElevation.ClimbingWay.__init__(self, nodes, eleProvider)

    @staticmethod
    def fromRecord(record, cursor, eleProvider=pyElevation.Elevation()):
        result = ClimbingWay(
            record[0],
            RawNode.orderedNodes(cursor, record[1]),
            record[2],
            record[3],
            eleProvider
        )
        return result

    @staticmethod
    def byId(cursor, id):
        return RawWay.byId(cursor, id, ClimbingWay.fromRecord)



class RawRelation(RawDBRow):

    def __init__(self, id, elements, attrs):
        super(RawRelation, self).__init__(id, attrs)
        if not elements:
            elements = []
        self.mElements = elements

    def elements(self):
        return self.mElements

    def addElement(self, type, id, role):
        self.mElements.append((type,id,role))




class RawRoute(RawRelation):

    KRoles = ["backward", "", "forward"]
    KRoleName2Id = {x:i for i,x in enumerate(KRoles)}

    def __init__(self, id, waysNroles=None, attrs=None):
        super(RawRoute, self).__init__(id, None, attrs)
        if not waysNroles:
            waysNroles = []
        self.mWaysNRoles = waysNroles

    def waysNroles(self):
        return self.mWaysNRoles

    def insert(self, cursor):
        if len(self.waysNroles()) > 0:
            assert len(self.attrs())>0,  "No attributes for RawRoute with id="+self.mId
            waysNrolesStr = ",".join(
                map(lambda wayNrole: "[%s,%s]"%(wayNrole[0],wayNrole[1]), self.waysNroles())
            )
            sqlInsertCommand = "INSERT INTO Routes VALUES (%d,ARRAY[%s],%s)" % \
                               (self.id(), waysNrolesStr, self.attrsToSql())
            cursor.execute(sqlInsertCommand)
            return True
        else:
            return False






class RawWaySection(RawDBRow, pyElevation.ClimbingWaySection):

    #Columns
    KWayId           = 0
    KKeyNode         = 1
    KNodes           = 2
    KDistanceMts     = 3
    KClimbPercent    = 4
    KWaySectionAttrs = 5
    KWayAttrs        = 6
    KColumns         = KWayAttrs

    KSqlColumns     = ["wayid", "keynode", "nodes", "distanceMts", "climbPercent", "attrs"]

    def __init__(self, rawWay, nodes, distanceMts, climb, attrs):
        RawDBRow.__init__(self, rawWay.id(), attrs)
        pyElevation.ClimbingWaySection.__init__(self, nodes, distanceMts, climb)
        self.mRawWay = rawWay

    def wayid(self):
        return self.id()

    def wayAttrs(self):
        return self.mRawWay.attrs()

    def oneway(self):
        return self.mRawWay.oneway()

    def cycleway(self):
        return self.mRawWay.cycleway()

    def insert(self, cursor):
        enclosingRegion = pyGeometry.GeoRegion()
        def processNode(node):
            enclosingRegion.add(node.latlon())
            return str(node.id())
        nodeIdsString   = "ARRAY[%s]" % ",".join(
            map(processNode, self.nodes())
        )

        climb = str(self.climb()) if self.climb() != None else "NULL"
        sqlInsertCommand = "INSERT INTO WaySections VALUES (%s,%s,%s,%s,%s,%s,ST_GeomFromText(\'%s\',4326))" % \
                           (self.wayid(), self.nodes()[0].id(), nodeIdsString,
                            self.distanceMts(), climb, self.attrsToSql(), enclosingRegion.to_WKT_Geom())
        cursor.execute(sqlInsertCommand)
        return sqlInsertCommand

    @staticmethod
    def inRegion(cursor, region):
        sql = "SELECT WaySections.wayid, WaySections.keynode, WaySections.nodes, WaySections.distanceMts, WaySections.climbPercent, WaySections.attrs, Ways.attrs FROM WaySections JOIN Ways ON (WaySections.wayid=Ways.id)"
        if region:
            sql += "WHERE ST_Intersects(WaySections.polygon, ST_GeomFromText(\'%s\',4326))" % region.to_WKT_Geom()
        cursor.execute(sql)
        return cursor

    @staticmethod
    def fromRecord(record, conn):
        assert conn
        cursor = conn.cursor()
        nodes = RawNode.orderedNodes(cursor, record[RawWaySection.KNodes])
        way = RawWay.byId(cursor, record[RawWaySection.KWayId], RawWay.fromRecordWithNodes)
        cursor.close()
        return RawWaySection(
            way,
            nodes,
            record[RawWaySection.KDistanceMts],
            record[RawWaySection.KClimbPercent],
            record[RawWaySection.KWaySectionAttrs]
        )