# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# (C) 2014 Remek Zajac
#!/usr/bin/env python

#external modules
import os
import subprocess
import json
import shutil
import sys
from google.protobuf.internal import encoder

#local modules
from lib.osmsql.pyBicycle import Bicycle
from .  import pyBase
from .. import pyGeometry
from .. import pyUtils
from .. import pyCountries

KThisFolder = os.path.dirname(os.path.abspath(__file__))
KRootFolder = os.path.dirname(os.path.dirname(os.path.dirname(KThisFolder)))

try:
    from . import pyNode_pb_autogenerated_pb2
except:
    pathToProto = KRootFolder+"/engine/readers"
    proto = pathToProto+"/osm.proto"
    protoc = subprocess.Popen(
        args=["protoc", "--python_out="+KThisFolder, "--proto_path="+pathToProto, proto],
        cwd=KThisFolder,
        stderr=subprocess.PIPE,
        stdout=subprocess.PIPE
    )
    stdout, stderr = protoc.communicate()
    assert protoc.returncode == 0, "protoc exited with %s, stdout='%s', stderr='%s'" % (protoc.returncode, stdout, stderr)
    subprocess.call(["mv", KThisFolder+"/osm_pb2.py", KThisFolder+"/pyNode_pb_autogenerated_pb2.py"])
    from . import pyNode_pb_autogenerated_pb2


class GPBExport(object):
    def __init__(self, db, region, dstFile, features, debug=None):
        self.mRegion   = region
        self.mDstFolder  = dstFile
        self.mFeatures = features
        self.db        = db
        self.mDebug    = debug if debug is not None else pyUtils.Debug(pyUtils.Debug.normal)

    def do(self):
        if self.mRegion:
            self.mDebug.log("Exporting OSM data for region:" + str(self.mRegion.tuple()) + " to folder "+self.mDstFolder)
        else:
            self.mDebug.log("Exporting all OSM data to folder "+self.mDstFolder)
        if os.path.exists(self.mDstFolder):
            if os.path.isdir(self.mDstFolder):
                shutil.rmtree(self.mDstFolder)
            else:
                os.remove(self.mDstFolder)
        os.mkdir(self.mDstFolder)

        if "meta" in self.mFeatures:
            self.dumpMeta(self.mDstFolder+"/meta.pb")
        if "coverage" in self.mFeatures:
            self.dumpCoverage(self.mFeatures["coverage"], self.mDstFolder+"/coverage.json")
        if "node" in self.mFeatures:
            self.dumpNodes(self.mDstFolder+"/nodes.pb")
        if "way" in self.mFeatures:
            self.dumpWays(self.mDstFolder+"/ways.pb")
        if "waysection" in self.mFeatures:
            self.dumpWaySections(self.mDstFolder+"/waySections.pb")

        self.mDebug.log("Finished exporting")

    @staticmethod
    def writeMessage(message, dstFile):
        serializedMessage = message.SerializeToString()
        delimiter = encoder._VarintBytes(len(serializedMessage))
        serializedMessage = delimiter + serializedMessage
        dstFile.write(serializedMessage)

    def dumpMeta(self, dstFilePath):
        with open(dstFilePath, "a") as dstFile:
            metaMessage = pyNode_pb_autogenerated_pb2.Meta()
            for idx, wayType in enumerate(pyBase.RawWay.KHighwayTypesArray):
                highwayTypeMessage = metaMessage.highwayTypes.add()
                highwayTypeMessage.name = wayType
                highwayTypeMessage.id = idx
            GPBExport.writeMessage(metaMessage, dstFile)
            self.mDebug.log("Exported Meta")


    def compactContour(self, contour, toNoOfNodes=70):
        while len(contour) > toNoOfNodes:
            removeIdx = 0
            shortestSegmentKms = sys.maxint
            for idx, val in enumerate(contour):
                prevNodeIdx = idx - 1
                if prevNodeIdx == -1:
                    prevNodeIdx = len(contour) - 1
                lenKms = pyGeometry.GeoPoint.latlondelta2km(contour[prevNodeIdx], contour[idx])
                if lenKms < shortestSegmentKms:
                    shortestSegmentKms = lenKms
                    removeIdx = idx
            del contour[removeIdx]
        return contour


    def dumpCoverage(self, polylines, dstFilePath):
        with open(dstFilePath, "a") as dstFile:
            if polylines == True:
                self.mDebug.log("Exporting Coverage by building concave hull of the requested region")
                cursor = self.db.conn.cursor()
                polylines = pyBase.RawNode.concaveHull(cursor, self.mRegion)
                cursor.close()
            elif isinstance(polylines, str):
                polylines = [pyCountries.Countries(self.db.conn).fromName(polylines).polyline()]

            for contour in polylines:
                dstFile.write(
                    json.dumps(self.compactContour(contour)) + "\n"
                )
            self.mDebug.log("Exported Coverage")

    def dumpNodes(self, dstFilePath):
        with open(dstFilePath, "a") as dstFile:
            dbcursor = self.db.conn.cursor()
            pyBase.RawNode.inRegion(dbcursor, self.mRegion)
            count = 0
            while 1:
                noderows = dbcursor.fetchmany(50)
                if len(noderows) == 0:
                    break;
                for noderow in noderows:
                    node = pyBase.RawNode(*noderow)
                    nodeMessage = pyNode_pb_autogenerated_pb2.Node()
                    nodeMessage.id    = node.id()
                    nodeMessage.lat   = node.lat()
                    nodeMessage.lon   = node.lon()
                    if node.eleMts() is not None:
                        nodeMessage.eleMts = node.eleMts()
                    nodeMessage.flags |= pyNode_pb_autogenerated_pb2.Node.Vertex if node.isVertex() else 0
                    GPBExport.writeMessage(nodeMessage, dstFile)
                    count += 1
                self.mDebug.logUpdated("Exporting Nodes ..%s" % count)
            dbcursor.close()
            self.mDebug.logUpdated("Exported %s Nodes" % count)
            self.mDebug.log("")

    def dumpWays(self, dstFilePath):
        with open(dstFilePath, "a") as dstFile:
            with self.db.conn.cursor() as iterCursor:
                pyBase.RawWay.inRegion(iterCursor, self.mRegion)
                count = 0
                while 1:
                    wayrows = iterCursor.fetchmany(50)
                    if len(wayrows) == 0:
                        break;
                    for wayrow in wayrows:
                        with self.db.conn.cursor() as wayCursor:
                            self.dumpWay(
                                dstFile, pyBase.RawWay.fromRecordWithNodes(wayrow, wayCursor)
                            )
                        count += 1
                    self.mDebug.logUpdated("Exporting Ways ..%s" % count)
            self.mDebug.logUpdated("Exported %s Ways" % count)
            self.mDebug.log("")

    def dumpWay(self, dstFile, way):
        highwayStr = way.attrs()["highway"]
        highwayInt   = pyBase.RawWay.KHighwayType2IdxDictionary.get(highwayStr)
        if highwayInt is None:
            return #Not interested in this highway
        wayMessage = pyNode_pb_autogenerated_pb2.Way()
        wayMessage.id = way.id()
        wayMessage.highway = highwayInt
        wayMessage.leftHandSideTraffic = way.mLeftHandSideTraffic
        oneway=way.oneway()

        def cyclewayToEnum(cycleway):
            if cycleway == Bicycle.Access.EAllowed:
                return pyNode_pb_autogenerated_pb2.Way.Directed.Allowed
            elif cycleway == Bicycle.Access.EDesignated:
                return pyNode_pb_autogenerated_pb2.Way.Directed.Designated
            return pyNode_pb_autogenerated_pb2.Way.Directed.Forbidden

        def oneWayToEnum(oneway):
            if oneway is 0:
                return pyNode_pb_autogenerated_pb2.Way.No
            elif oneway == 1:
                return pyNode_pb_autogenerated_pb2.Way.Along
            assert oneway == -1, "Unexpected value of one way attribute %s" % oneway
            return pyNode_pb_autogenerated_pb2.Way.Reverse

        cycleway=way.cycleway()
        wayMessage.along.cycleway   = cyclewayToEnum(cycleway[0])
        wayMessage.reverse.cycleway = cyclewayToEnum(cycleway[1])
        wayMessage.oneway = oneWayToEnum(oneway)
        GPBExport.writeMessage(wayMessage, dstFile)


    def dumpWaySections(self, dstFilePath):
        with open(dstFilePath, "a") as dstFile, \
             self.db.conn.cursor() as dbcursor:
            pyBase.RawWaySection.inRegion(dbcursor, self.mRegion)
            count = 0
            while 1:
                waysectionrows = dbcursor.fetchmany(50)
                if len(waysectionrows) == 0:
                    break;
                for waysectionrow in waysectionrows:
                    self.dumpWaySection(
                        dstFile=dstFile,
                        waySection=pyBase.RawWaySection.fromRecord(
                            record=waysectionrow,
                            conn=self.db.conn)
                    )
                    count += 1
                self.mDebug.logUpdated("Exporting WaySections ..%s" % count)
            self.mDebug.logUpdated("Exported %s WaySections" % count)
            self.mDebug.log("")


    def dumpWaySection(self, dstFile, waySection):
        highwayStr = waySection.wayAttrs()["highway"]
        highwayInt = pyBase.RawWay.KHighwayType2IdxDictionary.get(highwayStr)
        if highwayInt is None:
            return #Not interested in this highway
        waySectionMessage = pyNode_pb_autogenerated_pb2.WaySection()
        waySectionMessage.wayid = waySection.wayid()
        waySectionMessage.distanceMts = waySection.distanceMts()
        if waySection.climb() is not None:
            waySectionMessage.climb = waySection.climb()

        for node in waySection.nodes():
            waySectionMessage.nodeIds.append(node.id())
        waySectionMessage.flags |= pyNode_pb_autogenerated_pb2.WaySection.TrafficSignals if waySection.attrs().get("traffic_signals") != None else 0
        GPBExport.writeMessage(waySectionMessage, dstFile)
