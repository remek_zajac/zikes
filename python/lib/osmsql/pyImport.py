# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# (C) 2012 Remek Zajac
#!/usr/bin/env python
import xml.sax
import time
import math
import psycopg2

#local modules
from .  import pyBase
from .. import pyUtils
from .. import pyElevation
from .. import pyCountries
from .. import pyGeometry


class Stats:
    def __init__(self, name, timer):
        self.mMasterTimer = timer
        self.mTimer = pyUtils.DebugTimeReporter(name, self.mMasterTimer)
        self.mInserted = 0
        self.mParsed = 0
        self.mReportEverySec = 0.7
        self.mTimeLastReported = 0

    def report(self, final=False):
        currentTimeStr = time.strftime("%H:%M:%S")
        totalElapsedTimeStr = time.strftime("%H:%M:%S", self.mMasterTimer.timeSpentHMS())
        localElapsedTimeStr = time.strftime("%H:%M:%S", self.mTimer.timeSpentHMS())
        string = "["+currentTimeStr+"]["+totalElapsedTimeStr+"]["+localElapsedTimeStr+"] "+self.mTimer.name+"("+str(self.mParsed)+","+str(self.mInserted)+")"
        self.mTimer.debug.logUpdated(string)
        if final:
            self.mTimer.report()

    def inc(self, incTuple):
        self.mTimer.start()
        self.mParsed += incTuple[0]
        self.mInserted += incTuple[1]

        elapsed = self.mTimer.timeSpent()
        td = elapsed - self.mTimeLastReported
        if td > self.mReportEverySec:
            self.report()
            self.mTimeLastReported = elapsed

    def stop(self):
        if self.mTimer.stop():
            if not self.mTimeLastReported:
                self.report()
            self.mTimer.debug.log(None)

    def timer(self, name):
        return self.mTimer.reporter(name)



class OSMDatabaseImporter(xml.sax.handler.ContentHandler):

    #This code tries to determine the steepness of the roads and it relies on
    #data with certain accuracy. This accuracy will represent the climb better
    #(with more precission) the longer the road. But for short roads
    #its likely to come up with unrepresentative climb.
    KShortRoadClimbThrottlePercent    = 10
    KShortRoadDistanceMts             = 90
    KAcceptMissingEleNodeRatioPercent = 5

    class Cursor(object):
        KSavePoint = "OSM2SQLSavepoint"

        def __init__(self, psycopg2Cursor, autoCommit = False):
            self.mCursorImpl = psycopg2Cursor
            self.mAutoCommitEvery = 0
            if autoCommit == True:
                self.mAutoCommitEvery = 1
            elif type(autoCommit) is int:
                self.mAutoCommitEvery = autoCommit

            self.uncommittedOPs = 0
            self.mTransactionOpen = False

        def close(self):
            self.mCursorImpl.close()

        def execute(self, sqlCommand):
            if not self.mTransactionOpen:
                self.mCursorImpl.execute("BEGIN;")
                self.mTransactionOpen = True

            #secure before
            self.mCursorImpl.execute("SAVEPOINT "+OSMDatabaseImporter.Cursor.KSavePoint+";")
            try:
                self.mCursorImpl.execute(sqlCommand)
            except Exception, e:
                self.mCursorImpl.execute("ROLLBACK TO SAVEPOINT "+OSMDatabaseImporter.Cursor.KSavePoint+";")
                raise e
            finally:
                self.uncommittedOPs += 1
                if self.mAutoCommitEvery == self.uncommittedOPs:
                    self.commit()

        def commit(self):
            if self.mTransactionOpen:
                self.mCursorImpl.execute("RELEASE SAVEPOINT "+OSMDatabaseImporter.Cursor.KSavePoint+";COMMIT;")
                self.mTransactionOpen = False
                self.uncommittedOPs = 0

        def rowcount(self):
            return self.mCursorImpl.rowcount

        def fetchone(self):
            return self.mCursorImpl.fetchone()

        def fetchmany(self, number):
            return self.mCursorImpl.fetchmany(number)

        def fetchall(self):
            return self.mCursorImpl.fetchall()

    def __init__(self, db, countryName=None, debug=None, region=None):
        xml.sax.handler.ContentHandler.__init__(self)
        self.db = db
        self.mDebug             = debug if debug is not None else pyUtils.Debug(
            pyUtils.Debug.normal, pyUtils.KLogFolder+"/osm_import.log"
        )
        self.mElevationProvider = pyElevation.Elevation(self.mDebug)
        self.mCountries         = pyCountries.Countries(db.conn, self.mDebug)
        self.mCountry           = None
        if countryName:
            self.mCountry       = self.mCountries.fromName(countryName)
        self.mRegion = region or pyGeometry.GeoRegion.world()
        cursor = db.conn.cursor()
        pyBase.RawNode.addRefCountColumn(cursor)
        cursor.close()
        self.mInsertionCursor   = OSMDatabaseImporter.Cursor(db.conn.cursor(), 1000)
        assert pyBase.RawNode.count(self.mInsertionCursor) == 0, \
            "OsmImporter cannot add to a database - it will corrupt existing data" #drop valid nodes
        self.mImportTimer       = pyUtils.DebugTimeReporter("Importing OSM Data", self.mDebug)
        self.mNodeStats         = Stats("Nodes", self.mImportTimer)
        self.mWayStats          = Stats("Ways", self.mImportTimer)
        self.mRelationStats     = Stats("Relations", self.mImportTimer)
        self.mWaySectionStats   = Stats("WaySections", self.mImportTimer)
        self.mNode              = None
        self.mWay               = None
        self.mRelation          = None
        self.mObjectWithAttrs   = None


    def populateWaySections(self):
        with self.db.conn.cursor() as wayIterCursor, \
             self.db.conn.cursor() as genCursor:
            wayIterCursor.execute("SELECT * FROM Ways")
            while 1:
                wayrows = wayIterCursor.fetchmany(50)
                if len(wayrows) == 0:
                    break
                for wayrow in wayrows:
                    try:
                        self.createWaySectionsForWay(pyBase.ClimbingWay.fromRecord(wayrow, genCursor, self.mElevationProvider))
                    except:
                        #This should really be here - we created a way record that we ourselves cannot digest.
                        #But hey, there is a case, prolly has a an 'equal' sign in it. Need to see.
                        self.mDebug.exception("Exception occured analysing Way '"+str(wayrow)+"'", pyUtils.Debug.fine)



    KNoisePrevention={"construction", "platform", "services", "rest_area", "elevator", "bus_stop", "corridor", "proposed"}
    def createWaySectionsForWay(self, way):
        way.cycleway(self.mDebug)
        highwayType = way.attrs()["highway"]
        if not pyBase.RawWay.KHighwayType2IdxDictionary.has_key(highwayType):
            if not (highwayType in OSMDatabaseImporter.KNoisePrevention):
                self.mDebug.log("Skipped Way (id: '%s') with highway tag '%s' (as it's not in config.xml) " % (way.id(), highwayType), pyUtils.Debug.fine)
            return

        newVertices = []
        for section in way.sections():
            previousIntersectionNodeIdx = None
            if (section.mClimb and math.fabs(section.mClimb) > OSMDatabaseImporter.KShortRoadClimbThrottlePercent and
                pyBase.RawWay.KHighwayType2IdxDictionary[highwayType] <= pyBase.RawWay.KHighwayType2IdxDictionary["tertiary_link"] and
                section.mDistanceMts < OSMDatabaseImporter.KShortRoadDistanceMts):
                #the way we calculate climbs does't work very well on short spans. There are occurences where short stretches
                #of primary roads come to 40% climbs
                section.mClimb = math.copysign(OSMDatabaseImporter.KShortRoadClimbThrottlePercent, section.mClimb)

            #if section.climb() > 5:
            #    self.mDebug.log("Section with high climbPercent: {'magnitude':%d, 'wayid': %s, 'section':%s}" % (section.climb(), way.id(), map(lambda n: [n.x, n.y], section.nodes())), pyUtils.Debug.fine)

            #way.sections have have identified new sections of the road (with varying climb) and those must be correctly
            #qualified as vertices (it's in the gbp protocol and important downstream). We then collect the ids of the
            #newly identfied
            newVertices += section.mNodes[0].promiseSetVertex()
            newVertices += section.mNodes[-1].promiseSetVertex()
            for nodeidx, node in enumerate(section.mNodes):
                if node.isVertex():
                    if previousIntersectionNodeIdx != None:
                        self.insertWaySection(way, section.mNodes[previousIntersectionNodeIdx:nodeidx+1], section.climb())
                    else:
                        assert nodeidx != len(section.mNodes)-1, "seems like a section with only one node"
                    previousIntersectionNodeIdx = nodeidx
            pyBase.RawNode.fulfilSetVertex(newVertices, self.mInsertionCursor)

    def insertWaySection(self, way, nodes, climbPercent):
        distanceMts   = 0
        prevNode      = None
        trafficSignals = False
        for node in nodes:
            if node.attrs().get("highway") == "traffic_signals" or node.attrs().get("crossing") == "traffic_signals":
                trafficSignals = True
            if prevNode:
                distanceMts += prevNode.distanceMts(node)
            prevNode = node

        try:
            waySection = pyBase.RawWaySection(way, nodes, distanceMts, climbPercent, {"traffic_signals":"true"} if trafficSignals else {})
            waySection.insert(self.mInsertionCursor)
            self.mWaySectionStats.inc((0,1))
        except psycopg2.IntegrityError:
            pass #duplicate
        except:
            self.mDebug.exception(
                "Exception occured inserting WaySection. For wayId: %s and nodes: %s'" %
                (way.id(), nodes), pyUtils.Debug.normal
            )


    def processRelations(self):
        #This is really rather simple for now, but we will simply add an arbitrary k="route", v="mtb/bicycle/hiking"
        #so that it can be later analysed for suitability of cycling
        everyRouteSql = "SELECT * FROM Routes"
        with self.db.conn.cursor() as iterCursor:
            iterCursor.execute(everyRouteSql)
            while 1:
                routerows = iterCursor.fetchmany(50)
                if len(routerows) == 0:
                    break;
                for routerow in routerows:
                    route = pyBase.RawRoute(routerow[0], routerow[1], routerow[2])
                    roles = {role:[] for role in pyBase.RawRoute.KRoles}
                    for wayNrole in route.waysNroles():
                        roles[pyBase.RawRoute.KRoles[wayNrole[1]]].append(wayNrole[0])
                    for role, ways in roles.items():
                        if len(ways):
                            attrStr = "route:%s%s=yes" % (route.attrs()["route"], ":"+role if len(role) > 0 else "")
                            try:
                                updateWaysSql = "UPDATE Ways SET attrs = attrs || ARRAY[\'%s\'] WHERE Ways.id=ANY(ARRAY[%s]) AND NOT \'%s\'=ANY(attrs)" % (attrStr, pyBase.RawDBRow.arrayLong2arrayStr(ways), attrStr)
                                self.mInsertionCursor.execute(updateWaysSql)
                            except:
                                self.mDebug.exception("Exception occured processing relation: '"+str(route.id())+"'", pyUtils.Debug.fine)
                    self.mRelationStats.inc((0,1))


    def parseOsm(self, fromOsm):
        parser = xml.sax.make_parser()
        parser.setContentHandler(self)
        self.mFileName = fromOsm
        countryName = "Unspecified"
        if self.mCountry:
            countryName = self.mCountry.name()
        self.mDebug.log("**********************************************************************")
        self.mDebug.log("Importing OSM data for country: \'%s\' to db: \'%s\'" % (countryName, self.db.name()))
        self.mDebug.log("From :" + fromOsm)
        self.mDebug.log("This may take between minutes and hours depending on the size of the input file")
        self.mDebug.log("Following stats are: (parsed, inserted):")
        self.mImportTimer.start()
        parser.parse(self.mFileName)
        self.postProcess()
        self.mImportTimer.stop()
        self.mDebug.log("Finished importing OSM data for country: \'%s\' to db: \'%s\'" % (countryName, self.db.name()))
        self.mDebug.log("**********************************************************************")

    def postProcess(self):
        self.mInsertionCursor.commit()
        cursor = self.db.conn.cursor()
        self.mNodeStats.mInserted -= pyBase.RawNode.consumeRefCountColumn(cursor)
        self.mWayStats.stop() #in case there were no relations
        self.processRelations()
        self.mRelationStats.stop()
        self.populateWaySections()
        self.mWaySectionStats.stop()
        self.mDebug.log("Dropping unreferenced Nodes")
        self.mNodeStats.report(final=True)
        self.mWayStats.report(final=True)
        self.mRelationStats.report(final=True)
        self.mWaySectionStats.report(final=True)
        self.mInsertionCursor.commit() #final commit
        percentEleMissing = 100.00 * pyBase.RawNode.count(cursor, condition="eleMts is NULL")/pyBase.RawNode.count(cursor)
        message = "Elevation missing on %.2f%% nodes, accepting below %.2f%%" % (percentEleMissing, OSMDatabaseImporter.KAcceptMissingEleNodeRatioPercent)
        self.mDebug.log(message)
        cursor.close()
        self.mInsertionCursor.close()
        if percentEleMissing > OSMDatabaseImporter.KAcceptMissingEleNodeRatioPercent:
            self.mDebug.err(message)
            raise Exception(message)


    def startElement(self, name, attrs):
        if name == "node":
            latlon = (float(attrs.getValue('lat')), float(attrs.getValue('lon')))
            if self.mRegion.contains(latlon):
                eleMts = self.mElevationProvider.getAt(latlon)
                #We can't assert that elevation is set for all points at this stage
                #we haven't got elevation for features (e.g.: state borders) located on the sea
                self.mObjectWithAttrs = self.mNode = pyBase.RawNode(
                    id=int(attrs.getValue('id')),
                    lat=latlon[0],
                    lon=latlon[1],
                    eleMts=eleMts
                )

        elif name == "way":
            self.mNodeStats.stop()
            self.mObjectWithAttrs = self.mWay = pyBase.RawWay(
                id=int(attrs.getValue('id')),
                nodes=[],
                attrs=None,
                leftHandSideTraffic=None
            )

        elif name == "nd" and self.mWay:
            waynodeid = int(attrs.getValue('ref'))
            self.mWay.appendWayNode(waynodeid)

        elif name == "tag":
            if self.mObjectWithAttrs and attrs.has_key("k"):
                self.mObjectWithAttrs.attrs()[attrs.getValue("k")] = attrs.getValue("v")

        elif name == "relation":
            self.mWayStats.stop()
            self.mObjectWithAttrs = self.mRelation = Route(self.mDebug, int(attrs.getValue('id')))
            self.mRelationStats.inc((1,0))

        elif name == "member":
            if self.mRelation:
                self.mRelation.addElement(
                    attrs.getValue("type"),
                    attrs.getValue("ref"),
                    attrs.getValue("role")
                )


    def endElement(self, name):
        if name == "node":
            try:
                self.mNodeStats.inc((1,0))
                self.mNode.insert(self.mInsertionCursor)
                self.mNodeStats.inc((0,1))
            except psycopg2.IntegrityError:
                pass
            except:
                self.mDebug.exception("Exception occured inserting node: '%s'" % self.mNode, pyUtils.Debug.sparse)
            self.mObjectWithAttrs = None
        elif name == "way" and self.mWay:
            if self.mWay.attrs().get("route") == "ferry" and not self.mWay.attrs().has_key("highway"):
                del self.mWay.attrs()["route"]
                self.mWay.attrs()["highway"] = "ferry"
            if self.mWay.attrs().has_key("highway"):
                self.mWayStats.inc((1,0))
                try:
                    with self.db.conn.cursor() as cursor:
                        wayLatLon = None
                        for node in self.mWay.mNodes:
                            node = pyBase.RawNode.byId(cursor, node)
                            if node:
                                wayLatLon = node.latlon()
                                break
                        if self.mWay.insert(self.mInsertionCursor, self.mCountry or self.mCountries.getAt(wayLatLon)):
                            self.mWayStats.inc((0,1))
                except psycopg2.IntegrityError:
                    pass
                except:
                    self.mDebug.exception(
                        "Exception occurred inserting way: '%s'" % self.mWay.id(), pyUtils.Debug.sparse
                    )
            self.mObjectWithAttrs = self.mWay = None
        elif name == "relation":
            assert(self.mRelation)
            if self.mRelation.attrs().has_key("type"):
                if self.mRelation.attrs()["type"] == "route":
                    try:
                        cursor = self.db.conn.cursor()
                        if self.mRelation.finalise(cursor):
                            self.mRelation.insert(self.mInsertionCursor)
                        cursor.close()
                    except psycopg2.IntegrityError:
                        pass
                    except:
                        self.mDebug.exception(
                            "Exception occurred inserting route: '%s'" % self.mRelation.id(),
                            pyUtils.Debug.fine
                        )
            else:
                self.mDebug.log(
                    "Skipped relation '%s' as it has no 'type' tag" % self.mRelation.id(),
                    pyUtils.Debug.fine
                    )
            self.mObjectWithAttrs = self.mRelation = None

    def characters(self, chars):
        pass

    def startDocument(self):
        pass

    def endDocument(self):
        pass



class Route(pyBase.RawRoute):

    def __init__(self, debug, id):
        super(Route, self).__init__(id)
        self.mDebug = debug

    def finalise(self, cursor):
        if len(self.mElements) > 0:
            if not self.attrs().has_key("route"):
                raise KeyError("Relation (id: '"+str(self.id())+"') is type=route, but has no route=<type> tag")
            for element in self.mElements:
                if element[0] == "way":
                    if element[2] in Route.KRoleName2Id:
                        self.waysNroles().append(
                            (element[1], Route.KRoleName2Id[element[2]])
                        )
                elif element[0] == "relation":
                    sqlSelect = ("SELECT * FROM Routes WHERE Routes.id="+str(element[1]))
                    cursor.execute(sqlSelect)
                    record = cursor.fetchone()
                    if record:
                        record = pyBase.RawRoute(record[0], record[1], record[2])
                        self.mWaysNRoles += record.waysNroles()
                    else:
                        self.mDebug.log("Skipped member (id: %s) for relation (id: %s) as it's not in database" %
                                        (element[1], self.id()), pyUtils.Debug.fine)
            return True
        return False
