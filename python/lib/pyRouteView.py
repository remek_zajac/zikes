# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# (C) 2016 Remek Zajac
#!/usr/bin/python

import json
import cairo
import math

import pyGeometry
import pyPointView
import pyCairoDrawToolkit

class RouteView(pyPointView.PointView):

    KLineWidth           = 8
    KLineColour          = (0.0,0.0,0.0,0.5)

    def __init__(self, referenceMap, points):
        super(RouteView, self).__init__(referenceMap, points)
        def minmax(cum, point):
            if len(point) == 3:
                cost = point[2]["cost"]
                distance = point[2]["routeEdge"]["distanceMts"]
                factor = 1.0 * cost / (16 * distance) if distance > 0 else 1
                point[2]["factor"] = factor
                return (min(cum[0], factor), max(cum[1], factor))
            return cum
        self.mMinMaxFactor = reduce(minmax, points, (1,1))


    def draw(self, cr):
        prevText = None
        cr.select_font_face("Arial", cairo.FONT_SLANT_NORMAL,
                            cairo.FONT_WEIGHT_NORMAL)
        theRoute = SingleRoute(
            width=RouteView.KLineWidth,
            cr=cr,
            geo2Pix=self.mMap.latlon2pix
        )
        cr.set_font_size(18)
        rollingCost = 0
        rollingDistance = 0
        for geoPoint in self.mPoints:
            color         = RouteView.KLineColour
            text          = None
            if len(geoPoint) == 3:
                cost     = geoPoint[2]["cost"]
                distance = geoPoint[2]["routeEdge"]["distanceMts"]
                factor   = geoPoint[2]["factor"]
                if geoPoint[2].get("trafficSignals"):
                    pyCairoDrawToolkit.TrafficSignals(cr, self.mMap.latlon2pix, (0,0,1)).draw(geoPoint[2].get("trafficSignals"))
                if not distance > 0:
                    continue
                rollingCost += cost
                rollingDistance += distance
                text = {"f": round(factor, 3), "us": geoPoint[2]["urbanScore"]["b"]}
                text = json.dumps(text)
                red = (factor - self.mMinMaxFactor[0])/self.mMinMaxFactor[1]
                color = (
                    RouteView.KLineColour[0],
                    RouteView.KLineColour[1],
                    RouteView.KLineColour[2] + 1,
                    RouteView.KLineColour[3]
                )

            pixPoint = theRoute.next(geoPoint, color)
            if text and (not prevText or pyGeometry.Point(pixPoint).distance(pyGeometry.Point(prevText)) > 20):
                cr.set_source_rgba(0, 0, 0, 0.7)
                cr.move_to(pixPoint[0] + 5, pixPoint[1] + 5)
                #cr.show_text(text)
                prevText = pixPoint


class SingleRoute(object):

    def __init__(self, width, cr, geo2Pix, color=(1,1,1,1), points=None, offset=(0,0), arrowHeadLen=0):
        self.mWidth    = width
        self.mGeo2Pix  = geo2Pix
        self.mPrevPix  = None
        self.mColor    = color
        self.mCr       = cr
        self.mOffset   = offset
        self.mArrowH   = arrowHeadLen
        if points:
            for point in points:
                self.next(point)

    def next(self, geoPoint, color=None):
        color = color if color else self.mColor
        pixPoint = self.mGeo2Pix(geoPoint)
        pixPoint = (pixPoint[0]+self.mOffset[0], pixPoint[1]+self.mOffset[1])
        if self.mPrevPix:
            self.mCr.set_line_width(self.mWidth)
            self.mCr.set_source_rgba(*color)
            self.mCr.move_to(self.mPrevPix[0], self.mPrevPix[1])
            self.mCr.line_to(pixPoint[0], pixPoint[1])
            if self.mArrowH:
                arrowVector = pyGeometry.Vector((pixPoint, self.mPrevPix)).setLength(self.mArrowH)
                left = arrowVector.rotate(30).addToPoint(pixPoint)
                right = arrowVector.rotate(-30).addToPoint(pixPoint)
                self.mCr.move_to(left[0], left[1])
                self.mCr.line_to(pixPoint[0], pixPoint[1])
                self.mCr.line_to(right[0], right[1])
            self.mCr.stroke()
        self.mPrevPix = pixPoint
        return pixPoint
