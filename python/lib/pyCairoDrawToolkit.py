# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# (C) 2012 Remek Zajac
#!/usr/bin/python
import cairo
import pyGeometry
import math
from lib.osmsql import pyBicycle

g_colourDescriptors = {'Red': (1.0,0.0,0.0,0.5), "Max": (0.0,0.3,1.0,1.0), "Min": (1.0,0.0,0.0,0.5)}
g_widthDescriptors = {'Thin': 1, 'Normal': 4, 'Thick': 7}

class TripPathToolWithSpeed:
    DefaultStyleDescriptorMAX = {'Colour':g_colourDescriptors['Max'], 'Width':g_widthDescriptors['Thick']}
    DefaultStyleDescriptorMIN = {'Colour':g_colourDescriptors['Min'], 'Width':g_widthDescriptors['Thick']}
    StyleDescriptors = {'Default':{'Max':DefaultStyleDescriptorMAX, 'Min':DefaultStyleDescriptorMIN}}
    def __init__(self, cr, style = "Default"):
        self.lastPoint = None
        self.lastStationaryPoint = None
        self.styleDescriptor = TripPathToolWithSpeed.StyleDescriptors[style]
        self.cr = cr
        self.speedStyleModifier = None

    def draw(self, point, speedStyleModifierPrecent = 50):
        speedStyleModifer = speedStyleModifierPrecent/100.0
        if not self.lastPoint or (not self.lastPoint[2] and not point[2]):
            self.lastPoint = point
            return True
        #calculate width
        maxWidth = self.styleDescriptor['Max']['Width']
        minWidth = self.styleDescriptor['Min']['Width']
        width = maxWidth*speedStyleModifer+minWidth*(1-speedStyleModifer)
        vector = pyGeometry.Vector((self.lastPoint, point))
        if vector.length() <= width:
            #segment too short, refuse to draw
            return False
        self.cr.set_line_width(width)

        #calculate colour for the segment
        maxColour = self.styleDescriptor['Max']['Colour']
        minColour = self.styleDescriptor['Min']['Colour']
        red   = maxColour[0]*speedStyleModifer+minColour[0]*(1-speedStyleModifer)
        green = maxColour[1]*speedStyleModifer+minColour[1]*(1-speedStyleModifer)
        blue  = maxColour[2]*speedStyleModifer+minColour[2]*(1-speedStyleModifer)
        alpha = maxColour[3]*speedStyleModifer+minColour[3]*(1-speedStyleModifer)
        self.cr.set_source_rgba(red, green, blue, alpha)

        self.cr.move_to(self.lastPoint[0], self.lastPoint[1])
        self.cr.line_to(point[0], point[1])
        self.cr.stroke()
        self.lastPoint = point
        return True

    def markStationary(self, point):
        if not point[2]:
            #invisible, don't bother drawing
            return
        maxWidth = self.styleDescriptor['Max']['Width']
        if (self.lastStationaryPoint and pyGeometry.Vector((self.lastStationaryPoint, point)).length() <= maxWidth):
            #segment too short, refuse to draw
            return
        self.cr.arc(point[0], point[1], maxWidth, 0, 2*math.pi)
        self.cr.set_source_rgb(1,0,0)
        self.cr.set_line_width(maxWidth/2)
        self.cr.stroke_preserve()
        self.cr.set_source_rgb(1,1,1)
        self.cr.fill()
        self.lastStationaryPoint = point



class RoadTool:

    def __init__(self, cr, drawConfig, transform=lambda point: point):
        self.mCr            = cr
        self.mDrawConfig    = drawConfig
        self.mTransform     = transform

    def draw(self, nodes):
        colour = self.mDrawConfig.colour()
        self.mCr.set_source_rgba(colour[0],colour[1], colour[2], colour[3])
        self.mCr.set_line_width(self.mDrawConfig.width())
        prevNode = None
        for node in nodes:
            pixNode = self.mTransform(node)
            if not prevNode:
                self.mCr.move_to(pixNode[0], pixNode[1])
            else:
                self.mCr.line_to(pixNode[0], pixNode[1])
            prevNode = node
        self.mCr.stroke()

class CyclewayTool:
    KColour = (0.0,0.0,1.0,0.7)
    KWidth  = 3
    KArrowArmLen = 10
    KArrowAngle  = 30

    def __init__(self, cr, cycleway, transform=lambda point: point):
        self.mCr            = cr
        self.mCycleway      = cycleway
        self.mTransform     = transform

    def drawArrow(self, start, end):
        direction = pyGeometry.Vector((start[0]-end[0], start[1]-end[1])).setLength(CyclewayTool.KArrowArmLen)
        leftArm = direction.rotate(CyclewayTool.KArrowAngle)
        rightArm = direction.rotate(-CyclewayTool.KArrowAngle)
        left = leftArm.addToPoint(end)
        right   = rightArm.addToPoint(end)
        self.mCr.move_to(left[0], left[1])
        self.mCr.line_to(end[0], end[1])
        self.mCr.line_to(right[0], right[1])

    def draw(self, nodes):
        if (self.mCycleway[0] == pyBicycle.Bicycle.Access.EDesignated or
            self.mCycleway[1] == pyBicycle.Bicycle.Access.EDesignated):
            self.mCr.set_source_rgba(
                CyclewayTool.KColour[0],
                CyclewayTool.KColour[1],
                CyclewayTool.KColour[2],
                CyclewayTool.KColour[3]
            )
            arrows = [self.mCycleway[1] == pyBicycle.Bicycle.Access.EDesignated,
                      self.mCycleway[0] == pyBicycle.Bicycle.Access.EDesignated]
            self.mCr.set_line_width(CyclewayTool.KWidth)
            prevPrevPix = None
            prevPix = None
            for node in nodes:
                pixNode = self.mTransform(node)
                if not prevPix:
                    self.mCr.move_to(pixNode[0], pixNode[1])
                else:
                    self.mCr.line_to(pixNode[0], pixNode[1])
                    if arrows[0]:
                        self.drawArrow(pixNode, prevPix)
                        self.mCr.move_to(pixNode[0], pixNode[1])
                        arrows[0] = None
                prevPrevPix = prevPix
                prevPix = pixNode
            if arrows[1]:
                self.drawArrow(prevPrevPix, prevPix)
            self.mCr.stroke()


class TextTool:
    def __init__(self, cr):
        self.cr = cr

    def draw(self, xy, text, size=10):
        self.cr.set_source_rgb(0.1, 0.1, 0.1)

        self.cr.select_font_face("Arial", cairo.FONT_SLANT_NORMAL,
            cairo.FONT_WEIGHT_NORMAL)
        self.cr.set_font_size(size)

        self.cr.move_to(xy[0], xy[1])
        self.cr.show_text(text)
        return True

class NodeTool:
    def __init__(self, cr, transform=lambda point: point):
        self.mCr = cr
        self.mTransform = transform

    def draw(self, point, size=5):
        point = self.mTransform(point)
        self.mCr.set_source_rgba(0, 0, 0.5, 0.5)
        self.mCr.set_line_width(1)
        self.mCr.arc(point[0], point[1], size, 0, 2*math.pi)
        self.mCr.stroke_preserve()
        self.mCr.fill()


class TrafficSignals:
    KRadius = 4
    def __init__(self, cr, transform=lambda point: point, background=(1,1,1)):
        self.mCr = cr
        self.mTransform = transform
        self.mBackground = background

    def draw(self, point):
        point = self.mTransform(point)
        x = point[0] + TrafficSignals.KRadius
        y = point[1]
        self.mCr.arc(x, y + 2*TrafficSignals.KRadius , 3*TrafficSignals.KRadius, 0, 2*math.pi)
        self.mCr.set_source_rgb(0,0,0)
        self.mCr.set_line_width(1)
        self.mCr.stroke_preserve()
        self.mCr.set_source_rgb(*self.mBackground)
        self.mCr.fill()
        self.mCr.arc(x, y, TrafficSignals.KRadius, 0, 2*math.pi)
        self.mCr.set_source_rgb(1,0,0)
        self.mCr.fill()
        self.mCr.arc(x, y + 2*TrafficSignals.KRadius , TrafficSignals.KRadius, 0, 2*math.pi)
        self.mCr.set_source_rgb(1,0.5,0)
        self.mCr.fill()
        self.mCr.arc(x, y + 4*TrafficSignals.KRadius, TrafficSignals.KRadius, 0, 2*math.pi)
        self.mCr.set_source_rgb(0,1,0)
        self.mCr.fill()


class BezierCurve(object):
    KNormalControlPointRadius       = 6
    KHighlightedControlPointRadius  = 15

    def __init__(self, cntrPts):
        self.controlPoints = cntrPts
        self.highlightedControlPointIdx = None
        self.draggedPointIdx = None

    def binomial(self, i, n):
        """Binomial coefficient"""
        return math.factorial(n) / float(
            math.factorial(i) * math.factorial(n - i))

    def bernstein(self, t, i, n):
        """Bernstein polynom"""
        return self.binomial(i, n) * (t ** i) * ((1 - t) ** (n - i))


    def bezier(self, t, points):
        """Calculate coordinate of a point in the bezier curve"""
        n = len(points) - 1
        x = y = 0
        for i, pos in enumerate(points):
            bern = self.bernstein(t, i, n)
            x += pos[0] * bern
            y += pos[1] * bern
        return x, y


    def path(self, steps, points):
        """Range of points in a curve bezier"""
        for i in xrange(steps):
            t = i / float(steps - 1)
            yield self.bezier(t, points)

    def insertControlPointPair(self, points, idx=-1):
        idx += idx % 2
        self.controlPoints.insert(idx, points[1])
        self.controlPoints.insert(idx, points[0])

    def removeControlPointPair(self, idx):
        if len(self.controlPoints) > 4:
            if idx:
                self.controlPoints.pop(idx)
                if idx % 2 == 0:
                    self.controlPoints.pop(idx)
                else:
                    self.controlPoints.pop(idx-1)
            return True
        else:
            print "Cannot delete beyond the last 4"
            return False


    def draw(self, cr, steps, curveStyle, controlsStyle=None):
        curveColor = curveStyle[0]
        curveWidth = curveStyle[1]
        cr.set_source_rgba(curveColor[0], curveColor[1], curveColor[2], curveColor[3])
        cr.set_line_width(curveWidth)
        firstPoint = self.controlPoints[0]
        cr.move_to(firstPoint[0], firstPoint[1])
        for point in self.path(steps, self.controlPoints):
            cr.line_to(point[0], point[1])
        cr.stroke()

        if controlsStyle:
            controlColor = controlsStyle[0]
            controlWidth = controlsStyle[1]
            cr.set_source_rgba(controlColor[0], controlColor[1], controlColor[2], controlColor[3])
            cr.set_line_width(controlWidth)
            i = 0

            while i < len(self.controlPoints):
                point = self.controlPoints[i]
                radius = BezierCurve.KNormalControlPointRadius
                if i == self.highlightedControlPointIdx != None:
                    radius = BezierCurve.KHighlightedControlPointRadius
                cr.arc(point[0], point[1], radius, 0, 2*math.pi)
                cr.fill()
                if i % 2 == 0:
                    fromPoint = self.controlPoints[i]
                    toPoint   = self.controlPoints[i+1]
                    cr.move_to(fromPoint[0], fromPoint[1])
                    cr.line_to(toPoint[0], toPoint[1])
                i += 1
                cr.stroke()

    def on_mouse_down(self, event):
        if self.highlightedControlPointIdx != None:
            self.draggedPointIdx = self.highlightedControlPointIdx
            return True
        return False

    def on_mouse_up(self, event):
        if self.draggedPointIdx != None:
            self.draggedPointIdx = None
            return True
        return False

    def on_mouse_move(self, event):
        if self.draggedPointIdx != None:
            self.controlPoints[self.draggedPointIdx][0] = event.x
            self.controlPoints[self.draggedPointIdx][1] = event.y
            return True

        result = False
        if self.highlightedControlPointIdx != None:
            result = True
        self.highlightedControlPointIdx = None
        for idx, point in enumerate(self.controlPoints):
            if math.fabs(point[0]-event.x) <= BezierCurve.KHighlightedControlPointRadius and math.fabs(point[1]-event.y) <= BezierCurve.KHighlightedControlPointRadius:
                self.highlightedControlPointIdx = idx
                result = True
                break
        return result



class RectangleTool:
    def __init__(self):
        self.polygonTool = PolygonTool()

    def init(self, cr):
        self.polygonTool.init(cr)

    def draw(self, pixCoord, colorRGBA, shadow=None):
        self.polygonTool.draw(
            [[pixCoord[0], pixCoord[1]],[pixCoord[2],pixCoord[1]],[pixCoord[2],pixCoord[3]],[pixCoord[0],pixCoord[3]]],
            colorRGBA,
            shadow
        )


class PolygonTool:
    SHADOW_OUT                = 1
    SHADOW_IN                 = 2
    SHADOW_DEPTH              = 2
    SHADOW_COLOR_SHIFT_FACTOR = 0.3

    def __init__(self):
        pass

    def init(self, cr):
        self.cr = cr

    def draw(self, path, colorRGBA, shadow=None):
        if shadow:

            illuminatedColor = (
                min(1, colorRGBA[0]*(1+PolygonTool.SHADOW_COLOR_SHIFT_FACTOR)),
                min(1, colorRGBA[1]*(1+PolygonTool.SHADOW_COLOR_SHIFT_FACTOR)),
                min(1, colorRGBA[2]*(1+PolygonTool.SHADOW_COLOR_SHIFT_FACTOR)),
                1
            )
            shadowColor = (
                max(0, colorRGBA[0]*(1-PolygonTool.SHADOW_COLOR_SHIFT_FACTOR)),
                max(0, colorRGBA[1]*(1-PolygonTool.SHADOW_COLOR_SHIFT_FACTOR)),
                max(0, colorRGBA[2]*(1-PolygonTool.SHADOW_COLOR_SHIFT_FACTOR)),
                1
            )

            topLeftColor = illuminatedColor
            bottomRightColor = shadowColor
            if shadow == PolygonTool.SHADOW_IN:
                bottomRightColor = illuminatedColor
                topLeftColor = shadowColor
            #top-left shift
            self.cr.move_to(path[0][0]-PolygonTool.SHADOW_DEPTH, path[0][1]-PolygonTool.SHADOW_DEPTH)
            for vertex in path:
                self.cr.line_to(vertex[0]-PolygonTool.SHADOW_DEPTH, vertex[1]-PolygonTool.SHADOW_DEPTH)
            self.cr.close_path()
            self.cr.set_source_rgba(topLeftColor[0],topLeftColor[1],topLeftColor[2],topLeftColor[3])
            self.cr.fill()

            #then the shadow
            self.cr.move_to(path[0][0]+PolygonTool.SHADOW_DEPTH, path[0][1]+PolygonTool.SHADOW_DEPTH)
            for vertex in path:
                self.cr.line_to(vertex[0]+PolygonTool.SHADOW_DEPTH, vertex[1]+PolygonTool.SHADOW_DEPTH)
            self.cr.close_path()
            self.cr.set_source_rgba(bottomRightColor[0],bottomRightColor[1],bottomRightColor[2],bottomRightColor[3])
            self.cr.fill()

        #then the actual
        self.cr.move_to(path[0][0], path[0][1])
        for vertex in path:
            self.cr.line_to(vertex[0], vertex[1])
        self.cr.close_path()
        self.cr.set_source_rgba(colorRGBA[0],colorRGBA[1],colorRGBA[2],colorRGBA[3])
        self.cr.fill()

