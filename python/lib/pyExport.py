# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# (C) 2015 Remek Zajac
#!/usr/bin/python

import gtk

#local modules
from osmsql import pyGPBExport as pyGPBExportOsmDB
import pyPolyline

class ExportMapWindow(gtk.Window):

    def gpbExport(db, region, dstPath, features):
        pyGPBExportOsmDB.GPBExport(db, region, dstPath, features).do()

    KFormatOSMXML = "XML"
    KFormatGPB    = "Google Proto Buffers"
    KFormats      = [
        (KFormatGPB, gpbExport)
    ]
    KPolyLinePrompt = "PolyLine Url (otherwise concave hull will be generated)"

    KFeatures     = [
        ("meta", True),
        ("coverage", True),
        ("node", True),
        ("way", True),
        ("waysection", True),
        ("in visible region only", False)
    ]

    def __init__(self, db, region):
        super(ExportMapWindow, self).__init__()
        self.mRegion = region
        self.mDb = db
        self.set_title("Export Map Data")
        self.set_position(gtk.WIN_POS_CENTER)
        self.set_transient_for(None)
        self.add(self.ui())
        self.connect("destroy", self.on_destroy)

    def ui(self):
        chooseFileButton = gtk.Button("Export")
        chooseFileButton.connect("clicked", lambda foo: self.export())
        chooseFileButton.show()
        outerContainerBox = gtk.VBox(False, 2)
        outerContainerBox.set_border_width(4)
        outerContainerBox.pack_start(self.formatCombo(), False, False, 2)
        outerContainerBox.pack_start(self.polyLineUrl(), False, False, 2)
        outerContainerBox.pack_start(self.features(), True, True, 2)
        outerContainerBox.pack_start(self.dstFile(), False, False, 2)
        outerContainerBox.pack_start(chooseFileButton, False, False, 2)
        return outerContainerBox

    def formatCombo(self):
        self.mFormatCombo = gtk.combo_box_new_text()
        for format in ExportMapWindow.KFormats:
            self.mFormatCombo.append_text(format[0])
        self.mFormatCombo.set_active(0)
        return self.mFormatCombo

    def features(self):
        box = gtk.VBox(True, 2)
        self.mFeatureCheckButtons = {}
        for feature in ExportMapWindow.KFeatures:
            featureCheckButton = gtk.CheckButton(feature[0])
            featureCheckButton.set_active(feature[1])
            box.pack_start(featureCheckButton)
            self.mFeatureCheckButtons[feature[0]] = featureCheckButton
        return box

    def dstFile(self):
        box = gtk.HBox(False, 2)
        self.mFilePathEntry = gtk.Entry()
        self.mFilePathEntry.set_width_chars(20)
        self.mFilePathEntry.set_editable(gtk.TRUE)
        self.mFilePathEntry.set_text("./exported")
        chooseFileButton = gtk.Button("...")
        chooseFileButton.connect("clicked", self.showFileChooser)
        chooseFileButton.show()

        box.pack_start(self.mFilePathEntry, True, True, 2)
        box.pack_start(chooseFileButton, False, False, 2)
        return box

    def polyLineUrl(self):
        self.mPolyLineUrl = gtk.Entry()
        self.mPolyLineUrl.set_width_chars(40)
        self.mPolyLineUrl.set_editable(gtk.TRUE)
        self.mPolyLineUrl.set_text(ExportMapWindow.KPolyLinePrompt)
        self.mPolyLineUrl.modify_text(gtk.STATE_NORMAL, gtk.gdk.color_parse("grey"))

        def clicked(foo, bar):
            if self.mPolyLineUrl.get_text() == ExportMapWindow.KPolyLinePrompt:
                self.mPolyLineUrl.modify_text(gtk.STATE_NORMAL, gtk.gdk.color_parse("black"))
                self.mPolyLineUrl.set_text("")

        self.mPolyLineUrl.connect("button-press-event", clicked)
        return self.mPolyLineUrl

    def showFileChooser(self, foo):
        chooser = gtk.FileChooserDialog(title="Export to..",action=gtk.FILE_CHOOSER_ACTION_SAVE,
                  buttons=(gtk.STOCK_CANCEL,gtk.RESPONSE_CANCEL,gtk.STOCK_OPEN,gtk.RESPONSE_OK))
        chooser.set_current_folder(".")
        response = chooser.run()
        if response == gtk.RESPONSE_OK:
            self.mFilePathEntry.set_text(chooser.get_filename())
        chooser.destroy()


    def on_destroy(self, foo):
        self.hide()

    def show(self):
        self.show_all()

    def export(self):
        polylines = True
        if len(self.mPolyLineUrl.get_text()) and self.mPolyLineUrl.get_text() != ExportMapWindow.KPolyLinePrompt:
            if self.mPolyLineUrl.get_text().startswith("http://"):
                polylines = [pyPolyline.Polyline.parse(self.mPolyLineUrl.get_text())]
            else:
                polylines = self.mPolyLineUrl.get_text()
        features = {}
        for k,v in self.mFeatureCheckButtons.items():
            if k == "in visible region only":
                if not v.get_active():
                    self.mRegion = None
                continue
            if v.get_active():
                if k == "coverage":
                    features[k] = polylines
                else:
                    features[k] = v.get_active()
        exportFunction = ExportMapWindow.KFormats[self.mFormatCombo.get_active()][1]
        exportFunction(self.mDb, self.mRegion, self.mFilePathEntry.get_text(), features)
        self.destroy()
