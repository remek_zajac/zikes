# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# (C) 2012 Remek Zajac
#!/usr/bin/python

# Module Reads HGT files (to be downloaded from http://www.viewfinderpanoramas.org/dem3.html)
# An HGT file covers an area of 1deg x1deg . Its south western corner can be deduced from its file name:
# for example, n51e002.hgt covers the area between N 51deg  E 2deg  and N 52deg  E 3deg , and s14w077.hgt covers
# S 14deg  W 77deg  to S 13deg  W 76deg . The fils size depends on the resolution. If this is 1", there are 3601
# rows of 3601 cells each; if it is 3", there are 1201 rows of 1201 cells each. The rows are laid out
# like text on a page, starting with the northernmost row, with each row reading from west to east.
# Each cell has two bytes, and the elevation at that cell is 256*(1st byte) + (2nd byte). It follows
# that a 3" HGT file has a file length of 2 x 1201 x 1201. SRTM 3" cells are calculated by calculating
# the mean of 1"
import math
import os
import zipfile
import time
import shutil
import threading
import struct

#local modules
import pyGeometry
import pyUtils

class Elevation(object):

    class HGTFileMissingInZIP(Exception):
        pass

    #HGT file groups are downloadable from http://www.viewfinderpanoramas.org/dem3.html
    #in form of the XYY.zip files where Z:
    # - for northern hemisphere starts from A and goes northwards (B, C, ...) by 4 deg intervals
    # - for southern hemisphere starts from SA and goes southwards (SB, SC, ...) by 4 deg intevals
    # and where YY is a number starting from the western extremity and going east in 6 deg intervals
    KDownloadedFileLatLonSpan=(4,6)
    KDownloadURL="http://www.viewfinderpanoramas.org/dem3/"
    KHGTRows = 1201
    KHGTColumns = 1201

    def __init__(self, debug=None, cacheRoot=pyUtils.KCacheFolder):
        self.mCacheFolder = os.path.join(cacheRoot, "ElevationHGT")
        if not os.path.exists(self.mCacheFolder):
            os.makedirs(self.mCacheFolder)
        self.mRamCache = {}
        self.mProcessedRemoteFiles = set()
        if not debug:
            debug = self.mDebug = pyUtils.Debug(pyUtils.Debug.normal)
        self.mDebug = debug
        self.mLock = threading.Lock()

    # getAt returns elevation in metres for the given latlon tuple.
    # it can either (1) approximate the result from the elevation grid, which it
    # does with square weights (surface ratios) (2) coerce the latlong onto the grid
    #
    # approximateConfidence - 0..1 - the minimum acceptable surface of contributions
    #                         (all 4 add to 1).
    # approximateConfidence - None - coerce to grid.
    def getAt(self, latlon, approximateConfidence=0.5):
        self.mLock.acquire()
        try:
            result = self.__getAt(latlon, approximateConfidence)
        finally:
            self.mLock.release()
        return result

    def __getAt(self, latlon, approximateConfidence):

        #"An HGT file covers an area of 1deg x 1deg. Its south western corner can be
        #deduced from its file name: for example, n51e002.hgt covers the area between
        #N 51deg  E 2deg  and N 52deg  E 3deg (...) The rows are laid out
        #like text on a page, starting with the northernmost row, with each row reading
        #from west to east".
        #
        #lattitude coordinates grow south to north whilst longitude coordinates
        #grow west to east. Thus, for identifying the correct file (south-western
        #corner) it will be correct to floor (largest integer smaller than the argument)
        #both exact coordinates
        wholeDegreesLatLon = (int(math.floor(latlon[0])), int(math.floor(latlon[1])))

        #Having a ramCacheWindow representing the 1deg x 1deg window layed out
        #north->south and west->east we need to identify the correct value
        #among Elevation.KHGTRows and Elevation.KHGTColumns and thus come up
        #with the reminder (less significant portion behind the decimal point)
        #This reminder, in both directions will span between 0 and 0.99999999...
        #never reaching 1 (as that would be another window)
        reminderLatLon = (latlon[0]-wholeDegreesLatLon[0], latlon[1]-wholeDegreesLatLon[1])

        #For northern hemisphere, e.g.: lat 51.1, the reminder is 0.1 and the
        #row is (1-0.1)*Elevation.KHGTRows since the values in the window grow in
        #the reverse order (north to south) to the coordinate system (south to north)
        #For southern hemisphere, e.g.: lat -51.1, the rows grow with the latitude
        #abs values, but let's see if the same formula holds:
        #-51.1-(-52)=-51.1+52=52-51.1=0.9. 1-0.9 = 0.1, which should hold.
        ramCacheRowF = (1-reminderLatLon[0])*Elevation.KHGTRows
        ramCacheColF = reminderLatLon[1]*Elevation.KHGTColumns

        #Eventually, every sought after point (S) falls into a grid:
        #
        #      #         #
        #      #         #
        #  ####A#########B####
        #      #\       /#
        #      #         #
        #      #         #
        #      #         #
        #      #         #
        #      #     S   #
        #      #/       \#
        #  ####C#########D####
        #      #\        #
        #      #         #
        #
        #  Where we have precise measurements for A, B, C, D, but not for S, so we
        #  have to approximate somehow. And so, for each such precise measurements
        #  we'll calculate a distance-weighted average
        ramCacheRowNorth    = min(int(ramCacheRowF), Elevation.KHGTRows-1) #for reminder of 0 it can go beyond.
        ramCacheColumnWest  = int(ramCacheColF)
        coordsNW = (
            wholeDegreesLatLon[0]+(1.0-1.0*ramCacheRowNorth/Elevation.KHGTRows),
            wholeDegreesLatLon[1]+1.0*ramCacheColumnWest/Elevation.KHGTColumns
        )
        ramCacheWindow      = None
        try:
            ramCacheWindow = self.__getRamCacheWindow(wholeDegreesLatLon)
        except Elevation.HGTFileMissingInZIP:
            pass

        if not ramCacheWindow:
            if approximateConfidence==None:
                return (coordsNW, None)
            else:
                return None

        if approximateConfidence==None:
            #if it's not asked to approximate, it will return:
            #(cooridnates of the location it does report on, its elevation)
            return (
                coordsNW,
                ramCacheWindow[ramCacheRowNorth][ramCacheColumnWest]
            )

        ABCD = [(0, 0),(0, 1),(1, 0),(1, 1)]

        result = (0,0.0) #(current elevation, accumulated weigth)
        for adjMeasurement in ABCD:
            row = ramCacheRowNorth+adjMeasurement[0]
            col = ramCacheColumnWest+adjMeasurement[1]
            eleMts = None
            if row > Elevation.KHGTRows-1 or col > Elevation.KHGTColumns-1:
                #Now, there will be a case where we will be dealing with the last row or column
                #In which case we need to load the next window
                coords = (
                    coordsNW[0]-1.5*adjMeasurement[0]/Elevation.KHGTRows,    #1.5* because floating point arithemtics can bring us back to NW
                    coordsNW[1]+1.5*adjMeasurement[1]/Elevation.KHGTColumns
                )
                eleMts = self.__getAt(coords, approximateConfidence=None)
                if eleMts:
                    eleMts = eleMts[1]
                else:
                    #what now? There is window for the sought after point, but no this specific adjacent
                    #window. Does that mean there is no data or that there is sea? IDK, for now
                    #we simply not let this point influcence the result
                    continue
            else:
                eleMts = ramCacheWindow[row][col]

            if eleMts == None:
                #There is a hole in the window (grep for -32768 below) - the measurement is absent
                #we gonna need to approximate with what we have (same as when facing a sea)
                continue

            contributionFactor=(1.0 - math.fabs(ramCacheRowF-row))*(1.0 - math.fabs(ramCacheColF-col))
            eleMts = contributionFactor*eleMts
            result = (result[0]+eleMts, result[1]+contributionFactor)
        #in case some of the contributors were absent, result[1] will be less that 1
        #we'll reject the result if below required confidence
        if result[1] < approximateConfidence:
            return None
        #otherwise, we need to inflate the value of result[0] to reach 1.
        result = (result[0]/result[1], 1)
        return int(result[0])



    def getResolution(self):
        return (1.0/Elevation.KHGTRows, 1.0/Elevation.KHGTColumns)

    def __getRamCacheWindow(self, wholeDegreesLatLon):
        if not self.mRamCache.has_key(wholeDegreesLatLon[0]):
            self.mRamCache[wholeDegreesLatLon[0]] = {}
        if not self.mRamCache[wholeDegreesLatLon[0]].has_key(wholeDegreesLatLon[1]):
            self.mRamCache[wholeDegreesLatLon[0]][wholeDegreesLatLon[1]] = self.__loadHGTFile(wholeDegreesLatLon)
        return self.mRamCache[wholeDegreesLatLon[0]][wholeDegreesLatLon[1]]

    def __loadHGTFile(self, wholeDegreesLatLon):
        fileName = self.mCacheFolder+"/"
        ramCache = []

        latFileNameComponent = wholeDegreesLatLon[0]
        if (latFileNameComponent < 0):
            fileName += "S"
            latFileNameComponent = -latFileNameComponent
        else:
            fileName += "N"
        fileName += str(latFileNameComponent).zfill(2)
        lonFileNameComponent = wholeDegreesLatLon[1]
        if (lonFileNameComponent < 0):
            fileName += "W"
            lonFileNameComponent = -lonFileNameComponent
        else:
            fileName += "E"
        fileName += str(lonFileNameComponent).zfill(3)
        fileName += ".hgt"
        downloadedZipFile = None
        if not os.path.exists(fileName):
            downloadedZipFile = self.__downloadAndUnzipFileFor(wholeDegreesLatLon)

        if not os.path.exists(fileName):
            errorMessage = "Expected "+str(downloadedZipFile)+" to contain file "+fileName+" but it didn't. It's fine if there is a sea at ["+str(wholeDegreesLatLon)+"]."
            if downloadedZipFile:
                self.mDebug.warn(errorMessage)
            raise Elevation.HGTFileMissingInZIP(errorMessage)

        self.mDebug.logUpdated("Loading elevation data file: "+fileName)
        with open(fileName, "rb") as f:
            for rowIdx in range(Elevation.KHGTRows):
                ramCache.append([])
                for column in range(Elevation.KHGTColumns):
                    buf = f.read(2)  # read two bytes and convert them:
                    eleMts, = struct.unpack('>h', buf)  # ">h" is a signed two byte integer
                    if eleMts == -32768:  # the not-a-valid-sample value
                        eleMts = None
                    ramCache[rowIdx].append(eleMts)
        self.mDebug.logUpdated()
        return ramCache


    def __downloadAndUnzipFileFor(self, wholeDegreesLatLon):
        Z=math.ceil(
            math.fabs(wholeDegreesLatLon[0]/Elevation.KDownloadedFileLatLonSpan[0]))
        Z=chr(int(Z)+ord('A'))
        if wholeDegreesLatLon[0] < 0:
            # prepend "S" as this is how http://viewfinderpanoramas.org/Coverage%20map%20viewfinderpanoramas_org3.htm
            # likes it, also adjust for leaning a 4x6 tile, for
            # - lat 4 is one tile away from the equator (B)
            # - lat -4 should be touching the equator (A) but maths yield B, so needs shifting
            Z = "S"+chr(ord(Z)-1)
        YY=float(wholeDegreesLatLon[1]+180)/Elevation.KDownloadedFileLatLonSpan[1]
        YY=math.floor(1+YY) #adding 1 as the files are indexed from 1 (not 0)
        YY=str(int(YY)).zfill(2)
        fileName = Z+YY+".zip"
        remoteFileName = Elevation.KDownloadURL+fileName
        if remoteFileName in self.mProcessedRemoteFiles:
            #we've already processed this one. if it's needed again, then it probably doesn't
            #deliver on the hope, so skip the request
            return
        localFileName = os.path.join(self.mCacheFolder, fileName)
        KTrials = 5
        for i in range(0,KTrials):
            try:
                self.__doDownloadAndUnzip(remoteFileName, localFileName)
                break
            except:
                self.mDebug.err("Will try downloading %s %s more times" % (remoteFileName, KTrials-i))
                time.sleep(5)


        self.mProcessedRemoteFiles.add(remoteFileName)
        return remoteFileName




    def __doDownloadAndUnzip(self, remoteFileName, localFileName):

        if not os.path.exists(localFileName):
            localFile = open(localFileName, 'wb')
            try:
                self.mDebug.log("Downloading elevation data file: %s" % remoteFileName)
                pyUtils.FileDownload.download(remoteFileName, localFile, self.mDebug, 60*24)
                localFile.close()
            except pyUtils.FileDownload.PersistentError as e:
                self.mDebug.err("Unable to download %s because of persistent error: %s" % (remoteFileName, e))
                localFile.close()
                return remoteFileName
            except:
                localFile.close()
                os.remove(localFileName)
                raise

        zfile = None
        try:
            self.mDebug.log("  Extracting downloaded elevation data file: "+localFileName )
            zfile = zipfile.ZipFile(localFileName)
            for name in zfile.namelist():
                (dirname, filename) = os.path.split(name)
                if not filename:
                    continue

                # copy file (taken from zipfile's extract)
                source = zfile.open(name)
                target = file(os.path.join(self.mCacheFolder, filename), "wb")
                with source, target:
                    shutil.copyfileobj(source, target)
                source.close()
        except:
            self.mDebug.err("Could not extract elevation data file: "+localFileName)
            os.remove(localFileName)
            raise

        finally:
            if zfile:
                zfile.close()








class ClimbingWay(object):
    KMinSectionDistanceMts      = 90
    KMaxSectionEleDeviationMts  = 30

    def __init__(self, nodes, eleProvider):
        self.mNodes             = nodes
        self.mClimbingSections  = []
        self.mEleProvider       = eleProvider
        self.mDebug             = eleProvider.mDebug
        self.__process()

    def sections(self):
        return self.mClimbingSections

    def __process(self):
        nodesWithDistances  = []
        prevNode            = None
        haveAllElevations   = True
        for node in self.mNodes:
            nodesWithDistances.append({
                "fromPrevMts" : prevNode.distanceMts(node) if prevNode else 0,
                "node"        : node
            })
            prevNode = node
            if node.eleMts() is None:
                haveAllElevations = False
                raise Exception("Node %s has no elevation data" % node)

        if not haveAllElevations:
            self.mClimbingSections.append(
                ClimbingWaySection(
                    self.mNodes,
                    nodesWithDistances[-1][0],
                    None
                )
            )
            return

        startNodeIdx = 0
        # Given an array of tuples (fromPrevMts, eleMts) this loop progresses left to right until it
        # consumes the whole array and thus until, within the given array, it has identified all subsections
        # with a distinct climb. All tuples left from startNodeIdx have been processed
        while startNodeIdx < len(self.mNodes)-1:

            # Given a subarray of aformentioned tuples, one that starts somewhere in the array's middle (startNodeIdx),
            # and ends at the array's end. Try to see if such a subarray (trySection) represents a linear progression
            # (see self.__isDatasetLinear) and if it doesn't, retract at its end (take one elem less) until it
            # is linear. It is guaranteed to be linear when it reaches the length of two.
            endNodeIdx = len(self.mNodes)-1
            while True:
                trySection        = nodesWithDistances[startNodeIdx:endNodeIdx+1]
                # trySectionDataSet is a transform of trySection accepted by self.__isDatasetLinear, i.e.:
                # [(cum distanceMts from begining, eleMts),...]
                trySectionDataSet = []
                for node in trySection:
                    val = (0,node["node"].eleMts())
                    if len(trySectionDataSet) > 0:
                        val = (trySectionDataSet[-1][0]+node["fromPrevMts"], val[1])
                    trySectionDataSet.append(val)

                if (startNodeIdx+1 == endNodeIdx or                                    #reached the shortest (2 elem section)
                    trySectionDataSet[-1][0] < ClimbingWay.KMinSectionDistanceMts or   #reached the shortest precission distance
                    self.__isDatasetLinear(trySectionDataSet)):                        #found a section that fits into a linear progression
                    sectionLenMts = trySectionDataSet[-1][0]
                    sectionClimb  = 0
                    if sectionLenMts > 0:
                        dEleMts = trySection[-1]["node"].eleMts() - trySection[0]["node"].eleMts()
                        sectionClimb = int(100.0 * dEleMts / sectionLenMts)
                        if sectionLenMts < ClimbingWay.KMinSectionDistanceMts and math.fabs(sectionClimb) > 0:
                            # Some ways are too short to make a useful approximation. They can be of 1m length and
                            # happen to be at 1m elevation boundary and thus result in a 100% climb. We will then
                            # use self.mEleProvider to extend this distance and reach out to cover  ClimbingWay.KMinSectionDistanceMts
                            sectionClimb = self.__extend(trySection, sectionLenMts, sectionClimb)

                    self.mClimbingSections.append(
                        ClimbingWaySection(
                            map(lambda pair: pair["node"], trySection),
                            sectionLenMts,
                            sectionClimb
                        )
                    )
                    break
                else:
                    endNodeIdx -= 1
            startNodeIdx = endNodeIdx

    def __isDatasetLinear(self, dataset):
        smoothenDataset = pyGeometry.LinearSmoother.linearSmoothen(dataset, ClimbingWay.KMinSectionDistanceMts)
        return pyGeometry.LinearSmoother.isLinear(smoothenDataset, ClimbingWay.KMaxSectionEleDeviationMts)


    def __extend(self, trySection, sectionLenMts, origClimb):
        def extendSeg(fromPoint, toPoint, byMts):
            dstPoint = toPoint.offsetCourse(
                byMts,
                -pyGeometry.GeoPoint.toRad(fromPoint.bearing(toPoint))
            )
            assert math.fabs(dstPoint.distanceMts(toPoint) - byMts) < 0.1
            assert math.fabs(dstPoint.distanceMts(fromPoint) - fromPoint.distanceMts(toPoint) - byMts) < 0.1
            return dstPoint

        reachDMts = (ClimbingWay.KMinSectionDistanceMts - sectionLenMts) / 2
        left  = extendSeg(trySection[1]["node"], trySection[0]["node"], reachDMts)
        right = extendSeg(trySection[-2]["node"], trySection[-1]["node"], reachDMts)
        dEleMts = self.mEleProvider.getAt(right) - self.mEleProvider.getAt(left)
        newSectionLenMts = left.distanceMts(right)
        newClimb = int(100.0 * dEleMts / newSectionLenMts)
        if math.fabs(newClimb) < math.fabs(origClimb):
            self.mDebug.log(
                "Section too short to calculate climb (%dm->%dm), extended to %s and changed climb %d%%->%d%%" % (
                    sectionLenMts, newSectionLenMts,
                    map(lambda pt: [pt.x, pt.y],
                        [left, trySection[0]["node"], trySection[-1]["node"], right]),
                    origClimb,
                    newClimb
                ),
                pyUtils.Debug.fine
            )
            return newClimb
        return origClimb


class ClimbingWaySection(object):

    def __init__(self, nodes, distanceMts, climb):
        self.mNodes         = nodes
        #It can be perfectly normal for waysections to have
        #literally 0 length (elevator) and ignoring them
        #would be a mistake, because they link things
        self.mDistanceMts   = int(round(distanceMts))
        self.mClimb         = climb

    def nodes(self):
        return self.mNodes

    def climb(self):
        return self.mClimb

    def distanceMts(self):
        return self.mDistanceMts


