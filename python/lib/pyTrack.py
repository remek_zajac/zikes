# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# (C) 2012 Remek Zajac
#!/usr/bin/python

import math
import base64
from struct import *

#debug
import gobject
import time
import threading
import pyConfig
import psycopg2

#local modules
import pyGeometry
import pyCairoDrawToolkit
import pyUtils
import pyPriorityQ
from osmsql import pyBrowse as pyBrowseOsmDB
from osmsql import pyBase   as pyBaseOsmDB


class Track(object):

    class NoSpeedInformation(Exception):
        pass

    def __init__(self, parent=None):
        self.clear()
        self.mParent = parent

    def clear(self):
        self.mRegion     = pyGeometry.GeoRegion()
        self.mDistanceKm = 0
        self.mTripTime   = None
        self.mTail       = None
        self.mHead       = None

    def __iter__(self):
        return Track.IterableTrack(self)

    def hash(self):
        bin = pack(
            "fffff",
            self.mHead.latlon()[0],
            self.mHead.latlon()[1],
            self.mTail.latlon()[0],
            self.mTail.latlon()[1],
            self.mHead.mTimestampSec or self.distanceKm()
        )
        hashString = base64.b64encode(bin).replace("/","_")
        return hashString

    def append(self, milestone):
        if not isinstance(milestone, Track.Milestone):
            if isinstance(milestone, list) and len(milestone) > 0:
                if isinstance(milestone[0], list):
                    for elem in milestone:
                        self.append(elem)
                    return
                milestone = tuple(milestone)
            assert isinstance(milestone, tuple)
            milestone = Track.Milestone(latlon=milestone)
        self.mRegion.add(milestone.latlon())
        if not self.mHead:
            self.mHead = milestone
            if milestone.mTimestampSec:
                self.mTripTime = 0
        else:
            self.mTail.append(milestone)
            self.mDistanceKm += milestone.mDistanceKmFromPrevious
            if milestone.mTimeSecFromPrevious:
                self.mTripTime += milestone.mTimeSecFromPrevious
        milestone.mParent = self
        self.mTail        = milestone

    def region(self):
        return self.mRegion

    def show(self):
        self.mParent.osmReferenceMap.showRegion(self.region())
        print "Track distance: %smts " % (self.distanceKm()*1000)

    def hasSpeed(self):
        return self.mTail.mTimestampSec is not None

    def maxSpeedKph(self, throwAwayPercent=5):
        return self.mTail.maxSpeedKph(self.mHead, throwAwayPercent)

    def avgSpeedKph(self, throwAwayStationary=True, throwAwayPercent=5):
        # check this one from planet - make sure you show good error info 000343216.gpx
        return self.mTail.avgSpeedKph(self.mHead, throwAwayStationary, throwAwayPercent)

    def distanceKm(self):
        return self.mDistanceKm

    def draw(self, cr):
        if self.mHead != self.mTail != None:
            pathTool = pyCairoDrawToolkit.TripPathToolWithSpeed(cr)
            textTool = pyCairoDrawToolkit.TextTool(cr)
            maxSpeed = None
            if self.hasSpeed():
                maxSpeed = self.maxSpeedKph()
            speedModifierPercent = 50
            _milestone = None
            for milestone in self:
                pix = self.mParent.referenceMap().latlon2pix(milestone.latlon(), True)
                if maxSpeed:
                    if milestone.mSpeedKphFromPrevious == 0:
                        pathTool.markStationary(pix)
                        continue
                    if _milestone:
                        _milestone.merge(milestone)
                    else:
                        _milestone = milestone.copy()
                    if _milestone.mSpeedKphFromPrevious:
                        speedModifierPercent = (100*(_milestone.mSpeedKphFromPrevious)/(maxSpeed))
                else:
                    _milestone = milestone.copy()
                if pathTool.draw(pix, speedModifierPercent):
                    _milestone = None
                if milestone.mText and self.mParent.referenceMap().zoom >= 17:
                    lines = milestone.mText.split("\n")
                    for idx, line in enumerate(lines):
                        textTool.draw((pix[0], pix[1]+(idx*13)), line, 13)


    def invalidate(self, calledBy):
        pass

    def milestones(self):
        return self.mHead

    def len(self):
        return self.mHead.milestonesTo()



    class IterableTrack:
        def __init__(self, track):
            self.mHead  = track.mHead


        def next(self):
            if not self.mHead:
                raise StopIteration
            result = self.mHead
            self.mHead = self.mHead.mNextMilestone
            return result


    class Milestone(pyGeometry.GeoPoint):
        def __init__(self, latlon, timestampSec=None, text = None):
            super(Track.Milestone, self).__init__(latlon)
            self.mTimestampSec              = timestampSec
            self.mDistanceKmFromPrevious    = None
            self.mTimeSecFromPrevious       = None
            self.mSpeedKphFromPrevious      = None
            self.mAccellerationFromPrevious = None
            self.mNextMilestone             = None
            self.mParent                    = None
            self.mText                      = text
            if self.mText:
                self.mText = str(self.mText)

        def append(self, milestone):
            assert(milestone)
            milestone.mDistanceKmFromPrevious = self.distanceKms(milestone)
            self.mNextMilestone = milestone
            if self.mTimestampSec and milestone.mTimestampSec:
                assert(self.mTimestampSec<=milestone.mTimestampSec) #must be given in sequence
                milestone.mTimeSecFromPrevious = milestone.mTimestampSec-self.mTimestampSec
                if milestone.mTimeSecFromPrevious > 0:
                    milestone.mSpeedKphFromPrevious = 1.0*milestone.mDistanceKmFromPrevious/(milestone.mTimeSecFromPrevious/3600)
                    if self.mSpeedKphFromPrevious:
                        #speed, as calculated above is, by definition, an average speed maintained through the relevant segment.
                        #Assuming then that the subject travels enters the current segment with that average speed (calculated for the
                        #previous segment, as mSpeedKphFromPrevious) the subject, in order to reach the average speed in the current segment
                        #would need reach that average speed in half the time (hence dTime/2).
                        #Note that this doesn't take the direction of movement into account.
                        milestone.mAccellerationFromPrevious = (milestone.mSpeedKphFromPrevious-self.mSpeedKphFromPrevious)/(milestone.mTimeSecFromPrevious/2)
                    else:
                        milestone.mAccellerationFromPrevious = 0 #no data, assume no acceleration
                else:
                    milestone.mSpeedKphFromPrevious = 0.0

        def merge(self, milestone):
            self.set(milestone.latlon())
            self.mTimestampSec = milestone.mTimestampSec
            #now, self.mSpeedKphFromPrevious and self.mDistanceKmFromPrevious may be None, if self is the first milestone only
            #speedFromPrevious may be None for followup milestones if time is not registered (say for routing rather than recording).
            #by defintion, the next milestone can't be first.
            assert(milestone.mDistanceKmFromPrevious or (milestone.mSpeedKphFromPrevious != None or milestone.mTimestampSec == None))
            if milestone.mDistanceKmFromPrevious:
                assert(milestone.mSpeedKphFromPrevious != None)
                #weigh the average
                self.mSpeedKphFromPrevious = ((self.mSpeedKphFromPrevious*self.mDistanceKmFromPrevious)+\
                                        (milestone.mSpeedKphFromPrevious*milestone.mDistanceKmFromPrevious))/\
                                        (milestone.mDistanceKmFromPrevious+self.mDistanceKmFromPrevious)
                self.mDistanceKmFromPrevious += milestone.mDistanceKmFromPrevious
            else:
                self.mSpeedKphFromPrevious = milestone.mSpeedKphFromPrevious
                self.mDistanceKmFromPrevious = milestone.mDistanceKmFromPrevious

        def speedKph(self):
            return self.mSpeedKphFromPrevious

        def maxSpeedKph(self, fromMilestone=None, throwAwayPercent=5):
            milestones = []
            if not fromMilestone:
                fromMilestone = self.mParent.mHead
            current = fromMilestone
            while True:
                if not current.mTimestampSec:
                    raise Track.NoSpeedInformation
                milestones.append(current)
                if current == self:
                    break
                current = current.next()
                if not current:
                    raise Exception("fromMilestone not before this")

            milestones.sort(cmp=lambda a,b: cmp(a.mSpeedKphFromPrevious,b.mSpeedKphFromPrevious), reverse=True)
            throwAwayNumber = int((throwAwayPercent/100.0)*len(milestones))
            return milestones[throwAwayNumber].mSpeedKphFromPrevious

        def avgSpeedKph(self, fromMilestone=None, throwAwayStationary=True, throwAwayPercent=5):
            if not fromMilestone:
                fromMilestone = self.mParent.mHead
            speeds              = []
            avgAdjustedDistance = 0.0
            maxSpeed = self.maxSpeedKph(fromMilestone, throwAwayPercent)
            current  = fromMilestone
            while True:
                if not (
                    current.mSpeedKphFromPrevious == None or
                    current.mSpeedKphFromPrevious > maxSpeed or
                    (throwAwayStationary and current.mSpeedKphFromPrevious == 0)):

                    speeds.append((current.mSpeedKphFromPrevious, current.mDistanceKmFromPrevious))
                    avgAdjustedDistance += current.mDistanceKmFromPrevious
                if current == self:
                    break
                current = current.next()
                if not current:
                    raise Exception("fromMilestone not before this")

            avgSpeed = 0
            for speed in speeds:
                avgSpeed += speed[0]*speed[1]/avgAdjustedDistance
            return avgSpeed

        def next(self, minDistanceMts=0):
            prev   = self
            result = self.mNextMilestone
            while minDistanceMts > 0 and result and result.mNextMilestone:
                minDistanceMts -= prev.distanceMts(result)
                prev   = result
                result = result.mNextMilestone
            return result

        def milestonesTo(self, toMilestone=None):
            res = 0
            currentMilestone = self
            while currentMilestone != toMilestone:
                res += 1
                currentMilestone = currentMilestone.next()
            return res

        def milestonesFrom(self, fromMilestone=None):
            if not fromMilestone:
                fromMilestone = self.mParent.mHead
            return fromMilestone.milestonesTo(self)

        def distanceKmRemaning(self):
            res = 0
            currentMilestone = self
            while currentMilestone.next():
                currentMilestone = currentMilestone.next()
                res += currentMilestone.mDistanceKmFromPrevious
            return res

        def distanceKmSoFar(self):
            return self.mParent.distanceKm()-self.distanceKmRemaning()

        def copy(self):
            milestone = Track.Milestone(self.latlon(), self.mTimestampSec)
            milestone.mDistanceKmFromPrevious    = self.mDistanceKmFromPrevious
            milestone.mTimeSecFromPrevious       = self.mTimeSecFromPrevious
            milestone.mSpeedKphFromPrevious      = self.mSpeedKphFromPrevious
            milestone.mAccellerationFromPrevious = self.mAccellerationFromPrevious
            return milestone




class GraphNode:

    def __init__(self, waynode, fromNode, viaEdge, trackPoint):
        self.mWaynode              = waynode
        self.mTrackPoint           = trackPoint
        self.mAvgDistanceFromEdge  = None
        self.setFrom(fromNode, viaEdge)

    def __hash__(self):
        return self.mWaynode.id()

    def setFrom(self, fromNode, viaEdge):
        assert bool(fromNode) == bool(viaEdge)
        self.mFromNode      = fromNode
        self.mViaEdge       = viaEdge

    def stats(self):
        current             = self
        cumLengthMts        = 0
        cost                = 0
        while current.mFromNode:
            distanceMts     = current.mViaEdge.distanceMts()
            cost           += (current.mAvgDistanceFromEdge*distanceMts)
            cumLengthMts   += distanceMts
            current         = current.mFromNode
        return (cost, cumLengthMts)

    def distanceMts(self):
        return self.stats()[1]

    def cost(self):
        return self.stats()[0]

    def id(self):
        return self.mWaynode.id()

    def neighbours(self):
        return [GraphNode(edge.toNode(), self, edge, None) for edge in self.mWaynode.edges()]

    def draw(self, cr, lineConfig, drawingTools):
        current = self
        while current.mViaEdge:
            if cr:
                roadDrawingTool = pyCairoDrawToolkit.RoadTool(
                    cr, lineConfig, lambda node: drawingTools.referenceMap().latlon2pix(node.latlon()))
                roadDrawingTool.draw(current.mViaEdge.nodes())
            current = current.mFromNode
        return current

    def insert2DB(self, cursor):
        current = self
        while current.mViaEdge:
            avgSpeedKph = str(int(current.mTrackPoint.avgSpeedKph(current.mFromNode.mTrackPoint)))
            wayid       = str(current.mViaEdge.wayid())
            keynode     = str(current.mViaEdge.mWaySection.nodes()[0].id())
            reversed    = "FALSE"
            if current.mViaEdge.mReversed:
                reversed = "TRUE"
            where       = "WHERE WaySectionHits.wayid='"+wayid+"' AND WaySectionHits.keynode='"+keynode+"' AND WaySectionHits.reversed='"+reversed+"'"
            sql         = ("SELECT COUNT(*) FROM WaySectionHits "+where)
            cursor.execute(sql)
            exists      = int(cursor.fetchone()[0])
            try:
                if exists:
                    sql = ("UPDATE WaySectionHits SET avgSpeedKph = ((avgSpeedKph*hits)+"+avgSpeedKph+")/(hits+1), hits = hits + 1 "+where)
                    res = cursor.execute(sql)
                else:
                    sql = ("INSERT INTO WaySectionHits VALUES ("+wayid+", "+keynode+", "+reversed+", 1, "+avgSpeedKph+")")
                    cursor.execute(sql)
            except psycopg2.IntegrityError as e:
                assert not current.mFromNode.mFromNode, "InegrtityError only expected for first (possibly cut) edge, whereas: "+e.message
            current = current.mFromNode


    def hasAncestor(self, ancestor):
        current = self
        while current:
            if current.mFromNode == ancestor:
                return True
            current = current.mFromNode
        return False



class ThreadedTrackAnalyser(threading.Thread):

    KMaxDiversionMts      = 30
    KMinTrackAdvanceMts   = 5
    KMinSectionLengthMts  = 100
    KAnimationBreakSec    = 0.01
    KShowProgressEverySec = 0.5

    def __init__(self, track, parent):
        super(ThreadedTrackAnalyser, self).__init__()
        self.mTrack               = track
        self.mParent              = parent
        self.mCurrentSection      = None
        self.mSections            = []
        self.mCurrentTrackPoint   = None
        self.mTrackLen            = None
        self.mHasDrawingConfig    = False
        self.mFinisgedCb          = None
        self.mLastProgressTimeSec = 0
        if getattr(parent, "config", None):
            self.mHasDrawingConfig = True
        self.mDebug    = pyUtils.Debug()
        if getattr(parent, "debug", None):
            self.mDebug = parent.debug()

    def drawingTools(self):
        if self.mHasDrawingConfig:
            return self.mParent
        return None

    def start(self, finishedCb=None):
        self.mFinisgedCb = finishedCb
        super(ThreadedTrackAnalyser, self).start()

    def run(self):
        self.foreground()

    def foreground(self):
        self.mCurrentTrackPoint = self.mTrack.mHead
        self.mTrackLen          = self.mTrack.len()
        currentGraphPoint       = None
        skipInitial             = 0
        while self.mCurrentTrackPoint:
            self.invalidate()
            if skipInitial:
                skipInitial -= 1
                self.mCurrentTrackPoint = self.mCurrentTrackPoint.next(
                    minDistanceMts=ThreadedTrackAnalyser.KMinTrackAdvanceMts)
                continue

            currentGraphPoint = self.mParent.mWayNodesBrowser.getNearest(
                self.mCurrentTrackPoint, ThreadedTrackAnalyser.KMaxDiversionMts)
            if currentGraphPoint:
                self.mCurrentSection = SectionForGraphPoint(self, self.mCurrentTrackPoint, currentGraphPoint)
                if self.mCurrentSection.do() > ThreadedTrackAnalyser.KMinSectionLengthMts:
                    self.mSections.append(self.mCurrentSection)
                    self.mCurrentTrackPoint = self.mCurrentSection.currentNode().mTrackPoint
                self.mCurrentSection = None

            self.mCurrentTrackPoint = self.mCurrentTrackPoint.next(
                minDistanceMts=ThreadedTrackAnalyser.KMinTrackAdvanceMts)
        self.invalidate()
        self.mDebug.log("")
        self.insert2DB()
        if self.mFinisgedCb:
            self.mFinisgedCb()


    def inserted2DB(self, dbcursor=None):
        closeCursor = False
        if not dbcursor:
            self.db = pyBaseOsmDB.OSMGisDatabase()
            dbcursor = self.db.conn.cursor()
            closeCursor = True
        hsh = self.mTrack.hash()
        trackMergedSql = ("SELECT COUNT(*) FROM MergedTracks WHERE MergedTracks.trackHash=\'"+hsh+"\'")
        dbcursor.execute(trackMergedSql)
        trackMerged = int(dbcursor.fetchone()[0])
        if closeCursor:
            dbcursor.close()
        return bool(trackMerged)

    def insert2DB(self):
        hsh = self.mTrack.hash()
        self.db = pyBaseOsmDB.OSMGisDatabase()
        dbcursor = self.db.conn.cursor()
        if self.inserted2DB(dbcursor):
            self.mDebug.log("Already have track with hash: "+hsh+" not db, not re-inserting")
            return
        try:
            for section in self.mSections:
                section.insert2DB(dbcursor)
            trackMergedSql = ("INSERT INTO MergedTracks VALUES (\'"+hsh+"\')")
            dbcursor.execute(trackMergedSql)
        except:
            self.mDebug.exception("Unable to insert track with hash "+hsh)
        finally:
            dbcursor.close()


    def invalidate(self, sleepSec=KAnimationBreakSec):
        if self.mHasDrawingConfig:
            gobject.idle_add(self.mParent.invalidate)
            time.sleep(sleepSec)
        else:
            self.showProgress()

    def draw(self, cr):
        self.showProgress(cr)

    def showProgress(self, cr=None):
        progressMessage = {
            "message"         : "",
            "length"          : self.mDebug.lineWidth()-4,
            "fromTrackPoint"  : self.mTrack.mHead,
            "trackLen"        : self.mTrackLen
        }

        for section in self.mSections:
            section.showProgress(progressMessage, cr)

        if self.mCurrentSection:
            self.mCurrentSection.showProgress(progressMessage, cr)

        if cr and self.mCurrentTrackPoint:
            trackPointDrawingTool = pyCairoDrawToolkit.NodeTool(
                cr, lambda point: self.mParent.referenceMap().latlon2pix(point.latlon()))
            trackPointDrawingTool.draw(self.mCurrentTrackPoint)

        nowSec = time.time()
        dTime  = nowSec - self.mLastProgressTimeSec
        if dTime > ThreadedTrackAnalyser.KShowProgressEverySec:
            paddingLen = progressMessage["length"]-len(progressMessage["message"])
            if paddingLen > 0:
                progressMessage["message"] += ' '*paddingLen
            self.mDebug.logUpdated("["+progressMessage["message"]+"]")
            self.mLastProgressTimeSec = nowSec



class SectionForGraphPoint(object):

    KAnimationBreakSec  = 0.01

    def __init__(self, parent, trackPoint, waynode):
        self.mParent       = parent
        self.mCurrentNode  = None
        self.mOPEN         = pyPriorityQ.PriorityQ()
        self.mOPEN.push(0,waynode.id(),GraphNode(
            waynode=waynode,
            fromNode=None,
            viaEdge=None,
            trackPoint=trackPoint))

        self.mDrawingTools        = parent.drawingTools()
        self.mProjeDrawConfigBad  = None
        self.mProjeDrawConfigGood = None
        self.mEdgeDrawConfigTry   = None
        self.mEdgeDrawConfigBad   = None
        self.mSectionDrawConfig   = None
        self.mDebugSpritesToDraw  = None
        if self.mDrawingTools:
            self.mProjeDrawConfigBad  = pyConfig.LineStyle(self.mDrawingTools.config().colour("Red"),2)
            self.mProjeDrawConfigGood = pyConfig.LineStyle(self.mDrawingTools.config().colour("Green"),2)
            self.mEdgeDrawConfigTry   = pyConfig.LineStyle(self.mDrawingTools.config().colour("Orange"),5)
            self.mEdgeDrawConfigBad   = pyConfig.LineStyle(self.mDrawingTools.config().colour("Red"),5)
            self.mSectionDrawConfig   = pyConfig.LineStyle(self.mDrawingTools.config().colour("Green"),5)
            self.mDebugSpritesToDraw  = []

    def showProgress(self, progressMessage, cr):
        if self.mCurrentNode:
            fromNode           = self.mCurrentNode.draw(cr, self.mSectionDrawConfig, self.mDrawingTools)
            skippedTrackPoints = fromNode.mTrackPoint.milestonesFrom(progressMessage["fromTrackPoint"])
            coveredTrackPoints = self.mCurrentNode.mTrackPoint.milestonesFrom(fromNode.mTrackPoint)

            messageLen         = progressMessage["length"]
            trackLen           = progressMessage["trackLen"]

            points             = (coveredTrackPoints+skippedTrackPoints)*messageLen
            points             = points/trackLen+(1 if points%trackLen != 0 else 0)
            skippedTrackPoints = skippedTrackPoints*messageLen/trackLen
            coveredTrackPoints = max(points - skippedTrackPoints - 1, 0)
            if points > 1:
                progressMessage["message"] += (skippedTrackPoints*' ')+'|'+(coveredTrackPoints*'.')
                progressMessage["fromTrackPoint"] = self.mCurrentNode.mTrackPoint
        if cr:
            self.drawSprites(cr, self.mDebugSpritesToDraw)

    def insert2DB(self, dbCursor):
        if self.mCurrentNode:
            self.mCurrentNode.insert2DB(dbCursor)

    def drawSprites(self, cr, sprites):
        for drawConfig, sprite in sprites:
            if isinstance(sprite, pyGeometry.GeoLine):
                lineDrawingTool = pyCairoDrawToolkit.RoadTool(
                    cr, drawConfig, lambda node: self.mDrawingTools.referenceMap().latlon2pix(node.latlon()))
                lineDrawingTool.draw([sprite.start(), sprite.end()])
            elif isinstance(sprite, pyGeometry.GeoPoint):
                pointDrawingTool = pyCairoDrawToolkit.NodeTool(
                    cr, lambda point: self.mDrawingTools.referenceMap().latlon2pix(point.latlon()))
                pointDrawingTool.draw(sprite)
            elif isinstance(sprite, list):
                self.drawSprites(cr, map(lambda subsprite: (drawConfig, subsprite), sprite))
            else:
                assert False, "Could not render sprite: "+str(sprite)

    def do(self):
        CLOSED  = {}
        while self.mOPEN.size():

            self.mCurrentNode = self.mOPEN.pop()
            CLOSED[self.mCurrentNode.id()] = self.mCurrentNode
            for neighbour in self.mCurrentNode.neighbours():

                trackPoint, avgDistanceMts = self.alignEdge(self.mCurrentNode.mTrackPoint, neighbour.mViaEdge)
                if trackPoint:
                    neighbour.mAvgDistanceFromEdge = avgDistanceMts
                    neighbour.mTrackPoint          = trackPoint

                    existingClosed = CLOSED.get(neighbour.id())
                    if existingClosed:
                        if not self.mCurrentNode.hasAncestor(existingClosed) and existingClosed.cost() > neighbour.cost():
                            existingClosed.setFrom(self.mCurrentNode, neighbour.mViaEdge)
                            existingClosed.mAvgDistanceFromEdge = avgDistanceMts
                            existingClosed.mTrackPoint          = trackPoint
                    else:
                        existingOpen = self.mOPEN.get(neighbour.id())
                        if not existingOpen or existingOpen.cost() > neighbour.cost():
                            self.mOPEN.push(neighbour.distanceMts(), neighbour.id(), neighbour)

        self.invalidate()
        return self.mCurrentNode.distanceMts()

    def currentNode(self):
        return self.mCurrentNode


    def alignEdge(self, trackPoint, edge):
        edgeLines       = []
        prevNode        = None
        inspectNBeyond  = 3
        remainingBeyond  = inspectNBeyond
        for node in edge.nodes():
            if prevNode:
                edgeLines.append(
                    pyGeometry.GeoLine(prevNode, node))
            prevNode = node

        tryTrackPoint      = trackPoint
        distancesMts       = []
        minDistance2EndMts = ThreadedTrackAnalyser.KMaxDiversionMts
        while remainingBeyond and tryTrackPoint:
            self.invalidate([(self.mEdgeDrawConfigTry, edgeLines), (None, tryTrackPoint)])
            distanceFromEdgeMts = self.__distance2Edge(edgeLines, tryTrackPoint, ThreadedTrackAnalyser.KMaxDiversionMts)
            if distanceFromEdgeMts != None:
                distancesMts.append((tryTrackPoint, distanceFromEdgeMts))
                remainingBeyond  = inspectNBeyond
                distance2End    = edge.toNode().distanceMts(tryTrackPoint)
                if distance2End < minDistance2EndMts:
                    minDistance2EndMts = distance2End
                    trackPoint         = tryTrackPoint
            else:
                remainingBeyond     -= 1
            tryTrackPoint = tryTrackPoint.next()


        avgDistance = None
        if (remainingBeyond == 0 or not tryTrackPoint) and minDistance2EndMts < ThreadedTrackAnalyser.KMaxDiversionMts:
            cumDistance  = 0
            measurements = 0
            for tryTrackPoint, distanceMts in distancesMts:
                cumDistance  += distanceMts
                measurements += 1
                if tryTrackPoint == trackPoint:
                    break
            if measurements:
                avgDistance = 1.0*cumDistance/measurements
        if avgDistance != None:
            return (trackPoint, avgDistance)
        self.invalidate([(self.mEdgeDrawConfigBad, edgeLines)])
        return (None, None)


    def invalidate(self, sprites2Draw=None, add=False, sleepSec=KAnimationBreakSec):
        if not sprites2Draw:
            sprites2Draw = []
        if add:
            self.mDebugSpritesToDraw += sprites2Draw
        else:
            self.mDebugSpritesToDraw  = sprites2Draw
        self.mParent.invalidate(sleepSec)


    def __distance2Edge(self, edgeLines, point, minDistanceMts):
        closestNode  = None
        prevEdgeLine = None
        for edgeLine in edgeLines:
            distance2Start = edgeLine.start().distanceMts(point)

            if not closestNode or distance2Start < closestNode[2]:
                closestNode = (prevEdgeLine, edgeLine, distance2Start)
            prevEdgeLine = edgeLine

        distance2End = closestNode[1].end().distanceMts(point)
        if distance2End < closestNode[2]:
            closestNode = (closestNode[1], None, distance2End)

        closestEdgeLine = None
        if not closestNode[1]:
            #we're towards the end of the edge
            closestEdgeLine = closestNode[0]
        elif not closestNode[0]:
            #we're towards the begining of the edge
            closestEdgeLine = closestNode[1]
        else:
            #point (p) is closest to a node (y) joining two edge lines
            #[(x..y),(y..z)]. In order to find the closer egde, we need
            #to calculate the angle between two vectors:
            #(1) y->p
            #(2) y->z
            #and see if it's a sharp angle, (p) is closer to z and therefore
            #(y..z) is the closest node, otherwise (x..y) is the closest
            yp = pyGeometry.Vector((closestNode[1].start(), point))
            yz = pyGeometry.Vector((closestNode[1].start(), closestNode[1].end()))
            angle = yp.angleRad(yz)
            if ( angle < math.pi/2 or angle > math.pi*3/2):
                #sharp angle
                closestEdgeLine = closestNode[1]
            else:
                closestEdgeLine = closestNode[0]

        pointOnLine = closestEdgeLine.projection(point)
        if not pointOnLine:
            if point.distanceMts(closestEdgeLine.start()) < point.distanceMts(closestEdgeLine.end()):
                pointOnLine = closestEdgeLine.start()
            else:
                pointOnLine = closestEdgeLine.end()

        distanceLine = pyGeometry.GeoLine(pointOnLine, point)
        distanceMts  = distanceLine.lengthMts()
        if distanceMts < minDistanceMts:
            self.invalidate(
                [(self.mProjeDrawConfigGood, distanceLine)],
                add=True)
            return distanceMts
        return None




