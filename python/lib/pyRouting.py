# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# (C) 2012 Remek Zajac
#!/usr/bin/python

import math
import threading
import gobject

#local modules
import pyWayView
import pyGeometry
import pyUtils
import pyPriorityQ

from osmsql import pyBrowse as pyBrowseOsmDB


class Node:
    def __init__(self, waynode, routeConfig, fromNode = None, viaEdge = None):
        self.mRouteConfig = routeConfig
        self.mG           = None
        self.mWaynode     = waynode
        self.mFromNode    = fromNode
        self.mViaEdge     = viaEdge
        self.mToNode      = None
        if not fromNode:
            self.mG       = 0
        else:
            self.mG = self.mRouteConfig.cost(self.mViaEdge)+self.mFromNode.G()

    def __hash__(self):
        return self.mWaynode.id()

    def __cost(self, fromNode):
        return fromNode.mWaynode.edgeFor(self.mWaynode, self.mRouteConfig.cost)

    def cost(self, fromNode):
        return self.__cost(fromNode)[0]

    def G(self):
        return self.mG

    def setFrom(self, fromNode):
        self.mFromNode = fromNode
        self.mG, self.mViaEdge = self.__cost(self.mFromNode)
        self.mG += self.mFromNode.G()

    def distanceFromMts(self, fromNode):
        return fromNode.distanceMts(self.mWaynode)

    def H(self, destination):
        #manhattan heuristic
        return self.distanceFromMts(destination)

    def F(self, destination):
        return self.G() + self.H(destination)

    def id(self):
        return self.mWaynode.id()

    def neighbours(self):
        return [Node(edge.toNode(), self.mRouteConfig, self, edge) for edge in self.mWaynode.edges()]

    def hasAncestor(self, node):
        if self.mFromNode:
            if node.id() == self.mFromNode.id():
                return True
            #else:
                #return self.mFromNode.hasAncestor()
        return False


class ThreadedRouteBuilder(threading.Thread):
    def __init__(self, parent, timer, originWayNode, destinationWayNode, routeConfig):
        super(ThreadedRouteBuilder, self).__init__()
        self.parent = parent
        self.debug = parent.debug
        self.current = None
        self.originNode = Node(originWayNode, routeConfig)
        self.destinationWayNode = destinationWayNode
        self.timer = timer

    def run(self):
        OPEN = pyPriorityQ.PriorityQ()
        OPEN.push(self.originNode.F(self.destinationWayNode), self.originNode.id(), self.originNode)
        CLOSED = {}
        iterCount = 0
        dstJunction1, dstJunction2 = self.destinationWayNode.nearestJunctions()
        success = False
        while OPEN.size():
            self.current = OPEN.pop()
            if self.current.id() == dstJunction1.id() or self.current.id() == dstJunction2.id():
                #successfully navigated to a graph vertex nearest the destination
                success = True
                break
            iterCount += 1
            if iterCount%1000==0:
                self.debug.logUpdated("routing, iteration:"+str(iterCount))
            CLOSED[self.current.id()] = self.current

            for neighbour in self.current.neighbours():
                #if self.current.hasAncestor(neighbour):
                #    continue
                if neighbour.id() in CLOSED:
                    continue

                existingNeighbour = OPEN.get(neighbour.id())
                if not existingNeighbour or existingNeighbour.G() > neighbour.G():
                    OPEN.push(neighbour.F(self.destinationWayNode), neighbour.id(), neighbour)

        gobject.idle_add(self.parent.doneRouting, success)



class Route(pyWayView.WayView):
    def __init__(self, wayNodesBrowser, refMap):
        super(Route, self).__init__(wayNodesBrowser, refMap)
        self.origin = None
        self.destination = None
        self.routeBuilder = None
        self.mainTimer = None
        self.debug = pyUtils.Debug(pyUtils.Debug.normal)
        self.listeners = []

    def setOrigin(self, origin):
        self.origin = origin
        if self.isReadyToRoute():
            self.__emit("ready_to_route", True)

    def setDestination(self, destination):
        self.destination = destination
        if self.isReadyToRoute():
            self.__emit("ready_to_route", True)

    def setOriginAndDestination(self, origin, destination):
        self.origin = origin
        self.destination = destination
        if self.isReadyToRoute():
            self.__emit("ready_to_route", True)

    def isReadyToRoute(self):
        return bool(self.destination and self.origin and not self.isBusy())

    def connect(self, signalName, callback):
        self.listeners.append((signalName, callback))

    def __emit(self, signal, *args):
        if hasattr(self, "listeners"):
            for listener in self.listeners:
                if listener[0] == signal:
                    listener[1](*args)

    def clear(self):
        super(Route, self).clear()
        self.destination = None
        self.origin = None
        self.__emit("ready_to_route", False)

    def do(self, routeConfigs, callback):
        self.callback = callback
        super(Route, self).clear()
        if self.callback:
            self.callback(self) #clear the map
        self.mRouteConfigs = routeConfigs
        self.mainTimer = pyUtils.DebugTimeReporter("Routing", self.debug)
        self.mainTimer.start()
        nearestNodes = pyUtils.DebugTimeReporter("Finding Origin and Destination", self.mainTimer)
        nearestNodes.start()
        originWayNode = self.mWayNodesBrowser.getNearest(self.origin)
        destinationWayNode = self.mWayNodesBrowser.getNearest(self.destination)
        nearestNodes.stop()
        self.debug.log(
            "Found origin "+originWayNode.toString()+" and destination  "+destinationWayNode.toString()+"\nin "+str(round(nearestNodes.timeSpent(),3))+"s"
        )
        assert(originWayNode and destinationWayNode) #this will need to be more robust
        self.routeBuilder = ThreadedRouteBuilder(self, self.mainTimer, originWayNode, destinationWayNode, self.mRouteConfigs.getConfig())
        self.__emit("ready_to_route", False)
        self.routeBuilder.start()

    def routeAnalysis(self, current):
        fileName = pyUtils.KLogFolder+"/route.anal.log"
        debug = pyUtils.Debug(pyUtils.Debug.none, fileName)

        analysisColumnWidth = 20
        analysisHeadingStr = "Section: Attributes:         "
        for name in self.mRouteConfigs.configs():
            analysisHeadingStr += name.ljust(analysisColumnWidth)

        debug.log(analysisHeadingStr)
        sectionIdx      = 0
        accumulatedCost = {}
        accumulatedDist = {}
        #and now dump the route origin->dst
        while current.mToNode:
            edge          = current.mToNode.mViaEdge
            allAttributes = {}
            profiles      = []
            for name, profile in self.mRouteConfigs.configs().iteritems():
                costBucket = profile.costBucket(edge)
                cost     = int(costBucket.cost())
                if not accumulatedCost.has_key(name):
                    accumulatedCost[name] = 0
                accumulatedCost[name] += cost
                distance = costBucket.distanceMts()
                if not accumulatedDist.has_key(name):
                    accumulatedDist[name] = 0
                accumulatedDist[name] += distance
                allAttributes["distanceMts"] = str(distance)+"/"+str(accumulatedDist[name])
                allAttributes["cost"]     = ""
                attributes = {"cost":str(cost)+"/"+str(accumulatedCost[name])}
                for factorName, factorValue in costBucket.factors().iteritems():
                    attributes[factorName]      = str(factorValue[1])
                    allAttributes[factorName]   = factorValue[0]
                profiles.append(attributes)

            analysisStr =  "along way id: "+str(edge.wayid())+"\n".ljust(len("Section: ")+1)
            analysisStr += "from node id: "+str(current.mWaynode.id())+"\n".ljust(len("Section: ")+1)
            analysisStr += "to   node id: "+str(current.mToNode.id())+"\n".ljust(len("Section: ")+1)
            for attributeName, attributeValue in allAttributes.iteritems():
                analysisRowStr = ""
                analysisRowStr += attributeName+":"+attributeValue
                analysisRowStr = analysisRowStr.ljust(analysisColumnWidth)
                for idx, profile in enumerate(profiles):
                    if profile.has_key(attributeName):
                        analysisRowStr += profile[attributeName].ljust(analysisColumnWidth)
                analysisRowStr += "\n".ljust(len("Section: ")+1)
                analysisStr += analysisRowStr

            sectionIdxStr = "["+str(sectionIdx)+"]"
            debug.log(sectionIdxStr.ljust(len("Section: "))+analysisStr)
            sectionIdx += 1
            current = current.mToNode
        self.debug.log("Written route analysis to file: "+fileName)


    def doneRouting(self, success):
        self.routeBuilder.join()
        self.mainTimer.stop()
        if success:
            self.debug.log("Routing complete, rendering...")
            self.mainTimer.report()

            #don't be tempted by recursion!
            #We have the route going dst->origin navigable via fromNodes
            #Establish bi-directional relation now (was previosly impossible as nodes get setFrom called speculativel
            current = self.routeBuilder.current
            while current.mFromNode:
                current.mFromNode.mToNode = current
                current = current.mFromNode
            first = current

            #and now dump the route origin->dst
            sections = []
            while current.mToNode:
                edge          = current.mToNode.mViaEdge
                sections.append(edge)
                current = current.mToNode

            if self.routeBuilder.current.mWaynode != self.routeBuilder.destinationWayNode:
                #and the last leg to the destination if current vertex isn't destination itself, but the nearest
                #junction to it. .destinationWayNode.edgeFor(current) should return an edge going
                #from destinationWayNode towards current. We need the reverse.
                lastMile = self.routeBuilder.destinationWayNode.edgeFor(self.routeBuilder.current, lambda x: 0)[1].reversed()
                sections.append(lastMile)

            self.showSections(sections, "route")
            self.debug.log("Route distance:"+('%0.2f'%(self.distanceMts()/1000.00))+"km")

            self.routeAnalysis(first)
        else:
            self.debug.log("Routing complete, unable to find route")

        self.routeBuilder = None
        if self.callback:
            self.callback(self)
        self.__emit("ready_to_route", True)

    def isBusy(self):
        return bool(self.routeBuilder)



