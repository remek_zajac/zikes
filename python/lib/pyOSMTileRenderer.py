# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# (C) 2012 Remek Zajac
#!/usr/bin/python

import cairo
import threading
import gobject
import math

#local modules
import traceback

import time

import pyGpxOSMTiles
import pyCairoDrawToolkit


class Request(threading.Thread):
    def __init__(self, browser):
        super(Request, self).__init__()
        self.mCallbacks = []
        self.mRegion = None
        self.mClosed = False
        self.mBrowser = browser

    def add(self, callback, region=None):
        assert not self.mClosed
        if region:
            if self.mRegion is None:
                self.mRegion = region
            else:
                self.mRegion.add(region)
        self.mCallbacks.append(callback)
        return self

    def start(self):
        print ">>Looking sections in region %s " % str(self.mRegion)
        super(Request, self).start()
        self.mClosed = True

    def run(self):
        waySections = self.mBrowser.getWaySectionsInRegion(self.mRegion)
        print "<<Got %d sections in region %s " % (len(waySections), str(self.mRegion))
        for callback in self.mCallbacks:
            gobject.idle_add(callback, waySections)

class SectionFetcher(object):

    KMinWaitMs = 300

    def __init__(self, browser):
        self.mRequest = None
        self.mLastRequest = time.clock()
        self.mBrowser = browser

    def get(self, region, callback):
        if not self.mRequest:
            self.mRequest = Request(self.mBrowser)
        self.mRequest.add(callback, region)
        now = time.clock()
        if (now - self.mLastRequest) > SectionFetcher.KMinWaitMs:
            self._doGet()
        else:
            gobject.timeout_add(SectionFetcher.KMinWaitMs, lambda: self._doGet())

    def _doGet(self):
        if self.mRequest:
            self.mRequest.start()
            self.mRequest = None
            self.mLastRequest = time.clock()



class OSMTileRendererThread(threading.Thread):
    KLeftToRender               = 0
    KMaxPixDistanceForElevation = 30
    KMaxZoomForText             = 17

    def __init__(self, config, sectionFetcher, tile, callback=None):
        super(OSMTileRendererThread, self).__init__()
        self.tile = tile
        self.callback = callback
        self.tile.createCacheFolder()
        self.config = config
        self.sectionFetcher = sectionFetcher


    def start(self):
        OSMTileRendererThread.KLeftToRender+=1
        print ">>Start rendering tile %s, left to render: %d " % \
              (self.tile.getLocalFileName(), OSMTileRendererThread.KLeftToRender)
        self.sectionFetcher.get(
            self.tile.region(),
            lambda waySections: self.doStart(waySections)
        )

    def doStart(self, waySections):
        self.mWaySections = waySections
        super(OSMTileRendererThread, self).start()

    def run(self):
        try:
            self._renderTiles()
            OSMTileRendererThread.KLeftToRender -= 1
            print "<<Done rendering tile %s, left to render: %d" % \
                (self.tile.getLocalFileName(), OSMTileRendererThread.KLeftToRender)
        except:
            OSMTileRendererThread.KLeftToRender -= 1
            print ">>Failed rendering tile %s, left to render: %d" % \
                  (self.tile.getLocalFileName(), OSMTileRendererThread.KLeftToRender)
            traceback.print_exc()


    def _renderTiles(self):
        width = pyGpxOSMTiles.OSMTilesReferenceMap.ETileDimensions[0]
        height = pyGpxOSMTiles.OSMTilesReferenceMap.ETileDimensions[1]
        surface = cairo.ImageSurface(cairo.FORMAT_ARGB32, width, height)
        cr = cairo.Context(surface)

        for waySection in self.mWaySections:
            self._renderWaySection(cr, waySection)

        surface.write_to_png(self.tile.getLocalFileName())
        if self.callback:
            gobject.idle_add(self.callback, self.tile)

    def _renderWaySection(self, cr, waySection):
        wayDrawingConfig = self.config.ways.way(
            waySection.attrs()["highway"],
            self.tile.zoom
        )
        if wayDrawingConfig:
            # roadDrawingTool = pyCairoDrawToolkit.RoadTool(
            #    cr,
            #    wayDrawingConfig,
            #    lambda node: self.tile.latlon2pix(node.latlon())
            # )
            roadDrawingTool = pyCairoDrawToolkit.CyclewayTool(
                cr, waySection.cycleway(), lambda node: self.tile.latlon2pix(node.latlon())
            )
            roadDrawingTool.draw(waySection.nodes())

            # lastElevationPix = None
            # for node in waySection.mNodes:
            #    nodePix = self.tile.latlon2pix(node.latlon())
            #    if self.tile.zoom >= OSMTileRendererThread.KMaxZoomForText:
            #        if not lastElevationPix or (math.fabs(lastElevationPix[0]-nodePix[0]) > OSMTileRendererThread.KMaxPixDistanceForElevation and math.fabs(lastElevationPix[1]-nodePix[1]) > OSMTileRendererThread.KMaxPixDistanceForElevation):
            #            textTool.draw(nodePix, str(node.eleMts())+"m")
            #            lastElevationPix = nodePix

            # if self.tile.zoom >= OSMTileRendererThread.KMaxZoomForText:
            #    endOfSectionPix = self.tile.latlon2pix(node.latlon())
            #    nodeTool.draw(endOfSectionPix, 8)
            #    textTool.draw((endOfSectionPix[0], endOfSectionPix[1]+12), "way:"+str(waySection.mWayId)+" node:"+str(node.mId))
            #    if waySection.mClimbs:
            #        for idx, climb in enumerate(waySection.mClimbs):
            #            textTool.draw((endOfSectionPix[0], endOfSectionPix[1]+(12*(idx+2))), str(climb[0])+"%/"+str(climb[1])+"m")


#************************************************************************
# A generation and cache engine for locally rendered OSM tile files
#************************************************************************
class OSMLocalTileRenderingEngine(pyGpxOSMTiles.TilesGeneratingEngineBase):
    def __init__(self, parent, tileRenderer, sectionFetcher):
        super(OSMLocalTileRenderingEngine,self).__init__()
        self.cache = pyGpxOSMTiles.TileCache( r'./.localCache/locallyRenderedTiles/', None )
        self.cache.trimFilesOlderThan(parent.config().lastUpdated())
        self.parent = parent
        self.tileRenderer = tileRenderer
        self.sectionFetcher = sectionFetcher

    def getTile(self, x, y, zoom, callback = None):
        tile = pyGpxOSMTiles.CachedTile(x,y,zoom, self.cache)
        if tile.isCached():
            return tile
        threadedDownload = self.tileRenderer(self.parent.config(), self.sectionFetcher, tile, callback )
        return self.getTileThreaded(threadedDownload)



