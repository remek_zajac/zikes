# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# (C) 2015 Remek Zajac
#!/usr/bin/python

import os
import pyUtils
import StringIO

class Polyline(object):

    @staticmethod
    def parse(url):
        polyline = []
        fd = None
        if os.path.exists(url):
            fd = open(url, "r")
        else:
            fd = StringIO.StringIO()
            pyUtils.FileDownload.download(url, fd)
            fd.seek(0)
        while True:
            line = fd.readline()
            if line:
                splitted = line.split()
                if len(splitted) == 2 and len(splitted[0]) and len(splitted[1]):
                    polyline.append([float(splitted[1]), float(splitted[0])])
            else:
                break
        fd.close()
        return polyline
