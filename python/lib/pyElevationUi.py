# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# (C) 2012 Remek Zajac
#!/usr/bin/python

import cairo
import gobject
import threading
import pyElevation

class ElevationGrid(object):
    KMaxZoomForElevationGrid = 16
    KCrossSizePix            = 20
    KTextOffsetPix           = (5,15)

    def __init__(self, referenceMap):
        self.mMap               = referenceMap
        self.mElevation         = pyElevation.Elevation()
        self.mLatlonNW          = None
        self.mElevationMatrix   = None
        self.mElevationStep     = self.mElevation.getResolution()
        self.mBackgroundFetch   = None

    def draw(self, cr):
        cr.select_font_face("Arial", cairo.FONT_SLANT_NORMAL,
            cairo.FONT_WEIGHT_NORMAL)
        cr.set_font_size(12)
        cr.set_source_rgba(0,0,0,0.5)


        if self.mMap.zoom < ElevationGrid.KMaxZoomForElevationGrid:
            cr.move_to(ElevationGrid.KTextOffsetPix[0], ElevationGrid.KTextOffsetPix[1])
            cr.show_text("Elevation Grid only visible at zooms finer than "+str(ElevationGrid.KMaxZoomForElevationGrid))
        else:
            if self.mElevationMatrix:
                cr.set_line_width(1)
                for row in self.mElevationMatrix:
                    for cell in row:
                        pix = cell[0]
                        cr.move_to(pix[0]-ElevationGrid.KCrossSizePix,pix[1])
                        cr.line_to(pix[0]+ElevationGrid.KCrossSizePix,pix[1])
                        cr.move_to(pix[0],pix[1]-ElevationGrid.KCrossSizePix)
                        cr.line_to(pix[0],pix[1]+ElevationGrid.KCrossSizePix)
                        cr.stroke()
                        cr.move_to(pix[0]+ElevationGrid.KTextOffsetPix[0], pix[1]+ElevationGrid.KTextOffsetPix[1])
                        cr.show_text(str(cell[1])+"m")

            else:
                cr.move_to(ElevationGrid.KTextOffsetPix[0], ElevationGrid.KTextOffsetPix[1])
                cr.show_text("Fetching elevations")

    def getElevation(self, latlon, cb):
        def fetched(eleMts):
            cb(eleMts)
            self.mBackgroundFetch = None

        if not self.mBackgroundFetch:
            self.mBackgroundFetch = BackgroundElevationCollector(self.mElevation, latlon, fetched)
            self.mBackgroundFetch.start()

    def invalidate(self, mapWidget):
        def finished(resultMatrix):
            self.on_elevations_collected(resultMatrix)
            mapWidget.invalidate(self)

        if self.mMap.zoom >= ElevationGrid.KMaxZoomForElevationGrid:
            latlonNW = self.mMap.pix2latlon((0,0))
            if latlonNW != self.mLatlonNW:
                latlonSW = self.mMap.pix2latlon((2*mapWidget.allocation.width, 2*mapWidget.allocation.height)) #times two for a good measure - get all the grid cells
                elevationCollector = BackgroundElevationGridCollector(self.mElevation, latlonNW, latlonSW, finished)
                elevationCollector.start()
                self.mElevationMatrix = None
            self.mLatlonNW = latlonNW

    def clear(self):
        pass

    def on_elevations_collected(self, resultMatrix):
        self.mElevationMatrix = []
        firstPoint            = resultMatrix[0][0]
        nextDiagonalPoint     = resultMatrix[1][1]
        pixNW                 = self.mMap.latlon2pix(firstPoint[0])
        pixNextDiagonal       = self.mMap.latlon2pix(nextDiagonalPoint[0])
        xStep,yStep           = (pixNextDiagonal[0]-pixNW[0], pixNextDiagonal[1]-pixNW[1])
        pix = pixNW
        for input_row in resultMatrix:
            result_row = []
            self.mElevationMatrix.append(result_row)
            for input_cell in input_row:
                result_row.append((pix, input_cell[1], input_cell[0]))
                pix = (pix[0]+xStep, pix[1])
            pix = (pixNW[0], pix[1]+yStep)





class BackgroundElevationGridCollector(threading.Thread):

    def __init__(self, elevation, nwCorner, swCorner, cb):
        super(BackgroundElevationGridCollector, self).__init__()
        self.mElevation = elevation
        self.mNW = nwCorner
        self.mSW = swCorner
        self.mCB = cb

    def run(self):
        resultMatrix     = []
        latStep, lonStep = self.mElevation.getResolution()
        lat              = self.mNW[0]
        while lat > self.mSW[0]:
            lon          = self.mNW[1]
            row          = []
            resultMatrix.append(row)
            while lon < self.mSW[1]:
                row.append(self.mElevation.getAt((lat, lon), approximateConfidence=None))
                lon += lonStep
            lat -= latStep
        gobject.idle_add(self.mCB, resultMatrix)




class BackgroundElevationCollector(threading.Thread):

    def __init__(self, elevation, latlon, cb):
        super(BackgroundElevationCollector, self).__init__()
        self.mElevation = elevation
        self.mCb        = cb
        self.mLatlon    = latlon

    def run(self):
        gobject.idle_add(self.mCb, self.mElevation.getAt(self.mLatlon))