# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# (C) 2012 Remek Zajac
#!/usr/bin/python

import xml.sax
import time as time_module

#local modules
import pyGeometry
import pyTrack
import pyUtils


class ParseGPX:
    @staticmethod
    def str2timeInSeconds(timeString):
        try:
            time = time_module.strptime(timeString, "%Y-%m-%dT%H:%M:%S.%fZ")
        except ValueError:
            try:
                time = time_module.strptime(timeString, "%Y-%m-%dT%H:%M:%SZ")
            except ValueError:
                try:
                    time = time_module.strptime(timeString, "%Y-%m-%dT%H:%M:%S")
                except ValueError as err:
                    print "Cannot parse time string: "+timeString
                    raise err
        return time_module.mktime(time) #convert to seconds

    def parse(self, gpxFile, newTrackCallback):
        self.mNewTrackCallback = newTrackCallback
        self.mTrack = None
        self.mCurrentLatLon = None
        self.mCurrentTime   = None
        self.mTagStack      = []
        self.mCharacters    = ""
        parser = xml.sax.make_parser()
        parser.setContentHandler(self)
        parser.parse(gpxFile)

    def startElement(self, name, attrs):
        self.mCharacters  = ""
        self.mTagStack.append(name)
        if name == "trkseg" or name == "rte":
            self.mTrack = self.mNewTrackCallback()
        elif name == "trkpt" or name == "rtept":
            self.mCurrentLatLon = (float(attrs.getValue('lat')), float(attrs.getValue('lon')))

    def endElement(self, name):
        if name == "trkpt" or name == "rtept":
            self.mTrack.append(self.mCurrentLatLon, self.mCurrentTime)
            self.mCurrentLatLon = None
            self.mCurrentTime   = None
        elif name == "trkseg" or name == "rte":
            self.mTrack = None
        if name == "time":
            self.mCurrentTime = ParseGPX.str2timeInSeconds(self.mCharacters)
        self.mTagStack.pop()

    def processingInstruction(self, target, data):
        pass

    def characters(self, content):
        self.mCharacters += content

    def setDocumentLocator(self, locator):
        pass

    def startDocument(self):
        pass

    def endDocument(self):
        pass



class GPXRenderer:

    def __init__(self, parent):
        self.mParent = parent
        self.mTracks = []
        self.mRegion = None

    def parse(self, gpxFile):
        parser = ParseGPX()
        parser.parse(gpxFile, self._newGpxTrackCallback)
        for track in self.mTracks:
            track.align()
            if track.hasSpeed():
                print "Speed stats; max: "+"{0:.2f}".format(track.maxSpeedKph())+"kph, avg: "+"{0:.2f}".format(track.avgSpeedKph())+"kph"

    def _newGpxTrackCallback(self):
        self.mTracks.append(MapAlignedGPXTrack(self.mParent))
        return self.mTracks[-1]

    def region(self):
        if not self.mRegion:
            self.mRegion = pyGeometry.GeoRegion()
            for track in self.mTracks:
                self.mRegion.add(track.region())
        return self.mRegion

    def draw(self, cr):
        for track in self.mTracks:
            track.draw(cr)

    def invalidate(self, calledBy):
        for track in self.mTracks:
            track.invalidate(calledBy)




class GPXTrack(pyTrack.Track):

    @staticmethod
    def timeInSecondsToString(timeInSeconds):
        timeStruct = time_module.gmtime(timeInSeconds)
        timeStr = time_module.strftime("%Y-%m-%dT%H:%M:%SZ", timeStruct)
        return timeStr

    def __init__(self, parent=None):
        super(GPXTrack, self).__init__(parent)

    def append(self, latlon, time):
        super(GPXTrack, self).append(pyTrack.Track.Milestone(latlon,time))

    def write(self, dstFile):
        f = open(dstFile, 'w')
        self.writePreamble(f)
        for milestone in self:
            self.writeTrkPoint(milestone, f)
        self.writePostamble(f)
        f.close()

    def writePreamble(self, f):
        f.write('<?xml version="1.0" encoding="UTF-8"?>\n')
        f.write('<gpx xsi:schemaLocation="http://www.topografix.com/GPX/1/1 http://www.topografix.com/GPX/1/1/gpx.xsd http://www.garmin.com/xmlschemas/GpxExtensions/v3 http://www.garmin.com/xmlschemas/GpxExtensionsv3.xsd http://www.garmin.com/xmlschemas/TrackPointExtension/v1 http://www.garmin.com/xmlschemas/TrackPointExtensionv1.xsd" version="1.1" creator="BTT for Qt" xmlns="http://www.topografix.com/GPX/1/1" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:gpxtpx="http://www.garmin.com/xmlschemas/TrackPointExtension/v1" xmlns:gpxx="http://www.garmin.com/xmlschemas/GpxExtensions/v3">\n')
        f.write('    <trk>\n')
        f.write('        <name>Created by GPX experiments</name>\n')
        f.write('        <trkseg>\n')

    def writePostamble(self, f):
        f.write('        </trkseg>\n')
        f.write('    </trk>\n')
        f.write('</gpx>')

    def writeTrkPoint(self, milestone, f):
        tagstring = ('            <trkpt lat="'+str(milestone.lat())+'" lon="'+str(milestone.lon())+'">\n')
        f.write(tagstring)
        if milestone.mTimestampSec:
            timetagstring = ('                <time>'+self.timeInSecondsToString(milestone.mTimestampSec)+'</time>\n')
            f.write(timetagstring)
        f.write('            </trkpt>\n')






class MapAlignedGPXTrack(GPXTrack):
    KMaxDiversionMts = 25
    KProgressLen     = 60

    def __init__(self, parent):
        super(MapAlignedGPXTrack, self).__init__(parent)
        self.mAnalyser = pyTrack.ThreadedTrackAnalyser(self, parent)

    def draw(self, cr):
        super(MapAlignedGPXTrack, self).draw(cr)
        self.mAnalyser.draw(cr)

    def align(self, background=True):
        if background:
            self.mAnalyser.start(self._alignFinished)
        else:
            self.mAnalyser.foreground()
            self._alignFinished()

    def insert2DB(self, background=False):
        if not self.mAnalyser.inserted2DB():
            self.align(background)
            self.mAnalyser.insert2DB()
            return True
        return False

    def _alignFinished(self):
        pass