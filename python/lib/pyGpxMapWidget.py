# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# (C) 2012 Remek Zajac
#!/usr/bin/python

import gtk


#local modules
import pyGpxOSMTiles
import pyGeometry
import pyGPX
import pyPointView
import pyRouting
import pyWayView
import pyOSMTileRenderer
from lib.urbanRural import pyUrbanRuralView
import pyScripting
import pyRouteConfig
import pyExport
import pyTrack
import pySectionView
import pyLogAnalysis
from lib.osmsql.pyBrowse import WayNodes
from lib.osmsql.pyDb import OSMGisDatabase

from osmsql import pyBrowse as pyBrowseOsmDB
from osmsql import pyImport as pyImportOsmDB
import pyElevationUi

class GPXMapWidget(gtk.DrawingArea):
    #calibrators
    MaxZoom = 18
    SemiPanOnZoom = 0.6
    TARGET_TYPE_URI_LIST = 80
    GTK_BUTTON_LEFT = 1
    GTK_BOTTON_MIDDLE = 2
    GTK_BUTTON_RIGHT = 3

    KStaticDrawableComponents = {}
    def __init__(self, parent, dbName, latlon=(51.0918,17.0298), zoom=11):     #Wroclaw
   # def __init__(self, parent, dbName, latlon=(51.5131,-0.12645), zoom=13):     #London
    #def __init__(self, parent, dbName, latlon=(43.1887160577,2.34107929687), zoom=11):
        super(GPXMapWidget, self).__init__()
        self.mDb = OSMGisDatabase(dbName)
        self.mWayNodesBrowser = WayNodes(self.mDb)
        self.m_parent = parent
        self.m_config = parent.config
        self.lastButtonPressedAt = (-1,-1)
        self.localTileRenderer = None
        self.osmReferenceMap = pyGpxOSMTiles.OSMTilesReferenceMap(self, latlon, zoom)
        self.set_size_request(400, 300)
        self.statusBar = None
        self.m_viewMenu = gtk.Menu()
        self.m_toolsMenu = gtk.Menu()

        #register for events
        self.connect("expose-event", self.expose)
        self.connect("motion_notify_event", self.motion_notify_event)
        self.connect("button_press_event", self.button_press_event)
        self.connect("button_release_event", self.button_release_event)
        self.connect("scroll-event", self.scroll_event)
        self.connect("configure-event", self.configure_event)
        self.drag_dest_set( gtk.DEST_DEFAULT_MOTION |
                  gtk.DEST_DEFAULT_HIGHLIGHT | gtk.DEST_DEFAULT_DROP,
                  [ ( 'text/uri-list', 0, GPXMapWidget.TARGET_TYPE_URI_LIST ) ], gtk.gdk.ACTION_COPY)
        self.connect("drag-data-received", self.drag_data_received_event)

        self.set_events(gtk.gdk.EXPOSURE_MASK
            | gtk.gdk.BUTTON_PRESS_MASK
            | gtk.gdk.BUTTON_RELEASE_MASK
            | gtk.gdk.POINTER_MOTION_MASK
            | gtk.gdk.POINTER_MOTION_HINT_MASK
            | gtk.gdk.SCROLL_MASK)


        self.staticDrawableComponents = {}
        #A bit inapproprietary, the drawing sequence depends on the 'alphabetical' sorting order of
        #the drawable components and since the opaque OSM tiles need to go first, they need to
        #be first in the sorting order. Hence the trailing space
        self.staticDrawableComponents[" Downloaded OSM Tiles"] = pyGpxOSMTiles.OSMTilesMap(
            self.osmReferenceMap, pyGpxOSMTiles.OSMTiles(self.m_config)
        )
        self.staticDrawableComponents["Local OSM Tiles"] = pyGpxOSMTiles.OSMTilesMap(
            self.osmReferenceMap, pyOSMTileRenderer.OSMLocalTileRenderingEngine(
                self, pyOSMTileRenderer.OSMTileRendererThread, pyOSMTileRenderer.SectionFetcher(self.mWayNodesBrowser))
        )

        self.staticDrawableComponents["Node RAM Cache"] = pyBrowseOsmDB.WayNodesRAMCacheRenderer(
            self.osmReferenceMap, self.mWayNodesBrowser
        )
        self.staticDrawableComponents["Elevation Grid"] = pyElevationUi.ElevationGrid(
            self.osmReferenceMap
        )

        self.staticDrawableComponents["Urban/Rural View"] = pyUrbanRuralView.UrbanRuralView(
            self.osmReferenceMap
        )

        self.mRoutingEngine = pyRouting.Route(self.mWayNodesBrowser, self.osmReferenceMap)
        self.drawableComponents = {}
        self.clearMap()
        self.m_routeConfigs = pyRouteConfig.RouteConfigs(self)

    def referenceMap(self):
        return self.osmReferenceMap

    def invalidate(self, calledBy=None):
        self.viewMenu()
        self.toolsMenu()
        self.osmReferenceMap.invalidate(self)
        for k,v in self.drawableComponents.iteritems():
            v.invalidate(self)
        self.queue_draw()

    def clearMap(self):
        for k,v in self.drawableComponents.iteritems():
            v.clear()
        self.drawableComponents = {}
        self.drawableComponents[" Downloaded OSM Tiles"] = self.staticDrawableComponents[" Downloaded OSM Tiles"]
        self.invalidate(self)

    def region(self):
        NW = self.osmReferenceMap.pix2latlon((0,0))
        SE = self.osmReferenceMap.pix2latlon((self.allocation.width, self.allocation.height))
        return pyGeometry.GeoRegion((NW[0],NW[1],SE[0],SE[1]))

    def expose(self, widget, event):
        cr = widget.window.cairo_create()
        for k,v in sorted(self.drawableComponents.iteritems()):
            v.draw(cr)

    def motion_notify_event(self, widget, event):
        if self.lastButtonPressedAt != (-1,-1):
            #pan the map
            lanlonVector = pyGeometry.Vector(
                (self.osmReferenceMap.pix2latlon((event.x, event.y)), self.osmReferenceMap.pix2latlon(self.lastButtonPressedAt)))
            newLatLonCentre = lanlonVector.addToPoint(self.osmReferenceMap.latlonCentre)
            self.osmReferenceMap.panAndZoom(newLatLonCentre)
            self.lastButtonPressedAt = (event.x, event.y)
        if self.statusBar:
            latlon = self.osmReferenceMap.pix2latlon((event.x, event.y))
            statusText = "lat:"+str(round(latlon[0],6))+" lon:"+str(round(latlon[1],6))+" zoom:"+str(self.osmReferenceMap.zoom)
            eleText = ""
            def updateStatusWithElevation(eleMts):
                self.statusBar.push(1, statusText+" elevation:"+str(eleMts)+"m")
            if self.drawableComponents.has_key("Elevation Grid"):
                self.drawableComponents["Elevation Grid"].getElevation(latlon, updateStatusWithElevation)
            self.statusBar.push(1, statusText)


    def scroll_event(self, widget, event):
        newLatLonCentre = self.osmReferenceMap.latlonCentre
        newZoom = self.osmReferenceMap.zoom
        if event.direction == gtk.gdk.SCROLL_UP and newZoom < GPXMapWidget.MaxZoom:
            #lets zoom with the centre moved (by GPXMapWidget.SemiPanOnZoom) towards the current mouse position
            #the reason why wy don't move exactly to the current mouse position is that it feels unnatural,
            #but please judge yourself if you find it funny
            latlonEvent = self.osmReferenceMap.pix2latlon((event.x, event.y))
            latlonSemiPanVector = pyGeometry.Vector((newLatLonCentre,latlonEvent))
            latlonSemiPanVector = latlonSemiPanVector.setLength(latlonSemiPanVector.length()*GPXMapWidget.SemiPanOnZoom)
            newLatLonCentre = latlonSemiPanVector.addToPoint(newLatLonCentre)
            newZoom += 1
        elif event.direction == gtk.gdk.SCROLL_DOWN and newZoom > 1:
            newZoom -= 1
        self.osmReferenceMap.panAndZoom(newLatLonCentre, newZoom)

    def drag_data_received_event(self, widget, context, x, y, selection, targetType, time):
        print "dropped:", selection.data
        uri = selection.data.strip('\r\n\x00')
        if uri.find('.gpx') != -1:
            gpxRenderer = pyGPX.GPXRenderer(self)
            gpxRenderer.parse(uri)
            self.drawableComponents[uri] = gpxRenderer
            self.osmReferenceMap.showRegion(gpxRenderer.region())
        elif uri.find('.osm') != -1:
            db = pyImportOsmDB.OSMDatabaseImporter(self.m_config)
            db.parseOsm(uri, self.region())
        elif uri.find('.script') != -1:
            scriptEngine = pyScripting.ScriptingEngine(self, uri)
            scriptEngine.do(self.invalidate)
        elif uri.find('.sections') != -1:
            f = uri.split("file://")[1]
            self.drawableComponents[uri] = pySectionView.SectionsView(self.osmReferenceMap, f)
            self.osmReferenceMap.showRegion(self.drawableComponents[uri].region())
        elif uri.find('.points') != -1:
            f = uri.split("file://")[1]
            try:
                self.drawableComponents[uri] = pyPointView.PointView(self.osmReferenceMap, f)
            except:
                with open(f, "r") as fd:
                    self.showCoordinates(fd.read(),False)
        elif uri.find('.log') != -1:
            self.drawableComponents[uri] = pyLogAnalysis.LogView(uri.split("file://")[1], self.osmReferenceMap)
            self.osmReferenceMap.showRegion(self.drawableComponents[uri].region())
        else:
            print "Unsupported file type dropped, ignoring"

    def righclick_contextmenu(self, event):
        menu = gtk.Menu()
        enableRoutingItems = True
        enableRoutingItems = not self.routingEngine().isBusy()
        fromMenuItem = gtk.MenuItem("Ride From")
        menu.append(fromMenuItem)
        fromMenuItem.set_sensitive(enableRoutingItems)
        fromMenuItem.connect("activate", self.on_from_menu_item, (event.x, event.y))
        toMenuItem = gtk.MenuItem("Ride To")
        menu.append(toMenuItem)
        toMenuItem.set_sensitive(enableRoutingItems)
        toMenuItem.connect("activate", self.on_to_menu_item, (event.x, event.y))
        mouseLocation = gtk.MenuItem("Copy mouse location")
        mouseLocation.connect("activate", self.on_mouse_location_menu_item, (event.x, event.y))
        menu.append(mouseLocation)
        mouseLocation = gtk.MenuItem("Copy current region")
        mouseLocation.connect("activate", self.on_map_region_menu_item, (event.x, event.y))
        menu.append(mouseLocation)

        mouseLocation = gtk.MenuItem("Show nearest ways")
        mouseLocation.connect("activate", self.on_show_nearest_way, (event.x, event.y))
        menu.append(mouseLocation)

        mouseLocation = gtk.MenuItem("Clear map")
        mouseLocation.connect("activate", lambda foo: self.clearMap())
        menu.append(mouseLocation)

        menu.show_all()
        menu.popup(None, None, None, event.button, event.get_time())

    def on_to_menu_item(self, widget, mouseevent):
        self.routingEngine().setDestination(
            pyGeometry.GeoPoint(self.osmReferenceMap.pix2latlon((mouseevent[0], mouseevent[1]))))
        self.route()

    def on_from_menu_item(self, widget, mouseevent):
        self.routingEngine().setOrigin(
            pyGeometry.GeoPoint(self.osmReferenceMap.pix2latlon((mouseevent[0], mouseevent[1]))))
        self.route()

    def on_show_nearest_way(self, widget, mouseevent):
        wayViews = pyWayView.WayView.showWaysNearestTo(
            self.mWayNodesBrowser,
            self.osmReferenceMap,
            pyGeometry.GeoPoint(
                self.osmReferenceMap.pix2latlon((mouseevent[0], mouseevent[1]))
            )
        )
        region = pyGeometry.GeoRegion()
        for wayView in wayViews:
            self.drawableComponents["Way:"+wayView.name()] = wayView
            region.add(wayView.region())
        self.referenceMap().showRegion(region)

    def route(self, fromlatlon = None, tolatlon = None):
        routingEngine = self.routingEngine()
        if fromlatlon and tolatlon:
            routingEngine.setOriginAndDestination(self.osmReferenceMap.pix2latlon(fromlatlon), self.osmReferenceMap.pix2latlon(tolatlon))
        if routingEngine.isReadyToRoute():
            routingEngine.do(self.m_routeConfigs, self.invalidate)

    def routingEngine(self):
        self.drawableComponents["Route"] = self.mRoutingEngine
        return self.mRoutingEngine

    def on_mouse_location_menu_item(self, widget, mouseevent):
        location = self.osmReferenceMap.pix2latlon((mouseevent[0], mouseevent[1]))
        text = str(location[0])+','+str(location[1])
        clipboard = gtk.clipboard_get()
        clipboard.set_text(text)
        clipboard.store()

    def on_map_region_menu_item(self, widget, mouseevent):
        text = str(self.region())
        clipboard = gtk.clipboard_get()
        clipboard.set_text(text)
        clipboard.store()

    def button_press_event(self, widget, event):
        if event.button == GPXMapWidget.GTK_BUTTON_LEFT:
            self.lastButtonPressedAt = (event.x, event.y)
        elif event.button == GPXMapWidget.GTK_BUTTON_RIGHT:
            self.righclick_contextmenu(event)
        else:
            print "Middle button clicked - no function"

    def button_release_event(self, widget, event):
        if event.button == GPXMapWidget.GTK_BUTTON_LEFT:
            self.lastButtonPressedAt = (-1,-1)

    def configure_event(self, widget, event):
        self.invalidate(self)

    def viewMenu(self):
        #empty the menu
        for i in self.m_viewMenu.get_children():
            self.m_viewMenu.remove(i)

        #compose the list (keep it in the same order always)
        menuItemsList = []

        for drawableComponentKey in self.staticDrawableComponents:
            if not self.drawableComponents.has_key(drawableComponentKey):
                menuItemsList.append((drawableComponentKey, False))
            else:
                menuItemsList.append((drawableComponentKey, True))

        for drawableComponentKey in self.drawableComponents:
            if not self.staticDrawableComponents.has_key(drawableComponentKey):
                menuItemsList.append((drawableComponentKey, True))

        #finally compose the menu
        for element in menuItemsList:
            menuItem = gtk.CheckMenuItem(element[0])
            menuItem.set_active(element[1])
            menuItem.show()
            menuItem.connect("activate", self.on_view_menu_item_activate, element[0])
            self.m_viewMenu.append(menuItem)
        return self.m_viewMenu

    def toolsMenu(self):
        #empty the menu
        for i in self.m_toolsMenu.get_children():
            self.m_toolsMenu.remove(i)

        menuItem = gtk.MenuItem("Save Route")
        if self.routingEngine().isBusy():
            menuItem.set_sensitive(True)
        else:
            menuItem.set_sensitive(False)

        menuItem.show()
        menuItem.connect("activate", self.on_save_route)
        self.m_toolsMenu.append(menuItem)

        menuItem = gtk.MenuItem("Route Preferences")
        menuItem.connect("activate", lambda foo: self.m_routeConfigs.show())
        menuItem.show()
        self.m_toolsMenu.append(menuItem)

        menuItem = gtk.MenuItem("Export")
        menuItem.connect("activate", lambda foo: pyExport.ExportMapWindow(self.mDb, self.region()).show())
        menuItem.show()
        self.m_toolsMenu.append(menuItem)

        showMenu = gtk.Menu()
        showMenuItem = gtk.MenuItem("Show")
        showMenuItem.set_submenu(showMenu)
        showMenuItem.show()
        self.m_toolsMenu.append(showMenuItem)
        menuItem = gtk.MenuItem("Way")
        menuItem.connect("activate", self.on_show_way)
        menuItem.show()
        showMenu.append(menuItem)
        menuItem = gtk.MenuItem("Node")
        menuItem.connect("activate", self.on_show_node)
        menuItem.show()
        showMenu.append(menuItem)
        menuItem = gtk.MenuItem("Coordinates")
        menuItem.connect("activate", self.on_show_coordinates)
        menuItem.show()
        showMenu.append(menuItem)

        return self.m_toolsMenu


    def on_show_node(self, foo):
        dialog = gtk.MessageDialog(
            None,
            gtk.DIALOG_MODAL | gtk.DIALOG_DESTROY_WITH_PARENT,
            gtk.MESSAGE_QUESTION,
            gtk.BUTTONS_OK_CANCEL,
            None)
        dialog.set_markup("Find node by its id")
        #create the text input field
        entry = gtk.Entry()
        #allow the user to press enter to do ok
        #entry.connect("activate", responseToDialog, dialog, gtk.RESPONSE_OK)
        #create a horizontal box to pack the entry and a label
        hbox = gtk.HBox()
        hbox.pack_start(gtk.Label("Node id:"), False, 5, 5)
        hbox.pack_end(entry)
        #some secondary text
        #add it and show it
        dialog.vbox.pack_end(hbox, True, True, 0)
        dialog.show_all()
        #go go go
        response = dialog.run()
        if response == gtk.RESPONSE_OK:
            self.showNodeId(int(entry.get_text()))
        dialog.destroy()

    def on_show_way(self, foo):
        dialog = gtk.MessageDialog(
            None,
            gtk.DIALOG_MODAL | gtk.DIALOG_DESTROY_WITH_PARENT,
            gtk.MESSAGE_QUESTION,
            gtk.BUTTONS_OK_CANCEL,
            None)
        dialog.set_markup("Find way by its id")
        #create the text input field
        entry = gtk.Entry()
        #allow the user to press enter to do ok
        #entry.connect("activate", responseToDialog, dialog, gtk.RESPONSE_OK)
        #create a horizontal box to pack the entry and a label
        hbox = gtk.HBox()
        hbox.pack_start(gtk.Label("Way id:"), False, 5, 5)
        hbox.pack_end(entry)
        #some secondary text
        #add it and show it
        dialog.vbox.pack_end(hbox, True, True, 0)
        dialog.show_all()
        #go go go
        response = dialog.run()
        if response == gtk.RESPONSE_OK:
            self.showWayId(int(entry.get_text()))
        dialog.destroy()

    def on_show_coordinates(self, foo):
        dialog = gtk.MessageDialog(
            None,
            gtk.DIALOG_MODAL | gtk.DIALOG_DESTROY_WITH_PARENT,
            gtk.MESSAGE_QUESTION,
            gtk.BUTTONS_OK_CANCEL,
            None)
        dialog.set_markup("Paste comma separated coordinates (or its array)")
        #create the text input field
        entry = gtk.Entry()
        #allow the user to press enter to do ok
        def showAndDestroy():
            self.showCoordinates(entry.get_text())
            dialog.destroy()
        entry.connect("activate", lambda x,y,z: showAndDestroy(), dialog, gtk.RESPONSE_OK)
        #create a horizontal box to pack the entry and a label
        hbox = gtk.HBox()
        hbox.pack_start(gtk.Label("lat,lon:"), False, 5, 5)
        hbox.pack_start(entry)
        asPointsCheckbox = gtk.CheckButton("As points")
        asPointsCheckbox.set_active(False)
        hbox.pack_start(asPointsCheckbox)
        #some secondary text
        #add it and show it
        dialog.vbox.pack_end(hbox, True, True, 0)
        dialog.show_all()
        #go go go
        response = dialog.run()
        if response == gtk.RESPONSE_OK:
            self.showCoordinates(entry.get_text(), asPointsCheckbox.get_active())
        dialog.destroy()

    def showWayId(self, wayid):
        newView = pyWayView.WayView(self.mWayNodesBrowser, self.osmReferenceMap, True)
        newView.showWayId(wayid)
        key = "Way:"+newView.name()
        self.drawableComponents[key] = newView
        self.osmReferenceMap.showRegion(self.drawableComponents[key].region())

    def showNodeId(self, nodeid):
        wayViews = pyWayView.WayView.showWaysIntersectingNode(
            self.mWayNodesBrowser,
            self.osmReferenceMap,
            nodeid
        )
        region = pyGeometry.GeoRegion()
        for wayView in wayViews:
            self.drawableComponents["Way:"+wayView.name()] = wayView
            region.add(wayView.region())
        self.osmReferenceMap.showRegion(region)

    def showCoordinates(self, coordinates, asApoints):
        try:
            coordinates = eval(coordinates)
        except:
            print "Unable to evaluate '%s' as comma separated coordinates, or a list of them" %coordinates
            return
        if isinstance(coordinates, tuple):
           coordinates = list(coordinates)
        if not isinstance(coordinates, list) or len(coordinates) == 0:
            print "Unable to evaluate '%s' as comma separated coordinates, or a list of them" %coordinates
            return
        if not isinstance(coordinates[0], list):
            coordinates = [coordinates]
        name = "%s->%s" % (coordinates[0] , coordinates[-1])
        if len(coordinates) == 1 or asApoints:
            self.drawableComponents[name] = pyPointView.PointView(self.osmReferenceMap, coordinates)
            self.osmReferenceMap.showRegion(self.drawableComponents[name].region())
        else:
            self.drawableComponents[name] = pyTrack.Track(parent=self)
            self.drawableComponents[name].append(coordinates)
            self.drawableComponents[name].show();


    def on_view_menu_item_activate(self, event, key):
        if self.drawableComponents.has_key(key):
            del self.drawableComponents[key]
        else:
            assert(self.staticDrawableComponents.has_key(key))
            self.drawableComponents[key] = self.staticDrawableComponents[key]
        self.invalidate(self)

    def on_save_route(self, event):
        gpxWriter = pyGPX.WriteGpx()
        fileName = 'route.gpx'
        gpxWriter.do(self.routingEngine(), fileName)
        print "Route saved to file:", fileName

    def setStatusBar(self, statusBar):
        self.statusBar = statusBar

    def config(self):
        return self.m_config
