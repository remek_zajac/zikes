# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# (C) 2012 Remek Zajac
#!/usr/bin/python

import gobject
import gtk
import math
import json
import numpy

#local modules
from lib.osmsql import pyBase
import lib.pyOSMTileRenderer
import lib.pyCairoDrawToolkit
import lib.pyGeometry


class UrbanRuralDrawConfig(object):
    KUrban2LessUrban = [
        (0, 1, 0, 1),
        (0.5, 0.5, 0, 1),
        (1, 0, 0, 1)
    ]

    @staticmethod
    def create(idx):
        if idx != None:
            return UrbanRuralDrawConfig(idx)

    def __init__(self, idx):
        self.mUrbanIdx = idx

    def colour(self):
        return UrbanRuralDrawConfig.KUrban2LessUrban[self.mUrbanIdx]

    def width(self):
        return 3

class DiagnoseDrawConfig(object):

    def __init__(self, color):
        self.color = color

    def colour(self):
        return self.color

    def width(self):
        return 2

KDiagnoseConfigA = DiagnoseDrawConfig((0,1,1,1))
KDiagnoseConfigB = DiagnoseDrawConfig((1,0,1,1))

class UrbanRuralView(object):

    def __init__(self, referenceMap):
        super(UrbanRuralView, self).__init__()
        self.referenceMap = referenceMap
        self.waySections = None
        self.currentRegion = None
        self.mParamWindow = None

    def invalidate(self, parent=None):
        def fetched(waySections):
            self.waySections = waySections
            parent.invalidate()
            self.reportWaySectionStats()
        if not self.mParamWindow:
            self.mParamWindow = RouteConfigsWindow(parent)
            self.mParamWindow.show()
        if not (self.currentRegion and self.currentRegion.equals(parent.region())):
            self.waySections = None
            def dispatch():
                if not (self.currentRegion and self.currentRegion.equals(parent.region())):
                    self.currentRegion = parent.region()
                    lib.pyOSMTileRenderer.Request(parent.mWayNodesBrowser).add(fetched, self.currentRegion).start()
            gobject.timeout_add(1000, dispatch)


    KDivDistance = 10
    KIgnoreBelowMts = 30 #more indicative of junction design
    def reportWaySectionStats(self):
        buckets = {}
        for waySection in self.waySections:
            distance = waySection.distanceMts()
            if distance < UrbanRuralView.KIgnoreBelowMts:
                continue
            highway = waySection.highway()
            if highway not in buckets:
                buckets[highway] = {}
            slot = buckets[highway]
            distanceSlot = int(
                math.log(
                    max(1,distance/UrbanRuralView.KDivDistance),2
                )
            )
            if distanceSlot not in slot:
                slot[distanceSlot] = {
                    "rangeMts": [
                        UrbanRuralView.KDivDistance*pow(2,distanceSlot),
                        UrbanRuralView.KDivDistance*pow(2,distanceSlot+1)
                    ],
                    "allMts" : []
                }
            slot = slot[distanceSlot]
            slot["allMts"].append(distance)

        def statsString(array):
            res = (len(array),
               int(numpy.average(array)),
               numpy.percentile(array, 50),
               numpy.percentile(array, 80)
            )
            return "samples: %s, avg: %s, median: %s, 80%%: %s" % res, res


        buckets = sorted([
            {
                "hwy": {
                    "name" : hwy,
                    "idx" : pyBase.RawWay.KHighwayType2IdxDictionary[hwy]
                },
                "buckets": buckets[hwy]
            }
            for hwy in buckets
        ], key=lambda hwy: -reduce(
            lambda cum, bucket: cum+len(bucket["allMts"]), hwy["buckets"].itervalues(),
            0)
        )
        allHighways = {}
        for highway in buckets:
            print "Highway: %s" % highway["hwy"]["name"]
            highwayMts = []
            for slot in highway["buckets"].itervalues():
                print "  distance bucket %s %s" % (
                    json.dumps(slot["rangeMts"]),
                    statsString(slot["allMts"])[0]
                )
                highwayMts += slot["allMts"]
                rangeMts = tuple(slot["rangeMts"])
                if rangeMts not in allHighways:
                    allHighways[rangeMts] = []
                allHighways[rangeMts] += slot["allMts"]

            highway["allStats"] = statsString(highwayMts)
            print "  overall  : %s" % highway["allStats"][0]
            delta = map(
                lambda idx: round(1.0*highway["allStats"][1][idx] / buckets[0]["allStats"][1][idx],2),
                range(len(highway["allStats"][1]))
            )
            print "  delta[0] : samples: %s avg: %s median: %s 80%%: %s" % tuple(delta)

        print "Overall: "
        allHighways = sorted([
            {
                "rangeMts": rangeMts,
                "bucket": allHighways[rangeMts]
            }
            for rangeMts in allHighways
        ], key=lambda elem: elem["rangeMts"][1])
        allMts = []
        for rangeMts in allHighways:
            print "  distance bucket %s %s" % (
                rangeMts["rangeMts"],
                statsString(rangeMts["bucket"])[0]
            )
            allMts += rangeMts["bucket"]
        print "  overall  : %s" % statsString(allMts)[0]


    def draw(self, cr):
        if self.waySections:
            print "redrawing sections"
            for waySection in self.waySections:
                self._renderWaySection(cr, waySection)
            print "sections redrawn"

    def clear(self):
        self.waySections = None

    @staticmethod
    def isSignificantType(waySection):
        highway = waySection.highway()
        highwayIdx = pyBase.RawWay.KHighwayType2IdxDictionary[highway]
        return (highwayIdx >= pyBase.RawWay.KHighwayType2IdxDictionary["primary"] and
                highwayIdx <= pyBase.RawWay.KHighwayType2IdxDictionary["residential"])

    @staticmethod
    def isPromising(waySection):
        noOfExitingEdges = len(waySection.toNode().edges())
        return UrbanRuralView.isSignificantType(waySection) and noOfExitingEdges > 1

    KhighwayFactors = [
        10,  # "motorway",  # 0
        10,  # "motorway_link",  # 1
        3,  # "trunk",  # 2
        3,  # "trunk_link",  # 3
        2,  # "primary",  # 4
        2,  # "primary_link",  # 5
        1,  # "secondary",  # 6
        1,  # "secondary_link",  # 7
        1,  # "tertiary",  # 8
        1,  # "tertiary_link",  # 9
        1,  # "road",  # 10
        1,  # "unclassified",  # 11
        1,  # "service",  # 12
        1,  # "bus_guideway",  # 13
        1,  # "raceway",  # 14
        1,  # "living_street",  # 15
        1,  # "residential",  # 16
        1,  # "cycleway",  # 27
        1,  # "footway",  # 28
        1,  # "pedestrian",  # 19
        1,  # "steps",  # 20
        1,  # "bridleway",  # 21
        1,  # "track",  # 22
        1  # "path"  # 23
    ]


    @staticmethod
    def adjustDistance(waySection):
        highway = waySection.highway()
        highwayIdx = pyBase.RawWay.KHighwayType2IdxDictionary[highway]
        distance = waySection.distanceMts()
        distance /= UrbanRuralView.KhighwayFactors[highwayIdx]
        return distance




    def urbanColourSingleLenth(self, waySection, diagnoseCr=None):
        if waySection.distanceMts() < 30:
            return
        if UrbanRuralView.adjustDistance(waySection) > (300+80)/2:
            return UrbanRuralDrawConfig.create(0)
        return UrbanRuralDrawConfig.create(2)

    def _renderWaySection(self, cr, waySection):
        diagnoseWay, startingWithNodeId = self.mParamWindow.diagnoseWay()
        if len(diagnoseWay) and int(diagnoseWay) != waySection.wayid():
            return
        if len(startingWithNodeId) and int(startingWithNodeId) != waySection.nodes()[0].id():
            return

        config = self.urbanColourSingleLenth(waySection, cr if startingWithNodeId else None)
        if config:
            lib.pyCairoDrawToolkit.RoadTool(
                cr,
                config,
                lambda node: self.referenceMap.latlon2pix(node.latlon())
            ).draw(waySection.nodes())


class RouteConfigsWindow(gtk.Window):
    def __init__(self, invalidateMe):
        super(RouteConfigsWindow, self).__init__()
        self.invalidateMe = invalidateMe
        self.set_title("Configure urban/rural parameters")
        self.resize(600, 300)
        self.set_position(gtk.WIN_POS_CENTER)

        outerContainerBox = gtk.VBox(True, 2)
        outerContainerBox.set_border_width(4)
        self.add(outerContainerBox)
        innerContainerBox = gtk.VBox(False, 2)
        self.mWalkJunctions = self.addSlider("walk depth", innerContainerBox, (1,15), 8)
        self.mLowerBound  = self.addSlider("lower section length", innerContainerBox, (10,1000), 210)
        self.mUpperBound  = self.addSlider("upper section length", innerContainerBox, (10,1000), 290)
        diagnoseBox = gtk.HBox(False, 2)
        self.mDiagnoseWay = self.addEditBox("diagnose way", diagnoseBox)
        self.mStartingWithNodeId = self.addEditBox("starting with node id", diagnoseBox)
        innerContainerBox.pack_start(diagnoseBox, True, True, 10)
        outerContainerBox.pack_start(innerContainerBox, True, True, 10)
        outerContainerBox.show()
        innerContainerBox.show()
        self.invalidated = True

    def walkJunctions(self):
        return int(
            float(self.mWalkJunctions.get_text())
        )

    def lowerLengthBound(self):
        return int(
            float(self.mLowerBound.get_text())
        )

    def upperLengthBound(self):
        return int(
            float(self.mUpperBound.get_text())
        )

    def diagnoseWay(self):
        return (self.mDiagnoseWay.get_text(), self.mStartingWithNodeId.get_text())

    def addEditBox(self, name, container):
        box = gtk.HBox(False, 2)
        valueText = gtk.Entry(10)
        valueText.set_width_chars(10)
        valueText.set_editable(gtk.TRUE)
        valueText.set_size_request(100,25)
        def valueChanged(entry):
            self.invalidateMe.invalidate()
        valueText.connect("activate", valueChanged)

        mainLabel = gtk.Label(name)
        mainLabel.set_alignment(0, 0.5)
        mainLabel.set_width_chars(11)
        box.pack_start(mainLabel, False, False, 0)
        mainLabel.show()

        box.pack_start(valueText, False, False, 0)
        container.pack_start(box, True, True, 10)
        return valueText


    def addSlider(self, name, container, range, deftValue):
        box = gtk.HBox(False, 2)
        valueText = gtk.Entry(10)
        valueText.set_width_chars(5)
        valueText.set_editable(gtk.TRUE)
        valueText.set_size_request(50,25)
        valueText.set_text(str(deftValue))
        adjustment = gtk.Adjustment(
            value=deftValue,
            lower=range[0],
            upper=range[1],
            step_incr=(range[1]-range[0])/20,
            page_incr=(range[1]-range[0])/2,
            page_size=0.0
        )

        def invalidate():
            if not self.invalidated:
                self.invalidateMe.invalidate()
                self.invalidated = True

        def sliderMoved(adj):
            self.invalidated = False
            valueText.set_text(str(round(adj.value,1)))
            gobject.timeout_add(500, invalidate)

        adjustment.connect("value_changed", sliderMoved)
        def valueChanged(entry):
            self.invalidateMe.invalidate()
        valueText.connect("activate", valueChanged)

        mainLabel = gtk.Label(name)
        mainLabel.set_alignment(0, 0.5)
        mainLabel.set_width_chars(11)
        box.pack_start(mainLabel, False, False, 0)
        mainLabel.show()

        scrollbar = gtk.HScrollbar(adjustment=adjustment)
        scrollbar.show()

        box.pack_start(scrollbar, True, True, 2)
        box.pack_start(valueText, False, False, 0)
        container.pack_start(box, True, True, 10)
        return valueText

    def on_destroy(self, foo):
        self.hide()

    def show(self):
        self.show_all()