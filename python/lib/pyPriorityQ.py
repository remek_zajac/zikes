# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# (C) 2012 Remek Zajac
#!/usr/bin/python

import heapq

class PriorityQ:
    def __init__(self, inverse=False):
    	self.mSet 	    = {}
    	self.mHeapQ     = []
        self.mSign      = 1
        if inverse:
            self.mSign  = -1

    def push(self, priority, id, value):
        priority *= self.mSign
    	if id in self.mSet:
    		entry = self.mSet.pop(id)
    		entry[-1] = None
    	entry = [priority, id, value]
    	heapq.heappush(self.mHeapQ, entry)
    	self.mSet[id] = entry

    def pop(self):
    	while self.mHeapQ:
    		priority, id, value = heapq.heappop(self.mHeapQ)
    		if value:
    			return self.mSet.pop(id)[-1]
    	raise KeyError("pop from an empty priority queue")

    def get(self, id):
    	if id in self.mSet:
    		return self.mSet[id][-1]
    	return None

    def size(self):
    	return len(self.mSet)