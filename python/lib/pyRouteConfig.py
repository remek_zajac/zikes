# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# (C) 2012 Remek Zajac
#!/usr/bin/python

import gtk
import cairo
import math
import os
import time
import copy
import pyCairoDrawToolkit
import gobject

from copy import deepcopy
from lxml import etree

#local modules
import pyUtils
from osmsql import pyBrowse as pyBrowseOsmDB

class RouteConfigs:
    def __init__(self, parent):
        self.mMapWidget = parent
        self.mWindow = parent.m_parent
        self.mConfig = parent.m_config
        self.mRouteConfigs = {}
        self.mConfigWindow = None

        routingConfig = self.mConfig.root().find("routing")
        self.mDefaultProfileName = routingConfig.attrib["default"]
        routingProfiles = routingConfig.findall("*")
        for routingProfile in routingProfiles:
            self.mRouteConfigs[routingProfile.attrib["name"]] = XMLRouteConfigReadOnly(routingProfile)

        if os.path.exists(self.configsFolder()):
            for file in os.listdir(self.configsFolder()):
                if file.endswith(".xml"):
                    newConfig = XMLRouteConfig.fromFile(self.configsFolder()+"/"+file)
                    self.mRouteConfigs[newConfig.name()] = newConfig

        self.mCurrentRouteConfig = self.mRouteConfigs[self.mDefaultProfileName]

    def show(self):
        if not self.mConfigWindow:
            self.mConfigWindow = RouteConfigsWindow(self)
            self.mConfigWindow.load(self.mCurrentRouteConfig, False)
            self.mConfigWindow.show()

    def configs(self):
        return self.mRouteConfigs

    def loadConfig(self, configOrName):
        if not isinstance(configOrName, RouteConfig):
            configOrName = self.mRouteConfigs[configOrName]
        self.mCurrentRouteConfig = configOrName
        if self.mConfigWindow:
            self.mConfigWindow.load(self.mCurrentRouteConfig)

    def deleteConfig(self, configOrName):
        if not isinstance(configOrName, RouteConfig):
            configOrName = self.mRouteConfigs[configOrName]
        configOrName.delete()
        del self.mRouteConfigs[configOrName]
        if configOrName == self.mCurrentRouteConfig:
            self.loadConfig(self.mDefaultProfileName)

    def newConfig(self, config):
        self.mRouteConfigs[config.name()] = config
        self.loadConfig(config)

    def getConfig(self, configName=None):
        result = self.mCurrentRouteConfig
        if configName:
            if self.mRouteConfigs.has_key(configName):
                result = self.mRouteConfigs[configName]
            else:
                result = None
        return result

    def configsFolder(self):
        return self.mConfig.root().find("localCache").text+"/routingProfiles"



#Here's an interesting function used for displaying the
#linear scaling:
#y = 1/(1-x)-1
#See it here: http://fooplot.com/#W3sidHlwZSI6MCwiZXEiOiIxLygxLXgpLTEiLCJjb2xvciI6IiMwMDAwMDAifSx7InR5cGUiOjEwMDB9XQ--
#The feature we're interested is within its 0..1 domain where it goes from 0 (asymptotically) to infinity
#visiting (y=)1 at (x=)0.5, (y=)2 at 0.75.. essentially going +1 everytime it halves the span between current x and 1.
#We'll use its value as the positive or negative powers of 2 (respectivelly for punishment and reward)
#And thus, the closer to '1' (neutral) the greater the offered resolution
class GUISpreadingFunction:
    KMinFactor = 0.05
    KMaxFactor = 9999999.0

    @staticmethod
    def gui2Linear(value):
        absvalue = math.fabs(value)
        if absvalue != 1:
            result = 1/(1-absvalue)-1
            result = max(GUISpreadingFunction.KMinFactor, round(2 ** math.copysign(result, value),3))
            return result
        elif value == -1:
            return GUISpreadingFunction.KMinFactor #maximum reward
        else:
            return GUISpreadingFunction.KMaxFactor #maxium punishent


    @staticmethod
    def linear2Gui(value):
        result = 1
        if not math.isnan(value):
            sign = 1
            if value < 1:
                value = 1/value
                sign = -1
            result = math.log(value,2)
            result = 1-(1/(result+1))
            result = math.copysign(round(result, 3), sign)
        return result

    @staticmethod
    def displayToNumber(value):
        result = GUISpreadingFunction.KMaxFactor
        if not math.isnan(value):
            result = value
        return result

    @staticmethod
    def generateScale(stepsEachWay):
        scale = []
        for i in range(stepsEachWay-1):
            factor = 2 ** -(2 ** i-1)
            scale.append((factor, GUISpreadingFunction.linear2Gui(factor)))
        for i in range(stepsEachWay-2):
            factor = 2 ** (2 ** (i))
            scale.append((factor, GUISpreadingFunction.linear2Gui(factor)))
        return scale


class RouteConfigsWindow(gtk.Window):
    def __init__(self, routeConfigs):
        super(RouteConfigsWindow, self).__init__()
        self.mRerouteEnqueued = False
        self.mRouteConfigs = routeConfigs
        self.mCurrentRouteConfig = None
        self.set_title("Configure routing cost function")
        self.resize(600, 300)
        self.set_position(gtk.WIN_POS_CENTER)
        self.set_transient_for(self.mRouteConfigs.mWindow)
        self.mRouteConfigs.mMapWidget.routingEngine().connect("ready_to_route", self.readyToRoute)

        rerouteFrame = gtk.Frame()
        rerouteBox = gtk.VBox(True, 2)
        rerouteBox.set_border_width(2)
        self.mRerouteButton = gtk.Button("Reroute")
        self.mAutoRerouteCheckbox = gtk.CheckButton("Auto")
        self.mAutoRerouteCheckbox.set_active(True)
        rerouteBox.pack_start(self.mAutoRerouteCheckbox, False, False, 2)
        rerouteBox.pack_start(self.mRerouteButton, True, True, 2)
        rerouteFrame.add(rerouteBox)

        self.mRoutingProfileSaveButton = gtk.Button("Save")
        self.mRoutingProfileSaveButtonAs = gtk.Button("SaveAs")
        self.mRoutingProfileDeleteButton = gtk.Button("Delete")
        self.mRoutingProfilesCombo = gtk.combo_box_new_text()

        outerContainerBox = gtk.VBox(True, 2)
        outerContainerBox.set_border_width(4)
        self.add(outerContainerBox)
        innerContainerBox = gtk.VBox(False, 2)
        outerContainerBox.pack_start(innerContainerBox, True, True, 10)

        actionContainer = gtk.HBox(False, 2)
        actionContainer.pack_start(self.populateRoutingProfiles(), True, True, 2)
        #reroute button
        self.mRerouteButton.set_sensitive(
            self.mRouteConfigs.mMapWidget.routingEngine().isReadyToRoute()
        )
        self.mRerouteButton.connect("clicked", self.on_reroute)
        actionContainer.pack_start(rerouteFrame, False, False, 2)

        self.mNotebook = gtk.Notebook()
        innerContainerBox.pack_start(actionContainer, True, True, 2)
        innerContainerBox.pack_start(self.mNotebook, True, True, 2)
        self.connect("destroy", self.on_destroy)

    def on_destroy(self, foo):
        self.mRouteConfigs.mConfigWindow = None
        while self.mNotebook.get_n_pages() > 0:
            self.mNotebook.remove_page(0)
        self.hide()

    def on_reroute(self, button):
        self.enqueue_reroute()

    def readyToRoute(self, ready):
        self.mRerouteButton.set_sensitive(ready)
        if self.mRerouteEnqueued and self.mRouteConfigs.mMapWidget.routingEngine().isReadyToRoute():
            self.mRerouteEnqueued = False
            self.mRouteConfigs.mMapWidget.route()

    def enqueue_reroute(self):
        if self.mRouteConfigs.mMapWidget.routingEngine().isReadyToRoute():
            self.mRerouteEnqueued = False
            self.mCurrentRouteConfig.prepareForRouting()
            self.mRouteConfigs.mMapWidget.route()
        else:
            self.mRerouteEnqueued = True

    def on_config_changed(self, config, forceReroute=False):
        if self.mCurrentRouteConfig.dirty() or forceReroute:
            if self.mAutoRerouteCheckbox.get_active():
                self.enqueue_reroute()
        if self.mCurrentRouteConfig.dirty() and hasattr(config, "save"):
                self.mRoutingProfileSaveButton.set_sensitive(True)
        else:
            self.mRoutingProfileSaveButton.set_sensitive(False)

    def load(self, config, reroute=True):
        while self.mNotebook.get_n_pages() > 0:
            self.mNotebook.remove_page(0)
        self.mCurrentRouteConfig = config

        self.mRoutingProfilesCombo.get_model().clear()
        idx = 0
        for configName, config in self.mRouteConfigs.configs().iteritems():
            self.mRoutingProfilesCombo.append_text(config.name())
            if self.mCurrentRouteConfig.name() == config.name():
                self.mRoutingProfilesCombo.set_active(idx)
            idx += 1

        self.mRoutingProfileSaveButton.set_sensitive(False)
        if hasattr(self.mCurrentRouteConfig, "delete"):
            self.mRoutingProfileDeleteButton.set_sensitive(True)
        else:
            self.mRoutingProfileDeleteButton.set_sensitive(False)

        self.mCurrentRouteConfig.gui(self.mNotebook)
        self.mCurrentRouteConfig.subscribeForChange(self.on_config_changed)
        self.on_config_changed(self.mCurrentRouteConfig, reroute)
        self.mNotebook.show()
        self.show()


    def populateRoutingProfiles(self):
        #routing profiles
        self.mRoutingProfilesCombo.connect("changed", self.on_profile_selected)

        self.mRoutingProfileSaveButtonAs.connect("clicked", self.on_save_as)
        self.mRoutingProfileDeleteButton.connect("clicked", lambda _: self.mRouteConfigs.deleteConfig(self.mCurrentRouteConfig))
        self.mRoutingProfileSaveButton.connect("clicked", lambda _: self.mCurrentRouteConfig.save())
        routingProfileContainerBox = gtk.HBox(False, 2)
        routingProfileContainerBox.pack_start(self.mRoutingProfilesCombo, True, True, 2)
        routingProfileContainerBox.pack_start(self.mRoutingProfileSaveButton, False, False, 2)
        routingProfileContainerBox.pack_start(self.mRoutingProfileDeleteButton, False, False, 2)
        routingProfileContainerBox.pack_start(self.mRoutingProfileSaveButtonAs, False, False, 2)

        routingProfileComboFrame = gtk.Frame()
        routingProfileComboLabel = gtk.Label("Choose the profile (to load or modify)")
        routingProfileComboLabel.set_alignment(0, 0.5)
        routingProfileFrameContainer = gtk.VBox(False, 2)
        routingProfileFrameContainer.set_border_width(2)
        routingProfileFrameContainer.pack_start(routingProfileComboLabel, False, False, 2)
        routingProfileFrameContainer.pack_start(routingProfileContainerBox, True, True, 2)
        routingProfileComboFrame.add(routingProfileFrameContainer)
        return routingProfileComboFrame

    def on_profile_selected(self, combo):
        selectedText = combo.get_active_text()
        if selectedText and selectedText != self.mCurrentRouteConfig.mName:
            self.mRouteConfigs.loadConfig(selectedText)

    def show(self):
        self.show_all()


    def on_save_as(self, button):
        d = gtk.MessageDialog(self,
                              gtk.DIALOG_MODAL | gtk.DIALOG_DESTROY_WITH_PARENT,
                              gtk.MESSAGE_QUESTION,
                              gtk.BUTTONS_OK_CANCEL,
                              "Save Routing Preference As ...")
        entry = gtk.Entry()
        entry.show()
        d.vbox.pack_end(entry)
        okButton = d.get_widget_for_response(gtk.RESPONSE_OK)
        okButton.set_sensitive(False)
        entry.connect("changed", lambda _: self.on_newSaveAsNameProposed(entry,okButton))
        entry.connect("activate", lambda _: self.on_dialog_hit_enter(d,okButton))
        d.set_default_response(gtk.RESPONSE_OK)

        r = d.run()
        text = entry.get_text().decode('utf8')
        d.destroy()
        if r == gtk.RESPONSE_OK:
            elemTree = copy.deepcopy(self.mCurrentRouteConfig.mETreeRootElem)
            elemTree.attrib["name"] = text
            newPref = XMLRouteConfig(self.mRouteConfigs.configsFolder()+"/"+text+".xml", elemTree);
            newPref.save()
            self.mRouteConfigs.newConfig(newPref)
        else:
            return None


    def on_dialog_hit_enter(self, dialog, okButton):
        if okButton.get_sensitive():
            dialog.response(gtk.RESPONSE_OK)

    def on_newSaveAsNameProposed(self, entry, okButton):
        text = entry.get_text()
        if text != "" and not self.mRouteConfigs.getConfig(text):
            okButton.set_sensitive(True)
        else:
            okButton.set_sensitive(False)






class CostBucket(object):
    def __init__(self, distanceMts):
        self.mDistanceMts = distanceMts
        #A* algorithm (http://en.wikipedia.org/wiki/A*_search_algorithm)
        #uses the heuristic estimate (h(x)) of the future cost when traversing
        #the grapth. This heuristic "The h(x) part of the f(x) function must be an
        #admissible heuristic; that is, it must not overestimate the distance to the goal."
        #Returned cost must therefore be never smaller than the actual distance
        self.mCost        = self.mDistanceMts/GUISpreadingFunction.KMinFactor
        self.mFactors     = {}

    def cost(self):
        return self.mCost

    def distanceMts(self):
        return self.mDistanceMts

    def factors(self):
        return self.mFactors

    def addLinearFactor(self, factorName, influencer, factor):
        self.mFactors[factorName] = (influencer, factor)

    def finish(self):
        for factorName, factor in self.mFactors.iteritems():
            self.mCost *= factor[1]
        self.mCost = max(self.mDistanceMts, self.mCost)
        return self

class CallbackDelay(object):
    def __init__(self, callback, noSoonerThanAfterSec):
        self.mLastPoke = time.time()
        self.mCallback = callback
        self.mNoSoonerThanAfterSec = noSoonerThanAfterSec

    def poke(self):
        self.mLastPoke = time.time()
        def propagate():
            td = time.time() - self.mLastPoke
            if td > self.mNoSoonerThanAfterSec:
                self.mLastPoke = time.time()
                self.mCallback()
            return False

        gobject.timeout_add(int(self.mNoSoonerThanAfterSec*1100), propagate)


class RouteConfig(object):
    KDoNotNotifyMoreOftenThanEverySec = 2

    def __init__(self, name):
        self.mName            = name
        self.mDelayedNotifier = CallbackDelay(self.doNotifyChanged, RouteConfig.KDoNotNotifyMoreOftenThanEverySec)
        self.mNotifyChangedCb = None
        self.mDirty           = [False, False] #[0] whether it has been prepared for routing since last modification
                                               #[1] whether it has been saved since last modification

    def costBucket(self, directedWaySection):
        bucket = CostBucket(directedWaySection.distanceMts())
        if not directedWaySection.navigable(pyBrowseOsmDB.DirectedWaySection.KNavigableBicycle):
            bucket.addLinearFactor("access", "no bicycles", GUISpreadingFunction.KMaxFactor)
        return bucket

    def cost(self, directedWaySection):
        #(see CostBucket above to clarify the division)
        cost = directedWaySection.distanceMts()/GUISpreadingFunction.KMinFactor
        if not directedWaySection.navigable(pyBrowseOsmDB.DirectedWaySection.KNavigableBicycle):
            cost *= GUISpreadingFunction.KMaxFactor
        return cost

    def notifyChanged(self, dirty=True):
        self.mDirty = [dirty, dirty]
        self.mDelayedNotifier.poke()

    def doNotifyChanged(self):
        if self.mNotifyChangedCb:
            self.mNotifyChangedCb(self)

    def prepareForRouting(self):
        self.mDirty[0] = False

    def dirty(self):
        return self.mDirty[1]

    def subscribeForChange(self, cb):
        self.mNotifyChangedCb = cb

    def name(self):
        return self.mName







class XMLRouteConfigReadOnly(RouteConfig):
    def __init__(self, copyFromETreeElem):
        super(XMLRouteConfigReadOnly, self).__init__(copyFromETreeElem.attrib["name"])
        self.mETreeRootElem = None
        self.mHighwaysFactors = []
        self.mListeners = []
        self.mControls = []
        self.fromXML(copyFromETreeElem)

        controlPts = []
        climbConfig = self.mETreeRootElem.find("climb")
        controlPtsInConfig = climbConfig.findall("bezier/*")
        for controlPt in controlPtsInConfig:
            controlPts.append(
                [float(controlPt.attrib["climb"]),float(controlPt.attrib["factor"])]
            )
        self.mMaxDescent    = int(climbConfig.attrib["minDescentPercent"])
        self.mMaxClimb      = int(climbConfig.attrib["maxClimbPercent"])
        self.mClimbCurve = ScaledBezierCurve(
            controlPts,
            ((-1,1),(-1,1)),
            (self.mMaxDescent, self.mMaxClimb)
        )
        self.mOsmTab = self.osmGuiTab()
        self.mClimbGuiTab = self.climbGuiTab()


    def toString(self):
        return etree.tostring(self.mETreeRootElem, pretty_print=True)

    def updateHighwayFactors(self):
        self.mHighwaysFactors = []
        for control in self.mControls:
            self.mHighwaysFactors.append(
                (
                    GUISpreadingFunction.displayToNumber(control[0].value()),
                    GUISpreadingFunction.displayToNumber(control[1].value())
                )
            )

    def fromXML(self, etreeElem):
        self.mETreeRootElem = deepcopy(etreeElem)

    def prepareForRouting(self):
        if self.mDirty[0]:
            self.updateHighwayFactors()
            self.mClimbCurve.calculateFactors()

        super(XMLRouteConfigReadOnly, self).prepareForRouting()

    def doNotifyChanged(self):
        bezierConfig = self.mETreeRootElem.find("climb/bezier")
        bezierConfig.clear()
        for controlPt in self.mClimbCurve.getControlPts():
            elem = etree.Element("controlPt")
            elem.attrib["climb"] = str(round(controlPt[0],4))
            elem.attrib["factor"] = str(round(controlPt[1],4))
            bezierConfig.append(elem)
        super(XMLRouteConfigReadOnly, self).doNotifyChanged()


    def gui(self, gtkNotebook):
        gtkNotebook.append_page(self.mOsmTab, gtk.Label("OSM"))
        gtkNotebook.append_page(self.mClimbGuiTab, gtk.Label("Climb"))


    def climbGuiTab(self):
        climbGuiTab = gtk.VBox(False, 0)
        mainLabel = gtk.Label("Configure your climb/descent preference.")
        punishLegend = gtk.Label()
        punishLegend.set_markup("<span foreground=\"red\">Punish/Avoid</span>")
        rewardLegend = gtk.Label()
        rewardLegend.set_markup("<span foreground=\"green\">Reward/Prefer</span>")

        underneathFiller = gtk.VBox(False,0)
        instruction = "The above graph is the expression of the preference towards climbs and descends.\n"
        instruction += "The graph is a Bezier curve with the control point pairs presented as blue dots.\n"
        instruction += "The dots can be freely manipulated even to the point where the curve expresses\n"
        instruction += "no sensible preference (e.g.: ceases to be a function f(climb)=>prefernece).\n"
        instruction += "Be careful not to reward descends at a similar rate to how you punish climbs, as the\n"
        instruction += "resulting routes can take you uphill just to offer you the enjoyment of a downhill\n"
        instruction += "later on."

        underneathLabel = gtk.Label(instruction)
        underneathFiller.pack_start(underneathLabel, True, True, 0)

        climbWidgetFrame = gtk.Frame()
        climbWidgetFrame.set_border_width(2)

        climbWidget = ClimbWidget(self.mClimbCurve, self.notifyChanged, (self.mMaxDescent, self.mMaxClimb))
        climbWidgetFrame.add(climbWidget)

        maxDescentLabel = gtk.Label(str(self.mMaxDescent)+"%\ndescent")
        maxAscentLabel = gtk.Label(str(self.mMaxClimb)+"%\nclimb")

        bottomLegend = gtk.HBox(False, 0)
        bottomLegend.pack_start(maxDescentLabel, False, False, 0)
        bottomLegend.pack_start(rewardLegend, True, True, 0)
        bottomLegend.pack_end(maxAscentLabel, False, False, 0)

        climbGuiTab.pack_start(mainLabel, False, False, 0)
        climbGuiTab.pack_start(punishLegend, False, False, 0)
        climbGuiTab.pack_start(climbWidgetFrame, True, True, 0)
        climbGuiTab.pack_start(bottomLegend, False, False, 0)
        climbGuiTab.pack_start(underneathFiller, False, False, 0)

        climbGuiTab.show()
        return climbGuiTab

    def osmGuiTab(self):
        tags = self.mETreeRootElem.find("tags")
        osmGuiTab = gtk.VBox(False, 0)
        highwaysContainer = gtk.VBox(False, 0)
        highwaysContainer.set_border_width(2)
        osmHighwaysFrame = gtk.Frame()
        osmHighwaysFrame.set_border_width(2)
        osmHighwaysLabel = gtk.Label("tag:highway = ")
        osmHighwaysLabel.set_alignment(0, 0.5)
        osmHighwaysFrame.add(highwaysContainer)
        osmHighwaysFrame.show()
        osmGuiTab.pack_start(osmHighwaysFrame, False, False, 0)

        highwaysContainer.pack_start(osmHighwaysLabel, False, False, 0)
        self.populateHighways(
            highwaysContainer,
            tags.find("highway"),
        )
        self.updateHighwayFactors()

        osmGuiTab.show()
        return osmGuiTab

    def populateHighways(self, box, root):
        #load default config
        alternateColours = 0

        cycleWayTag = root.find("cycleway")
        def on_cycleway_update(control, value):
            cycleWayTag.attrib["linearCostFactor"] = str(value)
            self.notifyChanged()

        cyclewaySlider = InfluencingSlider(
            "cycleway",
            cycleWayTag.attrib["linearCostFactor"],
            on_cycleway_update
        )

        #first pass
        controls = {"cycleway" : (cyclewaySlider, None)}
        for elem in root.findall("*"):
            if elem.attrib.has_key("linearCostFactor"):

                if not elem.attrib.has_key("hide"):
                    keyContainer = gtk.VBox(False, 0)
                    keyContainer.set_border_width(2)
                    if alternateColours % 2:
                        eventBox = gtk.EventBox()
                        eventBox.add(keyContainer)
                        eventBox.modify_bg(gtk.STATE_NORMAL, gtk.gdk.color_parse("Light Steel Blue"))
                        eventBox.show()
                        box.pack_start(eventBox, False, False, 0)
                    else:
                        box.pack_start(keyContainer, False, False, 0)
                    alternateColours += 1

                    if not controls.has_key(elem.tag):
                        controlPair = self.highwaySlider(elem, cyclewaySlider)
                        controls[elem.tag] = controlPair
                    else:
                        controlPair = controls[elem.tag]

                    keyContainer.pack_start(controlPair[0].box(), True, True, 0)
                    if controlPair[1]:
                        keyContainer.pack_start(controlPair[1].box(), True, True, 0)
                    else:
                        controls[elem.tag] = (controlPair[0], controlPair[0])
                    keyContainer.show()
                else:

                    controls[elem.tag] = (
                        RoutingPreferenceControl(elem.tag, elem.attrib["linearCostFactor"]),
                        RoutingPreferenceControl(elem.tag, elem.attrib["linearCostFactor"])
                    )


        #second pass
        for elem in root.findall("*"):
            followSliderName = elem.tag
            if elem.attrib.has_key("inherit"):
                followSliderName = elem.attrib["inherit"]
            self.mControls.append(controls[followSliderName])

    def highwayType2Str(self, higwayType, cycleway):
        result = "unknown"
        for idx, control in enumerate(self.mControls):
            if idx == higwayType:
                result = control[0].name()
        if cycleway:
            result += "+cycleway"
        return result


    #verbose representation of the cost for diagnostics (more expensive that just cost below)
    def costBucket(self, directedWaySection):
        result = super(XMLRouteConfigReadOnly, self).costBucket(directedWaySection)

        #multiple by climb
        climb = directedWaySection.climb()
        if climb:
            factorForClimb = self.mClimbCurve.factorForClimb(climb)
            result.addLinearFactor("climb", str(climb)+"%", factorForClimb)

        #multiple by highway type factor
        highwayIdx = directedWaySection.highwayIndex()
        cycleway    = bool(directedWaySection.cycleway())
        result.addLinearFactor("highway", self.highwayType2Str(highwayIdx, cycleway), self.mHighwaysFactors[highwayIdx][int(cycleway)])

        #cost ready
        return result.finish()

    def cost(self, directedWaySection):
        result = super(XMLRouteConfigReadOnly, self).cost(directedWaySection)

        #multiple by climb
        climb = directedWaySection.climb()
        if climb:
            factorForClimb = self.mClimbCurve.factorForClimb(climb)
            result *= factorForClimb

        #multiple by highway type factor
        highwayIdx = directedWaySection.highwayIndex()
        cycleway    = bool(directedWaySection.cycleway())
        result     *= self.mHighwaysFactors[highwayIdx][int(cycleway)]
        #cost ready (see CostBucket above to clarify the max)
        return max(directedWaySection.distanceMts(), result)

    def highwaySlider(self, elem, cyclewaySlider):

        def on_update(control, value):
            elem.attrib["linearCostFactor"] = str(value)
            self.notifyChanged()

        def on_cycleway_update(control, value):
            elem.attrib["cycleway"] = str(control.value("cycleway"))
            self.notifyChanged()

        mainSlider = RoutingPreferenceSlider(
            elem.tag,
            elem.attrib["linearCostFactor"],
            on_update
        )

        withCycleWaySlider = None
        if elem.attrib.has_key("cycleway"):
            initialValue = None
            if elem.attrib["cycleway"] != "cycleway":
                initialValue = float(elem.attrib["cycleway"])
            withCycleWaySlider = RoutingPreferenceSlider(
                "as cycleway",
                (cyclewaySlider, mainSlider, initialValue),
                on_cycleway_update
            )

        return (mainSlider, withCycleWaySlider)





class XMLRouteConfig(XMLRouteConfigReadOnly):
    def __init__(self, fromFile, copyFromETreeElem):
        super(XMLRouteConfig, self).__init__(copyFromETreeElem)
        self.mSourceFile = fromFile

    @staticmethod
    def fromFile(file):
        parser = etree.XMLParser(remove_blank_text=True)
        return XMLRouteConfig(file, etree.parse(file, parser).getroot())

    def save(self):
        folder = os.path.dirname(self.mSourceFile)
        if not os.path.exists(folder):
            os.makedirs(folder)
        with open(self.mSourceFile, "w") as file:
            file.write(self.toString())
        self.notifyChanged(False)

    def delete(self):
        os.remove(self.mSourceFile)








class RoutingPreferenceControl(object):

    # Constructs a routing prefernece control, with value that can be:
    # a float  - to be considered the adjustable linear cost factor
    # a string - casted to float
    # a tuple  - a cost parameter can be a function of other parameters.
    #            For instance, user can specify global cycle-lane reward to
    #            be applied over a multiplicity of highway types
    #            And so if a cost of riding an A-class road has factor of 3
    #            and the global cycle-lane reward is 0.5, then the cost of
    #            navigating a cycle-lane along such A-class road will be
    #            3x0.5=1.5. Therefore the value can be a tuple containing
    #            two other RoutingPreferenceControl objects. Both of them
    #            will be observed and the value will be product of their
    #            values.
    def __init__(self, name, value, notifyCb=None):
        self.mName          = name
        self.notifyCb       = [notifyCb]
        self.mBox           = None
        self.mInfluencer    = None
        self.mOriginalValue = None

        if type(value) is str:
            value = float(value)
        if type(value) is float:
            self.mOriginalValue = self.mValue = value
        else:
            self.setInfluencer(value)

    def box(self):
        return self.mBox

    def name(self):
        return self.mName

    def isInfluenced(self):
        return bool(self.mInfluencer and not self.mInfluencer[2])

    def setInfluencer(self, influencer):
        self.mInfluencer = (influencer[0], influencer[1], influencer[2])
        self.mInfluencer[0].connect("value_changed", self.on_influencer_changed)
        self.mInfluencer[1].connect("value_changed", self.on_influencer_changed)
        self.toggleIfluencer(influencer[2])

    def toggleIfluencer(self, value):
        if self.mInfluencer:
            self.mInfluencer = (self.mInfluencer[0], self.mInfluencer[1], value)
            if value == None:
                self.on_influencer_changed(self.mInfluencer[0])
            elif value == False:
                self.set(self.mValue)
            else:
                self.set(value)

    def on_influencer_changed(self, foo):
        if self.isInfluenced():
            self.set(self.mInfluencer[0].influenceMyValue(self.mInfluencer[1].value()))

    #returns False if this is intial set (useful for subclasses to ignore during object construction)
    def set(self, value):
        firstAssignment = (self.mOriginalValue == None)
        self.mValue = value
        if not firstAssignment:
            for cb in self.notifyCb:
                cb(self, self.mValue)
        else:
            self.mOriginalValue = value
        return not firstAssignment

    def value(self, valueForInfluencer=None):
        if valueForInfluencer and self.mInfluencer and self.mInfluencer[1]:
            return valueForInfluencer
        return self.mValue

    def connect(self, signal, callback):
        self.notifyCb.append(callback)






class RoutingPreferenceSlider(RoutingPreferenceControl):
    def __init__(self, name, value, notifyCb):
        RoutingPreferenceControl.__init__(self, name, value, notifyCb)
        self.mBox = gtk.HBox(False, 2)
        self.mValueText = gtk.Entry(5)
        self.mValueText.set_width_chars(5)
        self.mValueText.set_editable(gtk.TRUE)
        self.mValueText.set_size_request(50,25)
        self.mValueText.set_text(str(self.value()))
        self.mAdjustment = gtk.Adjustment(
            value=GUISpreadingFunction.linear2Gui(self.value()),
            lower=-1,
            upper=1,
            step_incr=0.05,
            page_incr=0.2,
            page_size=0.0
        )
        self.mAdjustment.connect("value_changed", self.adjustment_changed)
        self.mValueText.connect("activate", lambda entry: self.set(float(entry.get_text())))
        mainLabel = gtk.Label(self.mName)
        mainLabel.set_alignment(0, 0.5)
        mainLabel.set_width_chars(11)
        self.mBox.pack_start(mainLabel, False, False, 0)
        mainLabel.show()

        self.mScrollBar = gtk.HScrollbar(adjustment=self.mAdjustment)
        self.mScrollBar .show()

        self.mBox.pack_start(self.mScrollBar , True, True, 2)
        self.mBox.pack_start(self.mValueText, False, False, 0)

        self.mResetButton = gtk.Button("Reset")
        self.mResetButton.set_size_request(50,25)
        self.mResetButton.set_sensitive(False)
        self.mResetButton.connect("clicked", self.on_reset)
        self.mBox.pack_start(self.mResetButton, False, False, 0)
        self.mResetButton.show()

        lockedBox = gtk.VBox(False, 2)
        lockedBox.set_size_request(75,25)
        if self.mInfluencer:
            checkBox = gtk.CheckButton("Locked")
            if self.isInfluenced():
                checkBox.set_active(True)
                self.mScrollBar.set_sensitive(False)
            else:
                checkBox.set_active(False)
                self.mScrollBar.set_sensitive(True)
            checkBox.connect("toggled", self.on_clutched)
            checkBox.show()
            lockedBox.pack_start(checkBox, False, False, 0)
        self.mBox.pack_start(lockedBox, False, False, 0)
        self.mValueText.show()

    def adjustment_changed(self, adj):
        self.set(GUISpreadingFunction.gui2Linear(adj.value))

    def on_reset(self, foo):
        self.set(self.mOriginalValue)

    def connect(self, signal, callback):
        self.mAdjustment.connect(signal, callback)

    def set(self, value):
        if super(RoutingPreferenceSlider, self).set(value):
            self.mAdjustment.set_value(GUISpreadingFunction.linear2Gui(value))
            value = round(value, 3)
            if not value < 10:
                value = round(value, 2)
            if not value < GUISpreadingFunction.KMaxFactor:
                value = "nan"
            self.mValueText.set_text(str(value))
            if value != self.mOriginalValue and not self.isInfluenced():
                self.mResetButton.set_sensitive(True)
            else:
                self.mResetButton.set_sensitive(False)

    def on_clutched(self, checkBox):
        if checkBox.get_active():
            self.mScrollBar.set_sensitive(False)
            self.toggleIfluencer(True)
        else:
            self.mScrollBar.set_sensitive(True)
            self.toggleIfluencer(False)





class InfluencingSlider(RoutingPreferenceSlider):
    def __init__(self, name, value, notifyCb):
        RoutingPreferenceSlider.__init__(self, name, value, notifyCb)

    def influenceMyValue(self, value):
        return value * self.value()




class ScaledBezierCurve(pyCairoDrawToolkit.BezierCurve):
    def __init__(self, cntrlPoints, currentCoordinateSystem, minMaxClimbs):
        super(ScaledBezierCurve, self).__init__(cntrlPoints)
        self.originalCntrlPoints        = cntrlPoints
        self.currentCoordinateSystem    = currentCoordinateSystem
        self.originalCoordinateSystem   = currentCoordinateSystem
        self.mFactors                   = None
        assert -minMaxClimbs[0] == minMaxClimbs[1], "We do not support assymetric climb ranges, maybe we should"
        self.mMinMaxClimbs              = minMaxClimbs
        self.calculateFactors()


    @staticmethod
    def rescalePoints(fromCoordinateSystem, toCoorindateSystem, controlPts):
        srcSpan = (
            fromCoordinateSystem[0][1]-fromCoordinateSystem[0][0],
            fromCoordinateSystem[1][1]-fromCoordinateSystem[1][0]
        )
        dstSpan = (
            toCoorindateSystem[0][1]-toCoorindateSystem[0][0],
            toCoorindateSystem[1][1]-toCoorindateSystem[1][0]
        )

        newControlPoints = []
        for cntrlPoint in controlPts:
            newControlPoints.append([
                toCoorindateSystem[0][0]+((cntrlPoint[0]-fromCoordinateSystem[0][0])*dstSpan[0])/srcSpan[0],
                toCoorindateSystem[1][0]+((cntrlPoint[1]-fromCoordinateSystem[1][0])*dstSpan[1])/srcSpan[1]
            ])
        return newControlPoints

    def rescaledPoints(self, toCoorindateSystem):
        return ScaledBezierCurve.rescalePoints(self.currentCoordinateSystem, toCoorindateSystem, self.controlPoints)

    def rescale(self, toCoorindateSystem):
        if self.currentCoordinateSystem != toCoorindateSystem:
            self.controlPoints = self.rescaledPoints(toCoorindateSystem)
            self.currentCoordinateSystem = toCoorindateSystem

    def on_mouse_move(self, event):
        if self.draggedPointIdx != None:
            #limit the movement of the edge points, pin to the edge
            if self.draggedPointIdx == len(self.controlPoints)-1:
                #righmost point
                event = ScaledBezierCurve.pinToX(event,
                    self.currentCoordinateSystem[0][1],
                    (self.currentCoordinateSystem[0][1]/2, self.currentCoordinateSystem[0][1]),
                    (self.currentCoordinateSystem[1][0],self.currentCoordinateSystem[1][1])
                )
            elif self.draggedPointIdx == 0:
                #leftmost point
                event = ScaledBezierCurve.pinToX(event,
                    self.currentCoordinateSystem[0][0],
                    (self.currentCoordinateSystem[0][0],self.currentCoordinateSystem[0][1]/2),
                    (self.currentCoordinateSystem[1][0],self.currentCoordinateSystem[1][1])
                )

        return super(ScaledBezierCurve, self).on_mouse_move(event)

    @staticmethod
    def pinToX(event, x, xRange, yRange):
        dx = math.fabs(x - event.x)
        dyup = math.fabs(yRange[0] - event.y)
        dydown = math.fabs(yRange[1] - event.y)
        if dx < dyup and dx < dydown:
            #stick to x
            event.x = float(x)
        elif dyup < dydown:
            #stick to upper y
            event.y = float(yRange[0])
        else:
            #stick to lower y
            event.y = float(yRange[1])

        if event.x < xRange[0]:
            event.x = float(xRange[0])
        if event.y < yRange[1]:
            event.y = float(yRange[1])
        if event.x > xRange[1]:
            event.x = float(xRange[1])
        if event.y > yRange[0]:
            event.y = float(yRange[0])

        return event

    def factorForClimb(self, climbPercent):
        if climbPercent < self.mMinMaxClimbs[0]:
            return self.mFactors[0]
        elif climbPercent > self.mMinMaxClimbs[1]:
            return self.mFactors[1]
        climbPercent += self.mMinMaxClimbs[1]
        return self.mFactors[climbPercent]

    def calculateFactors(self):
        resolution = self.mMinMaxClimbs[1]-self.mMinMaxClimbs[0]+1 #+1 to account for the zero in the middle
        controlPoints = self.rescaledPoints([self.mMinMaxClimbs,self.originalCoordinateSystem[1]])
        path = self.path(resolution*50, controlPoints)
        pathScan = {} #(-1,1 values for climb percentages)
        for point in path:
            pathScan[point[0]] = point[1]
        self.mFactors = []
        soughtClimb = self.mMinMaxClimbs[0]
        while soughtClimb <= self.mMinMaxClimbs[1]:
            closest = None
            for climb,factor in pathScan.iteritems():
                if not closest or math.fabs(soughtClimb-climb) < math.fabs(soughtClimb-closest[0]):
                    closest = (climb,factor)
            self.mFactors.append(GUISpreadingFunction.gui2Linear(closest[1]))
            soughtClimb += 1

    def getControlPts(self):
        return self.rescaledPoints(self.originalCoordinateSystem)

    def generateScale(self, resolution):
        srcYSpan = self.originalCoordinateSystem[1][1]-self.originalCoordinateSystem[1][0]
        dstYSpan = self.currentCoordinateSystem[1][1]-self.currentCoordinateSystem[1][0]

        scale = GUISpreadingFunction.generateScale(resolution/2)
        stepsWithLabels = []
        for point in scale:
            stepsWithLabels.append((
                point[0], #label
                self.currentCoordinateSystem[1][0]+((point[1]-self.originalCoordinateSystem[1][0])*dstYSpan)/srcYSpan
            ))
        return stepsWithLabels




class ClimbWidget(gtk.DrawingArea):

    KDoubleClickDurationSec     = 0.5
    KCurveStyle                 = ((0,0,0  ,1)  , 4)
    KControlPointsStyle         = ((0,0,0.5,0.8), 2)

    def __init__(self, curve, notifyCb, minMaxClimb):
        super(ClimbWidget, self).__init__()
        #register for events
        self.mCurve = curve
        self.connect("expose-event", self.expose)
        self.connect("motion_notify_event", self.motion_notify_event)
        self.connect("button_press_event", self.button_press_event)
        self.connect("button_release_event", self.button_release_event)
        self.connect("size-allocate", self.on_size_allocate)

        self.set_events(gtk.gdk.EXPOSURE_MASK
            | gtk.gdk.BUTTON_PRESS_MASK
            | gtk.gdk.BUTTON_RELEASE_MASK
            | gtk.gdk.POINTER_MOTION_MASK
            | gtk.gdk.POINTER_MOTION_HINT_MASK
            | gtk.gdk.SCROLL_MASK)

        self.lastLeftButtonPressedAt    = time.time()
        self.mNotifyCb                  = notifyCb
        self.mCurrentCoordinateSystem   = None
        self.mMinMaxClimb = minMaxClimb

    def expose(self, widget, event):
        self.draw(widget.window.cairo_create())

    def __val2pix(self, valXY, pixOffset):
        pixx = (valXY[0] * self.allocation.width)/(self.mXRange[1]-self.mXRange[0])
        pixy = -(valXY[1] * self.allocation.width)/(self.mYRange[1]-self.mYRange[0])
        pixx += pixOffset[0]
        pixy += pixOffset[1]
        return (int(pixx), int(pixy))

    def draw(self, cr):
        self.mCurve.rescale(self.mCurrentCoordinateSystem)
        self.mCurve.draw(cr, 100, ClimbWidget.KCurveStyle, ClimbWidget.KControlPointsStyle)
        self.drawGrid(cr)

    def invalidate(self, notify=False):
        self.queue_draw()
        if notify:
            self.mNotifyCb()

    def drawGrid(self, cr):
        scale = self.mCurve.generateScale(10)
        cr.set_source_rgba(0, 0, 0, 0.5)
        cr.set_line_width(1)
        cr.select_font_face("Arial", cairo.FONT_SLANT_NORMAL,
            cairo.FONT_WEIGHT_NORMAL)
        cr.set_font_size(14)

        #vertical divider
        cr.move_to(self.allocation.width/2, 0)
        cr.line_to(self.allocation.width/2, self.allocation.height)
        cr.stroke()
        climb        = 0
        climbTo      = self.mMinMaxClimb[1]
        verticalFrom = self.allocation.width/2
        for i in range(2):
            climbTo /= 2.0
            climb += climbTo
            verticalFrom += (self.allocation.width - verticalFrom)/2
            cr.move_to(verticalFrom, 0)
            cr.line_to(verticalFrom, self.allocation.height)
            cr.stroke()
            cr.move_to(2+verticalFrom, self.allocation.height/2-2)
            cr.show_text(str(climb)+"%")
        descent      = 0
        descentTo    = self.mMinMaxClimb[0]
        verticalFrom = self.allocation.width/2
        for i in range(2):
            descentTo /= 2.0
            descent += descentTo
            verticalFrom -= verticalFrom/2
            cr.move_to(verticalFrom, 0)
            cr.line_to(verticalFrom, self.allocation.height)
            cr.stroke()
            cr.move_to(2+verticalFrom, self.allocation.height/2-2)
            cr.show_text(str(descent)+"%")

        for scalePoint in scale:
            factor = scalePoint[0]
            cr.set_source_rgba(min(factor*0.04,1), min(0.04/factor,1), 0, 0.3)
            cr.move_to(0, scalePoint[1])
            cr.line_to(self.allocation.width, scalePoint[1])
            cr.stroke()
            cr.move_to(2+self.allocation.width/2, scalePoint[1]-2)
            cr.show_text(str(round(factor,4)))


        cr.set_source_rgba(0.2, 0.8, 0.2, 1)
        cr.move_to(2+self.allocation.width/2, self.allocation.height-2)
        cr.show_text("0")

        cr.set_source_rgba(1, 0, 0, 1)
        cr.move_to(2+self.allocation.width/2, 12)
        cr.show_text(u"\u221E")



    def motion_notify_event(self, widget, event):
        if self.mCurve.on_mouse_move(event):
            self.invalidate()

    def button_press_event(self, widget, event):
        if event.button == 1:
            pressedAt = time.time()
            td = pressedAt - self.lastLeftButtonPressedAt
            self.lastLeftButtonPressedAt = pressedAt
            if td < ClimbWidget.KDoubleClickDurationSec:
                #dbl click
                if self.mCurve.highlightedControlPointIdx:
                    self.mCurve.removeControlPointPair(self.mCurve.highlightedControlPointIdx)
                else:
                    self.mCurve.insertControlPointPair([[event.x-10, event.y],[event.x+10, event.y]], len(self.mCurve.controlPoints)/2)
                self.invalidate(True)
                self.lastLeftButtonPressedAt = 0
            else:
                #single click
                if self.mCurve.on_mouse_down(event):
                    self.invalidate()

    def button_release_event(self, widget, event):
        if self.mCurve.on_mouse_up(event):
            self.invalidate(True)

    def on_size_allocate(self, widget, allocation):
        self.mCurrentCoordinateSystem = [[0,self.allocation.width],[self.allocation.height,0]]