# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# (C) 2016 Remek Zajac
#!/usr/bin/env python

import sys
import json

import pyGeometry
import pyRouteView
import pyCairoDrawToolkit

class LogView(object):

    KRouteBeginMarker  = "Verbose route dump: ["
    KRouteLineTag      = "[routing]>"
    KTrafficSignalsTag = "[Cost::TrafficSignals]> "

    def __init__(self, filePath, referenceMap=None):
        self.mReferenceMap = referenceMap
        self.mWayViews = []
        with open(filePath) as routeFile:
            fileLines = routeFile.readlines()
            self.extractTrafficSignals(fileLines)
            routeText = "".join(
                filter(lambda line: line.startswith(LogView.KRouteLineTag), fileLines)
            ).replace("[routing]>", "")
            try:
                self.analyseRoute(routeText)
            except:
                prevRoute = None #in case it needs merging with the followup
                while True:
                    routeBeginIdx = routeText.find(LogView.KRouteBeginMarker)
                    if routeBeginIdx != -1:
                        routeText = routeText[routeBeginIdx+len(LogView.KRouteBeginMarker):]
                        stack  = 0
                        endIdx = -1
                        for i in range(0,len(routeText)):
                            if routeText[i] == "[":
                                stack+=1
                            elif routeText[i] == "]":
                                stack-=1
                            if stack == -1:
                                endIdx = i
                                break
                        route = routeText[0:endIdx-3]
                        route = json.loads("["+route+"]")
                        route.reverse() #route dump is back to forth
                        if prevRoute and LogView.isMergable(prevRoute, route):
                            prevRoute += route
                        else:
                            prevRoute = route
                    else:
                        break
                if prevRoute:
                    self.analyseRoute(prevRoute)

    @staticmethod
    def isMergable(routePrev, routeNext):
        lastOfPrev  = routePrev[-1]
        firstOfNext = routeNext[0]
        lastOfPrev  = pyGeometry.GeoPoint(lastOfPrev["routeEdge"]["nodes"][-1])
        firstOfNext = pyGeometry.GeoPoint(firstOfNext["routeEdge"]["nodes"][0])
        return lastOfPrev.equals(firstOfNext)

    def extractTrafficSignals(self, fileLines):
        self.mTrafficSignals = map(
            lambda line: json.loads(line[len(LogView.KTrafficSignalsTag):]),
            filter(lambda line: line.startswith(LogView.KTrafficSignalsTag), fileLines)
        )


    def analyseRoute(self, route):
        distance = 0
        climb = 0
        cost = 0
        points = []
        for edge in route:
            d = edge["routeEdge"]["distanceMts"]
            c = edge["routeEdge"]["climb"]
            if len(points) == 0:
                point = json.loads(
                    json.dumps(
                        edge["routeEdge"]["nodes"][0]
                    )
                )
                points.append(point)
            point = json.loads(
                json.dumps(
                    edge["routeEdge"]["nodes"][-1]
                )
            )
            point.append(edge)
            points.append(point)
            distance += d
            if c > 0:
                climb += d * c
            cost += edge["cost"]
        print "total distance: %s, total cost: %s, climb: %s" % (distance, cost, climb)
        if self.mReferenceMap:
            self.mWayViews.append(
                pyRouteView.RouteView(self.mReferenceMap, points)
            )

    def region(self):
        region = pyGeometry.GeoRegion()
        for wayView in self.mWayViews:
            region.add(wayView.region())
        return region

    def draw(self, cr):
        for trafficSignal in self.mTrafficSignals:
            pyCairoDrawToolkit.TrafficSignals(cr, self.mReferenceMap.latlon2pix).draw(trafficSignal)
        for wayView in self.mWayViews:
            wayView.draw(cr)

    def invalidate(self, who):
        for wayView in self.mWayViews:
            wayView.invalidate(who)

    def clear(self):
        for wayView in self.mWayViews:
            wayView.clear()







#drop the zikes engine log here and it will offer a cost/distance/climb report on all
#routes found within it
def main():
    for filePath in sys.argv[1:]:
        LogView(filePath)


if __name__ == "__main__":
    main()
