# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# (C) 2012 Remek Zajac
#!/usr/bin/python

import cairo
import math
import gtk
import cairo
from random import randint

#local modules
import pyGeometry
from osmsql import pyBrowse as pyBrowseOsmDB



class WayView(object):

    #Do not bother showing climb percentages for a way
    #if at no point the way climbs or descends by more
    #than this many percents (i.e.: its profile is boring)
    KMinClimbToShowPercent = 3

    def __init__(self, wayNodesBrowser, refmap, diagnose=False):
        super(WayView, self).__init__()
        self.mDrawnSections         = []
        self.mWayNodesBrowser       = wayNodesBrowser
        self.mRefMap                = refmap
        self.mElevationViewWindow   = None
        self.mDistanceMts           = None
        self.mDiagnose              = diagnose
        self.mRegion                = pyGeometry.GeoRegion()
        self.mWayName               = None

    def showWayId(self, wayid):
        climbingWay = self.mWayNodesBrowser.getWay(wayid)
        self.showWaySections(climbingWay)

    def name(self):
        return self.mWayName

    @staticmethod
    def showWaysNearestTo(wayNodesBrowser, refmap, latlon):
        return WayView.showWaysIntersectingNode(
            wayNodesBrowser,
            refmap,
            wayNodesBrowser.getNearest(latlon).id()
        )

    @staticmethod
    def showWaysIntersectingNode(wayNodesBrowser, refmap, nodeId):
        resultingViews = []
        climbingWays = wayNodesBrowser.getWaysIntersectingNode(nodeId)
        print "**************************************************************"
        print "Found : %s ways intersecting node with id %s" % (len(climbingWays), nodeId)
        for climbingWay in climbingWays:
            newView = WayView(wayNodesBrowser, refmap, True)
            newView.showWaySections(climbingWay)
            resultingViews.append(newView)
        return resultingViews

    def showWaySections(self, way):
        if self.mDiagnose:
            print "\n\nShowing way: %s" % way.id()
            print " attributes: %s" % way.attrs()
        self.showSections(self.mWayNodesBrowser.getWaySectionsForWayId(way.id()), "id: "+str(way.id()))

    def showWay(self, way):
        self.showSections(way.sections(), "id: "+str(way.id()))

    def region(self):
        return self.mRegion

    #waySections can either be verbatim from the db
    #or reconstructed on the spot. The latter is useful for
    #elevation smoothing diagnostics
    def showSections(self, waySections, name):
        self.mWayName = name
        self.mDistanceMts = 0
        prevNode = None
        combinedElevations  = []
        showClimbs = False

        for section in waySections:
            if section.climb() and math.fabs(section.climb()) > WayView.KMinClimbToShowPercent:
                showClimbs = True
            if self.mDiagnose:
                diagnoseStr = " WaySection begin, climb: %s%%, distanceMts: %s, oneway: %s, cycleway: (along: %s, reverse: %s), attrs: %s" % (section.climb(), section.distanceMts(), section.oneway(), section.cycleway()[0], section.cycleway()[1], section.attrs())
                if hasattr(section, "highwayType"):
                    diagnoseStr += " highwayType "+str(section.highwayType())
                print diagnoseStr
            for node in section.nodes():
                if prevNode:
                    distanceFromPrevious = prevNode.distanceMts(node)
                    self.mDistanceMts += distanceFromPrevious
                    combinedElevations.append((node.eleMts(), distanceFromPrevious))
                else:
                    combinedElevations.append((node.eleMts(), 0))
                if self.mDiagnose:
                    print "  node id: "+str(node.id())+", Elevation mts: "+str(node.eleMts())
                prevNode = node
                self.mRegion.add(node.latlon())

        mDistanceMts = combinedElevations[-1][1]
        if showClimbs and len(combinedElevations) > 0:
            labelStr = "Way ["+name+"] elevation profile; "
            self.mElevationViewWindow = WayElevationViewWindow()
            self.mElevationViewWindow.addElevationProfile(combinedElevations,  labelStr+"fine grained view")
            combinedClimbs = WayElevationViewWidget.fromWaySections(waySections)
            self.mElevationViewWindow.addElevationProfile(combinedClimbs, labelStr+"smoothed view")
            self.mElevationViewWindow.show()


        self.mDrawnSections = []
        for idx, waySection in enumerate(waySections):
            if showClimbs:
                self.mDrawnSections.append(WaySection(self.mRefMap, waySection, self.mDiagnose, (randint(20,100), bool(idx%2))))
            else:
                self.mDrawnSections.append(WaySection(self.mRefMap, waySection, self.mDiagnose))



    def distanceMts(self):
        return self.mDistanceMts

    def draw(self, cr):
        previousPix = None
        for drawnSection in self.mDrawnSections:
            previousPix = drawnSection.draw(cr, previousPix)

    def invalidate(self, parent):
        pass

    def clear(self):
        self.mDrawnSections = []
        self.mDistanceMts   = None
        if self.mElevationViewWindow:
            self.mElevationViewWindow.hide()



class WaySection(object):
    KArrowHeadSizePix       = 8
    KMaxClimb               = 20
    KMaxZoomForEleText      = 17
    KMaxZoomForClimbText    = 14
    KMaxZoomForArrow        = 14
    KMinClimbToShow         = 2
    KPathWidthPix           = 8
    KMinPointDistancePix    = 8
    KMinArrowDistancePix    = 20

    def __init__(self, parent, waySection, diagnose, climbPosPix=None):
        super(WaySection, self).__init__()
        self.mRefMap        = parent
        self.mWaySection    = waySection
        red                 = 0
        green               = 0
        if waySection.climb() != None:
            red             = ((self.mWaySection.climb()+WaySection.KMaxClimb)/(2.0*WaySection.KMaxClimb))
            green           = (1-red)
        self.color          = (red, green, 0.5, 0.7)
        self.mClimbPosPix   = climbPosPix
        self.mDiagnose      = diagnose


    def draw(self, cr, prevNodePix):
        climbWritten = False
        cr.set_font_size(12)
        pixPoints = []
        prevArrowPix = prevNodePix

        for idx, node in enumerate(self.mWaySection.nodes()):
            nodePix = self.mRefMap.latlon2pix(node.latlon())

            distFromPrev = WaySection.KMinPointDistancePix
            if prevNodePix:
                distFromPrev = math.sqrt(
                    (prevNodePix[0]-nodePix[0]) ** 2 + (prevNodePix[1]-nodePix[1]) ** 2
                )

            if distFromPrev >= WaySection.KMinPointDistancePix:
                if self.mDiagnose and self.mRefMap.zoom >= WaySection.KMaxZoomForEleText:
                    cr.select_font_face("Arial", cairo.FONT_SLANT_NORMAL,
                        cairo.FONT_WEIGHT_NORMAL)
                    cr.set_source_rgba(0,0,0,0.7)
                    cr.move_to(nodePix[0]+5, nodePix[1]+5)
                    #text = str(node.eleMts())+"m"
                    text = "id:"+str(node.id())
                    cr.show_text(text)

                if prevNodePix:
                    cr.set_line_width(WaySection.KPathWidthPix)
                    cr.set_source_rgba(self.color[0], self.color[1], self.color[2], self.color[3])
                    cr.move_to(prevNodePix[0], prevNodePix[1])
                    cr.line_to(nodePix[0], nodePix[1])
                    cr.stroke()

                if (self.mClimbPosPix and
                    math.fabs(self.mWaySection.climb()) > WaySection.KMinClimbToShow and
                    self.mRefMap.zoom >= WaySection.KMaxZoomForClimbText and
                    not climbWritten and
                    idx >= len(self.mWaySection.nodes())/2):

                    cr.select_font_face("Arial", cairo.FONT_SLANT_NORMAL,
                        cairo.FONT_WEIGHT_BOLD)
                    cr.set_line_width(1)
                    text = str(self.mWaySection.climb())+"% / "+str(self.mWaySection.distanceMts())+"m"
                    line = pyGeometry.Line(prevNodePix, nodePix)
                    vector = pyGeometry.Vector((prevNodePix, nodePix)).orthogonal().setLength(self.mClimbPosPix[0])
                    if self.mClimbPosPix[1]:
                        vector = vector.reverse()
                    writeAtPix = vector.addToPoint(line.middle())
                    cr.set_source_rgba(0,0,0,0.7)
                    cr.move_to(nodePix[0], nodePix[1])
                    cr.line_to(writeAtPix[0], writeAtPix[1])
                    cr.line_to(writeAtPix[0]+30, writeAtPix[1])
                    cr.stroke()
                    cr.move_to(writeAtPix[0], writeAtPix[1])
                    cr.set_source_rgba(0,0,0,1)
                    cr.show_text(text)
                    climbWritten = True

                prevNodePix = nodePix
                pixPoints.append(nodePix)

        if len(pixPoints) >= 2 and self.mRefMap.zoom >= WaySection.KMaxZoomForArrow:
            distFromPrevArrow = WaySection.KMinArrowDistancePix
            if prevArrowPix:
                distFromPrevArrow = math.sqrt(
                    (prevArrowPix[0]-nodePix[0]) ** 2 + (prevArrowPix[1]-nodePix[1]) ** 2
                )
            if distFromPrevArrow >= WaySection.KMinArrowDistancePix:
                self.drawArrow(cr, pixPoints)
        return prevNodePix


    def drawArrow(self, cr, pixPoints):
        cr.set_source_rgba(self.color[0], self.color[1], self.color[2], 1)
        arrowTip     = pixPoints[-1]
        arrowRootIdx = -2
        while (math.fabs(arrowRootIdx) <= len(pixPoints) and
              math.fabs(pixPoints[arrowRootIdx][0]-arrowTip[0]) < WaySection.KArrowHeadSizePix and
              math.fabs(pixPoints[arrowRootIdx][1]-arrowTip[1]) < WaySection.KArrowHeadSizePix):
            arrowRootIdx -= 1

        if math.fabs(arrowRootIdx) <= len(pixPoints):
            arrowRoot = pixPoints[arrowRootIdx]
            lastSegmentVector = pyGeometry.Vector((arrowTip, arrowRoot))
            lastSegmentVector = lastSegmentVector.setLength(WaySection.KArrowHeadSizePix)
            arrowWingVector1 = lastSegmentVector.orthogonal()
            arrowWingVector2 = arrowWingVector1.reverse()
            arrowWingVector1 = arrowWingVector1.add(lastSegmentVector.setLength(2*WaySection.KArrowHeadSizePix))
            arrowWingVector2 = arrowWingVector2.add(lastSegmentVector.setLength(2*WaySection.KArrowHeadSizePix))
            arrowPoint = arrowWingVector1.addToPoint(arrowTip)
            cr.move_to(arrowPoint[0], arrowPoint[1])
            arrowPoint = arrowWingVector2.addToPoint(arrowTip)
            cr.line_to(arrowPoint[0], arrowPoint[1])
            cr.line_to(arrowTip[0], arrowTip[1])
            cr.close_path()
            cr.fill()





class WayElevationViewWindow(gtk.Window):
    KResizeForEachByY = 300

    def __init__(self):
        super(WayElevationViewWindow, self).__init__()
        self.currentSize = 0
        self.set_title("Way Elevation View")
        self.set_position(gtk.WIN_POS_CENTER)
        self.set_transient_for(None)
        self.resize(600, WayElevationViewWindow.KResizeForEachByY)
        self.outerContainerBox = gtk.VBox(True, 2)
        self.outerContainerBox.set_border_width(4)
        self.add(self.outerContainerBox)

        self.connect("destroy", self.on_destroy)

    def on_destroy(self, foo):
        self.hide()

    def addElevationProfile(self, elevationProfile, description):
        eleWidget = WayElevationViewWidget(elevationProfile, description)
        self.outerContainerBox.pack_start(eleWidget.gui())
        self.currentSize += WayElevationViewWindow.KResizeForEachByY
        self.resize(600, self.currentSize)

    def show(self):
        self.show_all()



class WayElevationViewWidget(gtk.DrawingArea):

    def __init__(self, elevationProfile, description):
        super(WayElevationViewWidget, self).__init__()
        self.connect("expose-event", self.expose)
        self.set_events(gtk.gdk.EXPOSURE_MASK)
        self.mDistanceMts      = 0
        self.mMinElevation     = None
        self.mMaxElevation     = None
        self.mElevationProfile = elevationProfile
        for elevationPoint in self.mElevationProfile:
            self.mDistanceMts += elevationPoint[1]
            if not self.mMinElevation:
                self.mMinElevation = self.mMaxElevation = int(elevationPoint[0])
            else:
                self.mMinElevation = int(min(self.mMinElevation, elevationPoint[0]))
                self.mMaxElevation = int(max(self.mMaxElevation, elevationPoint[0]))
        self.mGuiBoxFrame = gtk.Frame()
        hbox = gtk.HBox(False, 2)
        leftVbox = gtk.VBox(False, 2)
        maxEleLabel = gtk.Label(str(self.mMaxElevation)+"m")
        leftVbox.pack_start(maxEleLabel, False, False, 2)
        dEleLabel = gtk.Label(str(self.mMaxElevation-self.mMinElevation)+"m")
        leftVbox.pack_start(dEleLabel, True, True, 2)
        minEleLabel = gtk.Label(str(self.mMinElevation)+"m")
        leftVbox.pack_end(minEleLabel, False, False, 2)

        bottomHBox = gtk.HBox(False, 2)
        startDistanceLabel = gtk.Label("0m")
        bottomHBox.pack_start(startDistanceLabel, False, False, 2)

        proportionalCheckBoxHBox = gtk.HBox(True, 2)
        self.mProportionalCheckBox = gtk.CheckButton("Proportional axes")
        self.mProportionalCheckBox.connect("toggled", self.on_proportionalCheckBox)
        proportionalCheckBoxHBox.pack_start(self.mProportionalCheckBox, False, False, 2)

        bottomHBox.pack_start(proportionalCheckBoxHBox, True, True, 2)
        endDistanceLabel = gtk.Label(str(self.mDistanceMts)+"m")
        bottomHBox.pack_start(endDistanceLabel, False, False, 2)

        mainBox = gtk.VBox(False, 2)
        mainBox.pack_start(self, True, True, 2)
        mainBox.pack_start(bottomHBox, False, False, 2)

        hbox.pack_start(leftVbox, False, False, 2)
        hbox.pack_start(mainBox, True, True, 2)

        vbox = gtk.VBox(False, 0)
        descriptionLabel = gtk.Label(description)
        vbox.pack_start(descriptionLabel, False, False, 2)
        vbox.pack_start(hbox, True, True, 2)
        self.mGuiBoxFrame.add(vbox)

    def on_proportionalCheckBox(self, checkButton):
        self.invalidate()

    @staticmethod
    def fromWaySections(waySections):
        result = []
        elevationMts    = waySections[0].nodes()[0].eleMts()
        result.append((elevationMts, 0))
        for waySection in waySections:
            elevationMts = (waySection.climb()*waySection.distanceMts()/100.0)+elevationMts
            result.append((elevationMts, waySection.distanceMts()))
        return result

    def gui(self):
        return self.mGuiBoxFrame

    def expose(self, widget, event):
        self.draw(widget.window.cairo_create())

    def draw(self, cr):
        cr.set_source_rgba(0,0,0,0.7)
        self.drawProfile(cr)

    def drawProfile(self, cr):
        halfHeight = self.allocation.height/2
        eleSpan = self.mMaxElevation-self.mMinElevation
        xFactor = 1.0 * self.allocation.width / self.mDistanceMts

        additionForFlatProfile = 0
        if self.mProportionalCheckBox.get_active():
            yFactor = xFactor
            additionForFlatProfile = halfHeight
        else:
            if eleSpan:
                yFactor = 1.0 * self.allocation.height / eleSpan
            else:
                yFactor = 1.0
                additionForFlatProfile = halfHeight

        distanceFromStartMts = 0
        cr.move_to(0, halfHeight)
        for elevationPoint in self.mElevationProfile:
            distanceFromStartMts += elevationPoint[1]
            xTo = distanceFromStartMts*xFactor
            yTo = self.allocation.height-((elevationPoint[0]-self.mMinElevation)*yFactor) - additionForFlatProfile
            cr.line_to(xTo, yTo)
        cr.line_to(self.allocation.width, self.allocation.height)
        cr.line_to(0, self.allocation.height)
        cr.close_path()
        cr.fill()

    def invalidate(self, notify=False):
        self.queue_draw()

