# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# (C) 2016 Remek Zajac

#!/usr/bin/python
import abc
import os
import random
import tempfile
import time

import errno
import paramiko

from lib import pyUtils
from lib.pyUtils import FileUtils


class Artifact(object):

    KDefaultReadyState = "up to date"

    class StalePolicy(object):
        def __init__(self, afterSec=None, randomiseFactor=0):
            self.mAfterMs = None if afterSec is None else afterSec*1000
            self.mRandomiseFactor = randomiseFactor

        def isStale(self, timestampMs, dependencies, referenceMs):
            if not self.mAfterMs or timestampMs is None:
                return timestampMs is None
            dTime = ((referenceMs-timestampMs)/self.mAfterMs)*(1 + (self.mRandomiseFactor * random.random()))
            return dTime > 1

    def __init__(self,
                 name,
                 stalePolicy=StalePolicy(),
                 dependsOn=None,
                 resourceSemaphore=None,
                 readyState=KDefaultReadyState
                 ):
        self.mStalePolicy = stalePolicy
        self.mDependsOn = dependsOn if dependsOn else []
        if not isinstance(self.mDependsOn, list):
            self.mDependsOn = [self.mDependsOn]
        for dep in self.mDependsOn:
            assert isinstance(dep, Artifact), "Can only depend on other Artifacts"
        self.mResourceSemaphore = resourceSemaphore
        self.mDependencyRunning = None
        self.mName = name
        self.mStatusUpdated  = True
        self.mState          = "not ready"
        self.mActivity       = "idle"
        self.mStatusConsumed = False
        self.mFinishedState  = readyState

    def name(self):
        return self.mName

    def isStale(self, timestamp=-1):
        return self.mStalePolicy.isStale(
            timestamp if timestamp is not -1 else self.timestamp(),
            self.mDependsOn,
            pyUtils.currentTimeMillis()
        )

    def make(self, force=False):
        self.updateStatus(activity="starting")
        timestamp = None if force else self.timestamp()
        if force or self.isStale(timestamp):
            for dependency in self.mDependsOn:
                self.mDependencyRunning = dependency
                dependency.make(force)
                self.mState, self.mActivity = dependency._consumeStatus()
            self.mDependencyRunning = None
            try:
                self.updateStatus(activity="awaiting system resources")
                if self.mResourceSemaphore:
                    self.mResourceSemaphore.acquire()
                self.updateStatus(activity="running")
                self._make()
                self._finished()
                self.updateStatus(state=self.mFinishedState)
            except Exception as e:
                self.updateStatus(state="Failed with: '%s'" % e.message)
                self._finished(e)
                raise
            finally:
                if self.mResourceSemaphore:
                    self.mResourceSemaphore.release()

        else:
            self.updateStatus(state=self.mFinishedState)

    def updateStatus(self, state=None, activity=None):
        if state and state != self.mState:
            self.mStatusUpdated = True
            self.mState         = state
        if activity != self.mActivity:
            self.mActivity      = activity
            self.mStatusUpdated = True

    def _consumeStatus(self):
        if self.mDependencyRunning:
            self.mState, self.mActivity = self.mDependencyRunning._consumeStatus()
        self.mStatusUpdated = False
        return self.mState, self.mActivity

    def consumeStatus(self):
        state, activity = self._consumeStatus()
        activityStr = " (%s)" % activity if activity else ""
        return ("%s%s" % (state, activityStr)).ljust(100)

    def hasStatusUpdate(self):
        if self.mDependencyRunning:
            return self.mDependencyRunning.hasStatusUpdate()
        return self.mStatusUpdated

    def _finished(self, exception=None):
        pass

    @abc.abstractmethod
    def _make(self): pass

    @abc.abstractmethod
    def timestamp(self): pass





class LocalArtifact(Artifact):

    @staticmethod
    def _defaultName(name, paths):
        if name:
            return name
        if paths:
            return os.path.commonprefix(paths)
        return "Anonymous Local Artefact"

    def __init__(self,
                 paths=None,
                 name=None,
                 stalePolicy=Artifact.StalePolicy(),
                 dependsOn=None,
                 resourceSemaphore=None,
                 readyState=Artifact.KDefaultReadyState
                 ):
        super(LocalArtifact, self).__init__(
            name=LocalArtifact._defaultName(name, paths),
            stalePolicy=stalePolicy,
            dependsOn=dependsOn,
            resourceSemaphore=resourceSemaphore,
            readyState=readyState
        )
        self.mPaths = paths if paths else [tempfile.mkdtemp()]
        self.mPlaceHolderPath = None

    def __del__(self):
        if self.mPlaceHolderPath:
            FileUtils.rm(self.mPlaceHolderPath)

    def timestamp(self):
        result = pyUtils.currentTimeMillis()
        for path in self.mPaths:
            if not os.path.exists(path):
                return None
            result = min(result, int(os.path.getmtime(path)*1000))
        return result


    def _getPlaceholderPath(self):
        if not self.mPlaceHolderPath:
            self.mPlaceHolderPath = tempfile.mkdtemp()
        return self.mPlaceHolderPath

    def _mvFile(self, heldFiles, dst):
        for heldFile in heldFiles:
            if dst.endswith(heldFile):
                FileUtils.rm(dst)
                FileUtils.mv(
                    os.path.join(
                        self.mPlaceHolderPath,
                        heldFile
                    ),
                    dst
                )
                return
        assert False, "%s not found" % dst

    def _finished(self, exception=None):
        if self.mPlaceHolderPath:
            if not exception and len(self.mPaths) > 0:
                try:
                    heldFiles = {f: True for f in FileUtils.listFiles(self.mPlaceHolderPath, True)}
                    for path in self.mPaths:
                        self._mvFile(heldFiles, path)
                finally:
                    FileUtils.rm(self.mPlaceHolderPath)
                    self.mPlaceHolderPath = None

    def get(self):
        return self.mPaths


class Sftp(object):

    class Session(object):
        def __init__(self, host, username, pkey):
            self.mTransport = None
            self.mSftp = None
            self.mHost = host
            self.mUsername = username
            self.mPkey = paramiko.RSAKey.from_private_key_file(pkey)

        def __enter__(self):
            self.mTransport = paramiko.Transport((self.mHost, 22))
            self.mTransport.connect(username=self.mUsername, password="", pkey=self.mPkey)
            self.mSftp = paramiko.SFTPClient.from_transport(self.mTransport)
            return self.mSftp

        def __exit__(self, exc_type, exc_val, exc_tb):
            self.mSftp.close()
            self.mTransport.close()
            self.mTransport = None
            self.mSftp = None

    def __init__(self, host, username, pkey):
        self.mHost = host
        self.mUsername = username
        self.mPkey = pkey

    def session(self):
        return Sftp.Session(self.mHost, self.mUsername, self.mPkey)

    @staticmethod
    def rm(sftp, remotePath):
        try:
            files = sftp.listdir(remotePath)
            for f in files:
                filepath = os.path.join(remotePath, f)
                try:
                    sftp.remove(filepath)
                except IOError:
                    Sftp.rm(sftp, filepath)
            sftp.rmdir(remotePath)
        except IOError as e:
            if e.errno == errno.ENOENT:
                return
            raise e


    @staticmethod
    def mkdirs(sftp, path):
        assert len(path) > 0 and path != "."
        try:
            sftp.listdir(path)
            return
        except IOError as e:
            if e.errno != errno.ENOENT:
                raise e
        try:
            sftp.mkdir(path)
        except IOError as e:
            Sftp.mkdirs(sftp, os.path.dirname(path))
            sftp.mkdir(path)



class RemoteArtifact(Artifact):

    def __init__(self,
                 host,
                 username,
                 pkey,
                 remotePaths,
                 localArtifact,
                 name=None,
                 stalePolicy=Artifact.StalePolicy(),
                 resourceSemaphore=None,
                 readyState=Artifact.KDefaultReadyState
        ):
        super(RemoteArtifact, self).__init__(
            name=(name if name else "%s@%s:%s" % (
                    username, host, remotePaths[0] if isinstance(remotePaths, list) else remotePaths)),
            stalePolicy=stalePolicy,
            dependsOn=localArtifact,
            resourceSemaphore=resourceSemaphore,
            readyState=readyState)
        self.mSftp = Sftp(host, username, pkey)
        self.mRemotePaths = {}
        self.mPlaceholderPath = "./temp/temp_ra_%s" % pyUtils.currentTimeMillis()
        if not isinstance(remotePaths, list):
            remotePaths = [remotePaths]
        for path in remotePaths:
            dir = os.path.dirname(path)
            if dir not in self.mRemotePaths:
                self.mRemotePaths[dir] = {}
            filename = os.path.basename(path)
            self.mRemotePaths[dir][filename] = os.path.join(dir, filename)


    def timestamp(self):
        result = None
        with self.mSftp.session() as sftp:
            for remoteRoot, expectedRemotes in self.mRemotePaths.items():
                try:
                    remoteListing = {fileAttr.filename: fileAttr for fileAttr in sftp.listdir_attr(remoteRoot)}
                    for expectedRemote in expectedRemotes:
                        if expectedRemote not in remoteListing:
                            return None
                        foundRemoteAttr = remoteListing[expectedRemote]
                        result = foundRemoteAttr.st_mtime if not result else min(result, foundRemoteAttr.st_mtime)
                except IOError as e:
                    if e.errno == errno.ENOENT:
                        return None
        return result*1000 if result is not None else None


    def _finished(self, exception=None):
        self.updateStatus(activity="finalising")
        with self.mSftp.session() as sftp:
            if not exception:
                try:
                    for path in self.mRemotePaths:
                        Sftp.rm(sftp, path)
                        Sftp.mkdirs(sftp, os.path.dirname(path))
                        sftp.rename(
                            os.path.normpath(
                                os.path.join(self.mPlaceholderPath, path)
                            ),
                            path
                        )
                finally:
                    Sftp.rm(sftp, self.mPlaceholderPath)

    def _uploadFile(self, sftp, local, remote, retries=5, sleepSec=2):
        self.updateStatus(activity="uploading ->%s" % remote)
        try:
            Sftp.rm(sftp, remote)
            sftp.put(local, remote)
            return
        except IOError as e:
            time.sleep(sleepSec)
            if retries < 0:
                raise
            with self.mSftp.session() as newSftp:
                self._uploadFile(newSftp, local, remote, retries=retries-1, sleepSec=sleepSec*3)


    def _uploadPath(self, sftp, localSources, relLocal, dstRoot, ):
        absRemote = os.path.normpath(
            os.path.join(dstRoot, relLocal)
        )
        for localPath in localSources:
            if localPath.endswith(relLocal):
                self._uploadFile(sftp, localPath, absRemote)
                return
        assert False, "'%s' no found for upload"


    def _make(self):
        localFiles = reduce(lambda cum, path: FileUtils.listFiles(path, True) + cum, self.mDependsOn[0].get(), [])
        assert len(localFiles) > 0, "No files to upload"
        assert len(self.mRemotePaths) > 0, "No files to upload"
        with self.mSftp.session() as sftp:
            Sftp.mkdirs(sftp, self.mPlaceholderPath)
            for root in self.mRemotePaths:
                placeholderRoot = os.path.join(self.mPlaceholderPath, root)
                Sftp.mkdirs(sftp, placeholderRoot)
                for file in self.mRemotePaths[root]:
                    self._uploadPath(sftp, localFiles, file, placeholderRoot)
