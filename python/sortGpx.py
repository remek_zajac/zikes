# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# (C) 2015 Remek Zajac
#!/usr/bin/env python

import sys
import os
import errno
import filecmp

from lib import pyGPX

class Walker(object):

    def __init__(self, srcFolder, dstFolder, speedBounds, minDistanceKm):
        self.mSrcFolder      = srcFolder
        self.mDstFolder      = dstFolder
        self.mSpeedBounds    = speedBounds
        self.mMinDistanceKm  = minDistanceKm
        self.mFilesSeen      = 0
        self.mTracksSeen     = 0
        self.mTracksOK       = 0
        self.mTracksBAD      = 0
        self.mCollisions     = 0

    def do(self):
        self.processFolder(self.mSrcFolder)

    def processFolder(self, srcFolder):
        print "Processing folder "+str(srcFolder)
        srcContents = os.listdir(srcFolder)
        for fileOrFolder in srcContents:
            fileOrFolder = srcFolder+"/"+fileOrFolder
            if os.path.isfile(fileOrFolder):
                if fileOrFolder.endswith(".gpx"):
                    self.processFile(fileOrFolder)
            elif os.path.isdir(fileOrFolder):
                self.processFolder(fileOrFolder)

    @staticmethod
    def _newTrack(tracks, track):
        tracks.append(track)
        return track

    @staticmethod
    def mkdirs(path):
        try:
            os.makedirs(path)
        except OSError as exc: # Python >2.5
            if exc.errno == errno.EEXIST and os.path.isdir(path):
                pass
            else: raise

    def processFile(self, file):
        self.mFilesSeen += 1
        print "visited [files: %d, tracks:%d] processed: [accepted:%d, collided:%d, unparsable:%d] %s" % \
              (self.mFilesSeen, self.mTracksSeen, self.mTracksOK, self.mCollisions, self.mTracksBAD, os.path.basename(file))
        tracks = []
        parser = pyGPX.ParseGPX()
        try:
            parser.parse(file, lambda: Walker._newTrack(tracks, pyGPX.GPXTrack()))
            for track in tracks:
                self.mTracksSeen += 1
                lat,lon     = track.mHead.latlon()
                lat         = int(lat)
                lon         = int(lon)
                dstFolder   = self.mDstFolder+"/"+str(lat)+"/"+str(lon)
                dstFileName = track.hash()
                dstFileBase = dstFolder + "/" + dstFileName + ".gpx"
                dstFile     = dstFileBase
                seq = 0
                while(os.path.isfile(dstFile)):
                    seq += 1
                    altDstFile = dstFolder + "/" + dstFileName + ".collided." + str(seq)
                    print "Track '"+dstFile+"' already in destination folder, trying to save as "+altDstFile
                    dstFile = altDstFile
                    self.mCollisions += 1

                if not track.hasSpeed():
                    print "No speed information"
                    continue
                maxSpeedKph = track.maxSpeedKph()
                avgSpeedKph = track.avgSpeedKph()
                distanceKm  = track.distanceKm()
                if distanceKm < self.mMinDistanceKm:
                    print "Too short, distance:"+"{0:.2f}".format(distanceKm)+"km, accepting above: "+str(self.mMinDistanceKm)+"km"
                    continue
                if maxSpeedKph > self.mSpeedBounds["max"]["max"]:
                    print "Too fast, max speed seen:"+"{0:.2f}".format(maxSpeedKph)+"kph, accepting below: %dkph" % self.mSpeedBounds["max"]["max"]
                    continue
                if maxSpeedKph < self.mSpeedBounds["max"]["min"]:
                    print "Too flow, max speed seen:"+"{0:.2f}".format(maxSpeedKph)+"kph, accepting above: %dkph" % self.mSpeedBounds["max"]["min"]
                    continue
                if avgSpeedKph < self.mSpeedBounds["avg"]["min"]:
                    print "Too slow, avg speed:"+"{0:.2f}".format(avgSpeedKph)+"kph, accepting above: %dkph" % self.mSpeedBounds["avg"]["min"]
                    continue
                if avgSpeedKph > self.mSpeedBounds["avg"]["max"]:
                    print "Too fast, avg speed:"+"{0:.2f}".format(avgSpeedKph)+"kph, accepting below: %dkph" % self.mSpeedBounds["avg"]["max"]
                    continue
                extentKms = track.region().nw().distanceKms(track.region().se())
                twistiness = extentKms/distanceKm
                maxTwistiness = 0.15
                if twistiness < maxTwistiness:
                    print "Too twisty. The track region spans %dkms (nw->se) - a mere %d%% of its distance %dkms, accepting above %d%%" % \
                          (extentKms, 100*twistiness, distanceKm, 100*maxTwistiness)
                    continue

                Walker.mkdirs(dstFolder)
                track.write(dstFile)
                if seq > 0:
                    if filecmp.cmp(dstFile, dstFileBase):
                        self.mCollisions -= 1
                        print "..which is a collision between identical files: %s == %s" % (dstFile, dstFileBase)
                        os.rename(dstFile, dstFile.replace("collided","identical"))
                else:
                    self.mTracksOK += 1
        except Exception as err:
            print "Failed processing file: "+os.path.basename(file)+" due to "+str(err)
            self.mTracksBAD += 1
            return


def usage():
    print "Usage:"
    print sys.argv[0]+" <path to folder with gpx file> <dst path>\n"
    print "Given a folder with gpx files, this script will sort them based"
    print "on their location into a directory structure reflecting latlon boxes"

def main():
    srcPath = None
    dstPath = None
    try:
        srcPath = os.path.abspath(sys.argv[1])
        dstPath = os.path.abspath(sys.argv[2])
        assert not os.path.isfile(srcPath)
        if not os.path.isdir(dstPath):
            os.mkdir(dstPath)
        assert os.path.isdir(srcPath)
    except Exception as err:
        print str(err)
        usage()
        sys.exit(2)

    walker = Walker(
        srcPath, dstPath, {
        "avg": {
            "min": 5,
            "max": 30
        },
        "max": {
            "min": 7.5,
            "max": 45
        }},
        2
    )
    walker.do()

if __name__ == "__main__":
    main()



