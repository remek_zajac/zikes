
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# (C) 2016 Remek Zajac
#!/usr/bin/env python

import sys
import os
import subprocess
import threading
import json
import time


#local modules
from lib import pyUtils
from lib.make import pyArtifact
from lib.osmsql import pyImport
from lib.osmsql import pyGPBExport
from lib.osmsql.pyDb import OSMGisDatabase
from lib.pyUtils import FileUtils

KThisFolder      = os.path.dirname(os.path.abspath(__file__))
KZikesRootFolder = os.path.dirname(KThisFolder)
KLogPath         = pyUtils.KLogFolder + "/e2e"


class OsmFile(pyArtifact.LocalArtifact):

    KFileName = "src.osm"

    def __init__(self, path, stalePolicy, downloader, cpuSemaphore):
        super(OsmFile, self).__init__(paths=[os.path.join(path, OsmFile.KFileName)],
                                      dependsOn=downloader,
                                      stalePolicy=stalePolicy,
                                      resourceSemaphore=cpuSemaphore,
                                      readyState="osm file ready")


    def _make(self):
        self.updateStatus(activity="unzipping")
        placeHolderUnzipped = os.path.join(
            os.path.dirname(self.mDependsOn[0].get()[0]),
            OsmFile.KFileName
        )
        FileUtils.rm(placeHolderUnzipped)
        bzip2 = subprocess.Popen(
            args=["/bin/bzip2", "-d", self.mDependsOn[0].get()[0]],
            cwd=KThisFolder,
            stderr=subprocess.PIPE, stdout=subprocess.PIPE, stdin=subprocess.PIPE,
            close_fds=True,
            bufsize=-1
        )
        output = bzip2.communicate()
        bzip2.wait()
        returnCode = bzip2.poll()
        if returnCode != 0:
            raise Exception("bzip2 exited with %s saying %s %s" % (str(returnCode), output[0], output[1]))
        FileUtils.mv(
            placeHolderUnzipped,
            self.get()[0]
        )
        self.updateStatus(state="unzipped")


class Downloader(pyArtifact.LocalArtifact):
    KDownloadUrlRoot    = "http://download.geofabrik.de/"
    KOSMSuffix          = "-latest.osm"
    KBz2                = ".bz2"
    KDownloadSuffix     = KOSMSuffix+KBz2
    KLocalSuffix        = OsmFile.KFileName+KBz2

    @staticmethod
    def dstFile(inPath):
        return os.path.join(inPath, Downloader.KLocalSuffix)

    def __init__(self, countryName, path, bandwidthSemaphore):
        super(Downloader, self).__init__(name=countryName,
                                         paths=[Downloader.dstFile(path)],
                                         resourceSemaphore=bandwidthSemaphore,
                                         readyState="zipped osm file downloaded")

    def _make(self):
            def progress(status):
                self.updateStatus(activity="downloading %s" % status)
            pyUtils.FileDownload.download(
                url=Downloader.KDownloadUrlRoot+self.mName+Downloader.KDownloadSuffix,
                dstFile=Downloader.dstFile(self._getPlaceholderPath()),
                progressCb=progress
            )


class ImportExport(pyArtifact.LocalArtifact):

    KExpectedFiles = ["meta.pb", "coverage.json", "nodes.pb", "ways.pb", "waySections.pb"]
    @staticmethod
    def expectedFiles(inPath):
        return map(lambda f: "%s/%s" % (inPath, f), ImportExport.KExpectedFiles)

    def __init__(self,
                 countryName,
                 dbName,
                 coverage,
                 dstFolder,
                 osmFile,
                 stalePolicy,
                 dbSemaphore,
                 cpuSemaphore):
        super(ImportExport, self).__init__(name=countryName,
                                           paths=ImportExport.expectedFiles(dstFolder),
                                           dependsOn=osmFile,
                                           stalePolicy=stalePolicy,
                                           resourceSemaphore=dbSemaphore,
                                           readyState="local pb maps up to date")
        self.mDbName = dbName
        self.mCpuSemaphore = cpuSemaphore
        self.mCoverage = countryName if coverage else True

    def osmFile(self):
        return self.mDependsOn[0].get()[0]

    def _make(self):
        self.updateStatus(activity="importing")
        with OSMGisDatabase.createFromTemplate(self.mDbName, dropExisting=True) as db:
            importer = pyImport.OSMDatabaseImporter(
                db=db,
                countryName=self.mName,
                debug=pyUtils.Debug(
                    pyUtils.Debug.none,
                    os.path.join(
                        os.path.join(
                            KLogPath, self.mName
                        ), "import.log"
                    )
                )
            )
            importer.parseOsm(self.osmFile())
            self.updateStatus(state="imported")
            self.exportOsm(db)

    def exportOsm(self, db):
        self.updateStatus(activity="awaiting system resources")
        self.mCpuSemaphore.acquire()
        try:
            self.updateStatus(activity="exporting")
            pyGPBExport.GPBExport(
                region=None,
                db=db,
                dstFile=self._getPlaceholderPath(),
                features={
                    "meta": True,
                    "coverage": self.mCoverage,
                    "node": True,
                    "way": True,
                    "waysection": True
                },
                debug=pyUtils.Debug(
                    pyUtils.Debug.none,
                    os.path.join(
                        os.path.join(
                            KLogPath, self.mName
                        ), "export.log"
                    )
                )
            ).do()
            self.updateStatus(state="exported")
        finally:
            self.mCpuSemaphore.release()






class Country(threading.Thread):

    KBandwidthSemaphore = threading.Lock()
    KDBSemaphore        = threading.Lock()
    KCPUSemaphore       = threading.Lock()


    def __init__(self,
                 name,
                 dbName,
                 osmFolder,
                 pbFolder,
                 remoteOutput,
                 coverage,
                 staleAfterSec):
        threading.Thread.__init__(self)
        self.mName          = name
        self.mCoverage      = name if coverage else True

        osmFolder = os.path.join(osmFolder, name)
        if not os.path.exists(osmFolder):
            os.makedirs(osmFolder)
        pbFolder = os.path.join(pbFolder, name)
        if not os.path.exists(pbFolder):
            os.makedirs(pbFolder)
        stalePolicy=pyArtifact.Artifact.StalePolicy(
            afterSec=staleAfterSec
        )

        self.mRoot = ImportExport(
            countryName=name,
            dbName=dbName,
            dstFolder=pbFolder,
            osmFile=OsmFile(
                path=osmFolder,
                stalePolicy=stalePolicy,
                downloader=Downloader(
                    countryName=name,
                    path=os.path.join("/tmp", name),
                    bandwidthSemaphore=Country.KBandwidthSemaphore
                ),
                cpuSemaphore=Country.KCPUSemaphore
            ),
            stalePolicy=stalePolicy,
            coverage=coverage,
            dbSemaphore=Country.KDBSemaphore,
            cpuSemaphore=Country.KCPUSemaphore
        )
        if remoteOutput:
            self.mRoot = pyArtifact.RemoteArtifact(
                host=remoteOutput["host"],
                username=remoteOutput["username"],
                pkey=remoteOutput["pkey"],
                remotePaths=ImportExport.expectedFiles(
                    os.path.join(remoteOutput["path"], name)
                ),
                localArtifact=self.mRoot,
                stalePolicy=pyArtifact.Artifact.StalePolicy(
                    afterSec=staleAfterSec
                ),
                resourceSemaphore=Country.KBandwidthSemaphore,
                readyState="remote maps up to date"
            )


    def run(self):
        self.mRoot.make()

    #def start(self):
    #    self.run() #temporary measure - can't debug threads

    def name(self):
        return self.mName

    def artifact(self):
        return self.mRoot



KDays2Sec = 24*60*60
debug = pyUtils.Debug()
def do():
    with open(sys.argv[1]) as jobConfigFile:
        longestCountryName = 0
        jobConfig = json.load(jobConfigFile)
        osmFolder = str(jobConfig["osmFolder"])
        if not os.path.exists(osmFolder):
            os.makedirs(osmFolder)
        pbFolder = str(jobConfig["pbFolder"])
        if not os.path.exists(pbFolder):
            os.makedirs(pbFolder)
        for i in range(len(jobConfig["countries"])):
            jobConfig["countries"][i] = Country(
                name=str(jobConfig["countries"][i]),
                dbName=str(jobConfig["dbName"]),
                osmFolder=osmFolder,
                pbFolder=pbFolder,
                remoteOutput=jobConfig.get("remoteOutput"),
                coverage=jobConfig["coverage"],
                staleAfterSec=jobConfig["staleAfterDays"]*KDays2Sec if "staleAfterDays" in jobConfig else float("inf")
            )
            jobConfig["countries"][i].start()
            longestCountryName = max(longestCountryName, len(jobConfig["countries"][i].name()))

        #inspection loop
        while reduce(lambda cum, country: cum or country.is_alive(), jobConfig["countries"], False):
            time.sleep(1)
            report = False
            for country in jobConfig["countries"]:
                if country.artifact().hasStatusUpdate():
                    report = True
                    break
            if report:
                for country in jobConfig["countries"]:
                    debug.log(country.name().ljust(longestCountryName) + " => " + country.artifact().consumeStatus())
                debug.log(" ")


def usage():
    print "Given a list of countries, this script does an end to end transformation from:"
    print " -> http://download.geofabrik.de/<country>-latest.osm.bz2"
    print " to"
    print " <- <output>/<country>.pb"
    print "Usage:"
    print sys.argv[0]+" <path to job config file>"
    print "Job config format:"
    print "{\n  \"countries\": [\"great-britain\", \"germany\"],\n" \
          "  \"output\": \".\",\n" \
          "}"


def main():
    try:
        do()
    except Exception as e:
        debug.exception("")
        usage()
        sys.exit(2)
    debug.log("Done")

if __name__ == "__main__":
    main()



