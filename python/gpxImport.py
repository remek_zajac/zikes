# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# (C) 2015 Remek Zajac
#!/usr/bin/env python

import sys
import getopt
import os

#local imports
from lib import pyGPX
from lib import pyUtils
from lib import pyGeometry

class GPXParser:

    def __init__(self, fileOrFolder, region):
        self.mSrc    = fileOrFolder
        self.mRegion = None
        self.mTracks = None
        self.mDebug  = pyUtils.Debug(pyUtils.Debug.normal, "./importGpx.log")

    def do(self):

        gpxFiles = None
        if os.path.isfile(self.mSrc):
            gpxFiles = [self.mSrc]
        else:
            self.mDebug.log("Importing gpx files from folder '"+str(self.mSrc)+"'", pyUtils.Debug.sparse)
            gpxFiles = [os.path.join(root, filename)
                for root, dirnames, filenames in os.walk(self.mSrc)
                for filename in filenames if filename.endswith(".gpx")]
        count       = 0
        totalCount  = len(gpxFiles)
        for gpxFile in gpxFiles:
            done = str(int((100*count/totalCount)))+"% done"
            self.mDebug.log("Importing gpx file '"+str(gpxFile)+"'\n"+str(totalCount-count)+" to go, "+done)
            self.parse(gpxFile)
            count += 1

    def parse(self, gpxFile):
        parser       = pyGPX.ParseGPX()
        self.mTracks = []
        parser.parse(gpxFile, self._newGpxTrackCallback)
        for track in self.mTracks:
            if not track.insert2DB(background=False):
                self.mDebug.log("Track already imported, ignoring")

    def _newGpxTrackCallback(self):
        self.mTracks.append(pyGPX.MapAlignedGPXTrack(self))
        return self.mTracks[-1]

    def debug(self):
        return self.mDebug


def usage():
    print "Usage:"
    print sys.argv[0]+" <path to gpx file or folder> [options]"
    print "\noptions:"
    print "-r|--region - narrow the scope of imported osm map features to the given region"
    print "              expressed as [NWlat,NWlon,SElat,SElon] (no spaces!), e.g.: "
    print "              [55.99,8.46,47.01,30.43]"

def main():
    region = pyGeometry.GeoRegion.world()
    try:
        opts, args = getopt.getopt(sys.argv[1:], "r:", ["region"])
    except getopt.GetoptError as err:
        print str(err)
        usage()
        sys.exit(2)
    for o, a in opts:
        if o in ("-r", "--region"):
            coords = map(lambda elem: float(elem), a.split(","))
            region = pyGeometry.GeoRegion(coords)
        else:
            assert False, "unhandled option"

    if len(args) < 1:
        usage()
        assert False, "path to gpx file or folder not specified"

    parser = GPXParser(args[0], region)
    parser.do()


if __name__ == "__main__":
    main()



