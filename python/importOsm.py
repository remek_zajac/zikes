# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# (C) 2012 Remek Zajac
#!/usr/bin/env python
import os
import sys
import traceback
import argparse

#local modules
from lib.osmsql import pyImport
from lib.osmsql.pyDb import OSMGisDatabase


def usage():
    print "Usage:"
    print sys.argv[0]+" <path to osm file> [<db_name>='osm'] [options]"
    print "\noptions:"
    print "-c|--clean    - will make sure this input goes to a fresh/clean database"
    print "                it will fail if a database with the given name already exists"
    print "-f|--force    - same as --clean but will drop the existing database rather than failing."
    print ""
    print "\nYou can download region specific osm files from http://download.geofabrik.de/"

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('osmFilePath')
    parser.add_argument('-d', '--dbName', dest='dbName', default="osm")
    parser.add_argument('-c', '--clean', dest='clean', action='store_true')
    parser.add_argument('-f', '--force', dest='force', action='store_true')
    args = parser.parse_args()
    try:
        assert os.path.exists(args.osmFilePath), args.osmFilePath + " does not exist."
        db = None
        if args.clean or args.force:
            db = OSMGisDatabase.createFromTemplate(args.dbName, dropExisting=args.force)
        else:
            db = OSMGisDatabase(args.dbName)
        pyImport.OSMDatabaseImporter(db).parseOsm(args.osmFilePath)
    except:
        traceback.print_exc()
        usage()
        sys.exit(2)

if __name__ == "__main__":
    main()

