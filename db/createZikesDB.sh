#!/usr/bin/env bash

THISFOLDER="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
DBNAME="$1"
if [ -z "$DBNAME" ]; then
	echo "expected first argument to carry the dbName to create"
	exit 1
fi

SCHEMAFILE=$2
if [ -z "$SCHEMAFILE" ]; then
	SCHEMAFILE="$THISFOLDER/zikesProdSchema.sql"
fi
if [ ! -f "$SCHEMAFILE" ]; then
   SCHEMAFILE="$THISFOLDER/$SCHEMAFILE"
fi
if [ ! -f "$SCHEMAFILE" ]; then
   SCHEMAFILE="$THISFOLDER/$SCHEMAFILE.sql"
fi
if [ ! -f "$SCHEMAFILE" ]; then
   echo "The schema file $SCHEMAFILE cannot be found"
   exit 1
fi

PSQLPARAMS="-U postgres -h localhost"

echo "recreating database with the name '$DBNAME' and with schema '$SCHEMAFILE'"
dropdb $PSQLPARAMS --if-exists $DBNAME
createdb $PSQLPARAMS $DBNAME

psql $PSQLPARAMS -n $DBNAME < "$SCHEMAFILE"
