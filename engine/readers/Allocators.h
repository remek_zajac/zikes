/*************************************************************************
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * (C) 2015 Remek Zajac
 *************************************************************************/
#ifndef ALLOCATORS_H
#define ALLOCATORS_H

#include <boost/pool/pool.hpp>
#include <boost/pool/object_pool.hpp>

#include "geomap/geomap.h"

namespace reader {

    class Allocator {
        public:
            virtual geomap::Way* allocateWay(
                    geomap::Way::HighwayType aHighway,
                    const geomap::Way::TCombinedAccessAttributes& aAccessAttribute,
                    bool aLeftHandSideTraffic
#ifndef NDEBUG
                    ,geomap::Way::Id aId
#endif
            ) = 0;

            virtual geomap::Edge::EdgeNode* allocateEdgeNode(
                const geo::Point& aPoint,
                geomap::Edge::EdgeNode::ElevationMts aEleMts
            ) = 0;

            virtual geomap::Edge* allocateEdge(
                geomap::Edge::EdgeNode::Handle& aFrom,
                geomap::Edge::EdgeNode::Handle& aTo,
                geomap::Edge::Nodes& aInBetweenNodes,
                const geomap::Way::ConstHandle& aWay,
                geomap::Edge::DistanceMts aDistanceMts,
                geomap::Edge::ClimbPercent aClimbPercent,
                bool trafficSignals
            ) = 0;
    };

    template<class WHAT>
    class BasicAllocator {
        public:
            template<typename ...Args>
            WHAT* allocate(Args &... args) {
                return new WHAT(args...);
            }
    };

    template<class WHAT>
    class ObjectPoolAllocator {
        public:
            template<typename ...Args>
            WHAT* allocate(Args &... args) {
                return mWhats.construct(args...);
            }
        private:
            boost::object_pool<WHAT> mWhats;
    };

    template<class WHAT>
    class ArrayAllocator {
        public:
            ArrayAllocator(unsigned long aBlockSize = 10000)
            :mBlockSize(aBlockSize),
             mOccupancy(aBlockSize)
            {}

            typedef WHAT _WHAT;
            enum { EWhatSize = sizeof(_WHAT) };

            template<typename ...Args>
            WHAT* allocate(Args &... args) {
                if (mOccupancy >= mBlockSize) {
                    mArrays.push_back(
                        new char[mBlockSize][EWhatSize]
                    );
                    mOccupancy = 0;
                }
                char* buf = mArrays.back()[mOccupancy++];
                return new (buf) WHAT(args...);
            }

        private:
            std::vector<char(*)[EWhatSize]> mArrays;
            unsigned long mBlockSize;
            unsigned long mOccupancy;
    };

    template <typename WayAllocator, typename EdgeNodeAllocator, typename EdgeAllocator>
    class AllocatorBase : public Allocator {
        public:
            virtual geomap::Way* allocateWay(
                    geomap::Way::HighwayType aHighway,
                    const geomap::Way::TCombinedAccessAttributes& aAccessAttribute,
                    bool aLeftHandSideTraffic
#ifndef NDEBUG
                    ,geomap::Way::Id aId
#endif
            ) {
                return mWayAllocator.allocate(aHighway, aAccessAttribute, aLeftHandSideTraffic
#ifndef NDEBUG
                  ,aId
#endif
                );
            }

        virtual geomap::Edge::EdgeNode* allocateEdgeNode(
            const geo::Point& aPoint,
            geomap::Edge::EdgeNode::ElevationMts aEleMts
        ) {
            return mEdgeNodeAllocator.allocate(aPoint, aEleMts);
        }

        virtual geomap::Edge* allocateEdge(
            geomap::Edge::EdgeNode::Handle& aFrom,
            geomap::Edge::EdgeNode::Handle& aTo,
            geomap::Edge::Nodes& aInBetweenNodes,
            const geomap::Way::ConstHandle& aWay,
            geomap::Edge::DistanceMts aDistanceMts,
            geomap::Edge::ClimbPercent aClimbPercent,
            bool trafficSignals
        ) {
            return mEdgeAllocator.allocate(
                aFrom,
                aTo,
                aInBetweenNodes,
                aWay,
                aDistanceMts,
                aClimbPercent,
                trafficSignals
            );
        }

        private:
            WayAllocator      mWayAllocator;
            EdgeNodeAllocator mEdgeNodeAllocator;
            EdgeAllocator     mEdgeAllocator;
    };
}

#endif // ALLOCATORS_H
