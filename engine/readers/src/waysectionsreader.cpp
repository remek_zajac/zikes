/*************************************************************************
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * (C) 2015 Remek Zajac
 *************************************************************************/
#include <boost/log/trivial.hpp>

#include "osm.pb.h"
#include "waysectionsreader.h"
#define LOG(level) BOOST_LOG_TRIVIAL(level) << "[GooglePb]> "

namespace reader {

    WaySectionsReader::WaySectionsReader(SingleSetReader& aReader, const osm::WaySection& aWaySectionPb)
    :mReader(aReader),
     mWaySectionPb(aWaySectionPb)
    {
        for (int i = 0; i < mWaySectionPb.nodeids_size(); i++) {
            geomap::Node::Id id = mWaySectionPb.nodeids(i);
            geomap::Node::ConstHandle node;
            EdgeNodesDictionary::const_iterator foundEdgeNode = mReader.edgeNodes().find(id);
            if (foundEdgeNode != mReader.edgeNodes().end()) {
                node = mNodes.push_back(foundEdgeNode->second, id);
            } else {
                NodesDictionary::iterator foundNode = mReader.nodes().find(id);
                if (foundNode != mReader.nodes().end()) {
                    node = mNodes.push_back(foundNode->second, id);
                }                
            }
        }

        WaysDictionary::const_iterator foundWayIt = mReader.ways().find(mWaySectionPb.wayid());

        if (mNodes.size() < 2 || foundWayIt == mReader.ways().end()) {
#ifdef STRICT_OSM
            if (mNodes.size() == 0 || foundWayIt == mReader.mWays.end()) {
                std::stringstream messagess;
                messagess << "Error @ WaySection with wayid " << mWaySectionPb.wayid() << std::endl;
                messagess << "Either could not find at least two nodes in the input (found " << mNodes.size() <<") or could not find the way" << std::endl;
                messagess << "This demonstrates corrupt input, aborting.";
                std::string message = messagess.str();
                LOG(error) << message;
                throw std::invalid_argument("");
            }
#endif
            return;
        }
        mWay = foundWayIt->second;

        geomap::Edge::EdgeNode::Handle firstEdgeNode = mNodes.commitFirstEdgeNode();
        geomap::Edge::EdgeNode::Handle lastEdgeNode  = mNodes.commitLastEdgeNode();
        mNodes.shrink_to_fit();

        mReader.edgeProcessor()(
            geomap::Edge::ConstHandle(
                mReader.allocator().allocateEdge(
                    firstEdgeNode,
                    lastEdgeNode,
                    mNodes,
                    mWay,
                    aWaySectionPb.distancemts(),
                    mWaySectionPb.has_climb() ? mWaySectionPb.climb() : 0,
                    mWaySectionPb.flags() & osm::WaySection::TrafficSignals
                 )
            )
        );
    }


    std::string WaySectionsReader::toString() const {
        std::stringstream result;
        result.imbue(std::locale("C"));
        result << "WaySectionPb(wayid:";
        result << mWaySectionPb.wayid();
        result << ")[";
        for (auto node : mNodes) {
            result << node->toString() << ",";
        }
        result << "]";
        return result.str();
    }

}
