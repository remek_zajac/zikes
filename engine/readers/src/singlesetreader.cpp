/*************************************************************************
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * (C) 2015 Remek Zajac
 *************************************************************************/
#include <fcntl.h>
#include <stdexcept>

#include <google/protobuf/io/coded_stream.h>
#include <google/protobuf/io/zero_copy_stream_impl.h>

#include <boost/log/trivial.hpp>

#include "../reader.h"
#include "osm.pb.h"
#include "waysectionsreader.h"

#define LOG(level) BOOST_LOG_TRIVIAL(level) << "[GooglePb]> "

namespace reader {

    class Input {
        public:
            Input(const std::string& aPathToFile)
            :mInputStream(open(aPathToFile.c_str(), O_RDONLY))
            {
                if (!(boost::filesystem::exists(aPathToFile) && boost::filesystem::is_regular_file(aPathToFile))) {
                    throw std::invalid_argument("File not found "+aPathToFile);
                }
            }

            google::protobuf::io::FileInputStream& input() {
                return mInputStream;
            }

            ~Input() {
                mInputStream.Close();
            }

        private:
            google::protobuf::io::FileInputStream mInputStream;
    };

    bool readDelimitedFrom(
        Input& rawInput,
        google::protobuf::MessageLite* message) {
      // We create a new coded stream for each message.  Don't worry, this is fast,
      // and it makes sure the 64MB total size limit is imposed per-message rather
      // than on the whole stream.  (See the CodedInputStream interface for more
      // info on this limit.)
      google::protobuf::io::CodedInputStream input(&rawInput.input());

      // Read the size.
      uint32_t size;
      if (!input.ReadVarint32(&size)) return false;

      // Tell the stream not to read beyond that size.
      google::protobuf::io::CodedInputStream::Limit limit =
          input.PushLimit(size);

      // Parse the message.
      if (!message->MergeFromCodedStream(&input)) return false;
      if (!input.ConsumedEntireMessage()) return false;

      // Release the limit.
      input.PopLimit(limit);

      return true;
    }

    void SingleSetReader::readMeta(const boost::filesystem::path& aPathToFile)
    {
        LOG(info) << "Reading " << aPathToFile << std::flush;
        Input input(aPathToFile.string());
        geomap::MapMeta::HighwayTypes highwayTypes;

        while (true) {
            osm::Meta meta;
            if (!readDelimitedFrom(input, &meta)) {
                break;
            }
            for (int i = 0; i < meta.highwaytypes_size(); i++) {
                const osm::Meta::HighwayType highwayType = meta.highwaytypes(i);
                highwayTypes[highwayType.name()] = (unsigned short)highwayType.id();
            }
        }
        mParentReader.mMetaProcessor.putHighwayTypes(highwayTypes);
    }

    void SingleSetReader::readNodes(const boost::filesystem::path& aPathToFile)
    {
        LOG(info) << "Reading " << aPathToFile << std::flush;
        Input input(aPathToFile.string());
        while (true) {
            osm::Node node;
            if (!readDelimitedFrom(input, &node)) {
                break;
            }
            geo::Point point = geo::Point(
                (geomap::Node::CoordinateType) node.lat(),
                (geomap::Node::CoordinateType) node.lon()
            );
            if ((node.flags() & osm::Node::Vertex) &&
                 edgeNodes().find(node.id()) == edgeNodes().end()) {
                geomap::Edge::EdgeNode::ElevationMts eleMts = node.has_elemts() ?
                    geomap::Edge::EdgeNode::ElevationMts(node.elemts()) :
                    geomap::Edge::EdgeNode::ElevationMts();

                geomap::Edge::EdgeNode::Handle edgeNode = geomap::Edge::EdgeNode::Handle(
                    allocator().allocateEdgeNode(
                        point,
                        eleMts
                    )
                );
                edgeNodes()[node.id()] = edgeNode;
            } else {
                mNodes.insert(
                    NodesDictionary::value_type(
                        node.id(),
                        NodesDictionary::mapped_type(
                            geomap::Node::ConstHandle(
                                new geomap::Node(point)
                            )
                        )
                    )
                );
            }
            std::string timestr;
            if (mParentReader.isTimeToReport(timestr)) {
                LOG(info) << "wrote " << mNodes.size() << " nodes" << "\r" << std::flush;
            }
        }
#ifndef NDEBUG
        mOriginalNodesCount = mNodes.size();
#endif
    }

    void SingleSetReader::readWays(const boost::filesystem::path& aPathToFile)
    {
        LOG(info) << "Reading " << aPathToFile << std::flush;
        Input input(aPathToFile.string());
        while (true) {
            osm::Way way;
            if (!readDelimitedFrom(input, &way)) {
                break;
            }

            geomap::Way::TAccessAttribute bicyleAlong(geomap::Way::TAccessAttribute::EAllowed);
            if (way.has_along()) {
                const osm::Way::Directed along = way.along();
                if (along.cycleway() == osm::Way::Directed::Allowed) {
                    bicyleAlong = geomap::Way::TAccessAttribute::EAllowed;
                } else if (along.cycleway() == osm::Way::Directed::Designated) {
                    bicyleAlong = geomap::Way::TAccessAttribute::EDesignated;
                } else if (along.cycleway() == osm::Way::Directed::Forbidden) {
                    bicyleAlong = geomap::Way::TAccessAttribute::EForbidden;
                }
            }
            geomap::Way::TAccessAttribute bicycleReverse(geomap::Way::TAccessAttribute::EAllowed);
            if (way.has_reverse()) {
                const osm::Way::Directed reverse = way.reverse();
                if (reverse.cycleway() == osm::Way::Directed::Allowed) {
                    bicycleReverse = geomap::Way::TAccessAttribute::EAllowed;
                } else if (reverse.cycleway() == osm::Way::Directed::Designated) {
                    bicycleReverse = geomap::Way::TAccessAttribute::EDesignated;
                } else if (reverse.cycleway() == osm::Way::Directed::Forbidden) {
                    bicycleReverse = geomap::Way::TAccessAttribute::EForbidden;
                }
            }

            geomap::Way::TAccessAttribute carAlong(geomap::Way::TAccessAttribute::EAllowed);
            geomap::Way::TAccessAttribute carReverse(geomap::Way::TAccessAttribute::EAllowed);
            if (way.oneway() == osm::Way::Along) {
                carReverse = geomap::Way::TAccessAttribute::EForbidden;
            } else if (way.oneway() == osm::Way::Reverse) {
                carAlong = geomap::Way::TAccessAttribute::EForbidden;
            }
            mWays.insert(
                WaysDictionary::value_type(
                    way.id(),
                    WaysDictionary::mapped_type(
                        geomap::Way::ConstHandle(
                            mParentReader.mAllocator.allocateWay(
                                way.highway(),
                                geomap::Way::TCombinedAccessAttributes(
                                    geomap::Way::TOneDirectionAccessAttributes(carAlong, bicyleAlong, geomap::Way::TAccessAttribute::EAllowed), //Pedestrians TODO in python
                                    geomap::Way::TOneDirectionAccessAttributes(carReverse, bicycleReverse, geomap::Way::TAccessAttribute::EAllowed)
                                ),
                                way.has_lefthandsidetraffic() ? way.lefthandsidetraffic() : false
#ifndef NDEBUG
                               ,way.id()
#endif
                            )
                        )
                    )
                )
            );
            std::string timestr;
            if (mParentReader.isTimeToReport(timestr)) {
                LOG(info) << "wrote " << mWays.size() << " ways" << "\r" << std::flush;
            }
        }
    }


    void SingleSetReader::readWaySections(const boost::filesystem::path& aPathToFile)
    {
        LOG(info) << "Reading " << aPathToFile << std::flush;
        Input input(aPathToFile.string());
        unsigned long writtenEdges = 0;

        while (true) {
            osm::WaySection waySection;
            if (!readDelimitedFrom(input, &waySection)) {
                break;
            }
            WaySectionsReader waySectionReader(*this, waySection);
            writtenEdges++;
            std::string timestr;
            if (mParentReader.isTimeToReport(timestr)) {
                LOG(info) << "wrote " << writtenEdges << " edges" << "\r" << std::flush;
            }
        }
    }


    const std::string SingleSetReader::KMetaFile         = "meta.pb";
    const std::string SingleSetReader::KNodesFile        = "nodes.pb";
    const std::string SingleSetReader::KWaysFile         = "ways.pb";
    const std::string SingleSetReader::KWaySectionsFile  = "waySections.pb";
    const std::string SingleSetReader::KCoverageFile     = "coverage.json";

    void SingleSetReader::read(const std::string& aFolderPathStr) {
        const boost::filesystem::path aFolderPath(aFolderPathStr);
        if (!boost::filesystem::exists(aFolderPath) || !boost::filesystem::is_directory(aFolderPath)) {
            throw std::invalid_argument("maps path: " + aFolderPath.string() + " does not exist or isn't a directory.");
        }
        readMeta(aFolderPath / KMetaFile);
        mParentReader.mMetaProcessor.putCoverageFile(aFolderPath / KCoverageFile);
        readNodes(aFolderPath / KNodesFile);
        readWays(aFolderPath / KWaysFile);
        readWaySections(aFolderPath / KWaySectionsFile);
    }
};
