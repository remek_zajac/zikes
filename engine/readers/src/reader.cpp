/*************************************************************************
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * (C) 2015 Remek Zajac
 *************************************************************************/
#include <boost/log/trivial.hpp>
#include "../reader.h"

#define LOG(level) BOOST_LOG_TRIVIAL(level) << "[GooglePb]> "

namespace reader {

    bool biggerFolderFirst(const boost::filesystem::path& first, const boost::filesystem::path& second)
    {
      if (boost::filesystem::exists(first / SingleSetReader::KWaySectionsFile) &&
          boost::filesystem::exists(second / SingleSetReader::KWaySectionsFile)) {
          auto firstSize  = boost::filesystem::file_size(first / SingleSetReader::KWaySectionsFile);
          auto secondSize = boost::filesystem::file_size(second / SingleSetReader::KWaySectionsFile);
          return ( firstSize > secondSize );
      }
      return first.compare(second) > 0;
    }

    void forEachFolder(
            const boost::filesystem::path& aFolder,
            std::function<void(const boost::filesystem::path& aFilePath)> aFunc) {
        boost::filesystem::directory_iterator endIt;
        std::list<boost::filesystem::path> folders;

        for (boost::filesystem::directory_iterator it(aFolder); it != endIt; it++) {
            if (boost::filesystem::is_directory(it->path())) {
                folders.push_back(*it);
            }
        }
        folders.sort(biggerFolderFirst);
        for (auto folder : folders) {
            aFunc(folder);
        }
    }


    void Reader::read(const std::string& aFolderPathStr) {
        const boost::filesystem::path aFolderPath(aFolderPathStr);
        if (!boost::filesystem::exists(aFolderPath) || !boost::filesystem::is_directory(aFolderPath)) {
            throw std::invalid_argument("maps path: " + aFolderPath.string() + " does not exist or isn't a directory.");
        }
        if (boost::filesystem::exists(aFolderPath / SingleSetReader::KWaySectionsFile)) {
            mSingleSetReader = std::unique_ptr<SingleSetReader>(
                new SingleSetReader(*this)
            );
            mSingleSetReader->read(aFolderPath.string());
            mNodesCount += mSingleSetReader->nodesCount();
            mWaysCount  += mSingleSetReader->waysCount();
            mSingleSetReader.reset();
            SingleSetReader::purge();
        } else {
            forEachFolder(aFolderPath, [&] (const boost::filesystem::path& aPath) {
                read(aPath.string());
            });
        }
    }

    bool Reader::isTimeToReport(std::string& aTimestr)
    {
        time_t ctt = time(0);
        if (ctt - mLastReport > 1) {
            struct tm * timeinfo;
            char buffer[80];
            timeinfo = localtime(&ctt);

            strftime(buffer,80,"[%d-%m-%Y %I:%M:%S]",timeinfo);
            aTimestr = buffer;
            mLastReport = ctt;
            return true;
        }
        return false;
    }

    void Reader::purge() {
        EdgeNodesDictionary::purge();
        SingleSetReader::purge();
    }


    AllocatorBase<
        BasicAllocator<geomap::Way>,
        BasicAllocator<geomap::Edge::EdgeNode>,
        BasicAllocator<geomap::Edge>
    > Reader::sDefaultAllocator;
}
