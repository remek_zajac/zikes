/*************************************************************************
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * (C) 2015 Remek Zajac
 *************************************************************************/
#ifndef WAYSECTIONSREADER_H
#define WAYSECTIONSREADER_H

#include "../reader.h"

namespace reader {

    class WaySectionsReader
    {
        class SectionNodes : public geomap::Edge::Nodes {

            public:
                geomap::Node::ConstHandle push_back(geomap::Node::ConstHandle& aNode, geomap::Node::Id aId) {
                    mLastEdgeNode = geomap::Edge::EdgeNode::Handle();
                    geomap::Edge::Nodes::push_back(aNode);
                    mIds.push_back(aId);
                    return aNode;
                }

                geomap::Node::ConstHandle push_back(const geomap::Edge::EdgeNode::Handle& aNode, geomap::Node::Id aId) {
                    if (size() == 0) {
                        mFirstEdgeNode = aNode;
                    }
                    mLastEdgeNode = aNode;
                    geomap::Node::ConstHandle node = zikes::handle_cast<geomap::Node::ConstHandle>(aNode);
                    geomap::Edge::Nodes::push_back(node);
                    mIds.push_back(aId);
                    return node;
                }

                geomap::Edge::EdgeNode::Handle& commitFirstEdgeNode() {
                    if (!mFirstEdgeNode) { //this should not happen
                        std::cerr << "Node with id " << mIds.front() << " should have been an EdgeNode"<< std::endl;
                        mFirstEdgeNode = geomap::Edge::EdgeNode::Handle(
                            new geomap::Edge::EdgeNode(
                                *this->front(),
                                geomap::Edge::EdgeNode::ElevationMts()
                            )
                        );
                    }
                    this->erase(begin());
                    return mFirstEdgeNode;
                }

                geomap::Edge::EdgeNode::Handle& commitLastEdgeNode() {
                    if (!mLastEdgeNode) {
                        std::cerr << "Node with id " << mIds.back() << " should have been an EdgeNode"<< std::endl;
                        mLastEdgeNode = geomap::Edge::EdgeNode::Handle(
                            new geomap::Edge::EdgeNode(
                                *this->back(),
                                geomap::Edge::EdgeNode::ElevationMts()
                            )
                        );
                    }
                    this->pop_back();
                    return mLastEdgeNode;
                }

            private:
                std::list<geomap::Node::Id>            mIds;
                geomap::Edge::EdgeNode::Handle         mFirstEdgeNode;
                geomap::Edge::EdgeNode::Handle         mLastEdgeNode;
        };

        public:
            WaySectionsReader(SingleSetReader& aReader, const osm::WaySection& aWaySectionPb);
            std::string toString() const;

        private:

            SingleSetReader&                             mReader;
            const osm::WaySection&                       mWaySectionPb;
            SectionNodes                                 mNodes;
            geomap::Way::ConstHandle                     mWay;
    };
}
#endif // WAYSECTIONSREADER_H
