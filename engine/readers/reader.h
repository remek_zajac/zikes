/*************************************************************************
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * (C) 2015 Remek Zajac
 *************************************************************************/
#ifndef READER_H
#define READER_H

#include <functional>
#include <boost/filesystem.hpp>
#include <boost/unordered/unordered_map.hpp>
#include "utils/pool_allocator_custom_tag.h"

#include "geomap/geomap.h"
#include "Allocators.h"

namespace reader {

    template <typename Key, typename Val>
    class PoolMap : public boost::unordered_map<Key, Val, boost::hash<Key>, std::equal_to<Key>, boost::fast_pool_allocator_custom_tag<std::pair<const Key, Val> > >
    {
        public:
            static void purge() {
                boost::singleton_pool<typename PoolMap::value_type, 8>::purge_memory();
                boost::singleton_pool<typename PoolMap::value_type, sizeof(typename PoolMap::value_type)>::purge_memory();
                boost::singleton_pool<typename PoolMap::value_type, sizeof(boost::unordered::detail::ptr_node<typename PoolMap::value_type>)>::purge_memory();
            }
    };

    typedef PoolMap<geomap::Node::Id, geomap::Edge::EdgeNode::Handle>    EdgeNodesDictionary;
    typedef PoolMap<geomap::Node::Id, geomap::Node::ConstHandle>         NodesDictionary;
    typedef PoolMap<geomap::Way::Id,  geomap::Way::ConstHandle>          WaysDictionary;

    class SingleSetReader;
    class Reader {

            friend class SingleSetReader;
        public:

            typedef std::function<void(const geomap::Edge::ConstHandle&)>        EdgeProcessorCb;
            class MetaProcessor {
                public:
                    virtual void putHighwayTypes(const geomap::MapMeta::HighwayTypes& aHighwayTypes) = 0;
                    virtual void putCoverageFile(const boost::filesystem::path& aPathToCoverageFile) = 0;
            };

            Reader(EdgeProcessorCb aEdgeProcessor, MetaProcessor& aMetaProcessor, Allocator& aAllocator = sDefaultAllocator)
            :mEdgeProcessor(aEdgeProcessor),
             mMetaProcessor(aMetaProcessor),
             mLastReport(time(0)),
             mNodesCount(0),
             mWaysCount(0),
             mAllocator(aAllocator)
            {}

            void read(const std::string& aFolderPath);

            unsigned long edgeNodesCount() const {
                return mEdgeNodes.size();
            }

            unsigned long nodesCount() const {
                return mNodesCount;
            }

            unsigned long waysCount() const {
                return mWaysCount;
            }

            static void purge();

        private:
            bool isTimeToReport(std::string& aTimestr);

            EdgeProcessorCb                  mEdgeProcessor;
            MetaProcessor&                   mMetaProcessor;
            EdgeNodesDictionary              mEdgeNodes;
            std::unique_ptr<SingleSetReader> mSingleSetReader;
            time_t                           mLastReport;
            unsigned long                    mNodesCount;
            unsigned long                    mWaysCount;
            Allocator&                       mAllocator;
            static AllocatorBase<
                BasicAllocator<geomap::Way>,
                BasicAllocator<geomap::Edge::EdgeNode>,
                BasicAllocator<geomap::Edge>
            > sDefaultAllocator;
    };

    class SingleSetReader {
            friend class WaySectionsReader;
        public:
            static const std::string KMetaFile;
            static const std::string KNodesFile;
            static const std::string KWaysFile;
            static const std::string KWaySectionsFile;
            static const std::string KCoverageFile;

            SingleSetReader(Reader& aParentReader)
            :mParentReader(aParentReader)
            {}

            void read(const std::string& aFolderPath);

            static void purge() {
                NodesDictionary::purge();
                WaysDictionary::purge();
            }

            unsigned long nodesCount() const {
                return mNodes.size();
            }

            unsigned long waysCount() const {
                return mWays.size();
            }

        private:
            void readMeta(const boost::filesystem::path& aPathToFile);
            void readNodes(const boost::filesystem::path& aPathToFile);
            void readWays(const boost::filesystem::path& aPathToFile);
            void readWaySections(const boost::filesystem::path& aPathToFile);

            WaysDictionary&         ways() {return mWays;}
            NodesDictionary&        nodes() {return mNodes;}
            EdgeNodesDictionary&    edgeNodes() {return mParentReader.mEdgeNodes;}
            Reader::EdgeProcessorCb edgeProcessor() {return mParentReader.mEdgeProcessor;}
            Allocator&              allocator() {return mParentReader.mAllocator; }

            NodesDictionary     mNodes;
            WaysDictionary      mWays;
            Reader&             mParentReader;
#ifndef NDEBUG
            unsigned long       mOriginalNodesCount;
#endif
    };


}

#endif // READER_H
