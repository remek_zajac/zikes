/*************************************************************************
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * (C) 2014 Remek Zajac
 *************************************************************************/
#ifndef AROUTE_H
#define AROUTE_H

#include <stdexcept>
#include <boost/filesystem.hpp>

#include "geomap/geomap.h"
#include "cost.h"

namespace routing {

    class Route : public std::list<geomap::Edge::DirectedEdge::ConstHandle>
    {
        public:
            void forEachNode(std::function<void(
                const geomap::Node&,
                const geomap::Edge::EdgeNode* /*aWhichIsEdgeNode*/,
                const geomap::Edge::DirectedEdge* /*aDirectedEdgeForward*/
            )> aFunc) const;
            void forEachEdgeNode(std::function<void(const geomap::Edge::EdgeNode&)> aFunc) const;
            unsigned long distanceMts() const;
            bool operator == (const Route& aOther) const;
    };


    class IAlgorithmFactory
    {
        public:
            class IAlgorithm {
                public:
                    class ResourceConsumptionOverload : public std::exception
                    {
                        public:
                            ResourceConsumptionOverload(unsigned long aJunctions)
                            :mJunctions(aJunctions),
                             mWhat(what(aJunctions))
                            {}

                            virtual const char* what() const throw()
                            {
                                return mWhat.c_str();
                            }

                            const unsigned long mJunctions;
                        private:
                            static const std::string what(unsigned long aJunctions)
                            {
                                std::stringstream ss;
                                ss << "Routing terminated due to resource overconsumption. Buffered over the threshold of " << aJunctions << " junctions.";
                                return ss.str();
                            }
                            const std::string mWhat;
                    };

                    virtual void route(Route& aRoute) = 0;
                    virtual std::string debugJSON() const = 0;
                    virtual ~IAlgorithm() {};
            };

            virtual IAlgorithm* create(
                const geomap::Edge::EdgeNode::ConstAutoHandle& aFrom,
                const geomap::Edge::EdgeNode::ConstAutoHandle& aTo,
                const CostFunction& aCostFunction
             ) = 0;

            virtual ~IAlgorithmFactory() {};
    };


    class Request : private boost::noncopyable
    {
        public:
            class NearestPointNotFound : public std::exception
            {
                public:
                    NearestPointNotFound(const geo::Point& aCulprit, unsigned int aAttemptedToleranceMts)
                    :mCulprit(aCulprit),
                     mAttemptedToleranceMts(aAttemptedToleranceMts),
                     mWhat(what(aCulprit, aAttemptedToleranceMts))
                    {}

                    virtual const char* what() const throw()
                    {
                        return mWhat.c_str();
                    }

                    const geo::Point  mCulprit;
                    const int         mAttemptedToleranceMts;

                private:
                    static const std::string what(const geo::Point& aCulprit, int aAttemptedToleranceMts)
                    {
                        std::stringstream ss;
                        ss << "Unable to find a highway within " << aAttemptedToleranceMts << "mts of the requested " << aCulprit.toString();
                        return ss.str();
                    }

                    const std::string mWhat;
            };

            Request(const CostFunctionFactory& aCostFunctionFactory)
            :mCostFunctionFactory(aCostFunctionFactory)
            {}

            inline Request& setFrom(const geo::Point& aFrom) {
                return setFrom(findNearest(aFrom));
            }

            inline Request& setTo(const geo::Point& aTo) {
                return setTo(findNearest(aTo));
            }

            inline Request& setFrom(const geomap::Edge::EdgeNode::ConstAutoHandle& aFrom) {
                mFrom = aFrom;
                return *this;
            }

            inline Request& setTo(const geomap::Edge::EdgeNode::ConstAutoHandle& aTo) {
                mTo = aTo;
                return *this;
            }

            inline const geomap::Edge::EdgeNode::ConstAutoHandle& getFrom() const {
                return mFrom;
            }

            inline const geomap::Edge::EdgeNode::ConstAutoHandle& getTo() const {
                return mTo;
            }

            inline const std::string debugJSON() const {
                return mAlgorithm->debugJSON();
            }

            /*
             * Finds the route and responds synchronously
             */
            Request& execute(Route& aRoute);

        private:

            static geomap::Edge::EdgeNode::ConstAutoHandle findNearest(const geo::Point& aTo) {
                geomap::Edge::EdgeNode::ConstAutoHandle to = geomap::Map::FallbackEdgeNodeStrategy(
                    geomap::Map::NearestProjectedEdgeNodeStrategy(geomap::Map::instance()),
                    geomap::Map::NearestEdgeNodeStrategy(geomap::Map::instance())
                ).find(aTo);
                if (!to) {
                    throw NearestPointNotFound(aTo, geomap::Map::NearestEdgeNodeStrategy::KDefaultToleranceMts);
                }
                return to;
            }

            geomap::Edge::EdgeNode::ConstAutoHandle mFrom;
            geomap::Edge::EdgeNode::ConstAutoHandle mTo;
            const CostFunctionFactory& mCostFunctionFactory;
            std::unique_ptr<routing::IAlgorithmFactory::IAlgorithm> mAlgorithm;
    };
};
#include "gpxroute.h"
#include "jsonRoute.h"
#endif //AROUTE_H
