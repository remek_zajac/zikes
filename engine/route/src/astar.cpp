/*************************************************************************
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * (C) 2015 Remek Zajac
 *************************************************************************/
#include <boost/pool/pool.hpp>
#include <boost/pool/object_pool.hpp>
#include <boost/log/trivial.hpp>

#include <atomic>

#include "assert.h"
#include "astar.h"

#ifndef NDEBUG
#include "cost/components/trafficsignals.h"
#endif

#define LOG(level) BOOST_LOG_TRIVIAL(level) << "[routing]> "

namespace routing {

    const void AStar::RouteEdge::dump(Route& aRoute) const {
        const RouteHead* current = this;
#ifndef NDEBUG
        LOG(debug) << "Verbose route dump: [";
#endif
        while (current->mPrev != nullptr) {
            aRoute.push_front(current->mDirectedEdge);
#ifndef NDEBUG
            std::stringstream ss;
            ss.imbue(std::locale("C"));
            const RouteEdge& routeEdge = *static_cast<const RouteEdge*>(current);
            const geomap::Edge::DirectedEdge& mapDirectedEdge = routeEdge.edge();
            ss << "  {\"routeEdge\": " << mapDirectedEdge.toString(true) << ", \"cost\":" << routeEdge.calculateCost();
            if (current->mUrbanScore) {
                ss << ", \"urbanScore\":" << routeEdge.mUrbanScore->toJson() ;
            }
            if (cost::TrafficSignals::crossingCascade(*current)) {
                ss << ", \"trafficSignals\":" << mapDirectedEdge.from().toString();
            }
            ss << "},";
            LOG(debug) << ss.str() << std::endl;
#endif
            current = current->mPrev;
        }
#ifndef NDEBUG
        LOG(debug) << "]";
        LOG(debug) << "Total cost: " << G() << std::endl;
#endif
    }

    bool AStar::AuxEdges::hasNeighbour(const geomap::Edge::EdgeNode& aReference, const geomap::Edge::EdgeNode& aNeighbour) {
        for (auto it = boost::begin(aReference.edges()); it != boost::end(aReference.edges()); it++) {
            if (&((*it)->to()) == &aNeighbour) {
                return true;
            }
        }
        return false;
    }

    boost::optional<AStar::AuxEdges> AStar::AuxEdges::populate(const geomap::Edge::EdgeNode& aTo) {        
       if ( aTo.edges().size() == 2) {
            if (!hasNeighbour(aTo.edges().at(0)->to(), aTo) ||
                !hasNeighbour(aTo.edges().at(1)->to(), aTo)) {
                return boost::optional<AuxEdges>(
                    AuxEdges(
                        *(aTo.edges().at(0)), *(aTo.edges().at(1))
                    )
                );
            }
        }
        return boost::optional<AuxEdges>();
    }

    void AStar::AuxEdges::checkAndVisit(
        const geomap::Edge::EdgeNode& aCurrent,
        std::function<void(const geomap::Edge::DirectedEdge::ConstHandle&)> aFunc) {
        if (&(first.from()) == &aCurrent) {
            aFunc(geomap::Edge::DirectedEdge::ConstHandle(&first));
        } else if (&(second.from()) == &aCurrent) {
            aFunc(geomap::Edge::DirectedEdge::ConstHandle(&second));
        }
    }


    AStar::AStar(const geomap::Edge::EdgeNode::ConstAutoHandle& aFrom,
                 const geomap::Edge::EdgeNode::ConstAutoHandle& aTo,
                 const CostFunction& aCostFunction)
    :mOriginEdge((*boost::begin(aFrom->edges()))->edge(),
                  *aFrom),
     mTo(aTo),
     mAuxEdges(AuxEdges::populate(*aTo)),
     mCostFunction(aCostFunction),
     mInClosedSet(0)
    {
        static std::atomic_ushort thread_id_seq(0);
        static __thread unsigned short thread_id = thread_id_seq++;
        mThreadId = thread_id;
    }

    bool AStar::destinationReached(const AStar::RouteEdge& aCurrent, std::function<void(const geomap::Edge::DirectedEdge::ConstHandle&)> aFunc) {
        const geomap::Edge::EdgeNode& reached = aCurrent.edge().to();
        if (mAuxEdges.is_initialized()) {
            mAuxEdges->checkAndVisit(reached, aFunc);
        }
        if ( &reached == &*mTo ) {
            return true;
        }
        return false;
    }


    void AStar::visitEdge(AStar::RouteEdge* aEdge) {
        const RouteEdge* existingEdge = aEdge->getFromOpenSet();

        if (existingEdge == nullptr || existingEdge->G() > aEdge->G()) {
            mOpenSetHeap.push(
                aEdge->F(*mTo),
                *aEdge
            );
        }
    }

    void AStar::route(Route& aRoute)
    {
        boost::object_pool<RouteEdge> routeEdgePool;
        AStar::RouteEdge* originRouteEdge = routeEdgePool.construct(
            geomap::DirectedEdge::ConstHandle(&mOriginEdge),
            *this
        );

        mOpenSetHeap.push(
            originRouteEdge->F(*mTo),
            *originRouteEdge
        );        

        while (mOpenSetHeap.size() > 0) {
            RouteEdge& current = mOpenSetHeap.pop();
            if (destinationReached(current, [&](const geomap::Edge::DirectedEdge::ConstHandle aAuxEdge) {
                    visitEdge(
                        routeEdgePool.construct(
                            aAuxEdge,
                            *this,
                            &current
                        )
                    );
                })) {
                //successfully navigated to a graph vertex nearest the destination
                current.dump(aRoute);
                break;
            }

            for (
                 auto it  = boost::begin(current.neighbours());
                      it != boost::end(current.neighbours());
                      it++
            ) {
                if (!RouteEdge::isInClosedSet(*(it->get()), mThreadId)) {
                    visitEdge(
                        routeEdgePool.construct(
                            it->get(),
                            *this,
                            &current
                        )
                    );
                }
            }
            current.moveToClosedSet();
            if (++mInClosedSet > AStar::KAbortOnNoOfJunctions) {
                throw AStar::ResourceConsumptionOverload(mInClosedSet);
            }
        }
    }
}
