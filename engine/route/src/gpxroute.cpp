/*************************************************************************
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * (C) 2015 Remek Zajac
 *************************************************************************/

#include "route/gpxroute.h"

namespace routing {

    void GpxRoute::write(std::ostream& aOStream, const Route& aRoute)
    {
        aOStream << "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>" << std::endl;
        aOStream << "<gpx xmlns=\"http://www.topografix.com/GPX/1/1\" version=\"1.1\" creator=\"Zikes\">" << std::endl;
        aOStream << "    <metadata>" << std::endl;
        aOStream << "        <name>Output gpx route by Zikes</name>" << std::endl;
        aOStream << "    </metadata>" << std::endl;
        aOStream << "    <rte>" << std::endl;
        aRoute.forEachNode( [&] (const geomap::Node& aNode, const geomap::Edge::EdgeNode* aWhichIsEdgeNode, const geomap::Edge::DirectedEdge* aDirectedEdgeForward) {
            aOStream << "        <rtept lon=\"" << aNode.lonStr() << "\" lat=\"" << aNode.latStr() << "\"/>" << std::endl;
        });
        aOStream << "    </rte>" << std::endl;
        aOStream << "</gpx>" << std::endl;
        aOStream.flush();
    }
}
