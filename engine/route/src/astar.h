/*************************************************************************
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * (C) 2015 Remek Zajac
 *************************************************************************/

#ifndef ASTAR_H
#define ASTAR_H

#include "../routing.h"
#include "OpenHeapFib.h"

namespace routing {


    class OriginEdge : public geomap::DirectedEdge {

        public:

            OriginEdge(
                const geomap::Edge& aEdge,
                const geomap::Edge::EdgeNode& aToFrom
            )
            :geomap::DirectedEdge(aEdge),
             mToFrom(aToFrom),
             mAccess(geomap::Way::TAccessAttribute::EAllowed,
                     geomap::Way::TAccessAttribute::EAllowed,
                     geomap::Way::TAccessAttribute::EAllowed)
            {}

            virtual const geomap::DirectedEdge::Nodes nodes() const {
                return geomap::DirectedEdge::Nodes();
            }

            virtual unsigned long distanceMts() const {
                return 0l;
            }

            virtual const geomap::Edge::EdgeNode& from() const {
                return mToFrom;
            }

            virtual const geomap::Edge::EdgeNode& to() const {
                return mToFrom;
            }

            virtual geomap::Edge::ClimbPercent climbPercent() const {
                return geomap::Edge::ClimbPercent(0);
            }

            virtual const geomap::Way::TOneDirectionAccessAttributes& accessAttributes() const  {
                return mAccess;
            }
        private:
            const geomap::Edge::EdgeNode& mToFrom;
            const geomap::Way::TOneDirectionAccessAttributes mAccess;
    };


    class AStar : public IAlgorithmFactory::IAlgorithm
    {

        class RouteEdge  : public routing::RouteHead {

            public:
                typedef OpenHeapFib<CostFunction::Cost, RouteEdge> TOpenHeap;

                RouteEdge(
                    const geomap::DirectedEdge::ConstHandle& aDirectedEdge,
                    const AStar& aAStar,
                    const RouteEdge* aPrev = nullptr
                )
                :routing::RouteHead(aDirectedEdge, aPrev),
                 mAStar(aAStar),
                 mG(0),
                 mInClosedSet(isInClosedSet(*aDirectedEdge, aAStar.mThreadId)),
                 mOpenSetHandle(nullptr)
                {
                    assert(!mInClosedSet);
                    mG = calculateG();
                }

                ~RouteEdge() {
                    mDirectedEdge->to().mutableThreadSlot(mAStar.mThreadId) = reinterpret_cast<void*>(0x00);
                }

                /*
                 * Returns A*'s H value, i.e.: the heuristic estimate of the cost from 'this' to destination
                 */
                inline CostFunction::Cost H(const geo::Point& aDestination) const {
                    //Manhattan heuristic towards destination
                    return mAStar.mCostFunction.heuristics_H(mDirectedEdge->to());
                }

                /*
                 * Returns A*'s G value, i.e.: the cost of navigating to this from the origin
                 */
                inline CostFunction::Cost G() const {
                    return mG;
                }

                /*
                 * Returns A*'s F value, i.e.: G+H
                 */
                inline CostFunction::Cost F(const geo::Point& aDestination) const {
                    return G() + H(aDestination);
                }

                inline const geomap::Edge::EdgeNode::DirectedEdges& neighbours() const {
                    return mDirectedEdge->to().edges();
                }

                const void dump(Route& aRoute) const;

                inline void moveToClosedSet() {
                    mInClosedSet = true;
                    mDirectedEdge->to().mutableThreadSlot(mAStar.mThreadId) = reinterpret_cast<void*>(this);
                }

                static inline bool isInClosedSet(const geomap::DirectedEdge& aDirectedEdge, unsigned short aThreadId) {
                    RouteEdge* stashed = reinterpret_cast<RouteEdge*>(aDirectedEdge.to().mutableThreadSlot(aThreadId));
                    return stashed != nullptr && stashed->mInClosedSet;
                }

                inline RouteEdge* getFromOpenSet() const {
                    assert(mInClosedSet == false);
                    return reinterpret_cast<RouteEdge*>(mDirectedEdge->to().mutableThreadSlot(mAStar.mThreadId));
                }

                inline TOpenHeap::OpenSetHandle getOpenSetHandle() const {
                    assert(mInClosedSet == false);
                    return mOpenSetHandle;
                }

                inline void moveToOpenSet(TOpenHeap::OpenSetHandle aOpenSetHandle) {
                    mOpenSetHandle = aOpenSetHandle;
                    mDirectedEdge->to().mutableThreadSlot(mAStar.mThreadId) = reinterpret_cast<void*>(this);
                }

#ifndef NDEBUG
                std::string toString() const {
                    std::stringstream result;
                    result.imbue(std::locale("C"));
                    result << "RouteEdge[";
                    result << this;
                    result << ": ";
                    result << edge().from().toString();
                    result << "->";
                    result << edge().to().toString();
                    result << "]";
                    return result.str();
                }
#endif
            private:
                inline CostFunction::Cost calculateG() const {
                    return (mPrev != nullptr ? static_cast<const RouteEdge*>(mPrev)->G() : 0) + calculateCost();
                }

                inline CostFunction::Cost calculateCost() const {
                    return mAStar.mCostFunction.evaluate(*this);
                }

                const AStar&                            mAStar;
                CostFunction::Cost                      mG;
                bool                                    mInClosedSet;
                TOpenHeap::OpenSetHandle                mOpenSetHandle;
        };

        public:
            const static unsigned long KAbortOnNoOfJunctions = 2000000;

            AStar(const geomap::Edge::EdgeNode::ConstAutoHandle& aFrom,
                  const geomap::Edge::EdgeNode::ConstAutoHandle& aTo,
                  const CostFunction& aCostFunction);

        public:
            virtual void route(Route& aRoute);
            inline std::string debugJSON() const {
                std::stringstream ss;
                ss << "{ \"name\" : \"astar\", \"visitedJunctions\": " << mInClosedSet + mOpenSetHeap.size() << "}";
                return ss.str();
            }

        private:

            class AuxEdges : public std::pair<geomap::ReversedDirectedEdge, geomap::ReversedDirectedEdge> {
                public:
                    AuxEdges(const geomap::Edge::DirectedEdge& aAwayOne, const geomap::Edge::DirectedEdge& aAwayTwo)
                    :std::pair<geomap::ReversedDirectedEdge, geomap::ReversedDirectedEdge>(
                        geomap::ReversedDirectedEdge(aAwayOne), geomap::ReversedDirectedEdge(aAwayTwo)
                    )
                    {}

                void checkAndVisit(const geomap::Edge::EdgeNode& aCurrent, std::function<void(const geomap::Edge::DirectedEdge::ConstHandle&)> aFunc);

                static boost::optional<AuxEdges> populate(const geomap::Edge::EdgeNode& aDestination);
                static bool hasNeighbour(const geomap::Edge::EdgeNode& aReference, const geomap::Edge::EdgeNode& aNeighbour);

                private:
                    std::pair<std::unique_ptr<RouteEdge>, std::unique_ptr<RouteEdge> > mRouteEdges;
            };

            inline bool destinationReached(const AStar::RouteEdge& aCurrent, std::function<void(const geomap::Edge::DirectedEdge::ConstHandle&)> aFunc);
            inline void visitEdge(AStar::RouteEdge* aEdge);

            RouteEdge::TOpenHeap                              mOpenSetHeap;
            unsigned short                                    mThreadId;
            OriginEdge                                        mOriginEdge;
            const geomap::Edge::EdgeNode::ConstAutoHandle&    mTo;
            boost::optional<AuxEdges>                         mAuxEdges;
            const CostFunction&                               mCostFunction;
            unsigned long                                     mInClosedSet;
    };

    class AStarFactory : public IAlgorithmFactory {
        public:
        virtual IAlgorithmFactory::IAlgorithm* create(
            const geomap::Edge::EdgeNode::ConstAutoHandle& aFrom,
            const geomap::Edge::EdgeNode::ConstAutoHandle& aTo,
            const CostFunction& aCostFunction
         ) {
            return new AStar(aFrom, aTo, aCostFunction);
        }

        virtual ~AStarFactory() {};
    };
}

#endif // ASTAR_H
