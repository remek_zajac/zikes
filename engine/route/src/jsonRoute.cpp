
/*************************************************************************
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * (C) 2015 Remek Zajac
 *************************************************************************/

#include <boost/algorithm/string/join.hpp>
#include <boost/optional/optional_io.hpp>
#include <boost/format.hpp>


#include "routing.h"
#include "geomap/geomap.h"

#include "route/jsonRoute.h"


namespace routing {

    const std::string KEleMts          = "e";
    const std::string KHighwayType     = "h";
    const std::string KTerminator      = "\"t\":true";
    const std::string KAccessForbidden = "\"a\":\"forbidden\"";

    void JsonRoute::write(std::ostream& aOStream, bool markEnd) const
    {
        this->forEachNode( [&] (const geomap::Node& aNode,
                                const geomap::Edge::EdgeNode* aWhichIsEdgeNode,
                                const geomap::Edge::DirectedEdge* aDirectedEdgeForward) {
            std::string decorationNode;
            bool last = false;
            if (aWhichIsEdgeNode != nullptr) {
                last = aDirectedEdgeForward == nullptr;
                std::list<std::string> decorators;
                if (aWhichIsEdgeNode->elevationMts()) {
                    decorators.push_back(
                        (boost::format("\"%s\":%d") % KEleMts % aWhichIsEdgeNode->elevationMts()).str()
                    );
                }
                if (aDirectedEdgeForward) {
                    decorators.push_back(
                        (boost::format("\"%s\":%d") % KHighwayType % aDirectedEdgeForward->highwayType()).str()
                    );
                    if ((aDirectedEdgeForward
                        ->accessAttributes()
                        .access(mMeans) == geomap::Way::TAccessAttribute::EForbidden) &&
                        (aDirectedEdgeForward->distanceMts() > 0) //TODO: sometimes those get picked accidentally as they cost nothing. Need to give them some token cost?
                    ) {
                            decorators.push_back(KAccessForbidden);
                    }
                }
                if (markEnd && last) {
                    decorators.push_back(KTerminator);
                }

                if (decorators.size() > 0) {
                    decorationNode = (boost::format(",{%s}") % boost::algorithm::join(decorators, ",")).str();
                }
            }

            aOStream << "[" << aNode.latStr() << "," << aNode.lonStr() << decorationNode << "]" << (last ? "" : ",");
        });
    }
}
