/*************************************************************************
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * (C) 2015 Remek Zajac
 *************************************************************************/
#ifndef OPENHEAP_H
#define OPENHEAP_H

#include <boost/heap/fibonacci_heap.hpp>

template <class PRIORITY, class ELEMENT>
class OpenHeapFib
{
    private:
        typedef std::pair<PRIORITY, ELEMENT*> OpenHeapElement;
        struct HeapElementCompare {
            /* this is a 'greater than' because boost implements max-heaps */
            bool operator()(const OpenHeapElement& n1, const OpenHeapElement& n2) const {
                return n1.first > n2.first;
            }
        };
        typedef boost::heap::fibonacci_heap<OpenHeapElement, boost::heap::compare<HeapElementCompare> > PriotityQ;

    public:
        typedef typename PriotityQ::handle_type OpenSetHandle;

        OpenHeapFib()
        :mCompare(HeapElementCompare())
        {}

        void push(PRIORITY aF, ELEMENT& aEdge) {
            OpenHeapElement newElement(aF, &aEdge);
            OpenSetHandle existingHandle = aEdge.getOpenSetHandle();
            if (existingHandle.node_ != nullptr) {
                assert(!mCompare(*existingHandle, newElement));
                mPriorityQueue.increase(existingHandle, newElement);
            } else {
                existingHandle = mPriorityQueue.push(newElement);
            }
            aEdge.moveToOpenSet(existingHandle);
        }

        ELEMENT& pop() {
            const OpenHeapElement& top = mPriorityQueue.top();
            mPriorityQueue.pop();
            return const_cast<ELEMENT&>(*top.second);
        }

        unsigned int size() const {return mPriorityQueue.size();}

    private:
        PriotityQ                mPriorityQueue;
        const HeapElementCompare mCompare;
};

#endif // OPENHEAP_H
