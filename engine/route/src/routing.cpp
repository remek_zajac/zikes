/*************************************************************************
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * (C) 2014 Remek Zajac
 *************************************************************************/

#include "geomap/geomap.h"
#include "../routing.h"
#include "astar.h"

namespace routing {

    void Route::forEachNode(std::function<void(const geomap::Node&,
                                               const geomap::Edge::EdgeNode* /*aWhichIsEdgeNode*/,
                                               const geomap::Edge::DirectedEdge* /*aDirectedEdgeForward*/)> aFunc) const {
        if (size() == 0) {
            return;
        }        
        for (auto eit = boost::begin(*this); eit != boost::end(*this); eit++) {
            const geomap::Edge::DirectedEdge& edge = **eit;
            aFunc(edge.from(), &edge.from(), &edge);
            for (auto nit = boost::next(boost::begin(edge.nodes()));
                      nit != boost::end(edge.nodes()) && boost::next(nit) != boost::end(edge.nodes());
                      nit++)
            {
                //bar the first and last node of each edge as it is repeated at the start of the next
                aFunc(**nit, nullptr, nullptr);
            }
        }
        aFunc((*boost::rbegin(*this))->to(), &(*boost::rbegin(*this))->to(), nullptr);
    }

    void Route::forEachEdgeNode(std::function<void(const geomap::Edge::EdgeNode&)> aFunc) const {
        auto it = boost::begin(*this);
        aFunc((*it)->from());
        for ( ;it != boost::end(*this); it++) {
            aFunc((*it)->to());
        }
    }

    unsigned long Route::distanceMts() const {
        unsigned long result = 0;
        for (auto it = boost::begin(*this); it != boost::end(*this); it++) {
            result += (*it)->distanceMts();
        }
        return result;
    }


    bool Route::operator==(const Route& aOther) const {
        if (size() == aOther.size()) {
            auto thisRoute_it  = boost::begin(*this);
            auto otherRoute_it = boost::begin(aOther);
            for (;thisRoute_it != boost::end(*this) && otherRoute_it != boost::end(aOther);
                 thisRoute_it++, otherRoute_it++) {
                auto this_nodeIt  = boost::begin((*thisRoute_it)->nodes());
                auto other_nodeIt = boost::begin((*otherRoute_it)->nodes());
                for (;this_nodeIt != boost::end((*thisRoute_it)->nodes()) && other_nodeIt != boost::end((*otherRoute_it)->nodes());
                     this_nodeIt++, other_nodeIt++) {
                    if (**this_nodeIt != **other_nodeIt) {
                        return false;
                    }
                }
                if (!(this_nodeIt == boost::end((*thisRoute_it)->nodes()) && other_nodeIt == boost::end((*otherRoute_it)->nodes()))) {
                    return false;
                }
            }
            return true;
        }
        return false;
    }

    /*
     * Finds the route and responds synchronously
     */
    Request& Request::execute(Route& aRoute)
    {
        if (!mFrom) {
            throw std::invalid_argument("'from' not specified");
        }
        if (!mTo) {
            throw std::invalid_argument("'to' not specified");
        }
        CostFunction::UniquePtr costFunction = mCostFunctionFactory.create(*mFrom, *mTo);
        mAlgorithm = std::unique_ptr<routing::IAlgorithmFactory::IAlgorithm>(
            AStarFactory().create(mFrom, mTo, *costFunction)
        );
        mAlgorithm->route(aRoute);
        return *this;
    }

};
