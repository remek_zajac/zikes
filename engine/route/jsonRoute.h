/*************************************************************************
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * (C) 2015 Remek Zajac
 *************************************************************************/

#ifndef JSONROUTE_H
#define JSONROUTE_H

#include <ostream>
#include "routing.h"

namespace routing {

    class JsonRoute : public Route {
        public:
            JsonRoute(geomap::Way::TMeans aMeans)
            :mMeans(aMeans) {}

            void write(std::ostream& aOStream, bool markEnd = false) const;
        private:
            geomap::Way::TMeans mMeans;
    };
};

#endif // JSONROUTE_H
