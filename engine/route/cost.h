/*************************************************************************
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * (C) 2015 Remek Zajac
 *************************************************************************/
#ifndef COST_H
#define COST_H

#include <climits>

#include "geomap/geomap.h"

namespace routing {

    /*
     * A class representing a candidate node in the route (being calculated). It's a RouteHead as it refers to the previous/upstream RouteHead.
     */
    class RouteHead : private boost::noncopyable {
        public:
            RouteHead(const geomap::DirectedEdge::ConstHandle& aDirectedEdge, const RouteHead* aPrev = nullptr)
            :mDirectedEdge(aDirectedEdge),
             mPrev(aPrev)
            {}

            inline const RouteHead* prev() const {
                return mPrev;
            }

            inline const geomap::Edge::DirectedEdge& edge() const {
                return *mDirectedEdge;
            }

            const geomap::Edge::DirectedEdge::ConstHandle mDirectedEdge;
            const RouteHead*                              mPrev;

            /*
             * As the CostFunction goes about doing its job it may like to deposit some kind of context
             * information that may be useful later. Namely; when a new RouteHead is put on top of 'this' and calculating
             * the cost for that new RouteHead is either easier or more efficient if 'this' (the upstream predecessor) stores
             * some context.
             *
             * For instance this is may be useful when working out urban/rural character from the density of the road
             * network. One cannot conclude this character based on the length of a signgle segment (DirectedEdge),
             * but when consecutive segments are observably short, it's a good chance we're trawling through a city and
             * thus the cost function, upon observing a short segment could look back to see if that was also the case
             * immedially upstream.
             *
             * TODO: as the CostFunction tends to be compound, different compounds may have different needs with respect
             * to the Context type. It would then be desirable to set up a cheap, compiletime facility that allows all
             * interested compounds to deposit different kinds of context. This is non-trivial and at the time of typing
             * this, not needed. It's only useful for the urban/rural comopund and is named accordingly.
             */
            class Context {
                public:
                    typedef std::unique_ptr<Context> UniqPtr;
#ifndef NDEBUG
                    virtual std::string toJson() const = 0;
#endif
            };
            mutable Context::UniqPtr mUrbanScore;
    };

    class CostFunction
    {
        public:
            typedef unsigned long long Cost;
            typedef unsigned short     CostFactor;

            typedef std::unique_ptr<const CostFunction> UniquePtr;


            /*
             * Should calculate and return the cost of navigating the given aEdge.
             * The implementation can consider previous edge (aPrevEdge) if it wishes
             * (e.g.: to allow different cost for right and left turns).
             *
             * The cost is expressed as Cost and:
             * * strictly, must not be less than aEdge.distanceMts().
             *   (#A* algorithm (http://en.wikipedia.org/wiki/A*_search_algorithm)
             *   uses the heuristic estimate (h(x)) of the future cost when traversing
             *   the grapth. This heuristic "The h(x) part of the f(x) function must be an
             *   admissible heuristic; that is, it must not overestimate the distance to
             *   the goal."
             * * comparable with other Cost values returned by this (and only this)
             *   implementation, i.e.: must each time be generated using the same logic.
             * * can express that the given edge isn't navigable, by returning
             *   CostFunction::FORBIDDEN;
             */
            virtual Cost evaluate(const RouteHead& aRouteHead) const = 0;

            virtual ~CostFunction() {};

            /*
             * For the befit of the A*, calculates the heuristic distance based on the given distance.
             */
            virtual Cost heuristics_H(const geo::Point& aForVertex) const = 0;
    };

    class CostFunctionFactory {
        public:
            virtual CostFunction::UniquePtr create(const geo::Point& aFrom, const geo::Point& aTo) const = 0;
    };


    /*
     * Utility class that will convert a floating number cost (multiplication) factor into an integral
     * CostFunction::CostFactor. It will also coerce it into the limits generated by the template parameters.
     *
     * Floats are a natural way of expressing cost factors and they naturally form reward [0..1] and punish
     * [1..9999] zones, but they're unsuitable for the path-finding algorithm for two reasons:
     * - floats are more expensive to manage (multiply by)
     * - reward floats used verbatim will violate the admissable heuristic property - will generate
     *   costs lower than distances.
     * In essence, the path-finding can only use punishments (never rewards). This class converts
     * reward..punishmment ranges to punishment only ranges.
     *
     * The class is intialised with two template parameters:
     * - _MaxFloatFactor - the maximum value of the float factor expected as the argument to
     *   FloatFactor2IntPunishment::convert as well as the upper limit for the punishment.
     *   This upper limit is only there to level the ground for all the contributors so that, misbehaving
     *   or not, they are coerced to the same order of magnitude of influence and don't end up overpowering
     *   each other. With that in mind, callers can still offer float factors exceeding _MaxFloatFactor provided
     *   they fit into CostFunction::CostFactor.
     * - REWARD_RES_EXPONENT number of bits of resolution reserved for the reward range between 0.._MaxFloatFactor.
     *
     * Suppose they're set to 4 and 512 then:
     * the reward range 0..1 will have 1..16 distinct values:
     * - 0.0    -> is illegal as it violates the admissable heuritisc property
     * - 0.01   -> max(0.01*16,1)   -> max(0.16,1) -> 1
     * - 0.0625 -> max(0.0625*16,1) -> max(1,1)    -> 1
     *   beyond which and until 1.0 it is a linear function capable of producting 16 distinct values.
     * the punish range is expected to be largely under-utilised - there is a more significant difference
     * between float factors of 1 and 1.5 than there is between 300 and 300.5 for instance, so a
     * quadratic or exponential scales could make a better use of the punish zone, but having it linear
     * keeps it intutivelly mapped to the float factors.
     */
    template <unsigned char REWARD_RES_EXPONENT, unsigned int _MaxFloatFactor>
    class FloatFactor2IntPunishment {
            static_assert(REWARD_RES_EXPONENT > 1, "reward resolution must be greater than 1");
            static_assert(_MaxFloatFactor > 1,     "_MaxFloatFactor must be greater than 1");
            enum {
                ERewardResolutionExponent = REWARD_RES_EXPONENT
            };
        public:
            enum {
                MinPunishFactor     = 1,
                NeutralPunishFactor = MinPunishFactor     << ERewardResolutionExponent,
                MaxPunishFactor     = NeutralPunishFactor * _MaxFloatFactor,
                MaxFloatFactor      = _MaxFloatFactor
            };

            /*
             * A utility class holding onto the rolling CostFunction::CostFactor as it accumulates
             * (in an overflow-safe way) the individual component contributions.
             *
             * A Multiplier object will make sure that, in whever state it is, when given a float factor
             * of 'a' (more precisely its punish-only expression returned by FloatFactor2IntPunishment::convert)
             * - its new value will be 'a' times of its previous value.
             */
            class Multiplier {
                public:
                    Multiplier()
                    :mFactor(NeutralPunishFactor) {}

                    enum {
                        MaxPunishFactor = 1 << sizeof(int)*4
                    };

                    Multiplier apply(CostFunction::CostFactor aFactor) {
                        mFactor = std::max(
                            std::min(
                                (CostFunction::Cost)(mFactor * aFactor) >> ERewardResolutionExponent,
                                (CostFunction::Cost)MaxPunishFactor
                            ),
                            (CostFunction::Cost)MinPunishFactor
                        );
                        return *this;
                    }

                    Multiplier applyf(float aFloatFactor) {
                        return apply(convert(aFloatFactor));
                    }

                    CostFunction::Cost factor() {
                       return mFactor;
                    }

                private:
                    CostFunction::Cost mFactor;
            };

            static CostFunction::CostFactor convert(float aFloatFactor) {
                aFloatFactor = std::min(aFloatFactor, (float)MaxFloatFactor);
                if (aFloatFactor < 0.0f) {
                    throw std::out_of_range("");
                }
                return std::max(
                    (CostFunction::CostFactor)(round(aFloatFactor*NeutralPunishFactor)),
                    (CostFunction::CostFactor)MinPunishFactor
                );
            }
    };



    /*
     * A Basic implementation of the CostFunction interface that simply
     * returns the distance (this is the default cost function)
     */
    class DistanceCostFunction : public CostFunction
    {
        public:
            typedef std::unique_ptr<DistanceCostFunction> UniquePtr;

            DistanceCostFunction(const geo::Point& aTo)
            :mTo(aTo)
            {}

            virtual Cost evaluate(const RouteHead& aRouteHead) const { return aRouteHead.edge().distanceMts(); }
         protected:
            const geo::Point mTo;
    };


    class MtsDistanceCostFunction : public DistanceCostFunction {
        public:

            MtsDistanceCostFunction(const geo::Point& aTo)
            :DistanceCostFunction(aTo)
            {}

            virtual Cost heuristics_H(const geo::Point& aForVertex) const {
                return aForVertex.distanceMts(mTo);
            }

            static inline CostFunction::Cost distanceMts(const geo::Point& aFrom, const geo::Point& aTo) {
                return aFrom.distanceMts(aTo);
            }
    };

    class ApproxDistanceCostFunction : public DistanceCostFunction {
        public:

            ApproxDistanceCostFunction(const geo::Point& aTo)
            :DistanceCostFunction(aTo)
            {}

            virtual Cost heuristics_H(const geo::Point& aForVertex) const {
                return aForVertex.approxDistanceMts(mTo);
            }

            static inline CostFunction::Cost distanceMts(const geo::Point& aFrom, const geo::Point& aTo) {
                return aFrom.approxDistanceMts(aTo);
            }
    };

    template <class KIND>
    class DistanceCostFunctionFactory : public CostFunctionFactory {
        public:
            virtual CostFunction::UniquePtr create(const geo::Point& aFrom, const geo::Point& aTo) const {
                return std::move(
                    CostFunction::UniquePtr(new KIND(aTo))
                );
            }
    };
}
#endif // COST_H
