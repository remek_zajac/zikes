/*************************************************************************
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * (C) 2015 Remek Zajac
 *************************************************************************/

#ifndef GPXROUTE_H
#define GPXROUTE_H

#include <ostream>
#include "routing.h"

namespace routing {

    class GpxRoute : public Route
    {
        public:
            void write(std::ostream& aOStream) const {
                write(aOStream, *this);
            }
            static void write(std::ostream& aOStream, const Route& aRoute);
    };
};

#endif // GPXROUTE_H
