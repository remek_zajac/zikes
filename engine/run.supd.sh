#!/usr/bin/env bash
now=$(date +%s)
pushd /home/zikes

ENGINE=./engine
MAPS=./maps.pb
$ENGINE --maps $MAPS

logFileGlob="./zikes_*.log"
unset -v latest
for file in $logFileGlob; do
	[[ $file -nt $latest ]] && latest=$file
done

if [ -n "$latest" ]; then
	mv "$latest" "$latest.$now.fatal"
	rm $logFileGlob
fi

popd