/*************************************************************************
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * (C) 2015 Remek Zajac
 *************************************************************************/
#ifndef URLQUERY_H
#define URLQUERY_H

#include <map>
#include "exceptions.h"

namespace http {

    class UrlQuery : public std::map<std::string, std::string> {
        public:

            UrlQuery(const std::string& aUrl);

            const std::string& at(const std::string& aKey, const std::string& aDefaultValue) const {
                if (find(aKey) == end()) {
                    return aDefaultValue;
                }
                return at(aKey);
            }

            const std::string& at(const std::string& aKey) const {
                if (find(aKey) == end()) {
                    throw http::Exception(
                        "Required query argument missing: "+aKey,
                        400
                    );
                }
                return std::map<std::string, std::string>::at(aKey);
            }
    };
}

#endif // URLQUERY_H
