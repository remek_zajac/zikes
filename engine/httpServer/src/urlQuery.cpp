/*************************************************************************
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * (C) 2014 Remek Zajac
 *************************************************************************/
#include <sstream>
#include "../urlQuery.h"

namespace http {

    UrlQuery::UrlQuery(const std::string& aQueryString) {
        std::size_t pos = aQueryString.find('#');
        if (pos == std::string::npos) {
            pos = aQueryString.length();
        }
        std::stringstream queryStream(aQueryString.substr(0,pos));
        std::string param;
        while (std::getline(queryStream, param, '&')) {
            pos = param.find("=");
            if (pos == std::string::npos) {
                (*this)[param] = "";
                continue;
            }
            (*this)[param.substr(0,pos)] = param.substr(pos+1);
        }
    }
}
