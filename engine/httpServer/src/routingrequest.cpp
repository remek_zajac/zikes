/*************************************************************************
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * (C) 2015 Remek Zajac
 *************************************************************************/

#include <algorithm>
#include <boost/log/trivial.hpp>
#define LOG(level) BOOST_LOG_TRIVIAL(level) << "[http::Server]> "

#include "cost/CompoundCostFunction.h"
#include "cost/components/all.h"

#include "routingrequest.h"
#include "../exceptions.h"

namespace http {

    RoutingRequest::RoutingRequest(
            const UrlQuery& aUrlQuery,
            std::ostream& aOutput,
            std::istream& aPostIstream
    )
    :mUrlQuery(aUrlQuery),
     mOutput(aOutput),
     mPostIstream(aPostIstream)
    {}


    RoutingRequest::RoutingRequest(
            const UrlQuery& aUrlQuery,
            std::ostream& aOutput
    )
    :mUrlQuery(aUrlQuery),
     mOutput(aOutput)
    {}

    std::shared_ptr<const std::vector<geo::Point> > RoutingRequest::parseMilestones(const std::string& aMilestones) {
        std::stringstream milestonesStream(aMilestones);
        std::shared_ptr<std::vector<geo::Point> > result = std::make_shared<std::vector<geo::Point> >();
        std::string dvalue;
        unsigned short accum = 0;
        geo::Point::CoordinateType lat;
        while (std::getline(milestonesStream, dvalue, ',')) {
            if (accum % 2 == 0) {
                lat = (geo::Point::CoordinateType) toDouble(
                    dvalue,
                    "When parsing lat of the "+std::to_string(accum/2+1)+"th milestone"
                );
            } else {
                geo::Point point = geo::Point(
                    lat,
                    (geo::Point::CoordinateType) toDouble(
                        dvalue,
                        "When parsing lon of the "+std::to_string(accum/2+1)+"th milestone"
                    )
                );
                result->push_back(point);
            }
            accum++;
        }
        return result;
    }

    void RoutingRequest::execute() {
        cost::CompoundCostFunctionProfile::ConstUniqPtr postedProfile;
        boost::optional<const cost::CompoundCostFunctionProfile&> requestedProfile;

        if (mPostIstream) {
            try {
                postedProfile = cost::CompoundCostFunctionProfile::ConstUniqPtr(
                    new cost::CompoundCostFunctionProfile(*mPostIstream, cost::AllComponents::construct())
                );
                requestedProfile = *postedProfile;
            } catch (const cost::CompoundCostFunctionProfile::Exception& e) {
                throw http::Exception(
                    e.what(),
                    400
                );
            } catch (const std::exception& e) {
                throw http::Exception(
                    "Unable to parse the uploaded profile",
                    400
                );
            }

        } else {
            requestedProfile = http::StaticProfiles::instance().routingProfile(mUrlQuery.at("profile"));
            if (!requestedProfile) {
                throw http::Exception(
                    "Unable to find the requested server-preconfigured profile: " + mUrlQuery.at("profile"),
                    400
                );
            }
        }
        auto milestones = parseMilestones(mUrlQuery.at("milestones"));
        cost::CompoundCostFunctionFactory<routing::MtsDistanceCostFunction> costFunctionFactory(
            *requestedProfile,
            cost::CompoundCostFunctionFactory<routing::MtsDistanceCostFunction>::ReferencePoint(10000,  10000),
            cost::CompoundCostFunctionFactory<routing::MtsDistanceCostFunction>::ReferencePoint(100000, 20000)
        );

        try {
            execute(*milestones, costFunctionFactory, requestedProfile->means());
        } catch (const routing::Request::NearestPointNotFound& aNearestPointNotFound) {
            throw http::NearestPointNotFound(aNearestPointNotFound);
        } catch (const routing::IAlgorithmFactory::IAlgorithm::ResourceConsumptionOverload& aResourceConsumptionOverload) {
            throw http::ServerOverload(aResourceConsumptionOverload.mJunctions);
        }
    }



    void RoutingRequest::execute(
        const std::vector<geo::Point>& aMilestones,
        const routing::CostFunctionFactory& aCostFunctionFactory,
        geomap::Way::TMeans aMeans) {

        if (aMilestones.size() < 2) {
            throw http::RoutingException("Not enough milestones supplied");
        }
        bool preambleWritten = false;
        unsigned long lengthMts = 0;
        mRequests.push_back(RequestPtr(new routing::Request(aCostFunctionFactory)));
        std::stringstream routingDebug;
        for (unsigned short i = 0; i < aMilestones.size(); i++) {
            if (i == 0) {
                mTimeStats.measureLookup( [&] () {
                    mRequests.back()->setFrom(aMilestones.at(i));
                });
            } else {
                routing::JsonRoute obtainedRoute(aMeans);
                routing::Request& routingRequest = *mRequests.back();

                mTimeStats.measureLookup( [&] () {
                    routingRequest.setTo(aMilestones.at(i));
                });

                mTimeStats.measureRouting( [&] () {
                    routingRequest.execute(obtainedRoute);
                });
                if (obtainedRoute.size() == 0) {
                    throw http::NoRoute();
                }

                if (!preambleWritten) {
                    mOutput << "{ \"waypoints\":[";
                    preambleWritten = true;
                } else {
                    mOutput << ",";
                    routingDebug << ",";
                }
                routingDebug << routingRequest.debugJSON();

                obtainedRoute.write(
                    mOutput,
                    aMilestones.size() > 2 && i != aMilestones.size()-1
                );
                lengthMts += obtainedRoute.distanceMts();

                mRequests.push_back(RequestPtr(new routing::Request(aCostFunctionFactory)));
                mRequests.back()->setFrom(routingRequest.getTo());
            }
        }

        if (preambleWritten) {
            mOutput << "],\n\"meta\":{\n"
                    << "    \"avgLookup_us\":"   << mTimeStats.avgLookupMicro() << ",\n"
                    << "    \"minLookup_us\":"   << mTimeStats.minLookupNano / 1000 << ",\n"
                    << "    \"maxLookup_us\":"   << mTimeStats.maxLookupNano / 1000 << ",\n"
                    << "    \"routing_us\":"     << mTimeStats.routingTookNano / 1000 << ",\n"
                    << "    \"lengthMts\":"      << lengthMts << ",\n"
                    << "    \"routingRequests\": [" << routingDebug.str() << "]\n"
                    << "}";
            mOutput << "}" << std::flush;
        }
    }


    double RoutingRequest::toDouble(const std::string& value, const std::string& whenMessage)
    {
        try {
            return std::stod(value);
        } catch (const std::invalid_argument& einvalid_arg) {
            throw http::Exception(
                "Unable to perform the expected conversion of value '"+value+"' to a double precission floating point number. "
                + whenMessage,
                400
            );
        } catch (const std::out_of_range& eoor) {
            throw http::Exception(
                "Unable to perform the expected conversion of value '"+value+"' to a double precission floating point number, value of out range. "
                + whenMessage,
                400
            );
        }
    }

    std::string RoutingRequest::availableProfilesHtml() {
        std::stringstream result;
        for (auto it = boost::begin(http::StaticProfiles::instance().routingProfiles());
                  it != boost::end(http::StaticProfiles::instance().routingProfiles());
                  it++) {
            const boost::filesystem::path pathToProfile;
            result << "<li><b><a href=\"" << it->second->fileName() << "\">" << it->first << "</a></b> - " << (it->second ? it->second->description() : "unable to parse") << "</li>";
        }
        return result.str();
    }
}
