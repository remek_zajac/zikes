/*************************************************************************
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * (C) 2015 Remek Zajac
 *************************************************************************/
#ifndef ROUTINGREQUEST_H
#define ROUTINGREQUEST_H
#include <ostream>
#include <boost/timer/timer.hpp>
#include "geomap/geo.h"

#include "route/routing.h"
#include "../httpServer.h"
#include "../urlQuery.h"
#include "../staticprofiles.h"

namespace http {
    class RoutingRequest
    {

        struct TimeStats {

            unsigned long         accumulatedLookupsNano = 0;
            boost::int_least64_t  minLookupNano = std::numeric_limits<boost::int_least64_t>::max();
            boost::int_least64_t  maxLookupNano = 0;
            boost::int_least64_t  routingTookNano = 0;
            unsigned short        lookups = 0;

            unsigned long avgLookupMicro() {
                return (accumulatedLookupsNano / lookups) / 1000;
            }

            boost::timer::cpu_times measure(std::function<void()> aFunc) {
                boost::timer::cpu_timer timer;
                timer.start();
                aFunc();
                timer.stop();
                return timer.elapsed();
            }

            void measureLookup(std::function<void()> aFunc) {
                boost::timer::cpu_times times = measure(aFunc);
                lookups++;
                minLookupNano = std::min(minLookupNano, times.wall);
                maxLookupNano = std::max(maxLookupNano, times.wall);
                accumulatedLookupsNano += times.wall;
            }

            void measureRouting(std::function<void()> aFunc) {
                routingTookNano += measure(aFunc).wall;
            }
        };


        public:
            RoutingRequest(const UrlQuery& aUrlQuery, std::ostream& aOutput);
            RoutingRequest(const UrlQuery& aUrlQuery, std::ostream& aOutput, std::istream& aPostIstream);

            void execute();
            static std::string availableProfilesHtml();

        private:
            void execute(
                const std::vector<geo::Point>& aMilestones,
                const routing::CostFunctionFactory& aCostFunctionFactory,
                geomap::Way::TMeans aMeans //this perhaps isn't most elegant, perhaps it's the routing preferences that you should produce a Route object that can render itself, but for now..
            );
            std::shared_ptr<const std::vector<geo::Point> > parseMilestones(const std::string& aMilestones);

            static double toDouble(const std::string& value, const std::string& whenMessage);
            typedef std::unique_ptr<routing::Request> RequestPtr;
            const UrlQuery                   mUrlQuery;
            std::ostream&                    mOutput;
            TimeStats                        mTimeStats;
            std::vector<RequestPtr>          mRequests;
            boost::optional<std::istream&>   mPostIstream;
    };
}

#endif // ROUTINGREQUEST_H
