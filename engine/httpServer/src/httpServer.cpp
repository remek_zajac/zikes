/*************************************************************************
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * (C) 2014 Remek Zajac
 *************************************************************************/
#include <boost/log/trivial.hpp>
#include <boost/log/core.hpp>
#include <fstream>

#include "../httpServer.h"
#include "../urlQuery.h"

#include "routingrequest.h"
#include "exceptions.h"

#define LOG(level) BOOST_LOG_TRIVIAL(level) << "[http::Server]> "
#define MAX_JSON_CONTENT_SIZE 5120

namespace http {

    Server::Server(const std::string& bindAddress,
                   unsigned int bindPort,
                   unsigned short threadNo,
                   const std::string& aStaticFilesFolder,
                   const geomap::Map& aMap)
    :mServerImpl(),
     mBindAddress(bindAddress),
     mBindPort(bindPort),
     mStaticFilesFolder(aStaticFilesFolder),
     mTheMap(aMap),
     mOperationCount(0)
    {
        mServerImpl.config.address = bindAddress;
        mServerImpl.config.port = bindPort;
        mServerImpl.config.thread_pool_size = threadNo;
        initLog();
        http::StaticProfiles::init(aStaticFilesFolder);

        mServerImpl.resource["^/info$"]["GET"]=[](std::shared_ptr<HttpServerImpl::Response> response, std::shared_ptr<HttpServerImpl::Request> request) {
            std::stringstream responseStream;
            responseStream << "<h1>Request:</h1>";
            responseStream << request->method << " " << request->path << " HTTP/" << request->http_version << "<br>";
            for(auto& header: request->header) {
                responseStream << header.first << ": " << header.second << "<br>";
            }

            //find length of responseStream (length received using responseStream.tellp())
            responseStream.seekp(0, std::ios::end);

            *response <<  "HTTP/1.1 200 OK\r\nContent-Length: " << responseStream.tellp() << "\r\n\r\n" << responseStream.rdbuf();
        };

        mServerImpl.resource["^/meta.json$"]["GET"]=[&](std::shared_ptr<HttpServerImpl::Response> response, std::shared_ptr<HttpServerImpl::Request> request) {
            std::stringstream responseStream;
            responseStream << "{ \"coverage\" : [";
            for (auto coverageFilesIt = boost::begin(mTheMap.getMeta().getCoverageFiles());
                      coverageFilesIt != boost::end(mTheMap.getMeta().getCoverageFiles());
                      coverageFilesIt++) {

                const boost::filesystem::path path(*coverageFilesIt);
                if (!(boost::filesystem::exists(path) && boost::filesystem::is_regular_file(path))) {
                    continue;
                }

                std::ifstream ifs;
                ifs.open(path.string(), std::ifstream::in | std::ios::binary);

                if(ifs) {
                    if (coverageFilesIt != boost::begin(mTheMap.getMeta().getCoverageFiles())) {
                        responseStream << ",";
                    }
                    ifs.seekg(0, std::ios::beg);

                    //read and send 128 KB at a time
                    size_t buffer_size=131072;
                    std::vector<char> buffer;
                    buffer.reserve(buffer_size);
                    size_t read_length;
                    try {
                        while((read_length=ifs.read(&buffer[0], buffer_size).gcount())>0) {
                            responseStream.write(&buffer[0], read_length);
                            responseStream.flush();
                        }
                    }
                    catch(const std::exception &e) {}
                    ifs.close();
                }
            }
            responseStream << "],";
            responseStream << "\"highways\" : {";
            long rewindToPos = 0;
            for (auto highway : mTheMap.getMeta().getHighwayTypes()) {
                responseStream  << "\"" << highway.first << "\"" << ":" << highway.second;
                rewindToPos = responseStream.tellp();
                responseStream << ",";
            }
            responseStream.seekp(rewindToPos);
            responseStream << "}";

            responseStream << "}";
            *response <<  "HTTP/1.1 200 OK\r\nContent-Length: " << responseStream.tellp() << "\r\n\r\n" << responseStream.rdbuf();
        };

        mServerImpl.resource["^/$"]["GET"]=[](std::shared_ptr<HttpServerImpl::Response> response, std::shared_ptr<HttpServerImpl::Request> request) {
            std::stringstream responseStream;
            responseStream << "<h1>Welcome to the Zikes routing server, available endpoints:</h1>";
            responseStream
                << "<ul> "
                <<      "<li><b>info</b> General server info</li>"
                <<      "<li><b>route?milestones=<i>milestone</i>,<i>milestone</i>,...<i>milestone</i>&profile=<i>profile</i></b>"
                <<              "<br>Basic routing api, where: "
                <<          "<ul>"
                <<              "<li><b>milestone</b> - lattitude and longitude separated by comma, e.g.: <i>0.510293,-29.30302</i></li>"
                <<              "<li><b>profile</b>   - name of a server-preconfigured routing profile, e.g.: <i>bicycle</i>, available profiles:"
                <<              "<ul>"
                <<                  RoutingRequest::availableProfilesHtml()
                <<              "</ul>"
                <<          "</ul>"
                <<      "</li>"
                << "</ul>";

            //find length of responseStream (length received using responseStream.tellp())
            responseStream.seekp(0, std::ios::end);

            *response <<  "HTTP/1.1 200 OK\r\nContent-Length: " << responseStream.tellp() << "\r\n\r\n" << responseStream.rdbuf();
        };

        mServerImpl.resource["^/route\?.+"]["GET"]=[](std::shared_ptr<HttpServerImpl::Response> response, std::shared_ptr<HttpServerImpl::Request> request) {
            std::stringstream responseStream;
            std::string code = "200 OK";

            //put RoutingRequest on the stack and let it manage cleanup on destruction (when the request is already fullfilled)
            RoutingRequest routingRequest(UrlQuery(request->query_string), responseStream);
            try {
                routingRequest.execute();
            } catch (const http::Exception& e) {
                responseStream << e.what();
                code = e.code2Str();
            } catch (const std::exception& e) {
                responseStream << e.what();
                code = "500 Internal Server Error";
            }

            //find length of responseStream (length received using responseStream.tellp())
            responseStream.seekp(0, std::ios::end);

            *response <<  "HTTP/1.1 " << code << "\r\nContent-Length: " << responseStream.tellp() << "\r\n\r\n" << responseStream.rdbuf();
        };

        mServerImpl.resource["^/route\?.+"]["POST"]=[this](std::shared_ptr<HttpServerImpl::Response> response, std::shared_ptr<HttpServerImpl::Request> request) {
            std::stringstream responseStream;
            std::string code = "200 OK";

            if (request->content.size() > MAX_JSON_CONTENT_SIZE) {
                responseStream << "Content too large";
                code = "400 Bad Request";
            } else {
                //put RoutingRequest on the stack and let it manage cleanup on destruction (when the request is already fullfilled)
                std::stringstream requestContentStream;
                requestContentStream << request->content.rdbuf();
                RoutingRequest routingRequest(UrlQuery(request->query_string), responseStream, requestContentStream);
                std::stringstream logStatement;
                logStatement << "serving[POST][" << request->path << "][" << requestContentStream.str();
                try {
                    log(logStatement.str(), [&routingRequest]() {
                        routingRequest.execute();
                    });
                } catch (const http::Exception& e) {
                    responseStream << e.what();
                    code = e.code2Str();
                } catch (const std::exception& e) {
                    responseStream << e.what();
                    code = "500 Internal Server Error";
                }
            }

            //find length of responseStream (length received using responseStream.tellp())
            responseStream.seekp(0, std::ios::end);
            *response <<  "HTTP/1.1 " << code << "\r\nContent-Length: " << responseStream.tellp() << "\r\n\r\n" << responseStream.rdbuf();
        };

        mServerImpl.default_resource["GET"]=[&](std::shared_ptr<HttpServerImpl::Response> response, std::shared_ptr<HttpServerImpl::Request> request) {
            boost::filesystem::path web_root_path(mStaticFilesFolder);
            if(boost::filesystem::exists(web_root_path)) {
                auto path=web_root_path;
                path+=request->path;
                if(boost::filesystem::exists(path)) {
                    if(boost::filesystem::canonical(web_root_path)<=boost::filesystem::canonical(path)) {
                        if(boost::filesystem::is_directory(path))
                            path+="/index.html";
                        if(boost::filesystem::exists(path) && boost::filesystem::is_regular_file(path)) {
                            std::ifstream ifs;
                            ifs.open(path.string(), std::ifstream::in | std::ios::binary);

                            if(ifs) {
                                ifs.seekg(0, std::ios::end);
                                size_t length=ifs.tellg();

                                ifs.seekg(0, std::ios::beg);

                                *response << "HTTP/1.1 200 OK\r\nContent-Length: " << length << "\r\n\r\n";

                                //read and send 128 KB at a time
                                size_t buffer_size=131072;
                                std::vector<char> buffer;
                                buffer.reserve(buffer_size);
                                size_t read_length;
                                try {
                                    while((read_length=ifs.read(&buffer[0], buffer_size).gcount())>0) {
                                        response->write(&buffer[0], read_length);
                                        response->flush();
                                    }
                                }
                                catch(const std::exception &e) {}
                                ifs.close();
                                return;
                            }
                        }
                    }
                }
            }
            std::string content="Could not open path "+request->path;
            *response << "HTTP/1.1 400 Bad Request\r\nContent-Length: " << content.length() << "\r\n\r\n" << content;
        };
    }


    void Server::run() {
        std::thread server_thread([=](){
            //Start server
            mServerImpl.start();
        });

        //Wait for server to start so that the client can connect
        std::this_thread::sleep_for(std::chrono::seconds(1));

        server_thread.join();
    }

    void Server::stop() {
        mServerImpl.stop();
    }

    void Server::log(const std::string aLogStatement, std::function<void()> aOperation) {
        char timeStr[100];
        auto in_time_t = std::chrono::system_clock::to_time_t(std::chrono::system_clock::now());
        std::strftime(timeStr, sizeof(timeStr), "%A %c", std::localtime(&in_time_t));
        LOG(info) << "[" << timeStr << ", " << ++mOperationCount << "]>>" << aLogStatement << "]" << std::endl;
        mLogSink->flush();
        aOperation();
        auto out_time_t = std::chrono::system_clock::to_time_t(std::chrono::system_clock::now());
        std::strftime(timeStr, sizeof(timeStr), "%A %c", std::localtime(&out_time_t));
        LOG(info) << "[" << timeStr << ", " << mOperationCount << "]<<" << std::endl;
        mLogSink->flush();
    }

    void Server::initLog() {
        boost::shared_ptr<boost::log::core > core = boost::log::core::get();
        boost::shared_ptr<boost::log::sinks::text_file_backend> backend = boost::make_shared<boost::log::sinks::text_file_backend >(
            boost::log::keywords::file_name = "zikes_%5N.log",
            boost::log::keywords::rotation_size = 5 * 1024 * 1024,
            boost::log::keywords::time_based_rotation = boost::log::sinks::file::rotation_at_time_point(12, 0, 0)
        );
        mLogSink = boost::shared_ptr<LogSink>(new Server::LogSink(backend));
        core->add_sink(mLogSink);
    }
}
