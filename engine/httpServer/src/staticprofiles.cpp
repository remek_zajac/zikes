/*************************************************************************
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * (C) 2016 Remek Zajac
 *************************************************************************/
 #include "cost/components/all.h"
 #include "../staticprofiles.h"

namespace http {

    StaticProfiles* StaticProfiles::mInstance = nullptr;
    const std::string StaticProfiles::KProfileNamePrefix = ".routeprofile.json";

    StaticProfiles::StaticProfiles(const boost::filesystem::path& aConfigPath)
    :mConfigPath(aConfigPath)
    {
        if (!boost::filesystem::exists(mConfigPath) || !boost::filesystem::is_directory(mConfigPath)) {
            throw std::invalid_argument("config path: " + mConfigPath.string() + " does not exist or isn't a directory.");
        }
        loadRoutingProfiles();
    }

    boost::optional<const cost::CompoundCostFunctionProfile&> StaticProfiles::routingProfile(const std::string& aName) {
        RoutingProfiles::const_iterator it = routingProfiles().find(aName);
        if (it == routingProfiles().end()) {
            return boost::optional<const cost::CompoundCostFunctionProfile&>();
        }
        return *(routingProfiles().at(aName));
    }

    const StaticProfiles::RoutingProfiles& StaticProfiles::routingProfiles() {
        if (!mRoutingProfiles) {
            loadRoutingProfiles();
        }
        return *mRoutingProfiles;
    }

    void StaticProfiles::loadRoutingProfiles() {
        mRoutingProfiles = RoutingProfiles();
        boost::filesystem::directory_iterator endIt;
        for (boost::filesystem::directory_iterator it(mConfigPath); it != endIt; it++) {
            if (!it->path().has_filename()) {
                continue;
            }
            const std::string fileSystemEntryName = it->path().filename().string();
            if (fileSystemEntryName.length() >= KProfileNamePrefix.length() &&
                fileSystemEntryName.compare(fileSystemEntryName.length() - KProfileNamePrefix.length(), KProfileNamePrefix.length(), KProfileNamePrefix) == 0) {
                const std::string profileName = fileSystemEntryName.substr(0,fileSystemEntryName.length() - KProfileNamePrefix.length());
                if (mRoutingProfiles->find(profileName) != mRoutingProfiles->end()) {
                    continue;
                }
                (*mRoutingProfiles)[profileName] = cost::CompoundCostFunctionProfile::ConstShrdProfile();
                try {
                    (*mRoutingProfiles)[profileName] = cost::CompoundCostFunctionProfile::ConstShrdProfile(
                        new cost::CompoundCostFunctionProfile(*it, cost::AllComponents::construct())
                    );
                } catch (const std::exception& e) {}
            }
        }
    }
}
