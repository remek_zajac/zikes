#ifndef STATICPROFILES_H
#define STATICPROFILES_H

#include "cost/CompoundCostFunctionProfile.h"

namespace http {

    class StaticProfiles {

        public:
            typedef std::map<const std::string, cost::CompoundCostFunctionProfile::ConstShrdProfile> RoutingProfiles;
            static void init(const boost::filesystem::path& aConfigPath) {
                mInstance = new StaticProfiles(aConfigPath);
            }

            static StaticProfiles& instance() {
                if (mInstance == nullptr) {
                    throw std::logic_error("Routing unintialised");
                }
                return *mInstance;
            }

            const boost::filesystem::path& configPath() {
                return mConfigPath;
            }

            boost::optional<const cost::CompoundCostFunctionProfile&> routingProfile(const std::string& aName);
            const RoutingProfiles& routingProfiles();

        private:

            void loadRoutingProfiles();
            StaticProfiles(const boost::filesystem::path& aConfigPath);

            static StaticProfiles* mInstance;
            static const std::string KProfileNamePrefix;

            const boost::filesystem::path    mConfigPath;
            boost::optional<RoutingProfiles> mRoutingProfiles;
    };
}

#endif // STATICPROFILES_H
