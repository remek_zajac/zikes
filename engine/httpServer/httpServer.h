#ifndef HTTPSERVER_H
#define HTTPSERVER_H

#include <boost/log/sinks/text_file_backend.hpp>
#include <boost/log/sinks/sync_frontend.hpp>
#include <boost/atomic.hpp>
#include "src/server_http.hpp"
#include "geomap/geomap.h"

namespace http {

    class Server
    {
        typedef SimpleWeb::Server<SimpleWeb::HTTP> HttpServerImpl;

        public:
            typedef boost::log::sinks::synchronous_sink<boost::log::sinks::text_file_backend> LogSink;
            Server(const std::string& bindAddress,
                   unsigned int bindPort,
                   unsigned short threadNo,
                   const std::string& aStaticFilesFolder,
                   const geomap::Map& aMap
            );
            void run();
            void stop();

        private:
            void initLog();
            void log(const std::string aLogStatement, std::function<void()> aOperation);
            HttpServerImpl     mServerImpl;
            const std::string  mBindAddress;
            const unsigned int mBindPort;
            const std::string  mStaticFilesFolder;
            const geomap::Map& mTheMap;
            boost::shared_ptr<LogSink> mLogSink;
            boost::atomic<unsigned long> mOperationCount;
    };
};


#endif // HTTPSERVER_H
