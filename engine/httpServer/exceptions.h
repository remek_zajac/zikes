/*************************************************************************
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * (C) 2015 Remek Zajac
 *************************************************************************/
#ifndef EXCEPTIONS_H
#define EXCEPTIONS_H

#include <string>
#include <exception>

#include "geomap/geo.h"
#include "route/routing.h"

namespace http {

    class Exception: public std::exception {
        public:
            Exception(const std::string& aMessage, unsigned int aCode)
            :message(aMessage),
             code(aCode)
            {}

            virtual const char* what() const throw() {
                return message.c_str();
            }

            const std::string& code2Str() const;
            const std::string message;
            const int code;
    };

    class RoutingException : public Exception {
        public:
            RoutingException(const std::string& aJsonMessage)
            :Exception(aJsonMessage, 400)
            {}
    };

    class NearestPointNotFound : public RoutingException {
        public:
            NearestPointNotFound(const routing::Request::NearestPointNotFound& aNearestPointNotFound)
            :RoutingException(
                 "{\"code\": \"RoutingException::NearestPointNotFound\", \"culprit\": "+aNearestPointNotFound.mCulprit.toString()+", \"attemptedToleranceMts\": "+std::to_string(aNearestPointNotFound.mAttemptedToleranceMts)+"}"
             )
            {}
    };

    class NoRoute : public RoutingException {
        public:
            NoRoute()
            :RoutingException("{\"code\": \"RoutingException::NoRoute\"}")
            {}
    };

    class ServerOverload : public RoutingException {
        public:
            ServerOverload(unsigned long aJunctions)
            :RoutingException("{\"code\": \"RoutingException::ServerOverload\", \"junctionsLimitExceeded\": "+std::to_string(aJunctions)+"}")
            {}
    };
}

#endif // EXCEPTIONS_H
