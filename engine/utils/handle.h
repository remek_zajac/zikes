/*************************************************************************
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * (C) 2015 Remek Zajac
 *************************************************************************/
#ifndef HANDLE_H
#define HANDLE_H


namespace zikes {

    /**
     * Shallow, never-owning handle to the underlying value_type.
     */
    template <class T>
    class Handle {

        public:
            typedef T value_type;

            Handle()
            :mPtr(nullptr)
            {}

            Handle(T* aFrom)
            :mPtr(aFrom)
            {}

            Handle(const Handle<T>& aFrom)
            :mPtr(aFrom.mPtr)
            {}

            const Handle<T>& operator=(const Handle<T>& aTo) {
                mPtr = aTo.mPtr;
                return *this;
            }

            bool operator==(const Handle<T>& aTo) const {
                return mPtr == aTo.mPtr;
            }

            bool operator!=(const Handle<T>& aTo) const {
                return !(*this==aTo);
            }

            T& operator*() {
                return *mPtr;
            }

            T& operator*() const {
                return *mPtr;
            }

            T* get() {
                return mPtr;
            }

            T* get() const {
                return mPtr;
            }

            operator Handle<const T>() const {
                return Handle<const T>(mPtr);
            }

            T* operator->() {
                return mPtr;
            }

            T* operator->() const {
                return mPtr;
            }

            operator bool() const {
                return mPtr != nullptr;
            }


        private:
            T* mPtr;
    };

    template <class TO, class FROM>
    Handle<typename TO::value_type> handle_cast(const FROM& aFrom) {
        return Handle<typename TO::value_type>(static_cast<typename TO::value_type*>(aFrom.get()));
    }

    template <class TO, class FROM>
    Handle<typename TO::value_type> handle_dynamic_cast(const FROM& aFrom) {
        return Handle<typename TO::value_type>(dynamic_cast<typename TO::value_type*>(aFrom.get()));
    }

    template <class T>
    class AutoHandle {

        class PolymorphicHandle
        {
            public:;
                typedef std::shared_ptr<PolymorphicHandle> ShrdPtr;

                virtual T& operator*() = 0;
                virtual T* get() = 0;
                virtual ~PolymorphicHandle() {};
        };

        class OwningHandle : public PolymorphicHandle
        {
            public:

                OwningHandle(T* aFrom)
                :mPtr(aFrom)
                {}

                OwningHandle(const std::shared_ptr<T>& aFrom)
                :mPtr(aFrom)
                {}

                virtual T& operator*() {
                    return *mPtr;
                }

                virtual T* get() {
                    return mPtr.get();
                }

            private:
                std::shared_ptr<T> mPtr;
        };

        class LoanedHandle : public PolymorphicHandle
        {
            public:

                LoanedHandle(const Handle<T>& aFrom)
                :mPtr(aFrom)
                {}

                virtual T& operator*() {
                    return *mPtr;
                }

                virtual T* get() {
                    return mPtr.get();
                }

            private:
                Handle<T> mPtr;
        };

        public:
            AutoHandle()
            {}

            AutoHandle(T* aFrom)
            :mHandle(new OwningHandle(aFrom) )
            {}

            AutoHandle(const std::shared_ptr<T>& aFrom)
            :mHandle(new OwningHandle(aFrom) )
            {}

            AutoHandle(const Handle<T>& aFrom)
            :mHandle(new LoanedHandle(aFrom))
            {}

            const AutoHandle<T>& operator=(const AutoHandle<T>& aTo) {
                mHandle = aTo.mHandle;
                return *this;
            }

            const AutoHandle<T>& operator=(const std::shared_ptr<T>& aTo) {
                mHandle = typename PolymorphicHandle::ShrdPtr(new OwningHandle(aTo));
                return *this;
            }

            T& operator*() {
                return *(mHandle->get());
            }

            T& operator*() const {
                return *(mHandle->get());
            }

            T* get() {
                return mHandle->get();
            }

            T* get() const {
                return mHandle->get();
            }

            T* operator->() {
                return mHandle->get();
            }

            T* operator->() const {
                return mHandle->get();
            }

            bool operator==(const std::shared_ptr<T>& aTo) const {
                return aTo.get() == get();
            }

            bool operator==(const AutoHandle<T>& aTo) const {
                return aTo.get() == get();
            }

            void reset() {
                mHandle.reset();
            }

            operator bool() const {
                return mHandle.get() != nullptr;
            }

        private:
            typename PolymorphicHandle::ShrdPtr mHandle;
    };
}


#endif // HANDLE_H

