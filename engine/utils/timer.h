/*************************************************************************
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * (C) 2015 Remek Zajac
 *************************************************************************/
#ifndef TIMER_H
#define TIMER_H
#include <sstream>
#include <boost/timer/timer.hpp>

#ifdef PERFORMANCE_MEASURE
    #define MASTER_TIMER(name)          Timer  name(#name)
    #define CHILD_TIMER(parent, name)   Timer  name(#name); parent.add(name)
    #define TIMER_START(name)           name.start()
    #define TIMER_STOP(name)            name.stop()
#else
    #define MASTER_TIMER(name)
    #define CHILD_TIMER(parent, name)
    #define TIMER_START(name)
    #define TIMER_STOP(name)
#endif


class Timer {
    public:
        Timer(const std::string& aName)
        :mName(aName),
         mElapsed(0)
        {}

        void start() {
            mTimer.start();
        }

        void stop() {
            mTimer.stop();
            mElapsed += mTimer.elapsed().wall;
        }

        std::string report() {
            std::stringstream aResult;
            report(0, aResult);
            return aResult.str();
        }

        void report(unsigned short aIndent, std::stringstream& aResult) {
            boost::int_least64_t elapsed_nano = this->elapsed_nano();
            boost::int_least64_t accountedFor_nano = 0;
            aResult << indent(aIndent) << mName << " took " << elapsedStr() << std::endl;

            for (auto child : mChildren) {
                boost::int_least64_t childTook_nano = child->elapsed_nano();
                accountedFor_nano += childTook_nano;
                aResult << indent(aIndent) << "* " << (100 * childTook_nano / elapsed_nano) << "%" << std::endl;
                child->report(aIndent + 2, aResult);
            }
            if (mChildren.size() > 0) {
                aResult << indent(aIndent) << 100 * (elapsed_nano - accountedFor_nano) /  elapsed_nano << "% unaccounted for" << std::endl;
            }
        }

        boost::int_least64_t elapsed_nano() {
            boost::int_least64_t elapsed_nano = mElapsed;
            if (elapsed_nano == 0) {
                //only add up the children
                for (auto child : mChildren) {
                    elapsed_nano += child->elapsed_nano();
                }
            }
            return elapsed_nano;
        }

        std::string elapsedStr() {
            static std::vector<std::string> units = {"nanoS", "us", "ms", "s"};
            boost::int_least64_t result = elapsed_nano();
            unsigned int i = 0;
            for (; i < units.size()-1; i++) {
                if (result < 1000) {
                    break;
                }
                result /= 1000;
            }
            std::stringstream ss;
            ss << result << units.at(i);
            return ss.str();
        }

        void add(Timer& aTimer) {
            mChildren.push_back(&aTimer);
        }

        template <class RESULT>
        RESULT measure(std::function<RESULT()> aFunc) {
            start();
            RESULT result = aFunc();
            stop();
            return result;
        }

        void measure(std::function<void()> aFunc) {
            start();
            aFunc();
            stop();
        }


    private:

        std::string indent(unsigned short aIndent) {
            std::stringstream result;
            for (unsigned short i = 0; i < aIndent; i++) {
                result << " ";
            }
            return result.str();
        }

        const std::string       mName;
        boost::timer::cpu_timer mTimer;
        std::vector<Timer*>     mChildren;
        boost::int_least64_t    mElapsed;
};

#endif // TIMER_H
