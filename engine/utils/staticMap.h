/*************************************************************************
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * (C) 2015 Remek Zajac
 *************************************************************************/
#include <iostream>

template <const std::string& TNAME, bool mandatory>
class TAttr
{
    public:
        static const std::string& NAME;
        static const bool         MANDATORY = mandatory;

        static bool match(const std::string& aWhat) {
            if (aWhat == NAME) {
                return true;
            }
            return false;
        }
};
template <const std::string& TNAME, bool mandatory>
const std::string& TAttr<TNAME, mandatory>::NAME=TNAME;

template <class T, class... Ts>
class TStaticMap {
    typedef TStaticMap<Ts...> TParent;
    typedef T TTail;

    public:
        static T get(const std::string& aKey) {
            if (TTail::match(aKey)) {
                TTail result;
                return result;
            }
            return TParent::get(aKey);
        }
};

template<class T>
class TStaticMap<T> {
    typedef T TTail;
    public:
        static T get(const std::string& aKey) {
            if (TTail::match(aKey)) {
                TTail result;
                return result;
            }
            assert(false);
        }
};