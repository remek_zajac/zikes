/*************************************************************************
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * (C) 2015 Remek Zajac
 *************************************************************************/
#ifndef RANGES_HPP
#define RANGES_HPP

#include <boost/range/functions.hpp>
#include <boost/range/join.hpp>


namespace boost {

    namespace range {

        template <class T, class WRAPPED_T>
        struct OnceAccessor
        {
            typedef T VALUE;
            typedef WRAPPED_T WRAPPED_VALUE;
            static VALUE& deref(WRAPPED_VALUE& what) {return *what;}
        };

        template <class T>
        struct PassThroughAccessor
        {
            typedef T VALUE;
            typedef T WRAPPED_VALUE;
            static VALUE& deref(WRAPPED_VALUE& what) {return what;}
        };

        template <class VALUE>
        class SingleElemRange
        {
            template <class Value>
            class iterator_base : public boost::iterator_facade<iterator_base<Value>, Value, boost::bidirectional_traversal_tag>
            {
             public:

                iterator_base(Value val, short idx)
                : mVal(val),
                  mIdx(idx) {}

                iterator_base()
                : mVal(),
                  mIdx(1) {}

                iterator_base<Value>& operator=(iterator_base<Value> const& other)
                {
                    mIdx=other.mIdx;
                    mVal=other.mVal;
                    return *this;
                }

             private:
                friend class boost::iterator_core_access;

                bool equal(iterator_base<Value> const& other) const
                { return this->mVal == other.mVal && this->mIdx == other.mIdx; }

                void increment()
                { mIdx = ++mIdx > 1 ? 1 : mIdx; }

                void decrement()
                { mIdx = --mIdx < -1 ? -1: mIdx; }

                Value& dereference() const
                {
                    assert(mIdx == 0);
                    return mVal;
                }

                typename boost::iterator_facade<iterator_base<Value>, Value, boost::bidirectional_traversal_tag>::value_type mVal;
                short mIdx;
            };

        public:
            SingleElemRange(const VALUE& val):mVal(val) {}

            typedef iterator_base<VALUE> iterator;
            typedef iterator_base<VALUE const> const_iterator;

            iterator begin() {return iterator(mVal, 0);}
            iterator end()   {return iterator(mVal, 1);}
            const_iterator begin() const {return const_iterator(mVal, 0);}
            const_iterator end() const {return const_iterator(mVal, 1);}

        private:
            VALUE mVal;
        };
    }
}


#endif // RANGES_HPP
