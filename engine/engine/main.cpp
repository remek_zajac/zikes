/*************************************************************************
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * (C) 2014 Remek Zajac
 *************************************************************************/
#include <iostream>
#include <boost/program_options.hpp>
#include <boost/log/trivial.hpp>

#include "route/routing.h"
#include "readers/reader.h"
#include "readers/Allocators.h"
#include "geomap/geomap.h"
#include "httpServer/httpServer.h"
#include "utils/memoryTracker.h"

#define LOG(level) BOOST_LOG_TRIVIAL(level) << "[Zikes]> "
namespace po = boost::program_options;

void processEdge(const geomap::Edge::ConstHandle& aEdge) {
    geomap::Map::instance().put(aEdge);
}

class MetaProcessor : public reader::Reader::MetaProcessor {
    public:
        virtual void putHighwayTypes(const geomap::MapMeta::HighwayTypes& aHighwayTypes) {
            geomap::Map::instance().getMeta().put(aHighwayTypes);
        }

        virtual void putCoverageFile(const boost::filesystem::path& aPathToCoverageFile) {
            geomap::Map::instance().getMeta().put(aPathToCoverageFile.string());
        }
};

static reader::AllocatorBase<
    reader::ArrayAllocator<geomap::Way>,
    reader::ArrayAllocator<geomap::Edge::EdgeNode>,
    reader::ArrayAllocator<geomap::Edge>
> allocator;

void populateFromGPB(const std::string& aNodesFolder)
{
    unsigned long nodeCount = 0;
    unsigned long wayCount  = 0;
    unsigned long edgeNodeCount = 0;
    {
        MetaProcessor metaProcessor;
        reader::Reader reader(
            processEdge,
            metaProcessor,
            allocator
        );
        reader.read(aNodesFolder);
        nodeCount = reader.nodesCount();
        wayCount = reader.waysCount();
        edgeNodeCount = reader.edgeNodesCount();
    }
    reader::Reader::purge();

    unsigned long edgeCount = geomap::Map::instance().edgeCount();
    size_t directedEdgeSize = sizeof(geomap::Edge::DirectedEdge) + sizeof(geomap::Edge::DirectedEdge::ConstUniquePtr);
    size_t edgeSize = sizeof(geomap::Edge) + sizeof(geomap::Edge::ConstHandle) + 2 * directedEdgeSize;
    size_t nodeSize = sizeof(geomap::Node) + sizeof(geomap::Node::ConstHandle);
    size_t waySize  = sizeof(geomap::Way) + sizeof(geomap::Way::ConstHandle);
    size_t edgeNodeSize = sizeof(geomap::Edge::EdgeNode) + sizeof(geomap::Edge::EdgeNode::ConstHandle);

    unsigned int  edgeFootprintKb = (edgeSize * edgeCount)/1024;
    unsigned int  nodeFootprintKb = (nodeSize * nodeCount)/1024;
    int nodesUnaccountedFor = geomap::Node::nodesRemaining() - (nodeCount + edgeNodeCount);
    unsigned int  nodesUnaccountedForFootprintKb = (nodesUnaccountedFor * nodeSize)/1024;
    unsigned int  wayFootprintKb  = (waySize  * wayCount)/1024;
    unsigned int  edgeNodeFootprintKb = (edgeNodeSize * edgeNodeCount)/1024;
    unsigned int  totalExpectedMB = (edgeFootprintKb + nodeFootprintKb + wayFootprintKb + edgeNodeFootprintKb + nodesUnaccountedForFootprintKb)/1024;

    LOG(info) << "Finished loading, collected:";
    LOG(info) << nodeCount << "\t nodes, " << nodeSize << "B each, totalling: \t" << nodeFootprintKb << "kB";
    LOG(info) << edgeNodeCount << "\t vertices, " << edgeNodeSize << "B each, totalling: \t" << edgeFootprintKb << "kB";
    if (nodesUnaccountedFor != 0) {
        LOG(info) << nodesUnaccountedFor << "\t unaccounted for nodes, " << nodeSize << "B each, totalling: \t" << nodesUnaccountedForFootprintKb << "kB";
    }
    LOG(info) << wayCount  << "\t ways,  " << waySize  << "B each, totalling: \t" << wayFootprintKb  << "kB";
    LOG(info) << edgeCount << "\t edges, " << edgeSize << "B each, totalling: \t" << edgeFootprintKb << "kB";

    LOG(info) << "Total memory consumption above: \t" << totalExpectedMB << "MB";
    LOG(info) << "Total RSS memory usage reported by memtracker: " << (getCurrentRSS()/(1024*1024)) << "MB";
}

int main(int ac, char* av[]) {
    try {

        po::options_description desc("This spawns the Zikes routing engine");
        desc.add_options()
            ("help", "show help")
            ("maps, m", po::value<std::string>(),"Google Protocol Buffers osm input file generated/exported by zikes python console")
            ("config, c", po::value<std::string>()->default_value("./config"), "folder with the configuration files (./config default)")
            ("address, a", po::value<std::string>()->default_value("127.0.0.1:8080"), "address:port for the http server to bind/listen to (127.0.0.1:8080 default)")
        ;

        po::variables_map vm;
        po::store(po::parse_command_line(ac, av, desc), vm);
        po::notify(vm);

        if (vm.count("help")) {
            std::cout << desc << "\n";
            return 0;
        }

        if (vm.count("maps")) {
            populateFromGPB(vm["maps"].as<std::string>());
        } else {
            std::cout << desc << "\n";
            return 1;
        }
        std::vector<std::string> splitAddress;
        boost::split(splitAddress, vm["address"].as<std::string>(), boost::is_any_of(":"));
        if (splitAddress.size() != 2) {
            std::cout << desc << "\n";
            return 1;
        }
        LOG(info) << "Server is up and listening on " << vm["address"].as<std::string>() << std::flush;
        http::Server server(
            splitAddress[0],
            std::stoi(splitAddress[1]),
            MUTABLE_THREAD_SLOTS,
            vm["config"].as<std::string>(),
            geomap::Map::instance()
        );
        server.run();
    }
    catch(std::exception& e) {
        std::cerr << "error: " << e.what() << "\n";
        return 1;
    }
    catch(...) {
        std::cerr << "Exception of unknown type!\n";
    }
}
