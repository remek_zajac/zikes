#!/usr/bin/env bash
set -e
EXECUTABLE=./bin/$1/engine
shift
$EXECUTABLE $* &> /dev/null &

echo "$!"