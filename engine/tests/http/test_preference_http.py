import os
import unittest
import requests
import json

import baseTest


class TestHttp(baseTest.ZikesTestCase):

    KRoutingPort    = "8087"
    SERVER = None


    @classmethod
    def setUpClass(cls):
        TestHttp.SERVER = baseTest.ZikesServer(
            port=TestHttp.KRoutingPort,
            osmInputFolder=baseTest.KBuildFolder+"/tests/data/swissCottage",
            config=baseTest.KThisFolder
        )
        TestHttp.SERVER.start()

    def testOneWayPreferenceRoute(self):
        #from xssing of Park Street and Upper Brook Street towards Hyde Park
        response = requests.get(TestHttp.SERVER.url()+"/route?milestones=51.5464792791,-0.180731166452,51.5464859513,-0.18572007522&profile=test")
        response = json.loads(response.content)
        #with basic preferences it shoyuld obey traffic restrictions up Campayne Gardens
        self.assertEquals(
            response["waypoints"],
            [[51.5464859, -0.1807408, {u'h': 8, u'e': 56}], [51.546463, -0.1807447],
             [51.5464439, -0.1807713, {u'h': 8, u'e': 56}], [51.5459251, -0.181595, {u'h': 16, u'e': 52}],
             [51.5459633, -0.1816577], [51.5460167, -0.1818442], [51.5460548, -0.1821507], [51.5460625, -0.1825037],
             [51.5460434, -0.1828326], [51.5459938, -0.1831257], [51.5459366, -0.183356], [51.545887, -0.1835192],
             [51.545826, -0.183697], [51.5456772, -0.1840094], [51.5455551, -0.1842402],
             [51.545475, -0.1844154, {u'h': 16, u'e': 50}], [51.5465164, -0.1856846, {u'e': 49}]]
        )
        self.assertEquals(response["meta"]["lengthMts"], TestHttp.routeDistance(response["waypoints"]))

        preferenceJSON=json.loads(open(baseTest.KThisFolder+"/test.routeprofile.json").read())

        #now tell it to disobey restrictions and go Broadhurst Gardens Street against the traffic
        preferenceJSON["highwayCostFactors"]["default"]["forbidden"] = "1"
        response = requests.post(
            TestHttp.SERVER.url()+"/route?milestones=51.5464792791,-0.180731166452,51.5464859513,-0.18572007522",
            data=json.dumps(preferenceJSON)
        )
        response = json.loads(response.content)
        self.assertEquals(
            response["waypoints"],
            [[51.5464859, -0.1807408, {u'a': u'forbidden', u'h': 8, u'e': 56}], [51.5465202, -0.180759],
             [51.5465393, -0.180799, {u'a': u'forbidden', u'h': 8, u'e': 56}], [51.546566, -0.1809255], [51.5466347, -0.1811494],
             [51.5467148, -0.1814099], [51.5467682, -0.1815915], [51.5468369, -0.1818405], [51.546875, -0.1821495],
             [51.5468864, -0.1824967], [51.5468674, -0.182845], [51.5468521, -0.1830363, {u'a': u'forbidden', u'h': 8, u'e': 52}],
             [51.5468407, -0.1831628], [51.5468254, -0.1833002, {u'a': u'forbidden', u'h': 8, u'e': 51}],
             [51.5466728, -0.1844801, {u'a': u'forbidden', u'h': 8, u'e': 49}], [51.5465775, -0.1851545],
             [51.5465164, -0.1856846, {u'e': 49}]]
        )
        self.assertEquals(response["meta"]["lengthMts"], TestHttp.routeDistance(response["waypoints"]))

    def testCyclePreferenceRoute(self):
        #from xssing of Priory Road with Broadhurst Gardens to Fairhazel xssing with Campayne
        response = requests.get(TestHttp.SERVER.url()+"/route?milestones=51.546474,-0.188767,51.5454737,-0.1844154&profile=test")
        response = json.loads(response.content)

        #with basic preferences, expect down Broadhurst and right
        self.assertEquals(
            response["waypoints"],
            [[51.5464745, -0.188767, {u'h': 8, u'e': 51}], [51.5463448, -0.1884847], [51.5463028, -0.1883559],
             [51.5462494, -0.1881756], [51.5462303, -0.1879904], [51.5462379, -0.1878089],
             [51.5463219, -0.1871979, {u'h': 8, u'e': 50}], [51.5465164, -0.1856846, {u'h': 16, u'e': 49}],
             [51.545475, -0.1844154, {u'e': 50}]]
        )
        self.assertEquals(response["meta"]["lengthMts"], TestHttp.routeDistance(response["waypoints"]))
        preferenceJSON=json.loads(open(baseTest.KThisFolder+"/test.routeprofile.json").read())
        #now telling to prefer the cyclepath south onto Priory
        preferenceJSON["highwayCostFactors"]["default"]["designated"] = 0.7
        response = requests.post(
            TestHttp.SERVER.url()+"/route?milestones=51.546474,-0.188767,51.5454737,-0.1844154",
            data=json.dumps(preferenceJSON)
        )

        response = json.loads(response.content)
        self.assertEquals(
            response["waypoints"],
            [[51.5464745, -0.188767, {u'h': 8, u'e': 51}], [51.5451012, -0.1888859, {u'h': 16, u'e': 49}],
             [51.5450706, -0.1880315], [51.5450554, -0.1872598], [51.5450363, -0.186733], [51.5450325, -0.1862344],
             [51.5450516, -0.1859437], [51.5450935, -0.1856297], [51.5451393, -0.185359], [51.5451927, -0.1851544],
             [51.5452728, -0.1848457], [51.5453491, -0.1846717], [51.545475, -0.1844154, {u'e': 50}]]
        )
        self.assertEquals(response["meta"]["lengthMts"], TestHttp.routeDistance(response["waypoints"]))

    def testTrafficLights(self):
        thruMainRoad = [[51.5442429, -0.1753698, {u'h': 2, u'e': 61}], [51.5441284, -0.1752844, {u'h': 2, u'e': 60}],
             [51.5438499, -0.1750959], [51.543644, -0.1749584, {u'h': 2, u'e': 59}], [51.5434418, -0.1748202],
             [51.5433998, -0.1747871, {u'h': 2, u'e': 58}], [51.5433693, -0.174761, {u'h': 2, u'e': 58}],
             [51.5433159, -0.1747079], [51.5432854, -0.1746713, {u'h': 2, u'e': 58}], [51.5432205, -0.174584],
             [51.5431366, -0.1744552, {u'h': 2, u'e': 58}],
             [51.5426598, -0.1740303, {u'a': u'forbidden', u'h': 18, u'e': 58}], [51.5427132, -0.1738665],
             [51.5428123, -0.1737548], [51.5429039, -0.1735582], [51.542942, -0.173453, {u'e': 58}]
        ]
        preferenceJSON=json.loads(open(baseTest.KThisFolder+"/test.routeprofile.json").read())
        response = requests.post(
            TestHttp.SERVER.url()+"/route?milestones=51.5442420542,-0.175382694112,51.5429324063,-0.173447212087",
            data=json.dumps(preferenceJSON)
        )
        response = json.loads(response.content)
        self.assertEquals(
            response["waypoints"],
            thruMainRoad
        )

        preferenceJSON["trafficSignals"] = { "punishmentMts" : 100 }

        response = requests.post(
            TestHttp.SERVER.url()+"/route?milestones=51.5442420542,-0.175382694112,51.5429324063,-0.173447212087",
            data=json.dumps(preferenceJSON)
        )
        response = json.loads(response.content)
        self.assertEquals(
            response["waypoints"],
            thruMainRoad
        )

        preferenceJSON["highwayCostFactors"] = {
            "default" : {
                "cost" : 1.0,
                "forbidden" : 1.0
            }
        }
        response = requests.post(
            TestHttp.SERVER.url()+"/route?milestones=51.5442420542,-0.175382694112,51.5429324063,-0.173447212087",
            data=json.dumps(preferenceJSON)
        )
        response = json.loads(response.content)
        self.assertEquals(
            response["waypoints"],
            [[51.5442429, -0.1753698, {u'h': 2, u'e': 61}], [51.5441284, -0.1752844, {u'h': 7, u'e': 60}],
             [51.5441093, -0.1752623], [51.5440941, -0.1752347], [51.5440865, -0.1752023], [51.5440826, -0.1751558],
             [51.5440826, -0.1749765], [51.5440788, -0.1749258, {u'h': 7, u'e': 60}], [51.544075, -0.1748816],
             [51.5440598, -0.174844], [51.5440369, -0.1748241], [51.5440102, -0.1748144, {u'h': 6, u'e': 60}],
             [51.5438538, -0.1748278], [51.5437889, -0.1748299, {u'h': 6, u'e': 59}],
             [51.5437241, -0.1748258, {u'h': 17, u'e': 59}], [51.543705, -0.1748122], [51.5436897, -0.174789],
             [51.5436783, -0.1747629], [51.5436745, -0.1747379], [51.5436745, -0.1747116],
             [51.5436783, -0.1746904, {u'h': 17, u'e': 58}], [51.5439034, -0.1742801, {u'h': 19, u'e': 59}],
             [51.5437508, -0.1745379], [51.5436821, -0.1744873], [51.5437393, -0.1742958], [51.5437698, -0.174193],
             [51.5436249, -0.1740683], [51.5436172, -0.17406], [51.5435333, -0.1739717],
             [51.5435524, -0.1738723, {u'a': u'forbidden', u'h': 18, u'e': 58}], [51.5434952, -0.1738565],
             [51.5432854, -0.1736996, {u'a': u'forbidden', u'h': 18, u'e': 58}], [51.5432663, -0.1736848],
             [51.5430603, -0.1735432], [51.542942, -0.173453, {u'e': 58}]]
        )


    @classmethod
    def tearDownClass(cls):
        TestHttp.SERVER.stop()

if __name__ == '__main__':
    unittest.main()