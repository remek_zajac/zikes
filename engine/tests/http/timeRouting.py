import os
import unittest
import requests
import time
import sys
import json

import baseTest
import pyGeometry


class TestRouteTime(unittest.TestCase):

    KStepMts    = 1000
    KStart      = [49.3557218186,0.999648632813]  #north from paris
    KFinish     = [44.8380232329,-0.574554858398] #Bordeaux

    KPbFolder   = "/media/remek/maps/maps/maps.pb/europe/france"
    KRoutingPort    = "8087"
    SERVER = None

    @classmethod
    def setUpClass(cls):
        TestRouteTime.SERVER = baseTest.ZikesServer(
            port=TestRouteTime.KRoutingPort,
            osmInputFolder=TestRouteTime.KPbFolder,
            release="release",
            config=baseTest.KThisFolder,
            serverLoadTimeoutSec=60*60
        )
        TestRouteTime.SERVER.startViaBash()

    @staticmethod
    def navigate(navigateFrom, navigateTo):
        url = "/route?milestones=%s,%s,%s,%s&profile=test" % (navigateFrom[0], navigateFrom[1], navigateTo[0], navigateTo[1])
        response = requests.get(
            TestRouteTime.SERVER.url()+url
        )
        try:
            response = json.loads(response.content)
            print "[[%s],[%s]];%s;%s;%s" % (navigateFrom, navigateTo, int(navigateFrom.distanceMts(navigateTo)), response["meta"]["lengthMts"], response["meta"]["routing_us"])
        except Exception as e:
            pass

    def testMeasure(self):
        fromMilestone = pyGeometry.GeoPoint(TestRouteTime.KStart)
        toMilestone   = pyGeometry.GeoPoint(TestRouteTime.KFinish)
        while fromMilestone.distanceMts(toMilestone) > TestRouteTime.KStepMts:
            self.navigate(fromMilestone, toMilestone)
            currentLine    = pyGeometry.GeoLine(fromMilestone, toMilestone)
            currentVector  = currentLine.vector()
            lengthMtsRatio = currentVector.length()/currentLine.lengthMts()
            currentVector  = currentVector.setLength(TestRouteTime.KStepMts*lengthMtsRatio)
            fromMilestone  = pyGeometry.GeoPoint(currentVector.add(fromMilestone))



    @classmethod
    def tearDownClass(cls):
        TestRouteTime.SERVER.stop()

if __name__ == '__main__':
    unittest.main()