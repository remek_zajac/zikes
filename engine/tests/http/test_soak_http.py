import os
import unittest
import requests
import sys
import json
import random

import baseTest
import pyGeometry


class TestHttp(unittest.TestCase):

    KAimIterations = 3000
    KAllowMemoryFootprintIncrease = 1.0002 #empirical
    KTestFolder = os.path.dirname(os.path.abspath(__file__))
    KRootFolder = os.path.dirname(os.path.dirname(KTestFolder))
    KRoutingPort    = "8087"
    SERVER = None

    @classmethod
    def setUpClass(cls):
        TestHttp.SERVER = baseTest.ZikesServer(
            port=TestHttp.KRoutingPort,
            osmInputFolder=TestHttp.KRootFolder + "/build/tests/data/urbanRural/dolny-slask-rural",
            release="release",
            config=baseTest.KThisFolder
        )
        TestHttp.SERVER.startViaBash()

    @staticmethod
    def navigate(navigateFrom, navigateTo):
        preferenceJSON=json.loads(open(baseTest.KThisFolder+"/test.routeprofile.json").read())
        preferenceJSON["ubranFactor"] = str(round(0.5+2.0*random.random(),2))
        url = "/route?milestones=%s,%s,%s,%s" % (navigateFrom[0], navigateFrom[1], navigateTo[0], navigateTo[1])
        return requests.post(
            TestHttp.SERVER.url()+url,
            data=json.dumps(preferenceJSON),
            timeout = 1.0
        )

    def routeIterations(self, region, aimIterations, label):
        successfulNavigations = 0
        iterations = 0
        avgLength = 0
        lastBadResponse = None
        while True:
            iterations += 1
            response = self.navigate(region.randomPoint(), region.randomPoint())
            if response.status_code == 200:
                response = json.loads(response.content)
                successfulNavigations += 1
                avgLength = (avgLength*(successfulNavigations-1) + response["meta"]["lengthMts"])/successfulNavigations
                if aimIterations <= successfulNavigations:
                    break
            else:
                lastBadResponse = response.content
            if iterations > aimIterations/4:
                self.assertGreater(successfulNavigations, iterations*0.5, "unable to navigate, last error: %s" % lastBadResponse)
            sys.stdout.write("%s Iteration: %d (successful: %d%%, avgLenthMts: %.2f)                           \r" %
                             (label, iterations, int(100*successfulNavigations/iterations), avgLength))


    def testSoak(self):
        response = requests.get(TestHttp.SERVER.url()+"/meta.json")
        coverage = json.loads(response.content)["coverage"]
        region = pyGeometry.GeoRegion()
        for area in coverage:
            for point in area:
                region.add(point)
        region = region.percentResize(120)
        kmsAcross = region.se().distanceKms(region.nw())
        self.assertGreater(kmsAcross, 2.5)
        #calibrate
        self.routeIterations(region, TestHttp.KAimIterations/3, "Calibrating")
        memoryStart = TestHttp.SERVER.memoryUsage()
        #soak
        self.routeIterations(region, TestHttp.KAimIterations, "Soak")
        memoryStop = TestHttp.SERVER.memoryUsage()
        print ""
        print "Memory %s->%s, increase by: %.6f%%, accepting below %s%%" % (memoryStart/1000.0, memoryStop/1000.0, 100*(1.0*memoryStop/memoryStart)-100, 100.0*TestHttp.KAllowMemoryFootprintIncrease-100)
        self.assertGreater(memoryStart*TestHttp.KAllowMemoryFootprintIncrease, memoryStop)


    @classmethod
    def tearDownClass(cls):
        TestHttp.SERVER.stop()

if __name__ == '__main__':
    unittest.main()