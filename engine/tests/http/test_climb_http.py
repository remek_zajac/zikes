import os
import unittest
import requests
import json

import baseTest

KThisFolder = os.path.dirname(os.path.abspath(__file__))
KTestFolder = os.path.dirname(KThisFolder)
KRootFolder = os.path.dirname(KTestFolder)

class TestHttp(unittest.TestCase):

    KRoutingPort    = "8087"
    SERVER = None

    @classmethod
    def setUpClass(cls):
        TestHttp.SERVER = baseTest.ZikesServer(
            port=TestHttp.KRoutingPort,
            osmInputFolder=baseTest.KBuildFolder+"/tests/data/swissCottage",
            config=KThisFolder
        )
        TestHttp.SERVER.start()

    def testClimb(self):
        #from bottom of Colledge Crescent (of Finchley Roard, next to petrol station) to Colledge Crescent xssing Buckland Crescent
        #the road is climbing rather sharply, at 3% claims python GUI (though seems steeper)
        response = requests.get(TestHttp.SERVER.url()+"/route?milestones=51.5457502751,-0.178233800125,51.5446693803,-0.174940047455&profile=test")
        response = json.loads(response.content)
        #with basic preferences, expect to climb
        self.assertEquals(
            response["waypoints"],
            [[51.5457344, -0.1782226, {u'h': 8, u'e': 58}], [51.5457687, -0.1781715], [51.5458527, -0.1780583],
             [51.5458794, -0.177918], [51.5458832, -0.1777644], [51.5458488, -0.1774095], [51.5458336, -0.177012],
             [51.5458641, -0.1765998], [51.5458298, -0.1764254], [51.5455551, -0.175839], [51.545536, -0.1757159],
             [51.5455551, -0.1756376, {u'h': 6, u'e': 64}], [51.545475, -0.175545, {u'h': 6, u'e': 64}],
             [51.545372, -0.1754398, {u'h': 6, u'e': 63}], [51.5450325, -0.1752012, {u'h': 6, u'e': 62}],
             [51.5446548, -0.1749537, {u'e': 62}]]
        )

        preferenceJSON=json.loads(open(KThisFolder+"/test.routeprofile.json").read())
        #now tell it you're scared of climbing
                        #-10% -9% -8%  -7%  -6%  -5%  -4%  -3%  -2%   -1%
        climbFactors = [300,  1, 0.8,  0.8, 0.8, 0.8, 0.8, 0.8, 0.9,   1,
                        1, #0%
                        1.2,  2,   3,   4,   6,  10,  17,  30,  50,  100]
                        #1%   2%   3%   4%   5%   6%   7%   8%   9%   10%

        preferenceJSON["climbCostFactors"] = {
            "costFactors" : climbFactors
        }
        response = requests.post(
            TestHttp.SERVER.url()+"/route?milestones=51.5457502751,-0.178233800125,51.5446693803,-0.174940047455",
            data=json.dumps(preferenceJSON)
        )
        response = json.loads(response.content)
        #and expect to be given a route that goes down Finchley Road towards Swiss Cottage and then sharply turns left
        #next to Eton Avenue
        self.assertEquals(
            response["waypoints"],
            [[51.5457344, -0.1782226, {u'h': 2, u'e': 58}], [51.5456657, -0.1780907], [51.5455894, -0.1779134],
             [51.5454979, -0.1776732, {u'h': 2, u'e': 58}], [51.5454063, -0.1774342, {u'h': 2, u'e': 57}],
             [51.5450249, -0.1764236], [51.5449715, -0.1762989, {u'h': 2, u'e': 60}], [51.544899, -0.1761564],
             [51.5448265, -0.1760296], [51.5447273, -0.1758747, {u'h': 2, u'e': 61}], [51.5445557, -0.1756605],
             [51.5444183, -0.1755202, {u'h': 2, u'e': 61}], [51.5442429, -0.1753698, {u'h': 2, u'e': 61}],
             [51.5441284, -0.1752844, {u'h': 7, u'e': 60}], [51.5441093, -0.1752623], [51.5440941, -0.1752347],
             [51.5440865, -0.1752023], [51.5440826, -0.1751558], [51.5440826, -0.1749765],
             [51.5440788, -0.1749258, {u'h': 6, u'e': 60}], [51.5442543, -0.1749181, {u'h': 6, u'e': 61}],
             [51.5443192, -0.1749098], [51.5443802, -0.1748972], [51.5444984, -0.1748614, {u'h': 6, u'e': 62}],
             [51.5445557, -0.1748809], [51.5446091, -0.1749153], [51.5446548, -0.1749537, {u'e': 62}]]
        )

    @classmethod
    def tearDownClass(cls):
        TestHttp.SERVER.stop()

if __name__ == '__main__':
    unittest.main()