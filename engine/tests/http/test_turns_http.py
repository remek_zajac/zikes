import os
import unittest
import requests
import json

import baseTest

KThisFolder = os.path.dirname(os.path.abspath(__file__))
KTestFolder = os.path.dirname(KThisFolder)
KRootFolder = os.path.dirname(KTestFolder)

class TestHttp(unittest.TestCase):

    KRoutingPort    = "8087"
    SERVER          = None
    # Going east on Warmwood (just south of Liverpool Street Station), crossing Bishposgate and bearing left onto Outwich Street just behind Salesforce Tower and
    # towards St Mary Axe
    KWayAcrossBishipsgate = [[51.5162086, -0.0819167, {u'h': 4, u'e': 33}], [51.5161819, -0.0817039],
             [51.5161781, -0.0816722, {u'h': 4, u'e': 33}], [51.5161629, -0.0815187, {u'h': 4, u'e': 33}],
             [51.515995, -0.0809259, {u'h': 4, u'e': 32}], [51.5159798, -0.0808723, {u'h': 4, u'e': 32}],
             [51.5159607, -0.0807664], [51.5159569, -0.080685], [51.5159721, -0.0806156, {u'h': 4, u'e': 31}],
             [51.5161247, -0.0804159], [51.5161514, -0.0803689, {u'h': 4, u'e': 30}],
             [51.5161591, -0.0803379, {u'h': 4, u'e': 30}], [51.5161705, -0.0802538, {u'h': 4, u'e': 30}],
             [51.5161705, -0.0801528, {u'h': 4, u'e': 29}], [51.5161476, -0.0800864],
             [51.5161133, -0.0799858, {u'h': 4, u'e': 29}], [51.5160179, -0.0798237, {u'h': 4, u'e': 29}],
             [51.5159874, -0.0797711], [51.5157509, -0.0793684, {u'h': 4, u'e': 28}],
             [51.5157166, -0.0793196, {u'e': 28}]]
    KFirstRightOffBishopsgate = [[51.5158997, -0.0819449, {u'h': 4, u'e': 32}], [51.5161133, -0.0817408, {u'h': 4, u'e': 33}],
             [51.5161781, -0.0816722, {u'h': 4, u'e': 33}], [51.5161629, -0.0815187, {u'h': 4, u'e': 33}], [51.515995, -0.0809259, {u'h': 4, u'e': 32}],
             [51.5159798, -0.0808723, {u'h': 4, u'e': 32}], [51.5159607, -0.0807664], [51.5159569, -0.080685], [51.5159721, -0.0806156, {u'h': 4, u'e': 31}],
             [51.5161247, -0.0804159], [51.5161514, -0.0803689, {u'h': 4, u'e': 30}], [51.5161591, -0.0803379, {u'h': 4, u'e': 30}],
             [51.5161705, -0.0802538, {u'h': 4, u'e': 30}], [51.5161705, -0.0801528, {u'h': 4, u'e': 29}], [51.5161476, -0.0800864],
             [51.5161133, -0.0799858, {u'h': 4, u'e': 29}], [51.5160179, -0.0798237, {u'h': 4, u'e': 29}], [51.5159874, -0.0797711],
             [51.5157509, -0.0793684, {u'h': 4, u'e': 28}], [51.5157166, -0.0793196, {u'e': 28}]]


    @classmethod
    def setUpClass(cls):
        TestHttp.SERVER = baseTest.ZikesServer(
            port=TestHttp.KRoutingPort,
            osmInputFolder=baseTest.KBuildFolder+"/tests/data/smallTests/turns/bishopsgate",
            config=KThisFolder
        )
        TestHttp.SERVER.start()

    def testTurnPunishment(self):
        response = requests.get(TestHttp.SERVER.url()+"/route?milestones=51.51629412522694,-0.08234381675720216,51.51558641397573,-0.07907152175903322&profile=test")
        response = json.loads(response.content)
        #with basic preferences, expect to be taken straight as described above
        self.assertEquals(
            response["waypoints"],
            TestHttp.KWayAcrossBishipsgate
        )

        response = requests.get(TestHttp.SERVER.url()+"/route?milestones=51.5158557728,-0.0820032427353,51.51558641397573,-0.07907152175903322&profile=test")
        response = json.loads(response.content)
        #with basic preferences, expect to be taken first right as described above
        self.assertEquals(
            response["waypoints"],
            TestHttp.KFirstRightOffBishopsgate
        )

        # however, with some bicycle rewarding, expect to be taken left and up Bishopsgate and then
        # right on the north side of the Salesforce Tower through a narrow cycleway.
        preferenceJSON=json.loads(open(KThisFolder+"/test.routeprofile.json").read())
        preferenceJSON["highwayCostFactors"]["default"]["designated"] = 0.7
        response = requests.post(
            TestHttp.SERVER.url()+"/route?milestones=51.51629412522694,-0.08234381675720216,51.51558641397573,-0.07907152175903322",
            data=json.dumps(preferenceJSON)
        )
        response = json.loads(response.content)
        self.assertEquals(
            response["waypoints"],
            [[51.5162086, -0.0819167, {u'h': 4, u'e': 33}], [51.5161819, -0.0817039],
             [51.5161781, -0.0816722, {u'h': 4, u'e': 33}], [51.5164146, -0.0814452, {u'h': 4, u'e': 31}],
             [51.5164986, -0.0813016, {u'h': 4, u'e': 30}], [51.5165825, -0.0812237, {u'h': 17, u'e': 29}],
             [51.5162773, -0.0804255], [51.516201, -0.0803581], [51.5161591, -0.0803379, {u'h': 4, u'e': 30}],
             [51.5161705, -0.0802538, {u'h': 4, u'e': 30}], [51.5161705, -0.0801528, {u'h': 4, u'e': 29}],
             [51.5161476, -0.0800864], [51.5161133, -0.0799858, {u'h': 4, u'e': 29}],
             [51.5160179, -0.0798237, {u'h': 4, u'e': 29}], [51.5159874, -0.0797711],
             [51.5157509, -0.0793684, {u'h': 4, u'e': 28}], [51.5157166, -0.0793196, {u'e': 28}]]
        )
        # however, with some bicycle rewarding, expect to be taken second right from Bishopsgate on the
        # north side of the Salesforce Tower through a narrow cycleway
        response = requests.post(
            TestHttp.SERVER.url()+"/route?milestones=51.5158557728,-0.0820032427353,51.51558641397573,-0.07907152175903322",
            data=json.dumps(preferenceJSON)
        )
        response = json.loads(response.content)
        self.assertEquals(
            response["waypoints"],
            [[51.5158997, -0.0819449, {u'h': 4, u'e': 32}], [51.5161133, -0.0817408, {u'h': 4, u'e': 33}],
             [51.5161781, -0.0816722, {u'h': 4, u'e': 33}], [51.5164146, -0.0814452, {u'h': 4, u'e': 31}],
             [51.5164986, -0.0813016, {u'h': 4, u'e': 30}], [51.5165825, -0.0812237, {u'h': 17, u'e': 29}],
             [51.5162773, -0.0804255], [51.516201, -0.0803581], [51.5161591, -0.0803379, {u'h': 4, u'e': 30}],
             [51.5161705, -0.0802538, {u'h': 4, u'e': 30}], [51.5161705, -0.0801528, {u'h': 4, u'e': 29}],
             [51.5161476, -0.0800864], [51.5161133, -0.0799858, {u'h': 4, u'e': 29}],
             [51.5160179, -0.0798237, {u'h': 4, u'e': 29}], [51.5159874, -0.0797711],
             [51.5157509, -0.0793684, {u'h': 4, u'e': 28}], [51.5157166, -0.0793196, {u'e': 28}]]
        )


        # Yet, turning right on and from Bishopsgate is a nightmare and not a very good idea since
        # we can just ignore that small cycleway and go straight across as described at the very top
        preferenceJSON["turnPunishment"] = {
            "easy": {"secondary": 9, "secondary_link": 9, "primary": 9, "primary_link": 9, "trunk": 9, "trunk_link": 9,
                     "motorway": 9, "motorway_link": 9},
            "difficult": {"secondary": 18, "secondary_link": 18, "primary": 27, "primary_link": 27, "trunk": 36,
                          "trunk_link": 36, "motorway": 45, "motorway_link": 45},
            "trafficSignals": {"secondary": 14, "secondary_link": 14, "primary": 18, "primary_link": 18, "trunk": 23,
                               "trunk_link": 23, "motorway": 27, "motorway_link": 27}
        };
        response = requests.post(
            TestHttp.SERVER.url()+"/route?milestones=51.51629412522694,-0.08234381675720216,51.51558641397573,-0.07907152175903322",
            data=json.dumps(preferenceJSON)
        )
        response = json.loads(response.content)
        self.assertEquals(
            response["waypoints"],
            TestHttp.KWayAcrossBishipsgate
        )
        response = requests.post(
            TestHttp.SERVER.url()+"/route?milestones=51.5158557728,-0.0820032427353,51.51558641397573,-0.07907152175903322",
            data=json.dumps(preferenceJSON)
        )
        response = json.loads(response.content)
        self.assertEquals(
            response["waypoints"],
            TestHttp.KFirstRightOffBishopsgate
        )


    @classmethod
    def tearDownClass(cls):
        TestHttp.SERVER.stop()

if __name__ == '__main__':
    unittest.main()