import os
import subprocess
import time
import signal
import requests
import unittest
import json

import pyGeometry

KThisFolder  = os.path.dirname(os.path.abspath(__file__))
KRootFolder  = os.path.dirname(os.path.dirname(KThisFolder))
KTestFolder  = os.path.dirname(KThisFolder)
KBuildFolder = KRootFolder+"/"+"build"

class ZikesServer:

    def __init__(self, port, osmInputFolder, release="debug", config=None, serverLoadTimeoutSec=1):
        self.mPort = port
        self.mOsmInputFolder = osmInputFolder
        self.mConfig = config
        self.mServer = None
        self.mPid = None
        self.mRelease = release
        self.mServerLoadTimeoutSec = serverLoadTimeoutSec

    def start(self):
        pathToExecutable = "%s/bin/%s/engine" % (KRootFolder, self.mRelease)
        assert os.path.exists(pathToExecutable)
        args = [pathToExecutable, "--maps", self.mOsmInputFolder, "--address", ":{}".format(self.mPort)]
        if self.mConfig:
            args += ["--config", self.mConfig]
        self.mServer = subprocess.Popen(
            args=args,
            cwd=KRootFolder,
            stderr=subprocess.PIPE, stdout=subprocess.PIPE,
            close_fds=True,
            bufsize=-1
        )
        time.sleep(2)
        returnCode = self.mServer.poll()
        assert returnCode is None, "Server did not stay up\nstdout: %s\nstderr: %s" % self.mServer.communicate()
        self.mPid = self.mServer.pid
        self.checkServerFullyLoaded()

    def startViaBash(self):
        pathToExecutable = KThisFolder+"/startEngine.sh"
        args = [pathToExecutable, self.mRelease, "--maps", self.mOsmInputFolder, "--address", ":{}".format(self.mPort)]
        if self.mConfig:
            args += ["--config", self.mConfig]
        assert os.path.exists(pathToExecutable)
        pathToEngine = KRootFolder+"/bin/%s/engine" % self.mRelease
        assert os.path.exists(pathToEngine), "You need to build the engine first"
        line = subprocess.check_output(
            args,
            cwd=KRootFolder
        )
        self.mPid = int(line[:-1])
        time.sleep(2)
        self.checkServerFullyLoaded()

    def checkServerFullyLoaded(self):
        os.kill(self.mPid, 0)
        startTime = time.time()
        while True:
            try:
                requests.get(self.url() + "/meta.json")
                print "server up and well"
                break
            except requests.ConnectionError as e:
                if time.time() > startTime + self.mServerLoadTimeoutSec:
                    self.stop()
                    raise e
                print "server not on-line yet, waiting..."
                time.sleep(3)

    def url(self):
        return "http://localhost:%s" % self.mPort

    def memoryUsage(self):
        psArgs = ["ps", "u", str(self.mPid)]
        line = subprocess.check_output(psArgs)
        return int(filter(lambda x: len(x) != 0, line.split('\n')[1].split(" "))[5])

    def stop(self):
        if self.mServer:
           self.mServer.terminate()
           #print "Server output \nstdout: %s\nstderr: %s" % self.mServer.communicate()
        elif self.mPid:
            try:
                os.kill(self.mPid, signal.SIGTERM)
            except:
                pass


class ZikesTestCase(unittest.TestCase):

    @staticmethod
    def routeDistance(route):
        distanceMts = 0
        prev = None
        for point in route:
            point = pyGeometry.GeoPoint(tuple(point))
            if prev:
                distanceMts += prev.distanceMts(point)
            prev = point
        return int(round(distanceMts))

    #calculates how much are two routes apart by finding two most distant
    #points on them
    @staticmethod
    def _routesApartDistanceMts(a,b):
        greatestDistance = None
        for pa in a:
            pa = pyGeometry.GeoPoint(pa)
            smallestDistanceToPa = None
            for pb in b:
                pb = pyGeometry.GeoPoint(pb)
                d = pa.distanceMts(pb)
                if (smallestDistanceToPa is None) or smallestDistanceToPa > d:
                    smallestDistanceToPa = d
            if (greatestDistance is None) or greatestDistance < smallestDistanceToPa:
                greatestDistance = smallestDistanceToPa
        return greatestDistance

    def assertRouteEquals(self, serverResponse, expectedRoute):
        obtainedRoute = json.loads(serverResponse.content)["waypoints"]
        try:
            self.assertEquals(
                obtainedRoute,
                expectedRoute
            )
        except AssertionError as e:
            routesApart = ZikesTestCase._routesApartDistanceMts(obtainedRoute, expectedRoute)
            obtainedRouteDistanceMts = ZikesTestCase.routeDistance(obtainedRoute)
            expectedRouteDistanceMts = ZikesTestCase.routeDistance(expectedRoute)
            avgDistance = (obtainedRouteDistanceMts + expectedRouteDistanceMts)/2
            ddistance = obtainedRouteDistanceMts - expectedRouteDistanceMts
            absddistance = abs(ddistance)
            print obtainedRoute
            asertionMsg = "Routes not equal. They are %dmts apart (%d%% of their avg distance of %dmts) and \ntheir distances are off by %dmts (%d%% of their avg distance, obtained is %s)" % \
                (routesApart, (100 * routesApart / avgDistance), avgDistance, absddistance, (100 * absddistance/avgDistance), "shorter" if ddistance < 0 else "longer")
            raise AssertionError(asertionMsg)

