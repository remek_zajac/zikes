import os
import unittest
import requests
import json
import math

import baseTest

KTestFolder = os.path.dirname(os.path.abspath(__file__))
KRootFolder = os.path.dirname(os.path.dirname(KTestFolder))

class TestHttp(baseTest.ZikesTestCase):

    KRoutingPort    = "8087"
    SERVER = None

    @classmethod
    def setUpClass(cls):
        TestHttp.SERVER = baseTest.ZikesServer(
            port=TestHttp.KRoutingPort,
            osmInputFolder=KRootFolder+"/build/tests/data/regentsPark",
            config=KTestFolder
        )
        TestHttp.SERVER.start()

    def testBadLat(self):
        response = requests.get(TestHttp.SERVER.url()+"/route?milestones=d51.0069306511,-0.155687649345,51.5286593972,-0.151943285561&profile=test")
        self.assertEqual(response.status_code, 400)
        self.assertEqual(response.content, "Unable to perform the expected conversion of value 'd51.0069306511' to a double precission floating point number. When parsing lat of the 1th milestone")

    def testBadProfile(self):
        response = requests.get(TestHttp.SERVER.url()+"/route?milestones=51.5269306511,-0.155687649345,51.5286593972,-0.151943285561&profile=foo")
        self.assertEqual(response.status_code, 400)
        self.assertEqual(response.content, "Unable to find the requested server-preconfigured profile: foo")

    def testOffGrid(self):
        response = requests.get(TestHttp.SERVER.url()+"/route?milestones=0.5269306511,0.155687649345,51.5286593972,-0.151943285561&profile=test")
        self.assertEqual(response.status_code, 400)
        error = response = json.loads(response.content)
        self.assertEqual(error["code"], "RoutingException::NearestPointNotFound")
        self.assertEqual(error["culprit"], [0.5269306, 0.1556876])
        self.assertEqual(error["attemptedToleranceMts"], 1200)

    def testMissingProfile(self):
        response = requests.get(TestHttp.SERVER.url()+"/route?milestones=51.5269306511,-0.155687649345,51.5286593972,-0.151943285561")
        self.assertEqual(response.status_code, 400)
        self.assertEqual(response.content, "Required query argument missing: profile")

    def testInfo(self):
        response = requests.get(TestHttp.SERVER.url())
        self.assertTrue(
            response.content.startswith("<h1>Welcome to the Zikes routing server, available endpoints:")
        )

    def testA2BRoute(self):
        response = requests.get(TestHttp.SERVER.url()+"/route?milestones=51.5269306511,-0.155687649345,51.5286593972,-0.151943285561&profile=test")
        response = json.loads(response.content)
        self.assertEquals(
            response["waypoints"],
            [[51.5269051, -0.1556183, {u'h': 23, u'e': 37}], [51.5269318, -0.1555095],
             [51.526989, -0.1552201, {u'h': 23, u'e': 37}], [51.5270691, -0.1547875, {u'h': 23, u'e': 38}],
             [51.5272598, -0.15484], [51.5274277, -0.1547846, {u'h': 23, u'e': 38}], [51.5274429, -0.1546073],
             [51.5274925, -0.1543295, {u'h': 23, u'e': 39}], [51.5276031, -0.1537, {u'h': 23, u'e': 40}],
             [51.5277214, -0.1528612, {u'h': 23, u'e': 39}], [51.5279732, -0.1515546, {u'h': 23, u'e': 39}],
             [51.5280151, -0.1515519], [51.5280457, -0.1515161], [51.5280571, -0.1514628, {u'h': 23, u'e': 39}],
             [51.52808, -0.1513609, {u'h': 11, u'e': 39}], [51.5281715, -0.1514145], [51.5282593, -0.1514777],
             [51.528347, -0.1515502], [51.5284309, -0.1516317], [51.5285072, -0.1517219], [51.5285835, -0.1518203],
             [51.528656, -0.1519265, {u'e': 39}]]
        )
        self.assertEquals(
            response["meta"]["lengthMts"], 428 #TestHttp.routeDistance(response["waypoints"])
        )

    def testA2B2CRoute(self):
        response = requests.get(TestHttp.SERVER.url()+"/route?milestones=51.526928545599134,-0.1556539535522461,51.5266628848,-0.153561830521,51.528063266238995,-0.15141606330871582&profile=test")
        response = json.loads(response.content)

        self.assertEquals(
            response["waypoints"],
            [[51.5269051, -0.1556183, {u'h': 23, u'e': 37}], [51.5269318, -0.1555095],
             [51.526989, -0.1552201, {u'h': 23, u'e': 37}], [51.5270691, -0.1547875, {u'h': 23, u'e': 38}],
             [51.5270119, -0.1547206, {u'h': 23, u'e': 38}], [51.5267982, -0.1545057], [51.5266838, -0.1543658],
             [51.5266266, -0.1542735], [51.5265923, -0.1541681], [51.5265694, -0.1539654, {u'h': 23, u'e': 38}],
             [51.526535, -0.1537008, {u'h': 23, u'e': 38}], [51.5265999, -0.1536026],
             [51.5267296, -0.1535328, {u'e': 39, u't': True}], [51.5267296, -0.1535328, {u'h': 23, u'e': 39}],
             [51.5267029, -0.1533834], [51.5267067, -0.1531832, {u'h': 23, u'e': 39}],
             [51.5267143, -0.1530191, {u'h': 23, u'e': 39}], [51.526722, -0.1528655, {u'h': 23, u'e': 39}],
             [51.5267525, -0.1527614], [51.5267906, -0.1526747], [51.5269432, -0.1525619], [51.5270271, -0.1525349],
             [51.5271378, -0.1525459, {u'h': 23, u'e': 40}], [51.5271339, -0.1524861], [51.5271339, -0.1524238],
             [51.5271416, -0.1523478], [51.527153, -0.1522677], [51.5271797, -0.1521824],
             [51.5272064, -0.1521187, {u'h': 23, u'e': 39}], [51.5270653, -0.1520579],
             [51.5268974, -0.1520256, {u'h': 23, u'e': 40}], [51.527195, -0.1518928, {u'h': 23, u'e': 39}],
             [51.5272522, -0.1518661], [51.5272751, -0.1518784, {u'h': 23, u'e': 39}],
             [51.5273285, -0.1518478, {u'h': 23, u'e': 39}], [51.5274239, -0.151798, {u'h': 23, u'e': 39}],
             [51.5274239, -0.1517673, {u'h': 23, u'e': 39}], [51.5274277, -0.1517373, {u'h': 23, u'e': 39}],
             [51.5274353, -0.1517104], [51.5274506, -0.1516899, {u'h': 23, u'e': 39}], [51.5274696, -0.1516785],
             [51.5274849, -0.1516753, {u'h': 23, u'e': 39}], [51.527504, -0.1516812],
             [51.527523, -0.1516963, {u'h': 23, u'e': 39}], [51.5275383, -0.1517212],
             [51.5275459, -0.1517524, {u'h': 23, u'e': 39}], [51.5276413, -0.1517087, {u'h': 23, u'e': 39}],
             [51.5276871, -0.151688, {u'h': 23, u'e': 39}], [51.52771, -0.1516786],
             [51.5279732, -0.1515546, {u'h': 23, u'e': 39}], [51.5280151, -0.1515519], [51.5280457, -0.1515161],
             [51.5280571, -0.1514628, {u'e': 39}]]
        )
        self.assertEquals(
            response["meta"]["lengthMts"],
            500
        )

    def testCustomPreferences_punishPath(self):
        preferenceJSON=json.loads(open(KTestFolder+"/test.routeprofile.json").read())
        preferenceJSON["highwayCostFactors"]["path"]["cost"] = 3
        response = requests.post(
            TestHttp.SERVER.url()+"/route?milestones=51.5269306511,-0.155687649345,51.5281076101,-0.151343832788",
            data=json.dumps(preferenceJSON)
        )
        response = json.loads(response.content)
        self.assertEquals(
            response["waypoints"],
            [[51.5269051, -0.1556183, {u'h': 11, u'e': 37}], [51.5267982, -0.1555111], [51.5266991, -0.1553895],
             [51.5266075, -0.1552545], [51.5265198, -0.1551072], [51.5264435, -0.1549485], [51.5263748, -0.1547797],
             [51.5263138, -0.154602], [51.5262642, -0.1544167, {u'h': 11, u'e': 37}],
             [51.5262413, -0.1543018, {u'h': 11, u'e': 37}], [51.5262184, -0.1541849],
             [51.5261993, -0.1540664, {u'h': 11, u'e': 36}], [51.5261803, -0.1538843],
             [51.5261726, -0.1537002, {u'h': 11, u'e': 36}], [51.5261688, -0.1535183], [51.5261765, -0.1533367],
             [51.5261955, -0.1531567], [51.5262184, -0.1529792], [51.5262527, -0.1528054], [51.5262947, -0.1526362],
             [51.5263443, -0.1524729], [51.5264015, -0.1523162, {u'h': 11, u'e': 40}], [51.5264702, -0.1521608],
             [51.5265465, -0.1520148], [51.5266266, -0.1518791], [51.5267181, -0.1517546], [51.5268135, -0.1516422],
             [51.5269127, -0.1515426], [51.5270195, -0.1514565], [51.5271301, -0.1513843], [51.5272408, -0.1513267],
             [51.5273552, -0.151284], [51.5274734, -0.1512563], [51.5275917, -0.1512441], [51.52771, -0.1512472],
             [51.5278282, -0.1512658], [51.5279427, -0.1512996, {u'h': 11, u'e': 39}], [51.5279808, -0.1513136],
             [51.5280151, -0.1513292, {u'h': 11, u'e': 39}], [51.528038, -0.1513382, {u'h': 11, u'e': 39}],
             [51.52808, -0.1513609, {u'e': 39}]]
        )

    def testBadPreferences_corruptJSON(self):
        response = requests.post(
            TestHttp.SERVER.url()+"/route?milestones=51.5269306511,-0.155687649345,51.5286593972,-0.151943285561",
            data="{\"description\" : \"DistanceOnly\",\"highwayCostFactors\" : {\"motorway_link\"  : {\"linearCostFactor\" : \"forbid\"},"
        )
        self.assertEquals(
            response.content,
            "Unable to parse the uploaded profile"
        )

    def testEmptyHighwaysJSON(self):
        response = requests.post(
            TestHttp.SERVER.url()+"/route?milestones=51.5269306511,-0.155687649345,51.5286593972,-0.151943285561",
            data="{}"
        )
        self.assertEquals(
            response.content,
            "Unable to find or parse the manadatory profile attribute: 'means' (pedestrian|bicycle|car)."
        )


    def testBadPreferences_badLinearCostFactor(self):
        preferenceJSON=json.loads(open(KTestFolder+"/test.routeprofile.json").read())
        preferenceJSON["highwayCostFactors"]["path"]["cost"] = "foo"
        response = requests.post(
            TestHttp.SERVER.url()+"/route?milestones=51.5269306511,-0.155687649345,51.5286593972,-0.151943285561",
            data=json.dumps(preferenceJSON)
        )
        self.assertEquals(
            response.content,
           "Unable to interpret the value of 'foo' at highwayCostFactors::path::cost as an expected positive floating point number"
        )

    def testBadPreferences_badLinearCostFactorForBicycle(self):
        preferenceJSON=json.loads(open(KTestFolder+"/test.routeprofile.json").read())
        preferenceJSON["highwayCostFactors"]["service"]["designated"] = "foo"
        response = requests.post(
            TestHttp.SERVER.url()+"/route?milestones=51.5269306511,-0.155687649345,51.5286593972,-0.151943285561",
            data=json.dumps(preferenceJSON)
        )
        self.assertEquals(
            response.content,
            "Unable to interpret the value of 'foo' at highwayCostFactors::service::bicycle::designated as an expected positive floating point number"
        )


    def testBadPreferences_badClimbCostFactors_ValInsteadOfArray(self):
        preferenceJSON=json.loads(open(KTestFolder+"/test.routeprofile.json").read())
        preferenceJSON["climbCostFactors"] = {
            "costFactors" : -1
        }
        response = requests.post(
            TestHttp.SERVER.url()+"/route?milestones=51.5269306511,-0.155687649345,51.5286593972,-0.151943285561",
            data=json.dumps(preferenceJSON)
        )
        self.assertEquals(
            response.content,
           "There must be an odd number of entries in the 'costFactors' and not 0. The value in the middle is the factor for the 0% climb whilst single percent point steps going from it leftand right are cost factors for the discretelly (in/de)-creasing climbs."
        )

    def testBadPreferences_badClimbCostFactors_badEntry(self):
        preferenceJSON=json.loads(open(KTestFolder+"/test.routeprofile.json").read())
        preferenceJSON["climbCostFactors"] = {
            "costFactors" : [1,2,"seven",3,4]
        }
        response = requests.post(
            TestHttp.SERVER.url()+"/route?milestones=51.5269306511,-0.155687649345,51.5286593972,-0.151943285561",
            data=json.dumps(preferenceJSON)
        )
        self.assertEquals(
            response.content,
             "Unable to interpret the value of 'seven' at climbCostFactors::costFactors as an expected positive floating point number"
        )

    def testBadPreferences_evenNumberOfEntries(self):
        preferenceJSON=json.loads(open(KTestFolder+"/test.routeprofile.json").read())
        preferenceJSON["climbCostFactors"] = {
            "costFactors" : [1,2,3,4]
        }
        response = requests.post(
            TestHttp.SERVER.url()+"/route?milestones=51.5269306511,-0.155687649345,51.5286593972,-0.151943285561",
            data=json.dumps(preferenceJSON)
        )
        self.assertEquals(
            response.content,
            "There must be an odd number of entries in the 'costFactors' and not 4. The value in the middle is the factor for the 0% climb whilst single percent point steps going from it leftand right are cost factors for the discretelly (in/de)-creasing climbs."
        )

    def testBadPreferences_noClimbFactorEntries(self):
        preferenceJSON=json.loads(open(KTestFolder+"/test.routeprofile.json").read())
        preferenceJSON["climbCostFactors"] = {
            "costFactors" : []
        }
        response = requests.post(
            TestHttp.SERVER.url()+"/route?milestones=51.5269306511,-0.155687649345,51.5286593972,-0.151943285561",
            data=json.dumps(preferenceJSON)
        )
        self.assertEquals(
            response.content,
            "There must be an odd number of entries in the 'costFactors' and not 0. The value in the middle is the factor for the 0% climb whilst single percent point steps going from it leftand right are cost factors for the discretelly (in/de)-creasing climbs."
        )

    @classmethod
    def tearDownClass(cls):
        TestHttp.SERVER.stop()

if __name__ == '__main__':
    unittest.main()