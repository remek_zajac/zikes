/*************************************************************************
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * (C) 2016 Remek Zajac
 *************************************************************************/

#include <boost/test/unit_test.hpp>

#include "tests/maploader.h"
#include "tests/utils.h"


void verifyTurn(const geo::Point& from, unsigned long distanceMts, float baseBearingDeg, float turnBearingDeg ) {
    geo::Point pivot       = from.offsetMtsCourse( distanceMts, baseBearingDeg*geo::GeoTypes::deg2Rad);
    geo::Point to          = pivot.offsetMtsCourse(distanceMts, (turnBearingDeg+baseBearingDeg)*geo::GeoTypes::deg2Rad);
    geo::EuclidianVector     headVector(from, pivot);
    geo::EuclidianVector     tailVector(pivot, to);
    float turnBearingGot   = headVector.diamondAngle(tailVector) * 90;
    float delta            = fabs(turnBearingGot - turnBearingDeg);
    //std::cout << "{ 'magnitude' : " << delta << ", 'section':[" << from.toString(true) << "," << pivot.toString(true) << "," << to.toString(true) << "] }" << std::endl;
    BOOST_CHECK(delta < 16);
}

void rotateAndVerify(const geo::Point& from, unsigned long distanceMts, float turnBearingDeg) {
    for (int angleDeg = 0; angleDeg <= 360; angleDeg += 15) {
        verifyTurn(from, distanceMts, angleDeg, turnBearingDeg);
    }
}

BOOST_AUTO_TEST_CASE(diamon_angles)
{
    geo::Point referenceFrom(51.5269019058,-0.155619559479);
    for (int turnDeg = -170; turnDeg <= 170; turnDeg += 20) {
        rotateAndVerify(referenceFrom.offsetMtsCourse(100+turnDeg+abs(turnDeg*10), turnDeg), 100, turnDeg);
    }
}


BOOST_AUTO_TEST_CASE(basic_lengths)
{
    geo::Point prevFrom(51.5269019058,-0.155619559479);
    geo::Point prevTo(51.5270687778,-0.15479880352);
    geo::Point pointToTheLeft(51.5272656869,-0.154836354447);
    geo::Point pointToTheRight(51.526631573,-0.154278454972);
    geo::EuclidianVector headVector(prevFrom, prevTo);
    geo::EuclidianVector leftTailVector(prevTo, pointToTheLeft);
    geo::EuclidianVector rightTailVector(prevTo, pointToTheRight);

    float leftRelAngle = headVector.diamondAngle(leftTailVector);
    float rightRelAngle = headVector.diamondAngle(rightTailVector);
    BOOST_CHECK(leftRelAngle  - -0.796187878 < 0.1);
    BOOST_CHECK(rightRelAngle - 0.917997897  < 0.1);
}

