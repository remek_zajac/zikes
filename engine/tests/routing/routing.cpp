/*************************************************************************
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * (C) 2015 Remek Zajac
 *************************************************************************/

#include <fstream>
#include <streambuf>
#include <chrono>
#include <stdexcept>

#include "testModule.h"
#include "route/routing.h"


struct Fixture
{
    Fixture() {}
    ~Fixture() {}
};


BOOST_FIXTURE_TEST_SUITE(Routing, Fixture)


BOOST_AUTO_TEST_CASE(basic_route)
{
    geo::Point from = geo::Point(51.5269306511,-0.155687649345);
    geo::Point to = geo::Point(51.5286593972,-0.151943285561);

    routing::DistanceCostFunctionFactory<routing::MtsDistanceCostFunction> distanceCostFunctionFactory;
    routing::GpxRoute obtainedRoute;
    routing::Request  routingRequest(distanceCostFunctionFactory);
    routingRequest
    .setFrom(from)
    .setTo(to)
    .execute(obtainedRoute);

    //std::ofstream ofs ("basic_route_obtained.gpx", std::ofstream::out);
    //obtainedRoute.write(ofs);

    BOOST_CHECK(MapFacts::verifyRouteAsGpxFile(obtainedRoute, KThisPath + "/basic_route.gpx"));
}

BOOST_AUTO_TEST_CASE(route_to_non_edge_node)
{
    geo::Point from = geo::Point(51.5268997891,-0.155609903526);
    geo::Point to = geo::Point(51.5266161065,-0.154247341347);

    routing::DistanceCostFunctionFactory<routing::MtsDistanceCostFunction> distanceCostFunctionFactory;
    routing::GpxRoute obtainedRoute;
    routing::Request  routingRequest(distanceCostFunctionFactory);
    routingRequest
    .setFrom(from)
    .setTo(to)
    .execute(obtainedRoute);

    //std::ofstream ofs ("route_to_non_edge_node_obtained.gpx", std::ofstream::out);
    //obtainedRoute.write(ofs);

    BOOST_CHECK(MapFacts::verifyRouteAsGpxFile(obtainedRoute, KThisPath + "/route_to_non_edge_node.gpx"));
}

bool from_off_limits(const routing::Request::NearestPointNotFound& ex) {
    std::string what = ex.what();
    return what == "Unable to find a highway within 1200mts of the requested [0.5269306,-0.1556876]";
}

bool to_off_limits(const routing::Request::NearestPointNotFound& ex) {
    std::string what = ex.what();
    std::cout << "Remek" << ex.what() << std::endl;
    return what == "Unable to find a highway within 1200mts of the requested [0.5286594,-0.1519433]";
}

bool from_absent(const std::invalid_argument& ex ) {
    std::string what = ex.what();
    return what == "'from' not specified";
}

bool to_absent(const std::invalid_argument& ex ) {
    std::string what = ex.what();
    return what == "'to' not specified";
}


BOOST_AUTO_TEST_CASE(off_limits_route)
{
    routing::DistanceCostFunctionFactory<routing::MtsDistanceCostFunction> distanceCostFunctionFactory;
    const std::string KExpectedRouteFilePath = KThisPath + "/basic_route.gpx";
    routing::GpxRoute obtainedRoute;
    routing::Request  routingRequest(distanceCostFunctionFactory);

    BOOST_REQUIRE_EXCEPTION(routingRequest.execute(obtainedRoute), std::invalid_argument, from_absent);
    routingRequest.setFrom(geo::Point(51.5269306511,-0.155687649345));
    BOOST_REQUIRE_EXCEPTION(routingRequest.execute(obtainedRoute), std::invalid_argument, to_absent);
    BOOST_REQUIRE_EXCEPTION(routingRequest.setTo(geo::Point(0.5286593972,-0.151943285561)), routing::Request::NearestPointNotFound, to_off_limits);
    BOOST_REQUIRE_EXCEPTION(routingRequest.setFrom(geo::Point(0.5269306511,-0.155687649345)), routing::Request::NearestPointNotFound, from_off_limits);
}

class LinearFactorTestCostFunction : public routing::MtsDistanceCostFunction {

    public:
        typedef std::pair<float, geomap::Way::HighwayType> Overload;
        LinearFactorTestCostFunction(
            const geo::Point& aTo,
            Overload aOverload
        )
        :routing::MtsDistanceCostFunction(aTo),
         mHighwayOverload(aOverload)
        {}


        virtual Cost evaluate(const routing::RouteHead& aRouteHead) const {
            Cost costBase = routing::MtsDistanceCostFunction::evaluate(aRouteHead);
            if (aRouteHead.edge().highwayType() == mHighwayOverload.second) {
                costBase *= mHighwayOverload.first;
            }
            return costBase;
        }

    private:
         Overload mHighwayOverload;
};


class LinearFactorTestCostFunctionFactory : public routing::CostFunctionFactory {

    public:
        LinearFactorTestCostFunctionFactory(
            float aFactor, geomap::Way::HighwayType aHighwayType
        )
        :mHighwayOverload(std::pair<float, geomap::Way::HighwayType>(aFactor, aHighwayType))
        {}

        virtual routing::CostFunction::UniquePtr create(const geo::Point& aFrom, const geo::Point& aTo) const {
            return std::move(
                routing::CostFunction::UniquePtr(new LinearFactorTestCostFunction(aTo, mHighwayOverload))
            );
        }

    private:
         LinearFactorTestCostFunction::Overload mHighwayOverload;
};


BOOST_AUTO_TEST_CASE(linear_cost_factors)
{
    MapFacts& mapFacts = MapFacts::mInstance;
    routing::DistanceCostFunctionFactory<routing::MtsDistanceCostFunction> distanceCostFunctionFactory;

    //Obtain the shortest route from X to EdgeNode2100667055
    routing::Route shortestRoute;
    routing::Request routingRequestShortest(distanceCostFunctionFactory);
    routingRequestShortest
    .setFrom(mapFacts.X)
    .setTo(mapFacts.EdgeNode2100667055)
    .execute(shortestRoute);

    BOOST_CHECK(MapFacts::verifyRouteAsGpxFile(shortestRoute, KThisPath + "/Ato2100667055_shortest.gpx"));

    //now punish handsomely 'paths'
    LinearFactorTestCostFunctionFactory harshForPaths(
        5.0,
        geomap::Map::instance().getMeta().getHighwayTypes().at("path")
    );
    routing::GpxRoute roundRoute;
    routing::Request routingRequestAround(harshForPaths);
    routingRequestAround
    .setFrom(mapFacts.X)
    .setTo(mapFacts.EdgeNode2100667055)
    .execute(roundRoute);
    BOOST_CHECK(MapFacts::verifyRouteAsGpxFile(roundRoute, KThisPath + "/Ato2100667055_round.gpx"));

    float breakingFactor = 1.0 * roundRoute.distanceMts() / shortestRoute.distanceMts();
    //now punish 'paths' just below the threshold so they're still used
    routing::Route shortestRouteAgain;
    LinearFactorTestCostFunctionFactory barelyBelowBreakingCost(
        breakingFactor,
        geomap::Map::instance().getMeta().getHighwayTypes().at("path")
    );
    routing::Request routingRequestShortestAgain(barelyBelowBreakingCost);
    routingRequestShortestAgain
    .setFrom(mapFacts.X)
    .setTo(mapFacts.EdgeNode2100667055)
    .execute(shortestRouteAgain);
    BOOST_CHECK(MapFacts::verifyRouteAsGpxFile(shortestRouteAgain, KThisPath + "/Ato2100667055_shortest.gpx"));

    //now punish 'paths' just above the threshold so they're too expensive, and we're going around
    LinearFactorTestCostFunctionFactory barelyAboveBreakingCost(
        breakingFactor*1.1,
        geomap::Map::instance().getMeta().getHighwayTypes().at("path")
    );
    routing::Route roundRouteAgain;
    routing::Request routingRequestAroundAgain(barelyAboveBreakingCost);
    routingRequestAroundAgain
    .setFrom(mapFacts.X)
    .setTo(mapFacts.EdgeNode2100667055)
    .execute(roundRouteAgain);
    BOOST_CHECK(MapFacts::verifyRouteAsGpxFile(roundRouteAgain, KThisPath + "/Ato2100667055_round.gpx"));
}

BOOST_AUTO_TEST_CASE(float2int_factors)
{
    typedef routing::FloatFactor2IntPunishment<4,255> FF2I;
    BOOST_CHECK_EQUAL(FF2I::convert(1.0), FF2I::NeutralPunishFactor);
    BOOST_CHECK_EQUAL(FF2I::convert(0), FF2I::MinPunishFactor);
    BOOST_CHECK_EQUAL(FF2I::convert(256), FF2I::MaxPunishFactor);
    BOOST_CHECK_EQUAL(FF2I::convert(2), 32);
    BOOST_CHECK_EQUAL(FF2I::convert(1000), FF2I::MaxPunishFactor);
}

BOOST_AUTO_TEST_CASE(weights_equality)
{
    typedef routing::FloatFactor2IntPunishment<4,255> FF2I;
    FF2I::Multiplier multiplier;
    for (int i = 0; i < 10; i++) {
        multiplier.apply(FF2I::convert(2.0/4));
        multiplier.apply(FF2I::convert(4.0/2));
    }
    BOOST_CHECK_EQUAL(multiplier.factor(), FF2I::NeutralPunishFactor);

    FF2I::Multiplier multiplier1;
    int iterations = 4;
    for (int i = 0; i < iterations; i++) {
        multiplier1.apply(FF2I::convert(2.0/3));
        multiplier1.apply(FF2I::convert(3.0/2));
    }
    BOOST_CHECK_EQUAL(multiplier1.factor(), FF2I::NeutralPunishFactor);

    FF2I::Multiplier multiplier2;
    for (int i = 0; i < 3; i++) {
        multiplier2.apply(FF2I::convert(0.5));
    }
    BOOST_CHECK_EQUAL(multiplier2.factor(), 2);

    FF2I::Multiplier multiplier3;
    for (int i = 0; i < 10; i++) {
        multiplier3.apply(FF2I::convert(i));
    }
    BOOST_CHECK_EQUAL(multiplier3.factor(), FF2I::Multiplier::MaxPunishFactor);

    FF2I::Multiplier multiplier4;
    for (int i = 0; i < 8; i++) {
        multiplier4.apply(FF2I::convert(0.5));
    }
    BOOST_CHECK_EQUAL(multiplier4.factor(), FF2I::MinPunishFactor);
}

BOOST_AUTO_TEST_SUITE_END()
