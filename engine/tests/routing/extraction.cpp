/*************************************************************************
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * (C) 2015 Remek Zajac
 *************************************************************************/

#include "testModule.h"


struct Fixture
{
    Fixture() {}
    ~Fixture() {}
};


BOOST_FIXTURE_TEST_SUITE(Extraction, Fixture)


BOOST_AUTO_TEST_CASE(geo_box)
{
    const geo::Point r(51.5272496308,-0.154879269791);
    geo::Box interestBoxSrc(r);
    interestBoxSrc.inflateByMts(50);
    const geo::Point minCorner = interestBoxSrc.min_corner();
    const geo::Point maxCorner = interestBoxSrc.max_corner();
    unsigned long distanceAcrossMts = maxCorner.distanceMts(minCorner);

    unsigned long expectedDistanceAcrossMts = std::sqrt(100*100+100*100);
    BOOST_CHECK(distanceAcrossMts == expectedDistanceAcrossMts);
    BOOST_CHECK(minCorner.lat() < maxCorner.lat());
    BOOST_CHECK(minCorner.lon() < maxCorner.lon());
}

BOOST_AUTO_TEST_CASE(verify_Y)
{
    const geo::Point exp_Y(51.5270661104,-0.154780564499);
    MapFacts& mapFacts = MapFacts::mInstance;
    BOOST_CHECK(mapFacts.Y->distanceMts(exp_Y)==0);
    BOOST_CHECK(mapFacts.Y->edges().size()==3);

    const geomap::Edge::DirectedEdge* YtoU = MapFacts::findEdgeLeadingTo(mapFacts.Y, mapFacts.U);
    BOOST_CHECK(YtoU != nullptr);
    MapFacts::TExpectedEdge towardsU = {
        *mapFacts.Y,
        geo::Point(51.5272596,-0.15484),
        *mapFacts.U
    };
    MapFacts::verifyEdge(*YtoU, towardsU, "towardsU");

    const geomap::Edge::DirectedEdge* YtoZ = MapFacts::findEdgeLeadingTo(mapFacts.Y, mapFacts.Z);
    BOOST_CHECK(YtoZ != nullptr);
    MapFacts::TExpectedEdge towardsZ = {
        *mapFacts.Y,
        *mapFacts.Z
    };
    MapFacts::verifyEdge(*YtoZ, towardsZ, "towardsZ");

    const geomap::Edge::DirectedEdge* YtoN = MapFacts::findEdgeLeadingTo(mapFacts.Y, mapFacts.N);
    BOOST_CHECK(YtoN != nullptr);
    MapFacts::TExpectedEdge towardsN = {
        *mapFacts.Y,
        *mapFacts.N
    };
    MapFacts::verifyEdge(*YtoN, towardsN, "towardsN");
}


BOOST_AUTO_TEST_CASE(verify_V)
{
    const geo::Point exp_V(51.5272596,-0.15484);
    MapFacts& mapFacts = MapFacts::mInstance;
    BOOST_CHECK(mapFacts.V->distanceMts(exp_V)==0);
    BOOST_CHECK(mapFacts.V->edges().size()==2);

    const geomap::Edge::DirectedEdge* VtoY = MapFacts::findEdgeLeadingTo(mapFacts.V, mapFacts.Y);
    BOOST_CHECK(VtoY != nullptr);
    MapFacts::TExpectedEdge towardsY = {
        *mapFacts.V,
        *mapFacts.Y
    };
    MapFacts::verifyEdge(*VtoY, towardsY, "towardsY");

    const geomap::Edge::DirectedEdge* VtoU = MapFacts::findEdgeLeadingTo(mapFacts.V, mapFacts.U);
    BOOST_CHECK(VtoU != nullptr);
    MapFacts::TExpectedEdge towardsU = {
        *mapFacts.V,
        *mapFacts.U
    };
    MapFacts::verifyEdge(*VtoU, towardsU, "towardsU");
}


BOOST_AUTO_TEST_CASE(verify_p1)
{
    const geo::Point exp_p1(51.5270315076,-0.154986178308);
    MapFacts& mapFacts = MapFacts::mInstance;
    BOOST_CHECK(mapFacts.p1->distanceMts(exp_p1)==0);
    BOOST_CHECK(mapFacts.p1->edges().size()==2);

    const geomap::Edge::DirectedEdge* p1toN = MapFacts::findEdgeLeadingTo(mapFacts.p1, mapFacts.N);
    BOOST_CHECK(p1toN != nullptr);
    MapFacts::TExpectedEdge towardsN = {
        *mapFacts.p1,
        *mapFacts.N
    };
    MapFacts::verifyEdge(*p1toN, towardsN, "towardsN");

    const geomap::Edge::DirectedEdge* p1toY = MapFacts::findEdgeLeadingTo(mapFacts.p1, mapFacts.Y);
    BOOST_CHECK(p1toY != nullptr);
    MapFacts::TExpectedEdge towardsY = {
        *mapFacts.p1,
        *mapFacts.Y
    };
    MapFacts::verifyEdge(*p1toY, towardsY, "towardsY");
    BOOST_CHECK_EQUAL(p1toY->distanceMts(), 14);
    BOOST_CHECK_EQUAL(p1toN->distanceMts(), 17);
}


BOOST_AUTO_TEST_CASE(verifyV1)
{
    MapFacts& mapFacts = MapFacts::mInstance;
    //This verifies whether V1 is the same as V.
    //The pointers will be different (as both are adhoc nodes)
    //so this needs to verify that they both lead to the same
    //neighbours
    auto V_1st_edge_it  = boost::begin(mapFacts.V->edges());
    auto V1_1st_edge_it = boost::begin(mapFacts.V1->edges());
    const geomap::Edge::DirectedEdge& V_1st_edge  = **V_1st_edge_it;
    const geomap::Edge::DirectedEdge& V1_1st_edge = **V1_1st_edge_it;
    BOOST_CHECK(&V_1st_edge.to() == &V1_1st_edge.to());
}

BOOST_AUTO_TEST_CASE(verifyHandleSizes)
{
    BOOST_CHECK_EQUAL(sizeof(geomap::Node::ConstHandle), sizeof(geomap::Node*));
}


BOOST_AUTO_TEST_CASE(projection_tolerance)
{
    geomap::Edge::EdgeNode::ConstAutoHandle expectedEdgeNode = geomap::Map::NearestProjectedEdgeNodeStrategy(geomap::Map::instance()).find(geo::Point(51.526928545599134,-0.1556539535522461));
    BOOST_CHECK_EQUAL(expectedEdgeNode->distanceMts(geo::Point(51.5269055, -0.1556183)), 0);
}

BOOST_AUTO_TEST_CASE(fallback_nearest_strategy)
{
    //we'll take a point on the edge
    geo::Point pointAtAcceptableDistance = geo::Point(51.5291061,-0.1564272).offsetMtsCourse(geomap::Map::NearestEdgeNodeStrategy::KDefaultToleranceMts, -geo::GeoTypes::deg2Rad*90);
    std::cout << "pointAtAcceptableDistance " << pointAtAcceptableDistance.toString() << std::endl;

    geomap::Edge::EdgeNode::ConstAutoHandle withinTolerance = geomap::Map::FallbackEdgeNodeStrategy(
        geomap::Map::NearestProjectedEdgeNodeStrategy(geomap::Map::instance()),
        geomap::Map::NearestEdgeNodeStrategy(geomap::Map::instance())
    ).find(pointAtAcceptableDistance);
    BOOST_CHECK(withinTolerance);

    geo::Point pointAtUnacceptableDistance = geo::Point(51.5291061,-0.1564272).offsetMtsCourse(geomap::Map::NearestEdgeNodeStrategy::KDefaultToleranceMts+10, -geo::GeoTypes::deg2Rad*90);
    std::cout << "pointAtUnacceptableDistance " << pointAtUnacceptableDistance.toString() << std::endl;
    geomap::Edge::EdgeNode::ConstAutoHandle outsideTolerance;
    outsideTolerance = geomap::Map::FallbackEdgeNodeStrategy(
        geomap::Map::NearestProjectedEdgeNodeStrategy(geomap::Map::instance()),
        geomap::Map::NearestEdgeNodeStrategy(geomap::Map::instance())
    ).find(pointAtUnacceptableDistance);

    BOOST_CHECK(!outsideTolerance);
}

BOOST_AUTO_TEST_SUITE_END()
