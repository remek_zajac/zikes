/*************************************************************************
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * (C) 2015 Remek Zajac
 *************************************************************************/

#include "cost/components/all.h"
#include "testModule.h"
#include "httpServer/staticprofiles.h"


struct Fixture
{
    Fixture() {}
    ~Fixture() {}
};


BOOST_FIXTURE_TEST_SUITE(Loading, Fixture)


BOOST_AUTO_TEST_CASE(verify_expected_map_inventory)
{
    MapFacts& mapFacts = MapFacts::mInstance;

    //way 3996989 is in the input file, but none of sections should be because:
    //only one of its sections lurks in input files (regentsPark), namely: 2100667055->58
    //and only the first node of that section is among the loaded nodes (was in export region)
    BOOST_CHECK(mapFacts.EdgeNode2100667055); //this one

    //therefore EdgeNode2100667055 should only have 2 edges
    BOOST_CHECK(mapFacts.EdgeNode2100667055->edges().size() == 2);
    BOOST_CHECK(MapFacts::findEdgeLeadingTo(mapFacts.EdgeNode2100667055, geo::Point(51.5280815,-0.1513609)) != nullptr);
    BOOST_CHECK(MapFacts::findEdgeLeadingTo(mapFacts.EdgeNode2100667055, geo::Point(51.5280167,-0.1513292)) != nullptr);
}


BOOST_AUTO_TEST_CASE(highwayTypes_loaded)
{
    BOOST_CHECK(geomap::Map::instance().getMeta().getHighwayTypes().at("motorway") == 0);
    BOOST_CHECK(geomap::Map::instance().getMeta().getHighwayTypes().at("primary") == 4);
    BOOST_CHECK(geomap::Map::instance().getMeta().getHighwayTypes().at("bus_guideway") == 13);
    BOOST_CHECK(geomap::Map::instance().getMeta().getHighwayTypes().at("bridleway") == 21);
}

const std::string KConfigPath = KThisPath + "/../../config";
BOOST_AUTO_TEST_CASE(config_loaded)
{
    http::StaticProfiles::init(KConfigPath);
    geomap::Way::TOneDirectionAccessAttributes onlyAllowedForBicycles(
        geomap::Way::TAccessAttribute::EForbidden, //car
        geomap::Way::TAccessAttribute::EAllowed,   //bicycle
        geomap::Way::TAccessAttribute::EForbidden  //pedestrian
    );
    geomap::Way::TOneDirectionAccessAttributes onlyDesignatedForBicycles(
        geomap::Way::TAccessAttribute::EForbidden, //car
        geomap::Way::TAccessAttribute::EDesignated,//bicycle
        geomap::Way::TAccessAttribute::EForbidden  //pedestrian
    );
    geomap::Way::TOneDirectionAccessAttributes onlyAllowedForPedestrains(
        geomap::Way::TAccessAttribute::EForbidden, //car
        geomap::Way::TAccessAttribute::EForbidden, //bicycle
        geomap::Way::TAccessAttribute::EAllowed    //pedestrian
    );
    http::StaticProfiles::RoutingProfiles routingProfiles = http::StaticProfiles::instance().routingProfiles();
    const cost::HighwayCostFactors& hf = static_cast<const cost::HighwayCostFactors&>(
        *routingProfiles.at("bicycle")->components().at(cost::HighwayCostFactors::KNodeName)
    );
    const cost::ClimbFactors& cf = static_cast<const cost::ClimbFactors&>(
        *routingProfiles.at("bicycle")->components().at(cost::ClimbFactors::KNodeName)
    );
    const cost::TurnPunishment& tp = static_cast<const cost::TurnPunishment&>(
        *routingProfiles.at("bicycle")->components().at(cost::TurnPunishment::KNodeName)
    );
    BOOST_CHECK_EQUAL(hf.highway(0).factor(onlyAllowedForBicycles,      hf.means()), cost::CompoundCostFunctionProfile::ComponentBase::KForbiddenFactor);  //motorway (absent in config)
    BOOST_CHECK_EQUAL(hf.highway(0).factor(onlyDesignatedForBicycles,   hf.means()), 11);  //motorway (absent in config, but default for designated set to 0.7)
    BOOST_CHECK_EQUAL(hf.highway(1).factor(onlyDesignatedForBicycles,   hf.means()), cost::CompoundCostFunctionProfile::ComponentBase::KForbiddenFactor);  //motorway link (present in config)
    BOOST_CHECK_EQUAL(hf.highway(1).factor(onlyAllowedForBicycles,      hf.means()), cost::CompoundCostFunctionProfile::ComponentBase::KForbiddenFactor);  //motorway link (present in config)
    BOOST_CHECK_EQUAL(hf.highway(17).factor(onlyDesignatedForBicycles,  hf.means()), 11);  //cycleway (with cycleway, whatever that means)
    BOOST_CHECK_EQUAL(hf.highway(17).factor(onlyAllowedForBicycles,     hf.means()), cost::CompoundCostFunctionProfile::ComponentBase::KForbiddenFactor);  //cycleway (without cycleway, i.e.: highwayPath of type cycleway, which should be designated and not just allowed, but this test makes other things possible)
    BOOST_CHECK_EQUAL(hf.highway(17).factor(onlyAllowedForPedestrains,  hf.means()), cost::CompoundCostFunctionProfile::ComponentBase::KForbiddenFactor);  //cycleway (with cycleway, whatever that means)
    BOOST_CHECK_EQUAL(hf.highway(23).factor(onlyDesignatedForBicycles,  hf.means()), 11);  //path with cycleway
    BOOST_CHECK_EQUAL(hf.highway(23).factor(onlyAllowedForBicycles,     hf.means()), 48);  //path without cycleway
    BOOST_CHECK_EQUAL(hf.highway(23).factor(onlyAllowedForPedestrains,  hf.means()), cost::CompoundCostFunctionProfile::ComponentBase::KForbiddenFactor); //path with no cyclists allowed
    BOOST_CHECK_EQUAL(hf.highway(24).factor(onlyAllowedForBicycles,     hf.means()), 64);  //ferry
    BOOST_CHECK_EQUAL(static_cast<const cost::HighwayWithFlatCost&>(hf.highway(24)).flatCost(), 500);  //ferry

    BOOST_CHECK_EQUAL(cf.factor(geomap::Edge::ClimbPercent(-19)), 1600);
    BOOST_CHECK_EQUAL(cf.factor(geomap::Edge::ClimbPercent(-10)), 1600);
    BOOST_CHECK_EQUAL(cf.factor(geomap::Edge::ClimbPercent(-3)),  13);
    BOOST_CHECK_EQUAL(cf.factor(geomap::Edge::ClimbPercent(0)),   16);
    BOOST_CHECK_EQUAL(cf.factor(geomap::Edge::ClimbPercent(6)),   160);
    BOOST_CHECK_EQUAL(cf.factor(geomap::Edge::ClimbPercent(100)), 1600);
    BOOST_CHECK_EQUAL(cf.factor(geomap::Edge::ClimbPercent()),    cost::CompoundCostFunctionProfile::ComponentBase::KNeutralFactor);
    BOOST_CHECK_EQUAL(
        tp.difficult(
            geomap::Map::instance().getMeta().getHighwayTypes().at("motorway"),
            geomap::Map::instance().getMeta().getHighwayTypes().at("motorway_link")
        ), 0
    );
    BOOST_CHECK_EQUAL(
        tp.difficult(
            geomap::Map::instance().getMeta().getHighwayTypes().at("motorway"),
            geomap::Map::instance().getMeta().getHighwayTypes().at("trunk")
        ), 40
    );
    BOOST_CHECK_EQUAL(
        tp.difficult(
            geomap::Map::instance().getMeta().getHighwayTypes().at("primary"),
            geomap::Map::instance().getMeta().getHighwayTypes().at("trunk")
        ), 40
    );
    BOOST_CHECK_EQUAL(
        tp.difficult(
            geomap::Map::instance().getMeta().getHighwayTypes().at("primary"),
            geomap::Map::instance().getMeta().getHighwayTypes().at("secondary")
        ), 30
    );
    BOOST_CHECK_EQUAL(
        tp.easy(
            geomap::Map::instance().getMeta().getHighwayTypes().at("primary"),
            geomap::Map::instance().getMeta().getHighwayTypes().at("secondary")
        ), 3
    );
    BOOST_CHECK_EQUAL(
        tp.easy(
            geomap::Map::instance().getMeta().getHighwayTypes().at("primary"),
            geomap::Map::instance().getMeta().getHighwayTypes().at("path")
        ), 3
    );
    BOOST_CHECK_EQUAL(
        tp.easy(
            geomap::Map::instance().getMeta().getHighwayTypes().at("track"),
            geomap::Map::instance().getMeta().getHighwayTypes().at("path")
        ), 0
    );
}

class PretendDirectedEdge : public geomap::DirectedEdge {

    public:

        PretendDirectedEdge(
            const geomap::Edge::DirectedEdge& aEdge,
            const unsigned long aDistanceMts,
            const geomap::Edge::EdgeNode::ConstHandle aFrom = geomap::Edge::EdgeNode::ConstHandle(),
            const geomap::Edge::EdgeNode::ConstHandle aTo = geomap::Edge::EdgeNode::ConstHandle()
        )
        :geomap::DirectedEdge(aEdge.edge()),
         mDistanceMts(aDistanceMts),
         mAccess(geomap::Way::TAccessAttribute::EAllowed,
                 geomap::Way::TAccessAttribute::EAllowed,
                 geomap::Way::TAccessAttribute::EAllowed),
         mFrom(aFrom),
         mTo(aTo)
        {}

        virtual const geomap::DirectedEdge::Nodes nodes() const {
            return geomap::DirectedEdge::Nodes();
        }

        virtual unsigned long distanceMts() const {
            return mDistanceMts;
        }

        virtual const geomap::Edge::EdgeNode& from() const {
            if (mFrom) {
                return *mFrom;
            }
            return *(this->mEdge.from());
        }

        virtual const geomap::Edge::EdgeNode& to() const {
            if (mTo) {
                return *mTo;
            }
            return *(this->mEdge.to());
        }

        virtual geomap::Edge::ClimbPercent climbPercent() const {
            return geomap::Edge::ClimbPercent(0);
        }

        virtual const geomap::Way::TOneDirectionAccessAttributes& accessAttributes() const  {
            return mAccess;
        }

    private:
        unsigned long mDistanceMts;
        const geomap::Way::TOneDirectionAccessAttributes mAccess;
        const geomap::Edge::EdgeNode::ConstHandle mFrom;
        const geomap::Edge::EdgeNode::ConstHandle mTo;
};

BOOST_AUTO_TEST_CASE(routing_profile)
{
    MapFacts& mapFacts = MapFacts::mInstance;
    cost::CompoundCostFunctionProfile testProfile(
        KThisPath+"/../data/test.profile.json",
        cost::AllComponents::construct()
    );

    unsigned long distanceMts = 100000;
    geo::Point from(51.5269359477,-0.155107793999);
    geo::Point to = from.offsetMtsCourse(distanceMts, geo::GeoTypes::deg2Rad*90);
    BOOST_CHECK_EQUAL(from.distanceMts(to), distanceMts);

    const geomap::Edge::DirectedEdge& testEdge = *(mapFacts.Y->edges().front());
    BOOST_CHECK_EQUAL(testEdge.highwayType(), geomap::Map::instance().getMeta().getHighwayTypes().at("path"));
    PretendDirectedEdge edge(
        testEdge,   //over an edge that is very steep and over undesired highwayType
        distanceMts //very long section
    );
    const cost::HighwayCostFactors& hf = static_cast<const cost::HighwayCostFactors&>(
        *testProfile.components().at(cost::HighwayCostFactors::KNodeName)
    );
    BOOST_CHECK_EQUAL(
        hf.highway(testEdge.highwayType()).factor(edge.accessAttributes(), hf.means()),
        cost::CompoundCostFunctionProfile::ComponentBase::KForbiddenFactor
    );
    cost::CompoundCostFunctionFactory<routing::MtsDistanceCostFunction> costFunctionFactoryUnderTest(
        testProfile,
        cost::CompoundCostFunctionFactory<routing::MtsDistanceCostFunction>::ReferencePoint(10000,  10000),
        cost::CompoundCostFunctionFactory<routing::MtsDistanceCostFunction>::ReferencePoint(200000, 50000),
        distanceMts
    );
    routing::CostFunction::UniquePtr costFunctionUnderTest = costFunctionFactoryUnderTest.create(from, to);
    BOOST_CHECK_EQUAL(costFunctionUnderTest->evaluate(routing::RouteHead(geomap::Edge::DirectedEdge::ConstHandle(&edge))), distanceMts * cost::CompoundCostFunctionProfile::ComponentBase::KForbiddenFactor);
}


BOOST_AUTO_TEST_CASE(narrow_searchCone)
{
    cost::CompoundCostFunctionProfile testProfile(
        KThisPath+"/../data/neutral.profile.json",
        cost::AllComponents::construct()
    );
    float smallestFactor = cost::CompoundCostFunctionProfile::FF2I::Multiplier()
        .apply(
            static_cast<const cost::HighwayCostFactors&>(
                *testProfile.components().at(cost::HighwayCostFactors::KNodeName)
            ).mSmallestFactor
        ).factor();

    std::vector<unsigned long> testDistancesMts = {100, 49950, 100000};
    std::vector<unsigned long> coneWidthsMts = {1000,10500, 20000};
    geo::Point from(51.5269359477,-0.155107793999);
    for (unsigned short i = 0; i < testDistancesMts.size(); i++) {
        unsigned long testDistanceMts = testDistancesMts.at(i);
        unsigned long coneWidthMts = coneWidthsMts.at(i);
        geo::Point to = from.offsetMtsCourse(testDistanceMts, geo::GeoTypes::deg2Rad*90);
        BOOST_CHECK_EQUAL(from.distanceMts(to), testDistanceMts);
        cost::NarrowSearchConeCompoundCostFunctionFactory<routing::MtsDistanceCostFunction> costFunctionFactoryUnderTest(
            testProfile,
            cost::NarrowSearchConeCompoundCostFunctionFactory<routing::MtsDistanceCostFunction>::ReferencePoint(testDistancesMts.front(), coneWidthsMts.front()),
            cost::NarrowSearchConeCompoundCostFunctionFactory<routing::MtsDistanceCostFunction>::ReferencePoint(testDistancesMts.back(), coneWidthsMts.back())
        );
        routing::CostFunction::UniquePtr costFunctionUnderTest = costFunctionFactoryUnderTest.create(from, to);

        geo::Point onCourseVertex = from.offsetMtsCourse(testDistanceMts/2, geo::GeoTypes::deg2Rad*90);
        routing::CostFunction::Cost expectedHeuristics = testDistanceMts/2 * smallestFactor;
        BOOST_CHECK_EQUAL(costFunctionUnderTest->heuristics_H(onCourseVertex), expectedHeuristics);
        //lets now move step by step away from onCourse towards offCourse and see what heuristics we get
        unsigned long heightOfTriangleMts = 0;
        float overestimate;
        while (heightOfTriangleMts < coneWidthMts*2) {
            geo::Point semiOffCourseVertex = onCourseVertex.offsetMtsCourse(heightOfTriangleMts, 0.0);
            unsigned long distanceToDestinationMts = semiOffCourseVertex.distanceMts(to);
            overestimate = (float)costFunctionUnderTest->heuristics_H(semiOffCourseVertex)/(distanceToDestinationMts*cost::CompoundCostFunctionProfile::ComponentBase::KNeutralFactor);
            heightOfTriangleMts += coneWidthMts/10;
            if (overestimate > 1) {
                BOOST_CHECK(heightOfTriangleMts > coneWidthMts && heightOfTriangleMts < coneWidthMts*1.6);
                break;
            }
        }
        BOOST_CHECK(overestimate > 1);
    }
}

BOOST_AUTO_TEST_CASE(turn_punishments) {
    MapFacts& mapFacts = MapFacts::mInstance;
    geomap::Edge::EdgeNode prevFrom(geo::Point(51.5269019058,-0.155619559479), geomap::Edge::EdgeNode::ElevationMts());
    geomap::Edge::EdgeNode prevTo(geo::Point(51.5270687778,-0.15479880352), geomap::Edge::EdgeNode::ElevationMts());
    geomap::Edge::EdgeNode pointToTheLeft(geo::Point(51.5272656869,-0.154836354447), geomap::Edge::EdgeNode::ElevationMts());
    geomap::Edge::EdgeNode pointToTheRight(geo::Point(51.526631573,-0.154278454972), geomap::Edge::EdgeNode::ElevationMts());
    const geomap::Edge::DirectedEdge& refForPrevEdge = *(mapFacts.X->edges().at(1));
    PretendDirectedEdge* prevEdge = new PretendDirectedEdge(
        refForPrevEdge,
        100,
        geomap::Edge::EdgeNode::ConstHandle(
            &prevFrom
        ),
        geomap::Edge::EdgeNode::ConstHandle(
            &prevTo
        )
    );
    const geomap::Edge::DirectedEdge& refForPeelingEdges = *(mapFacts.Y->edges().front());
    PretendDirectedEdge* edgeToTheLeft = new PretendDirectedEdge(
        refForPeelingEdges,
        100,
        geomap::Edge::EdgeNode::ConstHandle(
            &prevTo
        ),
        geomap::Edge::EdgeNode::ConstHandle(
            &pointToTheLeft
        )
    );
    PretendDirectedEdge* edgeToTheRight = new PretendDirectedEdge(
        refForPeelingEdges,
        100,
        geomap::Edge::EdgeNode::ConstHandle(
            &prevTo
        ),
        geomap::Edge::EdgeNode::ConstHandle(
            &pointToTheRight
        )
    );
    prevTo.addEdge(PretendDirectedEdge::ConstUniquePtr(prevEdge));
    prevTo.addEdge(PretendDirectedEdge::ConstUniquePtr(edgeToTheLeft));
    prevTo.addEdge(PretendDirectedEdge::ConstUniquePtr(edgeToTheRight));

    routing::RouteHead prevRouteHead(geomap::Edge::DirectedEdge::ConstHandle(prevEdge), nullptr);
    routing::RouteHead leftTurn(geomap::Edge::DirectedEdge::ConstHandle(edgeToTheLeft), &prevRouteHead);
    routing::RouteHead rightTurn(geomap::Edge::DirectedEdge::ConstHandle(edgeToTheRight), &prevRouteHead);
    cost::CompoundCostFunctionProfile testProfile(
        KThisPath+"/turnPunishment.routprofile.json",
        cost::AllComponents::construct()
    );

    cost::CompoundCostFunctionProfile::Calculator::ConstUniqPtr calculator = static_cast<const cost::HighwayCostFactors&>(
        *testProfile.components().at(cost::TurnPunishment::KNodeName)
    ).calculator();
    cost::CompoundCostFunctionProfile::FF2I::Multiplier multiplier;
    routing::CostFunction::Cost distanceMts = 100;
    calculator->apply(leftTurn, multiplier, distanceMts);
    BOOST_CHECK_EQUAL(distanceMts, 105);
    distanceMts = 100;
    calculator->apply(rightTurn, multiplier, distanceMts);
    BOOST_CHECK_EQUAL(distanceMts, 140);
}

BOOST_AUTO_TEST_SUITE_END()

