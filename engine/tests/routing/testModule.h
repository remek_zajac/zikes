/*************************************************************************
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * (C) 2015 Remek Zajac
 *************************************************************************/

#ifndef TESTMODULE_H
#define TESTMODULE_H

#include "tests/utils.h"
#include "tests/maploader.h"

const std::string KThisFile     = std::string(__FILE__);
const std::string KThisPath     = KThisFile.substr(0, KThisFile.find_last_of("/\\"));
const std::string KDataFilesGPB = KThisPath + "/../../build/tests/data/regentsPark";
typedef LoadedMap<&KDataFilesGPB> RegentsPark;


struct MapFacts {
    /*
     * Context of the tests (graph fragment):
     *                     (s)
     *                       .           /
     *                       (V)------(U)
     *                       / (r)      \
     *  |                   /
     * (X)--(N)--(p1)----(Y)
     *  |    |    .     (q)\
     *            .         \
     *           (p)        (Z)
     *
     * (X), (Y) and (Z) are graph vertices, whilst (V) is not, just where the road bends.
     * The test checks that:
     * - Given (p), far enough from vertices to force projection
     *   geomap::Map::instance().getNearestEdgeNode returns (p1) projected onto (X)-(Y)
     * - Given (q), near enough to (Y) to have
     *   geomap::Map::instance().getNearestEdgeNode return (Y)
     * - Given (r), near enough to (V) to have
     *   geomap::Map::instance().getNearestEdgeNode return AdHoc vertex around (V)
     * - Given (s), far enough from (V) to favour projections, but
     *   with no projections available, geomap::Map::instance().getNearestEdgeNode
     *   should settle with (V)
     * It further checks that (p1) is an AdHoc vertex leading to (X) and (Y) respectivelly
     */
    const geo::Point p;
    const geo::Point q;
    const geo::Point r;
    const geo::Point s;
    const geo::Point u;
    const geo::Point z;
    const geo::Point x;
    const geo::Point n;
    const geo::Point node2100667055;

    geomap::Edge::EdgeNode::ConstAutoHandle Y;
    geomap::Edge::EdgeNode::ConstAutoHandle p1;
    geomap::Edge::EdgeNode::ConstAutoHandle V;
    geomap::Edge::EdgeNode::ConstAutoHandle V1;
    geomap::Edge::EdgeNode::ConstAutoHandle U;
    geomap::Edge::EdgeNode::ConstAutoHandle Z;
    geomap::Edge::EdgeNode::ConstAutoHandle X;
    geomap::Edge::EdgeNode::ConstAutoHandle N;
    geomap::Edge::EdgeNode::ConstAutoHandle EdgeNode2100667055;

    MapFacts()
    :p(51.5269706566,-0.154974756432),
     q(51.5270127103,-0.154823479843),
     r(51.5272596431,-0.154814896775),
     s(51.5272661165,-0.154951152992),
     u(51.5274286,-0.1547846),
     z(51.527012,-0.1547206),
     x(51.5269055,-0.1556183),
     n(51.5269873438,-0.155210790825),
     node2100667055(51.5280363,-0.1513382)
    {
        RegentsPark::loaded();
        assertNodeCount();
        V = geomap::Map::NearestProjectedEdgeNodeStrategy(geomap::Map::instance()).find(r);
        std::cout << "Found V  : " << V->toString()  << " with " << V->edges().size() << " egdes" << std::endl;
        Y = geomap::Map::NearestProjectedEdgeNodeStrategy(geomap::Map::instance()).find(q);
        std::cout << "Found Y  : " << Y->toString()  << " with " << Y->edges().size() << " egdes" << std::endl;
        p1 = geomap::Map::NearestProjectedEdgeNodeStrategy(geomap::Map::instance()).find(p);
        std::cout << "Found p1  : " << p1->toString()  << " with " << p1->edges().size() << " egdes" << std::endl;
        V1 = geomap::Map::NearestProjectedEdgeNodeStrategy(geomap::Map::instance()).find(s);
        U  = geomap::Map::NearestProjectedEdgeNodeStrategy(geomap::Map::instance()).find(u);
        Z  = geomap::Map::NearestProjectedEdgeNodeStrategy(geomap::Map::instance()).find(z);
        X  = geomap::Map::NearestProjectedEdgeNodeStrategy(geomap::Map::instance()).find(x);
        N  = geomap::Map::NearestProjectedEdgeNodeStrategy(geomap::Map::instance()).find(n);
        EdgeNode2100667055 = geomap::Map::NearestProjectedEdgeNodeStrategy(geomap::Map::instance()).find(node2100667055);
    }

    ~MapFacts() {
        V.reset();
        Y.reset();
        p1.reset();
        V1.reset();
        U.reset();
        Z.reset();
        X.reset();
        N.reset();
        EdgeNode2100667055.reset();
        assertNodeCount();
    }

    typedef std::vector<geo::Point>     TExpectedEdge;
    typedef std::vector<TExpectedEdge>  TExpectedEdges;

    static const geomap::Edge::DirectedEdge* findEdgeLeadingTo(
            const geomap::Edge::EdgeNode::ConstAutoHandle& edgeNode,
            const geo::Point& to
    ) {
        for (auto it = boost::begin(edgeNode->edges()); it != boost::end(edgeNode->edges()); ++it) {
            const geomap::Edge::DirectedEdge& edge = **it;
            if (edge.to().distanceMts(to) == 0) {
                return &edge;
            }
        }
        return nullptr;
    }

    static void assertNodeCount() {
        unsigned int nodesCount             = RegentsPark::loaded().reader().nodesCount();
        unsigned int edgeNodesCount         = RegentsPark::loaded().reader().edgeNodesCount();
        unsigned int totalNodesInExistence  = geomap::Node::nodesRemaining();
        if (nodesCount + edgeNodesCount != totalNodesInExistence) {
            std::cout << "nodesCount(" << nodesCount
                      << ") + edgeNodesCount(" << edgeNodesCount
                      << (nodesCount + edgeNodesCount)
                      << " does not equal totalNodesInExistence(" << totalNodesInExistence << ")" << std::endl;
            assert(false);
        }
    }

    static const geomap::Edge::DirectedEdge* findEdgeLeadingTo(
            const geomap::Edge::EdgeNode::ConstAutoHandle& edgeNode,
            const geomap::Edge::EdgeNode::ConstAutoHandle& to
    ) {
        for (auto it = boost::begin(edgeNode->edges()); it != boost::end(edgeNode->edges()); ++it) {
            const geomap::Edge::DirectedEdge& edge = **it;
            if (&edge.to() == to.get()) {
                return &edge;
            }
        }
        return nullptr;
    }

    static void verifyEdge(const geomap::Edge::DirectedEdge& edgeToVerify, const TExpectedEdge& expectedPoints, const std::string& extraMessage) {
        int expectedNodeCount = expectedPoints.size();
        auto edgeNodesIt = boost::begin(edgeToVerify.nodes());
        auto expectedPointsIt = boost::begin(expectedPoints);
        for (;
             edgeNodesIt != boost::end(edgeToVerify.nodes()) && expectedPointsIt != boost::end(expectedPoints);
             edgeNodesIt++, expectedPointsIt++, expectedNodeCount--) {
             const geomap::Node& node = **edgeNodesIt;
             const geo::Point& expectedLocation = *expectedPointsIt;
             unsigned int distanceFromExpectedMts = node.distanceMts(expectedLocation);
             bool distanceFromExpectedMtsZero = distanceFromExpectedMts == 0;
#ifndef NDEBUG
             BOOST_CHECK_MESSAGE(
                distanceFromExpectedMtsZero,
                "Edge " + edgeToVerify.toString() + " didn't include the expected milestone " + expectedPointsIt->toString() + " " + extraMessage
             );
#else
             BOOST_CHECK_MESSAGE(
                distanceFromExpectedMtsZero,
                "Edge didn't include the expected milestone " + extraMessage
             );
#endif
        }
        bool expectedNodeCountIsZero = expectedNodeCount == 0;
#ifndef NDEBUG
        BOOST_CHECK_MESSAGE(expectedNodeCountIsZero, "Edge " + edgeToVerify.toString() + " didn't include all of the expected nodes");
#else
        BOOST_CHECK_MESSAGE(expectedNodeCountIsZero, "Edge didn't include all of the expected nodes");
#endif
    }

#ifdef NDEBUG
    static void printEdges(geomap::Edge::EdgeNode::ConstAutoHandle edgeNode) {
        std::cout << edgeNode->toString() << " is found to lead to: "<< std::endl;
        for (auto it = boost::begin(edgeNode->edges()); it != boost::end(edgeNode->edges()); ++it) {
            const geomap::Edge::DirectedEdge& edge = *(it->get());
            std::cout << "  edge: " << std::endl;
            for (auto eit = boost::begin(edge.nodes()); eit != boost::end(edge.nodes()); eit++) {
                std::cout << "    " << (*eit)->toString() << std::endl;
            }
        }
    }
#endif

    static bool verifyRouteAsGpxFile(const routing::Route& aActual, const std::string& aExpected) {
        std::ifstream expectedRouteFileStream(aExpected);
        std::string expectedRouteFileStr((std::istreambuf_iterator<char>(expectedRouteFileStream)),
                                          std::istreambuf_iterator<char>());
        std::stringstream resultRouteStream;
        routing::GpxRoute::write(resultRouteStream, aActual);
        bool result = resultRouteStream.str() == expectedRouteFileStr;
        if (!result) {
            std::cout << "obtained route " << std::endl;
            std::cout << resultRouteStream.str();
            std::cout << " does not equal expected: " << std::endl;
            std::cout << expectedRouteFileStr;
        }
        expectedRouteFileStream.close();
        return result;
    }

    static const CapturedEdges& capturedEdges() {
        return RegentsPark::loaded().capturedEdges();
    }

    static MapFacts mInstance;
};


#endif // TESTMODULE_H
