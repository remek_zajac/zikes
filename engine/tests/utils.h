/*************************************************************************
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * (C) 2015 Remek Zajac
 *************************************************************************/

#ifndef UTILS_H
#define UTILS_H

#include <boost/test/unit_test.hpp>

struct Equal {
    static bool withPrecission(long lhs, long rhs, float precission) {
        return std::abs(rhs-lhs) <= (double)(std::max(lhs,rhs)*precission);
    }

    static bool withPrecission(double lhs, double rhs, float precission) {
        return std::abs(rhs-lhs) <= (double)(std::max(lhs,rhs)*precission);
    }
};


#define BOOST_CHECK_EQUAL_WITH_PRECISSION(rhs, lhs, precission, message) do { \
    std::stringstream ss; \
    float fraction = std::max(lhs,rhs) != 0 ? 1.0*std::min(rhs,lhs)/std::max(lhs,rhs) : std::max(rhs,lhs); \
    ss << rhs <<  " was not equal to " << lhs << " within " << precission << " precission (off by " << 1.0-fraction << "), when: " << message; \
    BOOST_CHECK_MESSAGE(Equal::withPrecission(lhs, rhs, precission), ss.str()); } while( ::boost::test_tools::dummy_cond ) \

#endif // UTILS_H
