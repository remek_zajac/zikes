/*************************************************************************
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * (C) 2016 Remek Zajac
 *************************************************************************/

#include <boost/test/unit_test.hpp>

#include "cost/components/urbanRural/starstrategy.h"
#include "cost/components/urbanRural/CostCapacitor.h"
#include "cost/components/urbanRural.h"

#include "tests/maploader.h"
#include "tests/utils.h"
#include "tests/costedroute.h"

const std::string KThisFile     = std::string(__FILE__);
const std::string KThisPath     = KThisFile.substr(0, KThisFile.find_last_of("/\\"));
const std::string KDataFilesGPB = KThisPath + "/../../build/tests/data/urbanRural";
typedef LoadedMap<&KDataFilesGPB> RuralDolnySlask;


struct Fixture
{
    Fixture() {
        RuralDolnySlask::loaded();
    }
    ~Fixture() {}
};


BOOST_FIXTURE_TEST_SUITE(UrbanRuralFixture, Fixture)


BOOST_AUTO_TEST_CASE(basic_lengths)
{
    BOOST_CHECK_EQUAL(cost::SectionLengthUrbanScore::scoreUrban(cost::SectionLengthUrbanScore::ETypicalUrbanSectionDistanceMts), 100);
    BOOST_CHECK_EQUAL(cost::SectionLengthUrbanScore::scoreUrban(cost::SectionLengthUrbanScore::ETypicalRuralSectionDistanceMts), -100);
    BOOST_CHECK_EQUAL(cost::SectionLengthUrbanScore::scoreUrban(
        cost::SectionLengthUrbanScore::ETypicalUrbanSectionDistanceMts + cost::SectionLengthUrbanScore::EUrbanRuralDelta/2
    ), 0);
}

struct CostFunctionFactory {
    CostFunctionFactory()
    :urbanRuralComponents {{ cost::UrbanRural::KNodeName, std::make_shared<cost::UrbanRural>()}},
     profileStream("{ \"means\" : \"bicycle\", \"ubranFactor\" : 2 }"),
     profile(profileStream, urbanRuralComponents),
     factory(
        profile,
        cost::CompoundCostFunctionFactory<routing::MtsDistanceCostFunction>::ReferencePoint(10000,  10000),
        cost::CompoundCostFunctionFactory<routing::MtsDistanceCostFunction>::ReferencePoint(100000, 20000)
    ){}

    cost::CompoundCostFunctionProfile::NamedComponents urbanRuralComponents;
    std::istringstream profileStream;
    cost::CompoundCostFunctionProfile profile;
    cost::CompoundCostFunctionFactory<routing::MtsDistanceCostFunction> factory;
};


BOOST_AUTO_TEST_CASE(ignore_fake_junctions)
{
    //a fake junction is such where two ways/sections meet ---->----->, but it's only a extension, not a junction
    //There is one such junction between those two:
    geo::Point from(51.1815453,16.7918301);
    geo::Point to(51.1787955767,16.7916335301);

    CostFunctionFactory costFunctionFactory;
    routing::CostFunction::UniquePtr costFunction = costFunctionFactory.factory.create(from, to);
    test::CostedRoute obtainedRoute(*costFunction);
    routing::Request  routingRequest(costFunctionFactory.factory);
    routingRequest
    .setFrom(from)
    .setTo(to)
    .execute(obtainedRoute);
    obtainedRoute.populate();

    BOOST_CHECK_EQUAL(obtainedRoute.size(), 3);
    for (auto& edge : obtainedRoute.edges()) {
        BOOST_CHECK_EQUAL(edge->factor(), cost::UrbanRural::FF2I::NeutralPunishFactor);
    }

    const geomap::DirectedEdge& fakeSection = obtainedRoute.edges()[1]->edge();
    BOOST_CHECK_EQUAL(89, fakeSection.distanceMts());
    BOOST_CHECK_EQUAL(cost::SectionLengthUrbanScore::score(fakeSection), 92); //in its own right it's a strong urban, but it only meets one other edge up front,
    BOOST_CHECK_EQUAL(cost::StarStrategy::score(fakeSection), -61); //so StarStrategy should merge it and turn it well rural
}


BOOST_AUTO_TEST_SUITE_END()
