/*************************************************************************
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * (C) 2016 Remek Zajac
 *************************************************************************/

#ifndef COSTEDROUTE_H
#define COSTEDROUTE_H


namespace test {

    class CostFunctionObserver : public routing::CostFunction {
        public:
            CostFunctionObserver(const routing::CostFunction& aCostFunction)
            :mCostFunction(aCostFunction)
            {}

            virtual Cost evaluate(const routing::RouteHead& aRouteHead) const {
                return mCostFunction.evaluate(aRouteHead);
            }
            virtual Cost heuristics_H(const geo::Point& aForVertex) const {
                return mCostFunction.heuristics_H(aForVertex);
            }

        private:
            const routing::CostFunction& mCostFunction;
    };



    class CostedRoute : public routing::Route {
        public:
            class Head : public routing::RouteHead {
                public:
                    Head(
                        const geomap::DirectedEdge::ConstHandle& aDirectedEdge,
                        const RouteHead* aPrev,
                        const routing::CostFunction& aCostFunction
                    )
                    :routing::RouteHead(aDirectedEdge, aPrev),
                     cost(aCostFunction.evaluate(*this))
                    {}

                    routing::CostFunction::Cost cost;
                    unsigned long distanceMts() const {
                        return edge().distanceMts();
                    }
                    routing::CostFunction::CostFactor factor() const {
                        return cost / distanceMts();
                    }
            };

            CostedRoute(const routing::CostFunction& aCostFunction)
            :mCostFunction(aCostFunction) {}

            void populate() {
                if (mBuffer.size() == 0) {
                    Head* prev = nullptr;
                    for (auto it = begin(); it != end(); it++) {
                        prev = new Head(*it, prev, mCostFunction);
                        mBuffer.emplace_back(prev);
                    }
                }
            }

            Head* head() const {
                return &*mBuffer.back();
            }

            const std::vector<std::unique_ptr<Head> >& edges() const {
                return mBuffer;
            }

            void print() const {
                std::cout << "CostedRoute: " << std::endl;
                auto it = mBuffer.begin();
                std::cout << (*it)->edge().from().toString(true);
                for (; it != mBuffer.end(); it++) {
                    std::cout << ", " << "[" << (*it)->edge().to().latStr() << "," << (*it)->edge().to().lonStr() << "," << (*it)->cost/(*it)->distanceMts() << "]";
                }
                std::cout << std::endl;
            }

        private:
            std::vector<std::unique_ptr<Head> > mBuffer;
            CostFunctionObserver mCostFunction;
    };
};

#endif // COSTEDROUTE_H







