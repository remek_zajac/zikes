
/*************************************************************************
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * (C) 2015 Remek Zajac
 *************************************************************************/
#include <boost/test/unit_test.hpp>

#include "utils/timer.h"
#include "cost/components/all.h"

#include "tests/maploader.h"
#include "tests/utils.h"

const std::string KThisFile     = std::string(__FILE__);
const std::string KThisPath     = KThisFile.substr(0, KThisFile.find_last_of("/\\"));
const std::string KDataFilesGPB = KThisPath + "/../../build/tests/data/swissCottage";
typedef LoadedMap<&KDataFilesGPB> SwissCottage;

struct Fixture
{
    Fixture() {}
    ~Fixture() {}
};


BOOST_FIXTURE_TEST_SUITE(Performance, Fixture)


BOOST_AUTO_TEST_CASE(approx_vs_mts)
{
    SwissCottage::loaded();
    geo::Point from(51.548145, -0.1910664);
    geo::Point to(51.5438101, -0.1595246);

    cost::CompoundCostFunctionProfile testProfile(
        KThisPath+"/../data/test.profile.json",
        cost::AllComponents::construct()
    );
    cost::CompoundCostFunctionFactory<routing::MtsDistanceCostFunction>    mtsCostFunctionFactory(
        testProfile,
        cost::CompoundCostFunctionFactory<routing::MtsDistanceCostFunction>::ReferencePoint(1.0,  10000),
        cost::CompoundCostFunctionFactory<routing::MtsDistanceCostFunction>::ReferencePoint(0.25, 200000)
    );
    cost::CompoundCostFunctionFactory<routing::ApproxDistanceCostFunction> approxCostFunctionFactory(
        testProfile,
        cost::CompoundCostFunctionFactory<routing::ApproxDistanceCostFunction>::ReferencePoint(1.0,  10000),
        cost::CompoundCostFunctionFactory<routing::ApproxDistanceCostFunction>::ReferencePoint(0.25, 200000)
    );

    routing::GpxRoute obtainedMtsRoute;
    routing::Request  mtsRoutingRequest(mtsCostFunctionFactory);
    mtsRoutingRequest
    .setFrom(from)
    .setTo(to)
    .execute(obtainedMtsRoute);

    routing::GpxRoute obtainedApproxRoute;
    routing::Request  approxRoutingRequest(approxCostFunctionFactory);
    approxRoutingRequest
    .setFrom(from)
    .setTo(to)
    .execute(obtainedApproxRoute);

    BOOST_CHECK(obtainedMtsRoute == obtainedApproxRoute);
}

BOOST_AUTO_TEST_CASE(inventory)
{
    SwissCottage::loaded();
    geomap::Edge::EdgeNode::ConstAutoHandle node1193978030 = geomap::Map::NearestEdgeNodeStrategy(geomap::Map::instance()).find(geo::Point(51.5441035907,-0.175169178833));
    BOOST_CHECK(node1193978030->edges().size() == 3);
    BOOST_CHECK(node1193978030->edges()[0]->trafficSignals());
    BOOST_CHECK(!node1193978030->edges()[1]->trafficSignals());
    BOOST_CHECK(!node1193978030->edges()[2]->trafficSignals());
}

BOOST_AUTO_TEST_SUITE_END()
