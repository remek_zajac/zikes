/*************************************************************************
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * (C) 2015 Remek Zajac
 *************************************************************************/
#ifndef MAPLOADER_H
#define MAPLOADER_H

#include <fstream>

#include "readers/reader.h"
#include "cost/CompoundCostFunction.h"

typedef std::vector<geomap::Edge::ConstHandle> CapturedEdges;

template <const std::string* GPB2LOAD>
class LoadedMap
{
    private:

        class MetaProcessor : public reader::Reader::MetaProcessor {
            public:
                virtual void putHighwayTypes(const geomap::MapMeta::HighwayTypes& aHighwayTypes) {
                    geomap::Map::instance().getMeta().put(aHighwayTypes);
                }

                virtual void putCoverageFile(const boost::filesystem::path& aPathToCoverageFile) {
                    geomap::Map::instance().getMeta().put(aPathToCoverageFile.string());
                }
        };

        static void processEdge(const geomap::Edge::ConstHandle& aEdge) {
            mInstance->mCapturedEdges.push_back(aEdge);
            geomap::Map::instance().put(aEdge);
        }

        void load()
        {
            mReader.read(*GPB2LOAD);
            std::cout << "Loading finished, collected: "
                      << mReader.nodesCount() << " nodes, "
                      << mReader.waysCount() << " ways, "
                      << geomap::Map::instance().edgeCount() << " edges"
                      << std::endl << std::flush;
        }

        LoadedMap()
        :mReader(processEdge, mMetaProcessor)
        {}

        bool isNull(const std::string* ptr) { //disable warning
            return ptr == nullptr;
        }

        ~LoadedMap()
        {}


    static LoadedMap* mInstance;
    CapturedEdges mCapturedEdges;
    MetaProcessor mMetaProcessor;
    reader::Reader mReader;

    public:
        static const LoadedMap<GPB2LOAD>& loaded() {
            if (mInstance == nullptr) {
                mInstance = new LoadedMap();
                mInstance->load();
            }
            return *mInstance;
        }

        const CapturedEdges& capturedEdges() const {
            return mCapturedEdges;
        }

        const reader::Reader& reader() const {
            return mReader;
        }
};

template <const std::string* GPB2LOAD>
LoadedMap<GPB2LOAD>* LoadedMap<GPB2LOAD>::mInstance = nullptr;


#endif // MAPLOADER_H
