/*************************************************************************
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * (C) 2014 Remek Zajac
 *************************************************************************/
#ifndef GEOMAP_NODE_H
#define GEOMAP_NODE_H

#include "geo.h"

namespace geomap {

	/*
	 * Node class represents a point on the map with latitude and longitude
	 * coordinates assigned. A Node object can contribute to any map feature, notably,
	 * it can be part of a road trace.
	 */
    class Node : public geo::Point, private boost::noncopyable {

		public:
            typedef zikes::Handle<const geomap::Node> ConstHandle;
			typedef unsigned long Id;

            inline Node(CoordinateType aLat, CoordinateType aLon)
            :geo::Point(aLat, aLon) {
                mNodesRemaining++;
            }

            inline Node(const geo::Point& aFrom)
            :geo::Point(aFrom) {
                mNodesRemaining++;
            }

            virtual ~Node() {
                mNodesRemaining--;
            }

            static unsigned int nodesRemaining() {
                return mNodesRemaining;
            }

        private:
            static unsigned int mNodesRemaining;
	};


};
#endif //GEOMAP_NODE_H
