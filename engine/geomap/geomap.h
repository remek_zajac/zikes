/*************************************************************************
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * (C) 2014 Remek Zajac
 *************************************************************************/
#ifndef GEOMAP_H
#define GEOMAP_H

#include <iostream>
#include <memory>
#include <boost/geometry/geometries/geometries.hpp>

#include "geomap_node.h"
#include "geomap_way.h"
#include "geomap_edge.h"
#include "src/SpatialIndex.h"
#include "src/SpatialSearchStrategies.h"

namespace geomap {

    class MapMeta {

        public:
            typedef std::map<std::string, Way::HighwayType> HighwayTypes;
            typedef std::vector<std::string> CoverageFiles;

            inline void put(const HighwayTypes& aHighwayTypes)
            {
                mHighwayTypes = aHighwayTypes;
            }

            inline void put(const std::string& aCoverageFile)
            {
                mCoverageFiles.push_back(aCoverageFile);
            }

            const HighwayTypes&  getHighwayTypes()  const { return mHighwayTypes; }
            const CoverageFiles& getCoverageFiles() const { return mCoverageFiles; }

        private:
            HighwayTypes    mHighwayTypes;
            CoverageFiles   mCoverageFiles;
    };


	/*
	 * The singleton representing the map, i.e.: the graph of nodes and edges
	 * embodying the navigable road network.
	 *
	 * The main use case it serves then is to faciliate the routing scenario
	 * where the client wishes to navigate from A to B. Map will enable
	 * finding the graph nodes nearest to A and B and expose the routing
	 * algorithm to the relevant section of the graph.
	 */
    class Map : private boost::noncopyable
	{
		public:
            void put(const geomap::Edge::ConstHandle& aEdge);

            MapMeta& getMeta() {
                return mMeta;
            }

            const MapMeta& getMeta() const {
                return mMeta;
            }

            unsigned long      edgeCount() const { return mSpatialIndex.size(); }
            void               report() const;


            /*
             * Finds the nearest existing edge node
             */
            class NearestEdgeNodeStrategy : public geomap::NearestEdgeNodeStrategy {
                public:
                    NearestEdgeNodeStrategy(const Map& aMap, geo::Point::ComparableDistanceType aTolerance = geomap::NearestEdgeNodeStrategy::KDefaultTolerance)
                    :geomap::NearestEdgeNodeStrategy(aMap.mSpatialIndex, aTolerance)
                    {}
            };

            /*
             * Finds the nearest edge node, which can either be existing or created ad-hoc by projecting
             * onto an edge.
             */
            class NearestProjectedEdgeNodeStrategy : public geomap::NearestProjectedEdgeNodeStrategy {
                public:

                    NearestProjectedEdgeNodeStrategy(const Map& aMap, int aToleranceMts = geomap::NearestProjectedEdgeNodeStrategy::KDefaultToleranceMts, geo::Point::ComparableDistanceType aProjectionTolerance=geomap::NearestProjectedEdgeNodeStrategy::KDefaultProjectionTolerance)
                    :geomap::NearestProjectedEdgeNodeStrategy(aMap.mSpatialIndex, aToleranceMts, aProjectionTolerance)
                    {}
            };

            /*
             * Finds the nearest edge node, by executing plan a, and, should that fail, by
             * executing plan b.
             */
            typedef geomap::FallbackEdgeNodeStrategy FallbackEdgeNodeStrategy;

            inline static Map& instance() {
                if (mInstance == nullptr) {
                    mInstance = new Map();
                }
                return *mInstance;
            }

		private:
            SpatialIndex 	mSpatialIndex;           
            static Map* 	mInstance;
            MapMeta         mMeta;
			Map();
	};

    typedef Edge::DirectedEdge DirectedEdge;
};
#endif
