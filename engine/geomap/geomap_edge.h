/*************************************************************************
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * (C) 2014 Remek Zajac
 *************************************************************************/
#ifndef GEOMAP_EDGE_H
#define GEOMAP_EDGE_H

#include <unordered_map>
#include <memory>
#include <ctime>
#include <boost/geometry/geometries/geometries.hpp>
#include <boost/range/any_range.hpp>
#include <boost/noncopyable.hpp>
#include <boost/optional.hpp>

#include "geomap_node.h"
#include "geomap_way.h"

#include "utils/ranges.hpp"
#include "utils/any_iterator/any_iterator.hpp"



namespace geomap {

    /*****************************************************************************
     *
     * Edge is bi-directional edge in a map graph. It is a sequence of Nodes
     * the first and last of which are EdgeNodes that connect 'this' with
     * other Edges. This sequence is typically a subset/segment of the sequence
     * represented by a Way.
     *
     ****************************************************************************/
    class Edge : private boost::noncopyable {

        public:
            typedef std::vector<Node::ConstHandle> Nodes;
            typedef zikes::Handle<const geomap::Edge> ConstHandle;
            typedef geo::GeoTypes::ClimbPercent ClimbPercent;
            typedef unsigned int DistanceMts;
            typedef boost::any_range<
                    const Node::ConstHandle
                  , boost::bidirectional_traversal_tag
                  , const Node::ConstHandle
                  , std::ptrdiff_t
                > NodesRange;
            class EdgeNode; //fwd declaration

            /*****************************************************************************
             *
             * DirectedEdge is (possibly a sub-segment of) an Edge given a direction
             * (from one EdgeNode to another). It is used for graph navigation/traversal.
             * DirectedEdge can encompass an entire underlying Edge or, in case of adhoc (dangling)
             * EdgeNodes (see Map::nearestEdgeNode), it may be a fragent of such an Edge. Either
             * way, each DirectedEdge must have an associated Edge.
             * @see Map::getNearestEdgeNode()
             *
             ****************************************************************************/
            class DirectedEdge : private boost::noncopyable {

                public:
                    typedef NodesRange Nodes;
                    typedef std::unique_ptr<const DirectedEdge> ConstUniquePtr;
                    typedef zikes::Handle<const DirectedEdge> ConstHandle;

                    DirectedEdge(const Edge& aEdge)
                    :mEdge(aEdge)
                    {}

                    virtual ~DirectedEdge()
                    {}

                    Way::HighwayType highwayType() const {
                        return edge().way().highwayType();
                    }

                    /*
                     * Perhaps counter-intuitivelly the existence of traffic signals is kept against
                     * an edge, not a node. This is because traffic lights are not always associated
                     * with a junction and we do not want to look at non-junction nodes when calculating
                     * a route (this would be too intensive).
                     */
                    const bool trafficSignals() const {
                        return edge().trafficSignals();
                    }

                    const Edge& edge() const {
                        return mEdge;
                    }

                    std::string toString(bool includeInbetween=false) const;
                    virtual unsigned long distanceMts() const {
                        return mEdge.distanceMts();
                    }
                    virtual const Nodes nodes() const = 0;
                    virtual const Edge::EdgeNode& from() const  = 0;
                    virtual const Edge::EdgeNode& to() const = 0;
                    virtual Edge::ClimbPercent climbPercent() const= 0;
                    virtual const Way::TOneDirectionAccessAttributes& accessAttributes() const = 0;

                protected:
                    const Edge& mEdge;
            };


            /*****************************************************************************
             *
             * Whilst a Node is simply a contributor to an Edge, EdgeNode is such where multple
             * Edges meet, or more specifically; where Edges end (as there can be an EdgeNode
             * marking an end of a road). Whilst Nodes don't know what edges they contribute to,
             * EdgeNodes do know what Edges they terminate/commence. In turn an EdgeNode represents
             * a vertex in the map graph.
             *
             * EdgeNodes are either there to represent an existing navigable road network,
             * or they can be created in an adhoc manner to provide an entry to that road
             * network.
             * @see Map::getNearestEdgeNode()
             *
             ****************************************************************************/
            class EdgeNode : public Node {

                public:
                    typedef zikes::Handle<EdgeNode>                      Handle;
                    typedef zikes::Handle<const EdgeNode>                ConstHandle;
                    typedef zikes::AutoHandle<const EdgeNode>            ConstAutoHandle;
                    typedef std::vector<DirectedEdge::ConstUniquePtr>    DirectedEdges;
                    typedef boost::optional<geo::GeoTypes::ElevationMts> ElevationMts;

                    inline EdgeNode(const geo::Point& aPoint, ElevationMts aEleMts)
                    :Node(aPoint.lat(), aPoint.lon()),
                     mEleMts(aEleMts) {
#ifdef MUTABLE_THREAD_SLOTS
                        std::memset(mThreadSlots, 0, sizeof(mThreadSlots));
#endif
                    }

                    inline const DirectedEdges& edges() const {
                        return mEdges;
                    }

                    inline void addEdge(DirectedEdge::ConstUniquePtr&& aEdge) {
                        mEdges.push_back(std::move(aEdge));
                        mEdges.shrink_to_fit();
                    }

                    inline const ElevationMts& elevationMts() const {
                        return mEleMts;
                    }

                    std::string toString(bool showEdges=false) const;

#ifdef MUTABLE_THREAD_SLOTS
                    void*& mutableThreadSlot(unsigned short aThreadId) const {
                        assert(aThreadId < MUTABLE_THREAD_SLOTS);
                        return mThreadSlots[aThreadId];
                    }
#endif

                private:
                    DirectedEdges mEdges;
                    ElevationMts  mEleMts;
#ifdef MUTABLE_THREAD_SLOTS
                    mutable void* mThreadSlots[MUTABLE_THREAD_SLOTS];
#endif
            };


        public:
            typedef std::vector<Node::Id> NodeIds;
            typedef boost::geometry::model::linestring<geo::GeoTypes::Point> LineString;

            inline unsigned long distanceMts() const {
                return (unsigned long) mDistanceMts;
            }

            inline const Way& way() const {
                return *mWay;
            }

            inline ClimbPercent climbPercent() const {
                return mClimbPercent;
            }

            const Nodes& inBetweenNodes() const {
                return mInBetweenNodes;
            }

            NodesRange nodes() const;

            const EdgeNode::ConstHandle& from() const {
                return mFrom;
            }

            const EdgeNode::ConstHandle& to() const {
                return mTo;
            }

            const bool trafficSignals() const {
                return mTrafficSignals;
            }

            geo::GeoTypes::Box boundingBox() const;
            LineString lineString() const;

            DirectedEdge::ConstUniquePtr directedEdgeAlong() const;
            DirectedEdge::ConstUniquePtr directedEdgeReversed() const;

            std::string toString(bool aIncludeInBetween=false) const;

            Edge(
                 EdgeNode::Handle& aFrom,
                 EdgeNode::Handle& aTo,
                 Nodes& aInBetweenNodes,
                 const Way::ConstHandle& aWay,
                 DistanceMts aDistanceMts,
                 ClimbPercent aClimbPercent,
                 bool aTrafficSignals
            );

        private:
            EdgeNode::ConstHandle   mFrom;
            EdgeNode::ConstHandle   mTo;
            Nodes                   mInBetweenNodes;
            Way::ConstHandle        mWay;
            DistanceMts             mDistanceMts;
            ClimbPercent            mClimbPercent;
            bool                    mTrafficSignals;
    };


    template <bool reversed>
    class BaseDirectedEdge : public Edge::DirectedEdge {

        public:

            BaseDirectedEdge(
                const Edge& aEdge
            )
            :Edge::DirectedEdge(aEdge)
            {}

            virtual Edge::ClimbPercent climbPercent() const {
                return edge().climbPercent();
            }

            virtual const Way::TOneDirectionAccessAttributes& accessAttributes() const {
                return edge().way().accessAttributes().along();
            }

    };

    template <>
    class BaseDirectedEdge<true> : public Edge::DirectedEdge {

        public:

            BaseDirectedEdge(
                const Edge& aEdge
            )
            :Edge::DirectedEdge(aEdge)
            {}

            virtual Edge::ClimbPercent climbPercent() const {
                return -edge().climbPercent();
            }

            virtual const Way::TOneDirectionAccessAttributes& accessAttributes() const {
                return edge().way().accessAttributes().reverse();
            }

    };

    class ReversedDirectedEdge : public Edge::DirectedEdge {

        public:
            ReversedDirectedEdge(const Edge::DirectedEdge& baseDirectedEdge)
            :Edge::DirectedEdge(baseDirectedEdge.edge()),
             mBaseDirectedEdge(baseDirectedEdge) {}

            ReversedDirectedEdge(const ReversedDirectedEdge& aFrom)
            :Edge::DirectedEdge(aFrom.edge()),
             mBaseDirectedEdge(aFrom.mBaseDirectedEdge) {}

            virtual unsigned long distanceMts() const {
                return mBaseDirectedEdge.distanceMts();
            }
            virtual const Nodes nodes() const {
                return boost::adaptors::reverse(mBaseDirectedEdge.nodes());
            }
            virtual const Edge::EdgeNode& from() const {
                return mBaseDirectedEdge.to();
            }
            virtual const Edge::EdgeNode& to() const {
                return mBaseDirectedEdge.from();
            }
            virtual Edge::ClimbPercent climbPercent() const {
                return -mBaseDirectedEdge.climbPercent();
            }
            virtual const Way::TOneDirectionAccessAttributes& accessAttributes() const {
                return &mBaseDirectedEdge.accessAttributes() == &mBaseDirectedEdge.edge().way().accessAttributes().along() ?
                    mBaseDirectedEdge.edge().way().accessAttributes().reverse() :
                    mBaseDirectedEdge.edge().way().accessAttributes().along();
            }

        private:
            const Edge::DirectedEdge& mBaseDirectedEdge;
    };

};

#endif // GEOMAP_EDGE_H
