/*************************************************************************
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * (C) 2014 Remek Zajac
 *************************************************************************/
#include <iostream>
#include <vector>
#include <limits>
#include <boost/log/trivial.hpp>

#include "SpatialIndex.h"
#include "SpatialSearchStrategies.h"
#include "NearestEdgeNodeAccumulator.h"


#define LOG(level) BOOST_LOG_TRIVIAL(level) << "[SpatialIndex]> "

namespace geomap {

    const geo::Point::ComparableDistanceType NearestEdgeNodeStrategy::KDefaultTolerance =
            geo::Point::comparableDistanceForMts(NearestEdgeNodeStrategy::KDefaultToleranceMts);

    const geo::Point::ComparableDistanceType NearestProjectedEdgeNodeStrategy::KDefaultProjectionTolerance =
            geo::Point::comparableDistanceForMts(NearestProjectedEdgeNodeStrategy::KDefaultProjectionToleranceMts);

    SpatialIndex::SpatialIndex() {
	}

    void SpatialIndex::put(const geomap::Edge::ConstHandle& aEdge)
	{
		SpatialIdxBoxType boundingBox;
		//because we use different coordinate systems (cartesian to index, spherical otherwise)
		//we need to convert
        boost::geometry::convert(aEdge->boundingBox(), boundingBox);
		mContainer.insert(
			RTreeValueType(
				boundingBox,
                aEdge
            )
		);
	}

    Edge::EdgeNode::ConstAutoHandle SpatialIndex::findNearest(const geo::Point& aTo, geo::Point::ComparableDistanceType aTolerance) const {
#ifndef NDEBUG
        LOG(debug) << "::findNearest(" << aTo.toString() << ") >>";
#endif
        SpatialIdxPointType to;
        boost::geometry::convert<geo::Point::GeometryType, SpatialIdxPointType>(aTo, to);

        std::vector<RTreeValueType> queryResult;
        bgi::query(mContainer, bgi::nearest(to, 1), std::back_inserter(queryResult));

        Edge::EdgeNode::ConstAutoHandle nearest;
        if (queryResult.size() > 0) {
            Edge::EdgeNode::ConstAutoHandle nearer = NearestEdgeNodeAccumulator(
                *(queryResult.front().second.get()), aTo, aTolerance
            ).find();
            if (nearer) {
                nearest = nearer;
            }
        }

#ifndef NDEBUG
        if (nearest) {
            LOG(debug) << "::findNearest(" << aTo.toString() << ") << " << nearest->toString();
        } else {
            LOG(debug) << "::findNearest(" << aTo.toString() << ") << none found";
        }
#endif
        return nearest;
    }

    Edge::EdgeNode::ConstAutoHandle SpatialIndex::findProjectedNearest(const geo::Point& aTo, int aToleranceMts, geo::Point::ComparableDistanceType aProjectionTolerance) const {
#ifndef NDEBUG
        LOG(debug) << "::findProjectedNearest(" << aTo.toString() << ") >>";
#endif
        geo::Box interestBoxSrc(aTo);
        interestBoxSrc.inflateByMts(aToleranceMts/2);

        SpatialIdxBoxType interestBox;
        boost::geometry::convert<geo::Box::GeometryType, SpatialIdxBoxType>(interestBoxSrc, interestBox);

        std::vector<RTreeValueType> queryResult;
        bgi::query(mContainer, bgi::intersects(interestBox), std::back_inserter(queryResult));

        Edge::EdgeNode::ConstAutoHandle nearest;
        geo::Point::ComparableDistanceType maxDistance = std::numeric_limits<geo::Point::ComparableDistanceType>::max();

        for (RTreeValueType value : queryResult) {
            Edge::EdgeNode::ConstAutoHandle nearer = NearestEdgeNodeAccumulatorWithProjections(
                *value.second.get(), aTo, maxDistance, aProjectionTolerance
            ).find();
            if (nearer) {
                nearest = nearer;
            }
        }
#ifndef NDEBUG
        if (nearest) {
            LOG(debug) << "::findProjectedNearest(" << aTo.toString() << ") << " << nearest->toString();
        } else {
            LOG(debug) << "::findProjectedNearest(" << aTo.toString() << ") << none found";
        }
#endif
        return nearest;
    }
};
