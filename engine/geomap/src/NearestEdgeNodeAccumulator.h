#ifndef NEARESTEDGENODE_H
#define NEARESTEDGENODE_H

#include "geomap_edge.h"

namespace geomap {

    class NearestEdgeNodeAccumulator {

        public:
            NearestEdgeNodeAccumulator(
                const geomap::Edge& aEdge,
                const geo::Point& aTo,
                geo::Point::ComparableDistanceType& aTolerance
            )
            :mEdge(aEdge),
             mTo(aTo),
             mMaxDistance(aTolerance),
             mBestLocalDistance(std::numeric_limits<geo::Point::ComparableDistanceType>::max()),
             mDistanceFromStartMts(0),
             mBestCandidateIt(aEdge.inBetweenNodes().end())
            {}

            Edge::EdgeNode::ConstAutoHandle find();

        protected:

            bool rate(const geo::Point& aCandidate) {
                geo::Point::ComparableDistanceType cmpDistanceFromTarget = mTo.comparableDistance(aCandidate);
                mBestLocalDistance = std::min(mBestLocalDistance, cmpDistanceFromTarget);
                if (cmpDistanceFromTarget < mMaxDistance) {
                    mMaxDistance = cmpDistanceFromTarget;
                    mBestCandidateDistanceFromStartMts = mDistanceFromStartMts;
                    return true;
                }
                return false;
            }

            virtual void rate(const Edge::EdgeNode::ConstHandle& aEdgeNode) {
                if (rate(*aEdgeNode)) {
                    mBestCandidate = aEdgeNode;
                    mBestCandidateIt = mEdge.inBetweenNodes().end();
                }
            }

            virtual void rate(const Edge::Nodes::const_iterator& aIt) {
                if (rate(**aIt)) {
                    mBestCandidate = mBestNodeCandidate = std::shared_ptr<Edge::EdgeNode>(
                        new Edge::EdgeNode(**aIt, eleMtsForDistanceFromStart(mBestCandidateDistanceFromStartMts))
                    );
                    mBestCandidateIt = aIt;
                }
            }

            geomap::Edge::EdgeNode::ElevationMts eleMtsForDistanceFromStart(unsigned long aDistanceFromStart) {
                geomap::Edge::EdgeNode::ElevationMts eleMts;
                if (mEdge.from()->elevationMts() && mEdge.to()->elevationMts()) {
                    int dEle = *mEdge.to()->elevationMts()-*mEdge.from()->elevationMts();
                    float slope = (float)aDistanceFromStart/mEdge.distanceMts();
                    eleMts = *mEdge.from()->elevationMts() + dEle*slope;
                }
                return eleMts;
            }

            virtual Edge::EdgeNode::ConstAutoHandle finalise();

            void addDirectedEdges(std::shared_ptr<Edge::EdgeNode> aEdgeNode,
                                  const Edge::Nodes::const_iterator& aForwards,
                                  const Edge::Nodes::const_reverse_iterator& aBackwards);

        protected:
            const geomap::Edge&                 mEdge;
            const geo::Point                    mTo;
            geo::Point::ComparableDistanceType& mMaxDistance;
            geo::Point::ComparableDistanceType  mBestLocalDistance;

            Edge::EdgeNode::ConstAutoHandle     mBestCandidate;
            std::shared_ptr<Edge::EdgeNode>     mBestNodeCandidate;
            Node::ConstHandle                   mPrevNode;
            unsigned long                       mDistanceFromStartMts;
            unsigned long                       mBestCandidateDistanceFromStartMts;
            Edge::Nodes::const_iterator         mBestCandidateIt;
    };





    class NearestEdgeNodeAccumulatorWithProjections : public NearestEdgeNodeAccumulator {

        public:
            NearestEdgeNodeAccumulatorWithProjections(
                    const geomap::Edge& aEdge,
                    const geo::Point& aTo,
                    geo::Point::ComparableDistanceType& aTolerance,
                    geo::Point::ComparableDistanceType  aProjectionTolerance
            )
            :NearestEdgeNodeAccumulator(aEdge, aTo, aTolerance),
             mProjectionTolerance(aProjectionTolerance)
            {}

            using NearestEdgeNodeAccumulator::rate;
            virtual void rate(const Edge::EdgeNode::ConstHandle& aEdgeNode) {
                NearestEdgeNodeAccumulator::rate(aEdgeNode);
                if (rateProjection(*aEdgeNode)) {
                    mBestCandidateIt = mEdge.inBetweenNodes().end();
                }
            }

            virtual void rate(const Edge::Nodes::const_iterator& aIt) {
                NearestEdgeNodeAccumulator::rate(aIt);
                if (rateProjection(**aIt)) {
                    mBestCandidateIt = aIt;
                }
            }

            bool rateProjection(const geo::Point& aEndOfSegment) {
                if (mBestLocalDistance > mProjectionTolerance && mPrevNode) {
                    //Only project if the best LOCAL candidate (which now includes both ends of the segment)
                    //is further than mProjectionTolerance
                    geo::Point projection(mTo);
                    if (
                        mTo.projectOnto(
                            geo::GeoTypes::Segment(*mPrevNode, aEndOfSegment),
                            projection
                        ) && rate(projection)
                    ) {
                        mBestCandidateDistanceFromStartMts = mDistanceFromStartMts - projection.distanceMts(aEndOfSegment);
                        mBestCandidate = mBestProjectedCandidate = std::shared_ptr<Edge::EdgeNode>(
                            new Edge::EdgeNode(
                                projection,
                                eleMtsForDistanceFromStart(mBestCandidateDistanceFromStartMts)
                            )
                        );
                        return true;
                    }
                }
                return false;
            }

            virtual Edge::EdgeNode::ConstAutoHandle finalise();

        private:
            const geo::Point::ComparableDistanceType mProjectionTolerance;
            std::shared_ptr<Edge::EdgeNode>          mBestProjectedCandidate;
    };

};


#endif // NEARESTEDGENODE_H
