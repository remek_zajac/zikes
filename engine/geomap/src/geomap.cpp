/*************************************************************************
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * (C) 2014 Remek Zajac
 *************************************************************************/
#include <iostream>
#include <limits>
#include <boost/log/trivial.hpp>
#include <boost/range/join.hpp>

#ifdef STRICT_OSM
#include <stdexcept>
#endif

#include "../../utils/utils.h"
#include "../geomap.h"
#include "assert.h"

#define LOG(level) BOOST_LOG_TRIVIAL(level) << "[Map]> "

namespace geomap {

    unsigned int Node::mNodesRemaining = 0;

    /**********************************************************************
     *
     *
     * Map
     *
     *
     **********************************************************************/
    Map* Map::mInstance = nullptr;

    Map::Map()
    :mSpatialIndex()
    {
    }

    void Map::put(const geomap::Edge::ConstHandle& aEdge)
    {
        mSpatialIndex.put(aEdge);
    }


    template <bool reversed>
    class MirrorEdge : public BaseDirectedEdge<reversed> {

        public:
            MirrorEdge(
                const Edge& aEdge
            )
            :BaseDirectedEdge<reversed>(aEdge)
            {}

            virtual const Edge::EdgeNode& from() const {
                return *(this->mEdge.from());
            }

            virtual const Edge::EdgeNode& to() const {
                return *(this->mEdge.to());
            }

            virtual const Edge::DirectedEdge::Nodes nodes() const {
                return boost::range::join(
                    boost::range::SingleElemRange<const Node::ConstHandle>(
                        zikes::handle_cast<Node::ConstHandle>(this->mEdge.from())
                    ),
                    boost::range::join(
                        this->mEdge.inBetweenNodes(),
                        boost::range::SingleElemRange<const Node::ConstHandle>(
                            zikes::handle_cast<Node::ConstHandle>(this->mEdge.to())
                        )
                    )
                );
            }
    };

    template <>
    class MirrorEdge<true> : public BaseDirectedEdge<true> {

        public:

            MirrorEdge(
                const Edge& aEdge
            )
            :BaseDirectedEdge<true>(aEdge)
            {}

            virtual const Edge::EdgeNode& from() const {
                return *(this->mEdge.to());
            }

            virtual const Edge::EdgeNode& to() const {
                return *(this->mEdge.from());
            }

            virtual const Edge::DirectedEdge::Nodes nodes() const {
                return boost::range::join(
                    boost::range::SingleElemRange<const Node::ConstHandle>(
                        zikes::handle_cast<Node::ConstHandle>(this->mEdge.to())
                    ),
                    boost::range::join(
                        boost::adaptors::reverse(this->mEdge.inBetweenNodes()),
                        boost::range::SingleElemRange<const Node::ConstHandle>(
                            zikes::handle_cast<Node::ConstHandle>(this->mEdge.from())
                        )
                    )
                );
            }
    };



    /**********************************************************************
     *
     *
     * Edge
     *
     *
     **********************************************************************/
    Edge::Edge(
        EdgeNode::Handle& aFrom,
        EdgeNode::Handle& aTo,
        Nodes& aInBetweenNodes,
        const Way::ConstHandle& aWay,
        Edge::DistanceMts aDistanceMts,
        ClimbPercent aClimbPercent,
        bool aTrafficSignals
    )
    :mFrom(aFrom),
     mTo(aTo),
     mInBetweenNodes(aInBetweenNodes),
     mWay(aWay),
     mDistanceMts(aDistanceMts),
     mClimbPercent(aClimbPercent),
     mTrafficSignals(aTrafficSignals)
    {
        aFrom->addEdge(directedEdgeAlong());
        aTo->addEdge(directedEdgeReversed());
    }

    Edge::DirectedEdge::ConstUniquePtr Edge::directedEdgeAlong() const {
        return DirectedEdge::ConstUniquePtr(
           new MirrorEdge<false>(
               *this
           )
       );
    }

    Edge::DirectedEdge::ConstUniquePtr Edge::directedEdgeReversed() const {
        return DirectedEdge::ConstUniquePtr(
            new MirrorEdge<true>(
                *this
            )
        );
    }

    geo::GeoTypes::Box Edge::boundingBox() const
    {
        geo::GeoTypes::Box result;
        boost::geometry::envelope(lineString(), result);
        return result;
    }

    Edge::LineString Edge::lineString() const
    {
        Edge::LineString ls;
        ls.push_back(*to());
        for (Node::ConstHandle node : mInBetweenNodes) {
            ls.push_back(*node);
        }
        ls.push_back(*from());
        return ls;
    }

    std::string Edge::toString(bool aIncludeInBetween) const {
        std::stringstream result;
        result.imbue(std::locale("C"));
        result << "{";
        result << "   \"what\" : \"Edge\",";
        result << "   \"ptr\" : \"" << this;
#ifndef NDEBUG
        result << ",  \"wayId\": "  << way().id();
#endif
        result << ",  \"climb\": "       << (int)climbPercent();
        result << ",  \"distanceMts\": " << distanceMts();
        result << ",  \"nodes\" : [";
        result << from()->toString() << ",";
        if (aIncludeInBetween) {
            for (auto it = boost::begin(mInBetweenNodes); it != boost::end(mInBetweenNodes); it++) {
                result << (*it)->toString() << ",";
            }
        }
        result << to()->toString();
        result << "]}";
        return result.str();
    }


    Edge::NodesRange Edge::nodes() const {
        return boost::range::join(
            boost::range::SingleElemRange<const Node::ConstHandle>(
                zikes::handle_cast<Node::ConstHandle>(mFrom)
            ),
            boost::range::join(
                mInBetweenNodes,
                boost::range::SingleElemRange<const Node::ConstHandle>(
                    zikes::handle_cast<Node::ConstHandle>(mTo)
                )
            )
        );
    }


    /**********************************************************************
     *
     *
     * Edge::DirectedEdge
     *
     *
     **********************************************************************/
    std::string Edge::DirectedEdge::toString(bool aIncludeInBetween) const {
        std::stringstream result;
        result.imbue(std::locale("C"));
        result << "{";
        result << "   \"what\" : \"DirectedEdge\",";
        result << "   \"ptr\" : \"" << this << "\",";
        result << "   \"distanceMts\": " << distanceMts();
        result << ",  \"climb\": " << (int)climbPercent();
#ifndef NDEBUG
        result << ",  \"wayId\": " << edge().way().id();
#endif
        result << ",  \"nodes\": [";
        if (aIncludeInBetween) {
            for (auto it = boost::begin(nodes()); it != boost::end(nodes()); it++) {
                result << (*it)->toString();
                if (boost::next(it) != boost::end(nodes())) {
                    result << ",";
                }
            }
        } else {
            result << from().toString() << "," << to().toString();
        }

        result << "]}";
        return result.str();
    }

    /**********************************************************************
     *
     *
     * Edge::EdgeNode
     *
     *
     **********************************************************************/
    std::string Edge::EdgeNode::toString(bool showEdges) const {
        std::stringstream result;
        result.imbue(std::locale("C"));
        result << "[";
        result << latStr() << "," << lonStr() << ", {";
        std::vector<std::string> decorators;
        if (elevationMts().is_initialized()) {
            decorators.push_back(
                std::string("\"e\":") + std::to_string((int)(*elevationMts()))
            );
        }
        if (showEdges) {
            std::vector<std::string> edgesArray;
            for (auto it = boost::begin(edges()); it != boost::end(edges()); it++) {
                edgesArray.push_back((*it)->toString());
            }
            decorators.push_back(
                utils::join(edgesArray, ",", "\"edges\":")
            );
        }

        result << utils::join(decorators, ",") << "}]";
        return result.str();
    }
};
