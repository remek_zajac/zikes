/*************************************************************************
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * (C) 2014 Remek Zajac
 *************************************************************************/
#ifndef SPATIALINDEX_H
#define SPATIALINDEX_H

#include <boost/geometry/index/rtree.hpp>

#include "../geo.h"
#include "../geomap_node.h"
#include "../geomap_way.h"
#include "../geomap_edge.h"

namespace bgi = boost::geometry::index;

namespace geomap {

	/*
     * The purpose of SpatialIndex is to provide a spatial index container
	 * for the scenario, where the caller will want to find the point on
	 * an existing map graph edge closest to the specified point.
	 *
	 * In order to implement this feature, SpatialIndex stores graph Edges
	 * in boxed indices and will perform the nearest point lookup as follows:
	 * - inflate the given reference point to a box B that expresses the desired
	 *   distance tolerance;
	 * - find all edges whose boxes intersect with B
	 * - attempt to find (and if found, return) an edge Node within aNodeToleranceMts
	 * - find the nearest edge line segment and return the reference projection
	 *   onto it if that projection is within aToleranceMts
	 * - otherwise return nullptr
	 */
	class SpatialIndex {

		public:
			SpatialIndex();

            void put(const geomap::Edge::ConstHandle& aEdge);
			inline unsigned long size() const
			{
				return mContainer.size();
			}

            /*
             * Finds the nearest existing edge node
             */
            Edge::EdgeNode::ConstAutoHandle findNearest(const geo::Point& aTo, geo::Point::ComparableDistanceType aTolerance) const;

            /*
             * Finds the nearest edge node, which can either be existing or created ad-hoc by projecting
             * onto an edge.
             */
            Edge::EdgeNode::ConstAutoHandle findProjectedNearest(const geo::Point& aTo, int aToleranceMts, geo::Point::ComparableDistanceType aProjectionTolerance) const;


		private:
			//cs::geographic appears to be buggy (http://lists.boost.org/geometry/2014/01/2752.php),
			//shouldn't matter for tree ballancing:
			typedef boost::geometry::model::point<geo::Point::CoordinateType,
                                        2, boost::geometry::cs::cartesian>   SpatialIdxPointType;
            typedef boost::geometry::model::box<SpatialIdxPointType> 		 SpatialIdxBoxType;
            typedef std::pair<SpatialIdxBoxType, geomap::Edge::ConstHandle>  RTreeValueType;
            typedef bgi::rtree<RTreeValueType, bgi::rstar<64> > 			 RTreeType;
			RTreeType mContainer;
	};
};
#endif
