/*************************************************************************
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * (C) 2015 Remek Zajac
 *************************************************************************/

#include <boost/log/trivial.hpp>
#include <boost/range/join.hpp>

#include "NearestEdgeNodeAccumulator.h"

#define LOG(level) BOOST_LOG_TRIVIAL(level) << "[Map]> "

//this test still not working: http://localhost:8087/route?milestones=51.5109099445,-0.156728821898,51.5108189497,-0.157712940475&profile=test

namespace geomap {

    template <bool reversed>
    class AdHocEdge : public BaseDirectedEdge<reversed> {

        public:

            AdHocEdge(
                const Edge::DirectedEdge::Nodes& aNodeRange,
                Edge::DistanceMts aDistanceMts,
                const Edge& aEdge
            )
            :BaseDirectedEdge<reversed>(aEdge),
             mNodeRange(aNodeRange),
             mDistanceMts(aDistanceMts)
            {}

            virtual const Edge::DirectedEdge::Nodes nodes() const {
                return mNodeRange;
            }

            virtual const Edge::EdgeNode& from() const {
                return static_cast<const Edge::EdgeNode&>(**boost::begin(mNodeRange));
            }

            virtual const Edge::EdgeNode& to() const {
                return static_cast<const Edge::EdgeNode&>(**boost::rbegin(mNodeRange));
            }

            virtual unsigned long distanceMts() const {
                return (unsigned long) mDistanceMts;
            }


        private:
            const Edge::DirectedEdge::Nodes mNodeRange;
            Edge::DistanceMts               mDistanceMts;
    };

    Edge::EdgeNode::ConstAutoHandle NearestEdgeNodeAccumulator::find() {
        rate(mEdge.from());
        mPrevNode = zikes::handle_cast<Node::ConstHandle>(mEdge.from());
        for (Edge::Nodes::const_iterator it = mEdge.inBetweenNodes().begin(); it != mEdge.inBetweenNodes().end(); it++)
        {
            mDistanceFromStartMts += mPrevNode->distanceMts(**it);
            rate(it);
            mPrevNode = *it;
        }
        mDistanceFromStartMts += mPrevNode->distanceMts(*mEdge.to());
        rate(mEdge.to());
        return finalise();
    }

    template <class SinglePassRange>
    const Edge::DirectedEdge::Nodes join(const Edge::EdgeNode::ConstAutoHandle& aFrom,
                                         const SinglePassRange inBetween,
                                         const Edge::EdgeNode::ConstHandle& aTo) {
        if (boost::begin(inBetween) == boost::end(inBetween)) {
            return boost::range::join(
                boost::range::SingleElemRange<const Node::ConstHandle>(
                    zikes::handle_cast<Node::ConstHandle>(aFrom)
                ),
                boost::range::SingleElemRange<const Node::ConstHandle>(
                    zikes::handle_cast<Node::ConstHandle>(aTo)
                )
            );
        }
        return boost::range::join(
            boost::range::SingleElemRange<const Node::ConstHandle>(
                zikes::handle_cast<Node::ConstHandle>(aFrom)
            ),
            boost::range::join(
                inBetween,
                boost::range::SingleElemRange<const Node::ConstHandle>(
                    zikes::handle_cast<Node::ConstHandle>(aTo)
                )
            )
        );
    }

    void NearestEdgeNodeAccumulator::addDirectedEdges(
            std::shared_ptr<Edge::EdgeNode> aEdgeNode,
            const Edge::Nodes::const_iterator& aForwards,
            const Edge::Nodes::const_reverse_iterator& aBackwards
    ) {
        aEdgeNode->addEdge(
            Edge::DirectedEdge::ConstUniquePtr(
                new AdHocEdge<false>(
                    join(
                        mBestCandidate,
                        std::pair<Edge::Nodes::const_iterator, Edge::Nodes::const_iterator>(
                            aForwards, mEdge.inBetweenNodes().end()
                        ),
                        mEdge.to()
                    ),
                    mEdge.distanceMts() - mBestCandidateDistanceFromStartMts,
                    mEdge
               )
            )
        );
        aEdgeNode->addEdge(
            Edge::DirectedEdge::ConstUniquePtr(
                new AdHocEdge<true>(
                    join(
                        mBestCandidate,
                        std::pair<Edge::Nodes::const_reverse_iterator, Edge::Nodes::const_reverse_iterator>(
                            aBackwards, mEdge.inBetweenNodes().rend()
                        ),
                        mEdge.from()
                   ),
                   mBestCandidateDistanceFromStartMts,
                   mEdge
               )
            )
        );
    }

    Edge::EdgeNode::ConstAutoHandle NearestEdgeNodeAccumulator::finalise() {
        if (mBestCandidate && mBestCandidate == mBestNodeCandidate) {
            addDirectedEdges(mBestNodeCandidate, boost::next(mBestCandidateIt), Edge::Nodes::const_reverse_iterator(mBestCandidateIt));
#ifndef NDEBUG
            LOG(debug) << "   > exiting node: " << mBestCandidate->toString() << " at distance: " << mBestCandidate->distanceMts(mTo) << "m from" << mEdge.toString();
#endif
        }
#ifndef NDEBUG
        else if (mBestCandidate)
        {
            LOG(debug) << "   > existing edge node " << mBestCandidate->toString() << " at distance: " << mBestCandidate->distanceMts(mTo) << "m from" << mEdge.toString();
        }
        else
        {
            LOG(debug) << "   > not found" << mEdge.toString();
        }
#endif
        return mBestCandidate;
    }


    Edge::EdgeNode::ConstAutoHandle NearestEdgeNodeAccumulatorWithProjections::finalise() {
        if (mBestCandidate && mBestCandidate == mBestProjectedCandidate) {
            addDirectedEdges(mBestProjectedCandidate, mBestCandidateIt, Edge::Nodes::const_reverse_iterator(mBestCandidateIt));
#ifndef NDEBUG
            LOG(debug) << "   > projected node: " << mBestCandidate->toString() << " at distance: " << mBestCandidate->distanceMts(mTo) << "m from" << mEdge.toString();
#endif
            return mBestCandidate;
        }
        return NearestEdgeNodeAccumulator::finalise();
    }
}
