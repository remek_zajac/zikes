/*************************************************************************
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * (C) 2016 Remek Zajac
 *************************************************************************/
#ifndef SPATIALSEARCHSTRATEGIES_H
#define SPATIALSEARCHSTRATEGIES_H

#include "SpatialIndex.h"

namespace geomap {

    /*
     * Interface for strategies that can find an EdgeNode within the spatial index 'this' holds
     */
    class FindEdgeNodeStrategy {
        public:
            virtual Edge::EdgeNode::ConstAutoHandle find(const geo::Point& aTo) const = 0;
    };

    class FindEdgeNodeStrategyBase : public FindEdgeNodeStrategy {
        public:
            FindEdgeNodeStrategyBase(const SpatialIndex& aSpatialIndex)
            :mSpatialIndex(aSpatialIndex)
            {}
        protected:
            const SpatialIndex& mSpatialIndex;
    };

    /*
     * Finds the nearest existing edge node
     */
    class NearestEdgeNodeStrategy : public FindEdgeNodeStrategyBase {
        public:
            const static unsigned int KDefaultToleranceMts = 1200;
            const static geo::Point::ComparableDistanceType KDefaultTolerance;
            NearestEdgeNodeStrategy(const SpatialIndex& aSpatialIndex, geo::Point::ComparableDistanceType aTolerance = KDefaultTolerance)
            :FindEdgeNodeStrategyBase(aSpatialIndex),
             mTolerance(aTolerance)
            {}

            virtual Edge::EdgeNode::ConstAutoHandle find(const geo::Point& aTo) const {
                return NearestEdgeNodeStrategy::find(aTo, mSpatialIndex, mTolerance);
            }

            inline static Edge::EdgeNode::ConstAutoHandle find(const geo::Point& aTo, const SpatialIndex& aSpatialIndex, geo::Point::ComparableDistanceType aTolerance = KDefaultTolerance) {
                return aSpatialIndex.findNearest(aTo, aTolerance);
            }

        protected:
            geo::Point::ComparableDistanceType mTolerance;
    };

    /*
     * Finds the nearest edge node, which can either be existing or created ad-hoc by projecting
     * onto an edge.
     */
    class NearestProjectedEdgeNodeStrategy : public NearestEdgeNodeStrategy {
            static const unsigned long KDefaultProjectionToleranceMts = 10;
        public:
            const static unsigned int KDefaultToleranceMts = 100;
            static const geo::Point::ComparableDistanceType KDefaultProjectionTolerance;

            NearestProjectedEdgeNodeStrategy(const SpatialIndex& aSpatialIndex, int aToleranceMts = KDefaultToleranceMts, geo::Point::ComparableDistanceType aProjectionTolerance=KDefaultProjectionTolerance)
            :NearestEdgeNodeStrategy(aSpatialIndex, aToleranceMts),
             mProjectionTolerance(aProjectionTolerance)
            {}

            virtual Edge::EdgeNode::ConstAutoHandle find(const geo::Point& aTo) const {
                return NearestProjectedEdgeNodeStrategy::find(aTo, mSpatialIndex, mTolerance, mProjectionTolerance);
            }

            inline static Edge::EdgeNode::ConstAutoHandle find(const geo::Point& aTo, const SpatialIndex& aSpatialIndex, int aToleranceMts = KDefaultToleranceMts, geo::Point::ComparableDistanceType aProjectionTolerance=KDefaultProjectionTolerance) {
                return aSpatialIndex.findProjectedNearest(aTo, aToleranceMts, aProjectionTolerance);
            }

        private:
            geo::Point::ComparableDistanceType mProjectionTolerance;
    };

    /*
     * Finds the nearest edge node, by executing plan a, and, should that fail, by
     * executing plan b.
     */
    class FallbackEdgeNodeStrategy : public FindEdgeNodeStrategy {
        public:
            FallbackEdgeNodeStrategy(const FindEdgeNodeStrategy& aPlanA, const FindEdgeNodeStrategy& aPlanB)
            :mPlanA(aPlanA),
             mPlanB(aPlanB)
            {}

            virtual Edge::EdgeNode::ConstAutoHandle find(const geo::Point& aTo) const {
                Edge::EdgeNode::ConstAutoHandle result = mPlanA.find(aTo);
                if (result) {
                    return result;
                }
                result = mPlanB.find(aTo);
                return result;
            }

        private:
            const FindEdgeNodeStrategy& mPlanA;
            const FindEdgeNodeStrategy& mPlanB;
    };

}
#endif // SPATIALSEARCHSTRATEGIES_H
