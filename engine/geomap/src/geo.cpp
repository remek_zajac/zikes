/*************************************************************************
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * (C) 2014 Remek Zajac
 *************************************************************************/
#include <iostream>
#include <assert.h>

#include "../geo.h"

namespace geo {

    Box& Box::inflateByPercent(unsigned int aInflateByPercent) {
        //TODO
        assert(false);
        return *this;
    }

    Box& Box::normalise() {
        //TODO - make sure the min_corner is north west and max corner is south east!
        return *this;
    }

    Box& Box::inflateByMts(unsigned int aInflateByMts) {
        //this may be a bit naive, but let's check how much one degree is worth
        //at this lattiude
        Point c = centre();
        Point min = min_corner();
        Point max = max_corner();

        Point degreeAwayLat(c.lat()+0.5,c.lon());
        Point degreeAwayLon(c.lat(),c.lon()+0.5);
        double degreesPerMetreLat = 0.5/c.distanceMts(degreeAwayLat);
        double degreesPerMetreLon = 0.5/c.distanceMts(degreeAwayLon);

        min.offset(-1.0*degreesPerMetreLat*aInflateByMts,-1.0*degreesPerMetreLon*aInflateByMts);
        max.offset(     degreesPerMetreLat*aInflateByMts,     degreesPerMetreLon*aInflateByMts);
        min_corner() = min;
        max_corner() = max;
        return *this;
    }

    GeoTypes::Point Box::centre() const {
        return GeoTypes::Point(
            (max_corner().get<0>() + min_corner().get<0>())/2,
            (max_corner().get<1>() + min_corner().get<1>())/2
        );
    }

    static Point refPoint(51.5269359477,-0.155107793999); //somewhere in London
    static const unsigned int refDistanceMts    = 100;
    static const unsigned int refDistanceApprox = refPoint.approxEuclidianDistance(refPoint.offsetMtsCourse(refDistanceMts,0.0));
    const float Point::Mts2Approx = 1.0*refDistanceApprox/refDistanceMts;
    const float Point::Approx2Mts = 1.0/Point::Mts2Approx;
};
