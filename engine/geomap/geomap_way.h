/*************************************************************************
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * (C) 2014 Remek Zajac
 *************************************************************************/

#ifndef GEOMAP_WAY_H
#define GEOMAP_WAY_H

namespace geomap {

    /*
     * Way represents a sequence of nodes (or sequence of Edges) that
     * share common attributes (streetname, surface, oneway regime, accesability)
     */
    class Way : private boost::noncopyable  {

        public:
            typedef zikes::Handle<const geomap::Way> ConstHandle;
            typedef unsigned long Id;
            typedef unsigned short HighwayType;

            enum TAccessAttribute {
                EAllowed = 0,
                EDesignated = 1,
                EForbidden = 2
            };

            enum TMeans {
                ECar        = 0,
                EBicycle    = 1,
                EPedestrian = 2
            };



            struct TOneDirectionAccessAttributes {

                TOneDirectionAccessAttributes(TAccessAttribute aCar, TAccessAttribute aBicycle, TAccessAttribute aPedestrian)
                :mAccess((aPedestrian << 4) + (aBicycle << 2) + aCar)
                {}

                TAccessAttribute access(TMeans aMeans) const {
                    return (TAccessAttribute)((mAccess >> (aMeans*2)) & 0b11);
                }

            private:
                unsigned char mAccess;
            };



            struct TCombinedAccessAttributes {

                enum {
                    EAlong   = 0,
                    EReverse = 1
                };

                TCombinedAccessAttributes(
                    const TOneDirectionAccessAttributes& aAlong,
                    const TOneDirectionAccessAttributes& aReverse
                ):mAlong(aAlong),
                  mReverse(aReverse)
                {}

                inline const TOneDirectionAccessAttributes& along() const {
                    return mAlong;
                }

                inline const TOneDirectionAccessAttributes& reverse() const {
                    return mReverse;
                }

                private:
                    TOneDirectionAccessAttributes mAlong;
                    TOneDirectionAccessAttributes mReverse;
            };

            inline Way(
                HighwayType aHighway,
                const TCombinedAccessAttributes& aAccessAttribute,
                bool aLeftHandSideTraffic
#ifndef NDEBUG
                ,Way::Id aId
#endif
            )
            :mHighway(aHighway),
             mAccessAttributes(aAccessAttribute),
             mLeftHandSideTraffic(aLeftHandSideTraffic)
#ifndef NDEBUG
             ,mId(aId)
#endif
            {}


#ifndef NDEBUG
            Id id() const { return mId; }
#endif
            inline HighwayType                      highwayType() const         { return mHighway; }
            inline const TCombinedAccessAttributes& accessAttributes() const    { return mAccessAttributes; }
            inline bool                             leftHandSideTraffic() const { return mLeftHandSideTraffic; }

        private:
            HighwayType               mHighway;
            TCombinedAccessAttributes mAccessAttributes;
            bool                      mLeftHandSideTraffic;
#ifndef NDEBUG
            Way::Id      mId;
#endif
    };
}

#endif //GEOMAP_WAY_H
