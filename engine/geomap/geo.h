/*************************************************************************
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * (C) 2014 Remek Zajac
 *************************************************************************/
#ifndef GEO_H
#define GEO_H

#include <limits>

#include <boost/geometry.hpp>
#include "utils/handle.h"

#ifndef COORDINATE_TYPE
#define COORDINATE_TYPE double
#endif

namespace geo {

    namespace GeoTypes {
            typedef COORDINATE_TYPE CoordinateType;
            typedef boost::geometry::cs::spherical_equatorial<boost::geometry::degree>   CoordinateSystem;
            typedef boost::geometry::model::point<CoordinateType, 2, CoordinateSystem>   Point;
            typedef Point                                                                Vector;
            typedef boost::geometry::model::box<Point>                                   Box;
            typedef boost::geometry::model::segment<Point>                               Segment;
            typedef short                                                                ElevationMts;
            typedef signed char                                                          ClimbPercent;
            static const double deg2Rad = boost::geometry::math::d2r<double>();
            static const double rad2Deg = boost::geometry::math::r2d<double>();
            static const long EARTH_RADIUS_MTS = 6371000;
    }

    class Box   : public GeoTypes::Box {

        public:
            typedef GeoTypes::Box GeometryType;

            inline Box(GeoTypes::Point const & aMinCorner, GeoTypes::Point const & aMaxCorner)
            :GeoTypes::Box(aMinCorner, aMaxCorner)
            {
                normalise();
            }

            inline Box(GeoTypes::Point const & aSole)
            :GeoTypes::Box(aSole, aSole)
            {}

            Box& inflateByPercent(unsigned int aInflateByPercent);
            Box& inflateByMts(unsigned int aInflateByMts);
            GeoTypes::Point centre() const;
            Box& normalise();
    };


    class Point : public GeoTypes::Point {

        public:
            typedef GeoTypes::Point GeometryType;
            typedef GeoTypes::CoordinateType CoordinateType;
            typedef boost::geometry::default_distance_result<GeoTypes::Point, GeoTypes::Point>::type ComparableDistanceType;


            static const int NONE = std::numeric_limits<int>::min();
            static const unsigned int PRECISSION = 7;
            static const float Mts2Approx;
            static const float Approx2Mts;

        public:
            inline Point(CoordinateType aLat, CoordinateType aLon)
            :GeoTypes::Point(aLon, aLat)
            {}

            inline Point(const GeoTypes::Point& aFrom)
            :GeoTypes::Point(aFrom)
            {}

            inline CoordinateType lat() const {
                //this is a bit confusing, but the mnemonic latlon is mostly implemented
                //with _lonlat_ (as in: x and y)
                return get<1>();
            }

            inline CoordinateType lon() const {
                return get<0>();
            }

            inline CoordinateType latRad() const {
                return lat() * GeoTypes::deg2Rad;
            }

            inline CoordinateType lonRad() const {
                return lon() * GeoTypes::deg2Rad;
            }

            inline Point& set(CoordinateType aLat, CoordinateType aLon) {
                GeoTypes::Point::set<1>(aLat);
                GeoTypes::Point::set<0>(aLon);
                return *this;
            }

            inline Point& setRad(CoordinateType aLat, CoordinateType aLon) {
                set(aLat * GeoTypes::rad2Deg, aLon * GeoTypes::rad2Deg);
                return *this;
            }

            inline Point& offset(CoordinateType aLatOffset, CoordinateType aLonOffset) {
                return set(lat()+aLatOffset, lon()+aLonOffset);
            }

            inline Point offsetMtsCourse(unsigned long aDistanceMts, double aCourseRad) const {
                unsigned long earthPerimeter = 2 * boost::math::constants::pi<double>() * GeoTypes::EARTH_RADIUS_MTS;
                double distance = 2.0 * boost::math::constants::pi<double>() * aDistanceMts / earthPerimeter;
                double srcLat = latRad();
                double srctLon = lonRad();

                // http://williams.best.vwh.net/avform.htm#LL
                double dstLat = asin(sin(srcLat)*cos(distance)+cos(srcLat)*sin(distance)*cos(-aCourseRad));
                double dlon = atan2(sin(-aCourseRad)*sin(distance)*cos(srcLat),cos(distance)-sin(srcLat)*sin(dstLat));
                double dstLon = srctLon - dlon;

                return Point(dstLat * GeoTypes::rad2Deg, dstLon * GeoTypes::rad2Deg);
            }

            static inline ComparableDistanceType comparableDistanceForMts(unsigned long aMts)
            {
                Point a = Point(0.0,0.0);
                Point b = a.offsetMtsCourse(aMts, 0);
                return a.comparableDistance(b);
            }

            inline Point* projectOnto(const GeoTypes::Segment& aLineSegment, Point& aResult) const {
                GeoTypes::Vector dst = aLineSegment.second;
                GeoTypes::Vector src = *this;
                subtract_point(dst, aLineSegment.first);
                subtract_point(src, aLineSegment.first);
                CoordinateType dstLengthSqrd = pow(dst.get<1>(),2) + pow(dst.get<0>(),2);
                CoordinateType projectionRatio = dot_product(src, dst)/dstLengthSqrd;
                if ( !(projectionRatio < 0 || projectionRatio > 1)) {
                    aResult.set(
                        aLineSegment.first.get<1>() + projectionRatio*(aLineSegment.second.get<1>()-aLineSegment.first.get<1>()),
                        aLineSegment.first.get<0>() + projectionRatio*(aLineSegment.second.get<0>()-aLineSegment.first.get<0>())
                    );
                    return &aResult;
                }
                return nullptr;
            }

            inline unsigned long distanceMts(const Point& aFrom) const {
                return boost::geometry::distance<GeoTypes::Point,GeoTypes::Point>(*this, aFrom) * GeoTypes::EARTH_RADIUS_MTS;
            }

            inline ComparableDistanceType comparableDistance(const Point& aFrom) const {
                return boost::geometry::comparable_distance<GeoTypes::Point,GeoTypes::Point>(*this, aFrom);
            }

            inline unsigned long approxDistanceMts(const Point& aFrom) const {
                return approxEuclidianDistance(aFrom) * Approx2Mts;
            }

            inline bool operator==(const Point& aOther) const {
                return lat() == aOther.lat() && lon() == aOther.lon();
            }

            inline bool operator!=(const Point& aOther) const {
                return !(*this == aOther);
            }

            inline unsigned long approxEuclidianDistance(const Point& aFrom) const
            {
               unsigned long dLon = 0x1000000 * fabs(lon() - aFrom.lon()) * cos(GeoTypes::deg2Rad*(aFrom.lat()+lat())/2);
               unsigned long dLat = 0x1000000 * fabs(lat() - aFrom.lat());
               unsigned long min, max, approx;

               if ( dLon < dLat )
               {
                  min = dLon;
                  max = dLat;
               } else {
                  min = dLat;
                  max = dLon;
               }

               approx = ( max * 1007 ) + ( min * 441 );
               if ( max < ( min << 4 ))
                  approx -= ( max * 40 );

               return (( approx + 512 ) >> 10 );
            }

            inline std::string latStr() const {
                std::stringstream result;
                result.setf(std::ios::fixed, std::ios::floatfield);
                result.precision(PRECISSION);
                result << lat();
                return result.str();
            }

            inline std::string lonStr() const {
                std::stringstream result;
                result.setf(std::ios::fixed, std::ios::floatfield);
                result.precision(PRECISSION);
                result << lon();
                return result.str();
            }

            inline virtual std::string toString() const {
                std::stringstream result;
                result << "[" << latStr() << "," << lonStr() << "]";
                return result.str();
            }
    };

    /*
     * Vector with operations assuming it's operating in an euclidian coordinate system,
     * which (WARNING) geographical coordinates are not!
     */
    class EuclidianVector : public Point {
        public:
            EuclidianVector(const Point& aFrom)
            :Point(aFrom)
            {}

            EuclidianVector(CoordinateType aLat, CoordinateType aLon)
            :Point(aLat, aLon)
            {}

            EuclidianVector(const Point& aFrom, const Point& aTo)
            :Point(aTo.lat() - aFrom.lat(), aTo.lon() - aFrom.lon())
            {}

            //In 2D space denotes whether the other vector is to the left or right of 'this'
            inline float cross(const EuclidianVector& aOther) {
                return (aOther.lon() * lat()) - (aOther.lat() * lon());
            }

            inline float dot(const EuclidianVector& aOther) {
                return (lon() * aOther.lon()) + (lat() * aOther.lat());
            }

            inline EuclidianVector add(const Point& aOther) const {
                return EuclidianVector(lat() + aOther.lat(), lon() + aOther.lon());
            }

            inline CoordinateType lengthSquared() const {
                return pow(lon(),2) + pow(lat(),2);
            }

            inline CoordinateType length() const {
                return sqrt(lengthSquared());
            }

            /*
             * Fast, no-trigonometry bearing approximation
             * see: http://www.freesteel.co.uk/wpblog/2009/06/05/encoding-2d-angles-without-trigonometry/
             * @returns bearing of 'this'
             *   0.0 - north
             *   1.0 - east
             *   2.0 - south
             *   3.0 - west
             *   4.0 - north
             */
            inline float diamondBearing() const {
                CoordinateType _lat = lat() * 2; //lat has half the resolution as lon (-90..90 vs -180..180)
                CoordinateType _lon = lon();
                if (_lon >= 0) {
                    return (_lat >= 0 ? _lon/(_lat+_lon) : 1.0-_lat/(-_lat+_lon));
                } else {
                    return (_lat < 0 ? 2.0-_lon/(-_lat-_lon) : 3.0+_lat/(_lat-_lon));
                }
            }

            /*
             * Fast, no-trigonometry angle approximation
             * see: diamondBearing()
             * @returns angle between 'this' and the given vector
             *   -2.0 - aOther on the left
             *    ...
             *    2.0 - aOther on the right
             */
            inline float diamondAngle(const EuclidianVector& aOther) const {
                float angle = aOther.diamondBearing() - diamondBearing();
                // normalise
                angle =  fmod(fmod(angle, 4) + 4, 4);
                if (angle > 2) {
                    angle -= 4;
                }
                return angle;
            }
    };


    class PointWithEle : public Point
    {
        public:
            inline PointWithEle(CoordinateType aLat, CoordinateType aLon, GeoTypes::ElevationMts aEleMts)
            :Point(aLat, aLon),
             mEleMts(aEleMts)
            {}

            inline PointWithEle(const PointWithEle& aFrom)
            :Point(aFrom),
             mEleMts(aFrom.mEleMts)
            {}

            inline GeoTypes::ElevationMts eleMts() const
            {
                return mEleMts;
            }

        private:
            const GeoTypes::ElevationMts mEleMts;
    };
};

#endif
