/*************************************************************************
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * (C) 2015 Remek Zajac
 *************************************************************************/
#include <map>
#include <algorithm>
#include <cctype>

#include <boost/property_tree/json_parser.hpp>
#include <boost/foreach.hpp>
#include <boost/log/trivial.hpp>

#include "../CompoundCostFunctionProfile.h"
#include "../components/highwaycostfactors.h"
#include "geomap/geomap.h"

#define LOG(level) BOOST_LOG_TRIVIAL(level) << "[RoutingProfile]> "

namespace cost {

    const std::string KBicycle        = "bicycle";
    const std::string KCar            = "car";
    const std::string KPedestrian     = "pedestrian";
    const std::string KMeans          = "means";
    const std::string KDescription    = "description";

    routing::CostFunction::CostFactor CompoundCostFunctionProfile::ComponentBase::getOptionalFactor(
            const boost::optional<const boost::property_tree::ptree&> aPath,
            const std::string& aFactorName,
            const TDefaultsDictionary& aDefaults,
            const boost::optional<const TKeywordsMap>& aKeywords,
            const std::string& aFaultyPath) {
        if (aPath.is_initialized()) {
            const boost::optional<const boost::property_tree::ptree&> optionalChild = aPath->get_child_optional(aFactorName);
            if (optionalChild) {
                return float2CostFactor(
                    optionalChild->get_value_optional<std::string>(),
                    aFaultyPath,
                    aKeywords
                );
            }
        }
        assert(aDefaults.find(aFactorName) != boost::end(aDefaults));
        return float2CostFactor(
            aDefaults.at(aFactorName),
            aFaultyPath,
            aKeywords
        );
    }

    routing::CostFunction::CostFactor CompoundCostFunctionProfile::ComponentBase::float2CostFactor(
            const boost::property_tree::ptree &aJsonNode,
            const std::string& aAttributeName,
            const std::string& aFaultyPath,
            const boost::optional<const TKeywordsMap>& aKeywords
    ) {
        const boost::optional<const boost::property_tree::ptree&> optionalChild = aJsonNode.get_child_optional(aAttributeName);
        if (!optionalChild) {
            throw CompoundCostFunctionProfile::Exception(
                "Required field missing: "+ aFaultyPath + "::" + aAttributeName
            );
        }
        return float2CostFactor(
            optionalChild->get_value<std::string>(), aFaultyPath, aKeywords
        );
    }

    routing::CostFunction::CostFactor CompoundCostFunctionProfile::ComponentBase::float2CostFactor(
            boost::optional<std::string> aFloatCostFactor,
            const std::string& aFaultyPath,
            const boost::optional<const TKeywordsMap>& aKeywords
    ) {
        if (!aFloatCostFactor.is_initialized() || aFloatCostFactor->size() == 0) {
            throw CompoundCostFunctionProfile::Exception("Required field missing: "+aFaultyPath);
        }
        if (aKeywords.is_initialized()) {
            TKeywordsMap::const_iterator foundKeyword = aKeywords->find(*aFloatCostFactor);
            if ( foundKeyword != boost::end(*aKeywords)) {
                if (foundKeyword->second.is_initialized()) {
                    return *(foundKeyword->second);
                }
                throw CompoundCostFunctionProfile::Exception(
                    "Unable find the value of '" + *aFloatCostFactor + "'in the current context"
                );
            }
        }
        try {
            //warning.. std::stod can output a nan (when given something in the shape of 'NaN')
            //which is bordering impossible to detect with --fastmath. This is a workaround
            //not to accept NaNs coming from the wire, but it makes certain assumptions that would
            //be better addressed in FF2I::convert (had it been possible to detect NaNs).
            if (std::tolower((*aFloatCostFactor)[0]) == 'n') {
                throw std::invalid_argument("");
            }
            return FF2I::convert((float)std::stod(*aFloatCostFactor));
        } catch (const std::exception& einvalid_arg) {
            throw CompoundCostFunctionProfile::Exception(
                "Unable to interpret the value of '" + *aFloatCostFactor + "' at " + aFaultyPath + " as an expected positive floating point number"
            );
        }
    }


    geomap::Way::TMeans loadMeans(boost::property_tree::ptree &aMeans) {
        if (aMeans.get_value<std::string>() == KCar) {
            return geomap::Way::TMeans::ECar;
        }
        if (aMeans.get_value<std::string>() == KBicycle) {
            return geomap::Way::TMeans::EBicycle;
        }
        if (aMeans.get_value<std::string>() == KPedestrian) {
            return geomap::Way::TMeans::EPedestrian;
        }
        throw CompoundCostFunctionProfile::Exception("'means' (of navigation) must be either [pedestrian|bicycle|car] an not "+aMeans.get_value<std::string>());
    }

    void CompoundCostFunctionProfile::load(std::istream& aIStream, const NamedComponents& aComponents)
    {
        boost::property_tree::ptree pt;
        boost::property_tree::read_json(aIStream, pt);
        try {
            mMeans = loadMeans(pt.get_child(KMeans));
        } catch (boost::property_tree::ptree_bad_path& e) {
            throw Exception("Unable to find or parse the manadatory profile attribute: 'means' (pedestrian|bicycle|car).");
        }

        BOOST_FOREACH(boost::property_tree::ptree::value_type &v, pt.get_child("")) {
            if (v.first == KDescription) {
                mDescription = v.second.get_value<std::string>();
            } else {
                auto foundIt = aComponents.find(v.first);
                if (foundIt != aComponents.end()) {
                    foundIt->second->load(v.second, *this);
                    mComponents[foundIt->first] = foundIt->second;
                }
            }
        }
    }

    CompoundCostFunctionProfile::CompoundCalculator::CompoundCalculator(const CompoundCostFunctionProfile& aProfile) {
        Multiplier multiplier;
        for (auto const& component : aProfile.mComponents ) {
            mCalculators.push_back(component.second->calculator());
            multiplier.apply(mCalculators.back()->smallestFactor());
        }
        mSmallestFactor = multiplier.factor();
    }
}


