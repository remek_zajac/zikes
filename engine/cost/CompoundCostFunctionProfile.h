/*************************************************************************
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * (C) 2015 Remek Zajac
 *************************************************************************/
#ifndef ROUTINGPROFILE_H
#define ROUTINGPROFILE_H

#include <fstream>
#include <boost/filesystem.hpp>
#include <boost/property_tree/ptree.hpp>

#include "route/routing.h"


namespace cost {

    class CompoundCostFunctionProfile {

        public:
            /*
             * Setting float->int factors for all components into the range of 1..16..512*16
             */
            typedef routing::FloatFactor2IntPunishment<4,512> FF2I;

            /*
             *  Calculator encapsulates/abstracts cost _or_ cost factor calculating logic. It is put forward
             *  for the individual CompoundCostFunction components to produce and apply their logic
             *  against their respective parameters expressed in the profile.
             */
            class Calculator {
                public:
                    typedef std::unique_ptr<const Calculator> ConstUniqPtr;
                    typedef FF2I::Multiplier Multiplier;

                    /*
                     * Given a RouteHead (a head of a candidate route leading to the origin), Multipler and current Cost,
                     * Calculator::apply will either:
                     * - manipulate the Multiplier - if the Calculator's contribution is to be multiplied by the Cost
                     * - manipulate the Cost - if the Calculator's contribition is independent of the current Cost - say
                     *   when punishing junctions (independent what kind of distance/highway leads to them).
                     * The reason the Calculator shouldn't always manipulate the Cost (which it mathematically could),
                     * is that Multiplier will guard for type overflows and won't allow them stack over some threshold.
                     * It is important to note that the caller WILL multiply the cost against the Multiplier
                     * so implementations must not do it.
                     */
                    virtual void apply(const routing::RouteHead& aRouteHead, Multiplier& aMultiplier, routing::CostFunction::Cost& aCost) const = 0;

                    /*
                     * Each Calculator will return the smallest multiplier/factor it can produce - this is to
                     * to secure the admissable heuristic property of the cost function. When forming the "cost to destination"
                     * heuristic it must not be smaller than the actual cost calculated towards the destination.
                     * It must also be meanighfully close to that cost (A-star poses the f-score - the sum
                     * of cost to origin with the heuristic towards destination.
                     */
                    virtual routing::CostFunction::CostFactor smallestFactor() const = 0;

                    virtual ~Calculator() {};
            };


            /*
             * Abstraction of a single cost component/contributor with parameters possibly present in the profile
             */
            class Component {
                public:
                    typedef std::shared_ptr<Component> ShrdPtr;
                    typedef CompoundCostFunctionProfile::FF2I FF2I;
                    enum {
                        KForbiddenFactor = FF2I::MaxPunishFactor,
                        KNeutralFactor = FF2I::NeutralPunishFactor
                    };

                    virtual Calculator::ConstUniqPtr calculator() const = 0;
                    virtual void load(const boost::property_tree::ptree &aJsonNode, const CompoundCostFunctionProfile& aParent) = 0;
            };





            class Exception: public std::invalid_argument
            {
                public:
                    Exception(const std::string& aMessage)
                    :std::invalid_argument(aMessage)
                    {}
            };

            class CalculatorBase : public Calculator {
                public:
                    CalculatorBase(routing::CostFunction::CostFactor aSmallestFactor = FF2I::NeutralPunishFactor)
                    :mSmallestFactor(aSmallestFactor) {}

                    virtual routing::CostFunction::CostFactor smallestFactor() const { return mSmallestFactor; }
                protected:
                    routing::CostFunction::CostFactor mSmallestFactor;
            };

            class ComponentBase : public Component {
                public:
                    typedef std::map<std::string, std::string> TDefaultsDictionary;
                    typedef float FloatFactor;
                    typedef const std::map<std::string, boost::optional<routing::CostFunction::CostFactor> > TKeywordsMap;
                    typedef CompoundCostFunctionProfile::Exception Exception;

                    ComponentBase(routing::CostFunction::CostFactor aDefaultSmallestFactor = FF2I::NeutralPunishFactor)
                    :mSmallestFactor(aDefaultSmallestFactor)
                    {}

                    routing::CostFunction::CostFactor mSmallestFactor;

                protected:
                    routing::CostFunction::CostFactor float2CostFactor(
                            boost::optional<std::string> aFloatCostFactor,
                            const std::string& aFaultyPath,
                            const boost::optional<const TKeywordsMap>& aKeywords = boost::optional<const TKeywordsMap>()
                    );
                    routing::CostFunction::CostFactor float2CostFactor(
                            const boost::property_tree::ptree &aJsonNode,
                            const std::string& aAttributeName,
                            const std::string& aFaultyPath,
                            const boost::optional<const TKeywordsMap>& aKeywords = boost::optional<const TKeywordsMap>()
                    );
                    routing::CostFunction::CostFactor getOptionalFactor(
                            const boost::optional<const boost::property_tree::ptree&> aPath,
                            const std::string& aFactorName,
                            const TDefaultsDictionary& aDefaults,
                            const boost::optional<const TKeywordsMap>& aKeywords,
                            const std::string& aFaultyPath
                    );
            };

            typedef std::vector<routing::CostFunction::CostFactor> CostFactors;
            typedef std::unique_ptr<const CompoundCostFunctionProfile> ConstUniqPtr;
            typedef std::map<std::string, Component::ShrdPtr> NamedComponents;
            typedef std::shared_ptr<const CompoundCostFunctionProfile> ConstShrdProfile;

            CompoundCostFunctionProfile(
                std::istream& aIStream,
                const NamedComponents& aNamedComponents
            )
            :mMeans(geomap::Way::TMeans::ECar) {
                load(aIStream, aNamedComponents);
            }

            CompoundCostFunctionProfile(
                const boost::filesystem::path& aPath,
                const NamedComponents& aNamedComponents
            )
            :mFileName(aPath.filename().generic_string()),
             mMeans(geomap::Way::TMeans::ECar) {
                std::ifstream fis(aPath.string(), std::ifstream::in);
                load(fis, aNamedComponents);
                fis.close();
            }

            Calculator::ConstUniqPtr calculator() const {
                return Calculator::ConstUniqPtr(
                    new CompoundCalculator(*this)
                );
            }
            const geomap::Way::TMeans means() const noexcept {
                return mMeans;
            }
            const std::string& description() const noexcept {
                return mDescription;
            }
            const NamedComponents& components() const noexcept {
                return mComponents;
            }
            const boost::optional<const std::string>& fileName() const {
                return mFileName;
            }

        private:
            class CompoundCalculator : public Calculator {
                public:
                    CompoundCalculator(const CompoundCostFunctionProfile& aProfile);

                    virtual void apply(const routing::RouteHead& aRouteHead, Multiplier& aMultiplier, routing::CostFunction::Cost& aCost) const {
                        for (auto it = mCalculators.begin(); it != mCalculators.end(); it++) {
                            (*it)->apply(aRouteHead, aMultiplier, aCost);
                        }
                    }
                    virtual routing::CostFunction::CostFactor smallestFactor() const {return mSmallestFactor;}
                private:
                    routing::CostFunction::CostFactor            mSmallestFactor;
                    std::list<typename Calculator::ConstUniqPtr> mCalculators;
            };

            void load(std::istream& aIStream, const NamedComponents& aComponents);

            boost::optional<const std::string>      mFileName;
            geomap::Way::TMeans                     mMeans;
            NamedComponents                         mComponents;

            std::string                             mDescription;
            unsigned short                          mClimbFactorsStep;
    };

}

#endif // ROUTINGPROFILE_H
