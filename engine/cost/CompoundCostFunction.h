/*************************************************************************
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * (C) 2015 Remek Zajac
 *************************************************************************/
#ifndef ROUTINGPROFILECOSTFUNCTION_H
#define ROUTINGPROFILECOSTFUNCTION_H

#include <exception>
#include "CompoundCostFunctionProfile.h"

namespace cost {

    template <class DISTANCE_COST_FUNCTION>
    class CompoundCostFunction : public DISTANCE_COST_FUNCTION
    {
        public:

            CompoundCostFunction(const geo::Point& aTo,
                                 const CompoundCostFunctionProfile& aProfile)
            :DISTANCE_COST_FUNCTION(aTo),
             mCalculator(aProfile.calculator()) {}

            virtual routing::CostFunction::Cost evaluate(const routing::RouteHead& aRouteHead) const {
                routing::CostFunction::Cost cost = DISTANCE_COST_FUNCTION::evaluate(aRouteHead);
#ifndef NDEBUG
                routing::CostFunction::Cost distanceMts = cost;
#endif
                CompoundCostFunctionProfile::FF2I::Multiplier multiplier;
                mCalculator->apply(aRouteHead, multiplier, cost);
                cost *= multiplier.factor();
                assert(cost >= distanceMts * mCalculator->smallestFactor());
                return cost;
            }

            virtual routing::CostFunction::Cost heuristics_H(const geo::Point& aForVertex) const {
                return DISTANCE_COST_FUNCTION::heuristics_H(aForVertex) * mCalculator->smallestFactor();
            }

        protected:
            const CompoundCostFunctionProfile::Calculator::ConstUniqPtr mCalculator;

    };

    template <class DISTANCE_COST_FUNCTION>
    class NarrowSearchConeCompoundCostFunction : public CompoundCostFunction<DISTANCE_COST_FUNCTION>
    {
        public:

            NarrowSearchConeCompoundCostFunction(const geo::Point& aFrom,
                                         const geo::Point& aTo,
                                         const CompoundCostFunctionProfile& aProfile,
                                         float aOverestimate,
                                         float aOffset
                                         )
            :CompoundCostFunction<DISTANCE_COST_FUNCTION>(aTo, aProfile),
             mFrom(aFrom),
             mOverestimate(aOverestimate),
             mOffset(aOffset)
            {}

            virtual routing::CostFunction::Cost heuristics_H(const geo::Point& aForVertex) const {
                routing::CostFunction::Cost toDestination = DISTANCE_COST_FUNCTION::distanceMts(aForVertex, this->mTo);
                routing::CostFunction::Cost toOrigin      = DISTANCE_COST_FUNCTION::distanceMts(aForVertex, this->mFrom);
                routing::CostFunction::Cost cost          = toDestination * this->mCalculator->smallestFactor();
                                                               /*measure of being off-course*/
                float overestimate = mOverestimate * (toDestination + toOrigin) - mOffset;
                if (overestimate > 1.0) {
                    return cost * overestimate;
                }
                return  cost;
            }


        protected:
            const geo::Point mFrom;
            float            mOverestimate;
            float            mOffset;
    };


    template <class DISTANCE_COST_FUNCTION>
    class NarrowSearchConeCompoundCostFunctionFactory : public routing::CostFunctionFactory {
        public:
            enum {
                EInflationSlopeFactor   = 5,
                EInflationSlopeDistance = 10000
            };

            struct ReferencePoint {
                unsigned long distanceMts;
                unsigned long coneWidthMts;
                ReferencePoint(unsigned long aDistanceMts, unsigned long aConeWidthMts)
                :distanceMts(aDistanceMts),
                 coneWidthMts(aConeWidthMts)
                {}
            };


            NarrowSearchConeCompoundCostFunctionFactory(
                const CompoundCostFunctionProfile& aProfile,
                const ReferencePoint& aMinDistanceRefPoint,
                const ReferencePoint& aMaxDistanceRefPoint
            )
            :mProfile(aProfile),
             mMin(aMinDistanceRefPoint),
             mMax(aMaxDistanceRefPoint)
            {}

            virtual routing::CostFunction::UniquePtr create(const geo::Point& aFrom, const geo::Point& aTo) const {
                unsigned long distanceMts = DISTANCE_COST_FUNCTION::distanceMts(aFrom, aTo);
                float slope = 1.0*(mMax.coneWidthMts - mMin.coneWidthMts)/(mMax.distanceMts - mMin.distanceMts);
                unsigned long coneWidth4DistanceMts = distanceMts * slope + (mMax.coneWidthMts - slope*mMax.distanceMts);
                if (distanceMts > mMax.distanceMts) {
                    coneWidth4DistanceMts = mMax.coneWidthMts;
                }
                float inflate = EInflationSlopeFactor;
                inflate /= EInflationSlopeDistance;
                return std::move(
                    routing::CostFunction::UniquePtr(
                        new NarrowSearchConeCompoundCostFunction<DISTANCE_COST_FUNCTION>(
                            aFrom,
                            aTo,
                            mProfile,
                            inflate,
                            (coneWidth4DistanceMts/2 + distanceMts)*inflate
                        )
                    )
                );
            }

        protected:
            const CompoundCostFunctionProfile& mProfile;
            const ReferencePoint mMin;
            const ReferencePoint mMax;

    };

    template <class DISTANCE_COST_FUNCTION>
    class CompoundCostFunctionFactory : public NarrowSearchConeCompoundCostFunctionFactory<DISTANCE_COST_FUNCTION> {
        public:
            enum {
                //with the experiment in France for Dunkirk -> Bordeaux in 1000m increments it appears
                //the non-optimised router peels off at about 130km of distance, hence this number:
                //https://dl.dropboxusercontent.com/u/11723457/Zikes/searchCone.png
                ENarrowSearchConeAboveMts = 100000
            };

            typedef typename NarrowSearchConeCompoundCostFunctionFactory<DISTANCE_COST_FUNCTION>::ReferencePoint ReferencePoint;

            CompoundCostFunctionFactory(
                const CompoundCostFunctionProfile& aProfile,
                const ReferencePoint& aMinDistanceRefPoint,
                const ReferencePoint& aMaxDistanceRefPoint,
                unsigned long aNarrowSearchConeAboveMts = ENarrowSearchConeAboveMts
            )
            :NarrowSearchConeCompoundCostFunctionFactory<DISTANCE_COST_FUNCTION>(aProfile, aMinDistanceRefPoint, aMaxDistanceRefPoint),
             mNarrowSearchConeAboveMts(aNarrowSearchConeAboveMts)
            {}

            virtual routing::CostFunction::UniquePtr create(const geo::Point& aFrom, const geo::Point& aTo) const {
                if (aFrom.distanceMts(aTo) > mNarrowSearchConeAboveMts) {
                    return NarrowSearchConeCompoundCostFunctionFactory<DISTANCE_COST_FUNCTION>::create(aFrom, aTo);
                }
                return std::move(
                    routing::CostFunction::UniquePtr(
                        new CompoundCostFunction<DISTANCE_COST_FUNCTION>(aTo, this->mProfile)
                    )
                );
            }

        private:
            const unsigned long   mNarrowSearchConeAboveMts;
    };
}

#endif // ROUTINGPROFILECOSTFUNCTION_H
