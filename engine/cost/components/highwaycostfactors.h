/*************************************************************************
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * (C) 2016 Remek Zajac
 *************************************************************************/
#ifndef HIGHWAYCOSTFACTORS_H
#define HIGHWAYCOSTFACTORS_H

#include <vector>
#include "geomap/geomap.h"
#include "../CompoundCostFunctionProfile.h"

namespace cost {

    class Highway {
        public:
            typedef boost::shared_ptr<Highway> ShrdPtr;
            typedef std::vector<routing::CostFunction::CostFactor> FactorsBlock;

            Highway(
                routing::CostFunction::CostFactor aCost,
                routing::CostFunction::CostFactor aDesignated,
                routing::CostFunction::CostFactor aForbidden
            )
            {
                mFactors.push_back(aCost);
                mFactors.push_back(aDesignated);
                mFactors.push_back(aForbidden);
            }

            virtual void apply(const geomap::Way::TOneDirectionAccessAttributes& aAttributes,
                              geomap::Way::TMeans aMeans,
                              CompoundCostFunctionProfile::FF2I::Multiplier& aMultiplier,
                              routing::CostFunction::Cost& aCost) const {
                aMultiplier.apply(
                    factor(aAttributes, aMeans)
                );
            }

            inline routing::CostFunction::CostFactor smallest(routing::CostFunction::CostFactor aSmallest) const {
                for (const auto& factor : mFactors) {
                    aSmallest = std::min(aSmallest, factor);
                }
                return aSmallest;
            }

            inline routing::CostFunction::CostFactor factor(geomap::Way::TOneDirectionAccessAttributes aAttributes, geomap::Way::TMeans aMeans) const {
                return mFactors[aAttributes.access(aMeans)];
            }

        protected:
             FactorsBlock mFactors;
    };

    class HighwayWithFlatCost : public Highway {
        public:
            HighwayWithFlatCost(
                routing::CostFunction::CostFactor aCost,
                routing::CostFunction::CostFactor aDesignated,
                routing::CostFunction::CostFactor aForbidden,
                routing::CostFunction::Cost       aFlatCost
            ):Highway(aCost, aDesignated, aForbidden)
            {
                mFlatCost = aFlatCost;
            }

            virtual void apply(const geomap::Way::TOneDirectionAccessAttributes& aAttributes,
                              geomap::Way::TMeans aMeans,
                              CompoundCostFunctionProfile::FF2I::Multiplier& aMultiplier,
                              routing::CostFunction::Cost& aCost) const {
                aCost += mFlatCost;
                Highway::apply(aAttributes, aMeans, aMultiplier, aCost);
            }

            inline routing::CostFunction::Cost flatCost() const {
                return mFlatCost;
            }

        protected:
            routing::CostFunction::Cost mFlatCost;
    };


    class HighwayCostFactors : public CompoundCostFunctionProfile::ComponentBase
    {
        private:
            class HighwayCostCalculator : public CompoundCostFunctionProfile::CalculatorBase {
                public:
                    HighwayCostCalculator(const HighwayCostFactors& aFactors)
                    :CompoundCostFunctionProfile::CalculatorBase(aFactors.mSmallestFactor),
                     mFactors(aFactors)
                    {}

                    inline void apply(const routing::RouteHead& aRouteHead, CompoundCostFunctionProfile::FF2I::Multiplier& aMultiplier, routing::CostFunction::Cost& aCost) const {
                        mFactors.mHighways[aRouteHead.edge().highwayType()]->apply(
                            aRouteHead.edge().accessAttributes(), mFactors.mMeans, aMultiplier, aCost
                        );
                    }

                private:
                    const HighwayCostFactors& mFactors;
            };

        public:
            const static std::string KNodeName;

            HighwayCostFactors();

            void load(const boost::property_tree::ptree &aJsonNode, const CompoundCostFunctionProfile& aParent);
            CompoundCostFunctionProfile::Calculator::ConstUniqPtr calculator() const;

            inline const Highway& highway(unsigned int aId) const {
                assert(aId < mHighways.size());
                return *mHighways[aId];
            }

            inline geomap::Way::TMeans means() const {
                return mMeans;
            }

        private:
            Highway::ShrdPtr parseHighwayFactorsNode(
                    const boost::property_tree::ptree& aNode,
                    const std::string& aFaultyPath,
                    const TDefaultsDictionary& aDefaults
            );

            void parseDefaults(
                    const boost::property_tree::ptree& aNode,
                    TDefaultsDictionary& aDefaults,
                    const std::string& aFaultyPath
            );
            typedef std::map<std::string, std::string> TDefaultsDictionary;
            geomap::Way::TMeans mMeans;
            std::vector<Highway::ShrdPtr> mHighways;
    };
}

#endif // HIGHWAYCOSTFACTORS_H
