/*************************************************************************
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * (C) 2016 Remek Zajac
 *************************************************************************/
#ifndef SCORE_H
#define SCORE_H

#include "../../CompoundCostFunctionProfile.h"

namespace cost {


    /*
     * A utility class that, given the length and (highway) type of a single segment (Edge), will conclude whether
     * it's more likely an urban or rural segment.
     *
     * Note that urban/rural character of an area cannot be concluded from a single segment alone, but (as this whole implementation
     * hopes) can be from a good sample of nearby segments.
     */
    class SectionLengthUrbanScore {

        public:

            typedef int Score;

            enum {
                EUrbanScoreThreshold            = 15,
                EMinDistanceToConsiderMts       = 30,   //ignore super-short segments - they're more likely indicative of the intersection design than the area character.
                ETypicalUrbanSectionDistanceMts = 80,   //A section with a strong urban character typically has this or less metres.
                ETypicalRuralSectionDistanceMts = 300,  //A section with a strong rural character is typically this or longer
                EUrbanRuralDelta = ETypicalRuralSectionDistanceMts - ETypicalUrbanSectionDistanceMts //delta between typical rural and typical urban in metres
            };

            /*
             * Returns the score for the given edge based on its type-adjusted length (big roads have longer sections in the cities).
             * It returns an optional score as it will not conclude based on on a too-short-a section.
             */
            static inline boost::optional<Score> score(const geomap::DirectedEdge& aEdge) {
                return score(aEdge.distanceMts(), aEdge.highwayType());
            }

            /*
             * Returns the score for the given edge distance and its highway type.
             * It returns an optional score as it will not conclude based on on a too-short-a section.
             */
            static inline boost::optional<Score> score(unsigned long aEdgeDistanceMts, geomap::Way::HighwayType aHwyType) {
                unsigned long highwayTypeAdjustedDistance = hwyTypeAdjustedDistance(aEdgeDistanceMts, aHwyType);
                if (highwayTypeAdjustedDistance > EMinDistanceToConsiderMts) {
                    return scoreUrban(highwayTypeAdjustedDistance);
                }
                return boost::optional<Score>();
            }

            static inline Score scoreUrban(int aHighwayAdjustedDistance) {
                //delta from rural - i.e.: how much longer than ETypicalUrbanSectionDistanceMts this is.
                int delta = std::max(0, aHighwayAdjustedDistance - ETypicalUrbanSectionDistanceMts);
                //calculate the ratio/score of how much urban aHighwayAdjustedDistance is - inflating to range [-100..0..infinity] to stay with integral values
                int uncappedRuralScore = (200*delta/EUrbanRuralDelta)-100;
                int score = std::min(100, uncappedRuralScore); //cap [-100..0..infinity] to [-100..0..100]
                return -score; //flip from urbanScore to ruralScore
            }

            static inline bool isUrban(const Score& aScore) {
                return aScore > EUrbanScoreThreshold;
            }


            static inline unsigned long hwyTypeAdjustedDistance(unsigned long aEdgeDistanceMts, geomap::Way::HighwayType aHwyType) {
                return KHwyTypeAdjusters[aHwyType]*aEdgeDistanceMts/EInflateHwyAdjusters;
            }

        private:

            /*
             * It may be true that ETypicalUrbanSectionDistanceMts is a typical distance of an urban section, but only if
             * it's a road, not an express-way/trunk cutting through a city with much fewer intersections. It's crucial not
             * to consider those rural.
             */
            enum {
                EInflateHwyAdjusters = 64
            };
            static const unsigned int KHwyTypeAdjusters[24];
    };
}

#endif // SCORE_H
