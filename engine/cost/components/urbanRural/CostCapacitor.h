/*************************************************************************
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * (C) 2016 Remek Zajac
 *************************************************************************/
#ifndef LOANEDCOST_H
#define LOANEDCOST_H

#include "sectionLengthScore.h"

namespace cost {

    /**
     * ThrottledCost, given a routing::CostFunction::CostFactor, calculates the cost and throttles
     * it to a specific (typically lower then the given) routing::CostFunction::CostFactor
     * accumulating the remainder as a loan that it will pay back in subsequent invocations
     * irrespective of the given factor.
     */
    template <routing::CostFunction::CostFactor NeutralFactor>
    class ThrottledCost : public routing::RouteHead::Context {
            typedef SectionLengthUrbanScore::Score Score;

            enum {
                EMaxFactor = NeutralFactor+2
            };

            ThrottledCost(const ThrottledCost* aPrev)
            :mPrev(aPrev),
             mLoan(aPrev ? aPrev->mLoan : 0) {}

            void apply(routing::CostFunction::Cost& aCost, routing::CostFunction::CostFactor aFactor) {
                routing::CostFunction::Cost additionalCost = aCost * (aFactor-1) + mLoan;
                routing::CostFunction::Cost maxOneOffCost  = aCost * (EMaxFactor-1);
                if (additionalCost > maxOneOffCost) {
                    mLoan = additionalCost - maxOneOffCost;
                    additionalCost = maxOneOffCost;
                } else {
                    mLoan = 0;
                }
                aCost += additionalCost;
            }

        private:
            routing::CostFunction::Cost mLoan;
            const ThrottledCost*        mPrev;
    };



    /**
     * JunctionCostCapacitor is much like a (electric/heat) capacitor for the cost. When the routing algorithm
     * encounters a qualifying edge (qualifying for a specific routing::CostFunction::CostFactor punishment),
     * instead of applying that factor directly, the algorithm saves a JunctionCostCapacitor on that edge charging
     * it up by one notch.
     *
     * Any edge following 'this' has its capacitor pre-charged by the predecesor. If it qualifies, it
     * charges the capacitor further and if it doesn't, it discharges it (one notch at a time).
     *
     * Whenever the capacitor reaches its capacity, it starts to give way, it starts to radiate the cost
     * (at a constant rate).
     *
     * The result is both subtle and powerful and it comes very useful for the rural/urban filter.
     * Suppose we need to make the rural/urban judgment solely based on the edge lengths, which _tend_ to
     * be longer in the countryside. Punishing shorter lengths demonstrably does not work as its effect
     * is too local. Doing so results in routes going around very small villages or finding refuge in
     * longer arteries inside cities.
     *
     * The slow-charge property of the capacitor has it ignore small villages and its slow-discharge causes
     * long city arteries to offer no refuge.
     */
    template <routing::CostFunction::CostFactor NeutralFactor>
    class JunctionCostCapacitor  : public routing::RouteHead::Context {

        public:
           typedef std::pair<int, int> MinMaxCapacitorCharge;

            inline static bool paramsValid(const MinMaxCapacitorCharge& aMinMaxCapacitorCharge) {
                return (aMinMaxCapacitorCharge.first  < aMinMaxCapacitorCharge.second &&
                        aMinMaxCapacitorCharge.first  <= 0 &&
                        aMinMaxCapacitorCharge.second >= 0);
            }

            JunctionCostCapacitor(
                const MinMaxCapacitorCharge& aMinMaxCapacitorCharge,
                routing::CostFunction::CostFactor aFactor,
                CompoundCostFunctionProfile::Calculator::Multiplier& aMultiplier,
                const JunctionCostCapacitor* aPrev
            )
            :mMinMaxCapacitorCharge(aMinMaxCapacitorCharge),
             mFactor(aFactor),
             mPrev(aPrev),
             mBalance(aPrev ? aPrev->mBalance : 0)
            {
                assert(paramsValid(aMinMaxCapacitorCharge));
                apply(aMultiplier);
            }

            JunctionCostCapacitor(
                const MinMaxCapacitorCharge& aMinMaxCapacitorCharge,
                routing::CostFunction::CostFactor aFactor,
                CompoundCostFunctionProfile::Calculator::Multiplier& aMultiplier,
                const JunctionCostCapacitor* aPrev,
                bool aCharge
            )
            :mMinMaxCapacitorCharge(aMinMaxCapacitorCharge),
             mFactor(aFactor),
             mPrev(aPrev),
             mBalance(aPrev ? aPrev->mBalance : 0)
            {
                assert(paramsValid(aMinMaxCapacitorCharge));
                if (aCharge) {
                    mBalance--;
                    mBalance = std::max(mBalance, mMinMaxCapacitorCharge.first);
                } else {
                    mBalance++;
                    mBalance = std::min(mBalance, mMinMaxCapacitorCharge.second);
                }
                apply(aMultiplier);
            }

        private:
            void apply(CompoundCostFunctionProfile::Calculator::Multiplier& aMultiplier) {
                if (mBalance < 0) {
                    aMultiplier.apply(mFactor);
                } else {
                    aMultiplier.apply(NeutralFactor);
                }
            }

#ifndef NDEBUG
            virtual std::string toJson() const {
                std::stringstream ss;
                ss << "{" << "\"b\": " << mBalance << "}";
                return ss.str();
            }
#endif

            const MinMaxCapacitorCharge         mMinMaxCapacitorCharge;
            routing::CostFunction::CostFactor   mFactor;
            const  JunctionCostCapacitor*       mPrev;
            int                                 mBalance;
    };
}

#endif // LOANEDCOST_H
