/*************************************************************************
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * (C) 2016 Remek Zajac
 *************************************************************************/
#ifndef URBANESCAPEWALKER_H
#define URBANESCAPEWALKER_H

#include <boost/heap/fibonacci_heap.hpp>
#include <boost/pool/object_pool.hpp>
#include <set>

#include "sectionLengthScore.h"

namespace cost {

    /*
     * Heap definition for the Breadth First Search - see SectionLenghUrbanEscapeScore;
     */
    template <class PRIORITY, class ELEMENT>
    class OpenHeapFib
    {
        private:
            typedef std::pair<PRIORITY, ELEMENT*> OpenHeapElement;
            struct HeapElementCompare {
                bool operator()(const OpenHeapElement& n1, const OpenHeapElement& n2) const {
                    return n1.first < n2.first;
                }
            };
            typedef boost::heap::fibonacci_heap<OpenHeapElement, boost::heap::compare<HeapElementCompare> > PriotityQ;

        public:
            typedef typename PriotityQ::handle_type OpenSetHandle;

            OpenHeapFib()
            :mCompare(HeapElementCompare())
            {}

            void push(PRIORITY aF, ELEMENT& aEdge) {
                OpenHeapElement newElement(aF, &aEdge);
                mPriorityQueue.push(newElement);
            }

            ELEMENT& pop() {
                const OpenHeapElement& top = mPriorityQueue.top();
                mPriorityQueue.pop();
                return const_cast<ELEMENT&>(*top.second);
            }

            unsigned int size() const {return mPriorityQueue.size();}

        private:
            PriotityQ                mPriorityQueue;
            const HeapElementCompare mCompare;
    };


    /*
     * SectionLenghUrbanEscapeScore is a urban/rural filter strategy that given an edge in a graph
     * and using BFS (Breadth First Search), tries to prove that the edge is urban. Of course it can
     * fail in doing that - but the important thing is that for rural roads skimming a city, this strategy
     * is hoped to identify them as urban. This would not have been a case had the strategy tried to
     * prove the opposite.
     *
     * BFS is a standard heap-based openset with calculated priority (see EscapeHead::priority)
     * It has the word 'escape' in its name as it will try to efficently run away from the given
     * edge (will not try to make circles), see ::priority function.
     *
     * In other words, it's looking a large enough sample of nearby urban roads.
     */
    class SectionLenghUrbanEscapeScore : public SectionLengthUrbanScore {
            enum {
                EMaxUrbanHwyType = 16
            };
        public:

            inline static Score score(const geomap::DirectedEdge::ConstHandle& aEdge) {
                SectionLenghUrbanEscapeScore strategy(aEdge);
                const EscapeHead* escapee = strategy.escape();
                return escapee ? *(escapee->urbanScore()) : -100;
            }

        private:

            /*
             * Head of the BFS search.
             */
            class EscapeHead : private boost::noncopyable {

                    static const unsigned long KInflateComparableDistance; //it's generally very small (low floating numbers) and needs inflating to integral orders of magnitude
                    static const unsigned long KMaxComparableDistanceFromOrigin;
                    static const geomap::Edge::EdgeNode::DirectedEdges KNoNeighbours;

                    enum {
                        EMaxJunctionsFromOrigin   = 6, //do not evaluate anything further than this many junctions from origin
                        EMaxDistanceFromOriginMts = EMaxJunctionsFromOrigin * ETypicalRuralSectionDistanceMts //or this far from origin
                    };

                public:

                    typedef unsigned long EscapeFactor;
                    EscapeHead(const geomap::DirectedEdge::ConstHandle& aDirectedEdge,
                               const geomap::Edge::EdgeNode& aOrigin,
                               const EscapeHead* aPrev)
                    :mDirectedEdge(aDirectedEdge),
                     mPrev(aPrev),
                     mEndsWithRealJunction(aDirectedEdge->to().edges().size() > 2),
                     mDistanceSinceLastJunction(aPrev && !aPrev->mEndsWithRealJunction ? aPrev->mDistanceSinceLastJunction+aDirectedEdge->distanceMts() : aDirectedEdge->distanceMts()),
                     mJunctionsAwayFromOrigin(aPrev ? aPrev->mJunctionsAwayFromOrigin+aPrev->mEndsWithRealJunction : 0),
                     mComparableDistanceFromOrigin(KInflateComparableDistance * aOrigin.comparableDistance(aDirectedEdge->to())),
                     mCombinedAdjustedDistances(
                        aPrev ?
                            aPrev->mCombinedAdjustedDistances + hwyTypeAdjustedDistance(aDirectedEdge->distanceMts(), aDirectedEdge->highwayType())
                            :
                            0
                    )
                    {}

                    /*
                     * Returns Openset heap priority for this EscapeHead
                     * This bit is quite important as it calculates the heap priority to favour (examine ahead of) paths
                     * (escapees from mOrigin) that lead to:
                     * a) highway types indicative of urban areas (<= EMaxUrbanHwyType)
                     * b) that are further away from mOrigin - non-semi-cyclical - efficient escape.
                     * c) that have short lengths (and thus indicating urban character) - this length has stronger influence
                     *    than (a) and thus should not necesserily favour quick escapees - just efficient ones.
                     */
                    EscapeFactor priority() const {
                        EscapeFactor result = 0;
                        if (mDistanceSinceLastJunction > 0 && mDirectedEdge->highwayType() <= EMaxUrbanHwyType ) {
                            result = mComparableDistanceFromOrigin / mDistanceSinceLastJunction;
                        }
                        return result;
                    }

                    boost::optional<Score> urbanScore() const {
                        if (mJunctionsAwayFromOrigin > EMaxJunctionsFromOrigin) {
                            return scoreUrban(mCombinedAdjustedDistances/mJunctionsAwayFromOrigin);
                        }
                        return boost::optional<Score>();
                    }

                    const geomap::Edge::EdgeNode::DirectedEdges& neighbours() const {
                        if (mComparableDistanceFromOrigin < KMaxComparableDistanceFromOrigin &&
                            mJunctionsAwayFromOrigin <= EMaxJunctionsFromOrigin) {
                            return mDirectedEdge->to().edges();
                        }
                        return KNoNeighbours;
                    }

                    const geomap::Edge::DirectedEdge::ConstHandle mDirectedEdge;
                    const EscapeHead*                             mPrev;
                    const unsigned char                           mEndsWithRealJunction; //1 if yes, 0 if not
                    const unsigned long                           mDistanceSinceLastJunction;
                    const unsigned int                            mJunctionsAwayFromOrigin;
                    const unsigned int                            mComparableDistanceFromOrigin;
                    const unsigned int                            mCombinedAdjustedDistances;
            };


            SectionLenghUrbanEscapeScore(const geomap::DirectedEdge::ConstHandle& aOriginEdge)
            :mOriginEdge(aOriginEdge) {}

            /*
             * A very bog-standard implementation of breadth first search;
             * Finishes with first EscapeHead that can come up with a score or (if none can)
             * produces an 100% rural score;
             */
            inline const EscapeHead* escape() {
                OpenHeapFib<EscapeHead::EscapeFactor, EscapeHead> openSet;
                std::set<const geomap::Edge*> closedSet;

                EscapeHead* originRouteEdge = mEscapeHeadMemPool.construct(
                    mOriginEdge,
                    mOriginEdge->from(),
                    nullptr
                );
                openSet.push(originRouteEdge->priority(), *originRouteEdge);
                while (openSet.size() > 0) {
                    EscapeHead& current = openSet.pop();
                    closedSet.insert(&current.mDirectedEdge->edge());
                    for (
                         auto it  = boost::begin(current.neighbours());
                              it != boost::end(current.neighbours());
                              it++
                    ) {
                        const geomap::Edge::EdgeNode* neighbourFrom = &(*it)->from();
                        const geomap::Edge::EdgeNode* thisFrom = &mOriginEdge->from();
                        if ( neighbourFrom != thisFrom &&
                             closedSet.find(&(*it)->edge()) == closedSet.end()) {
                            EscapeHead* neighbourEscapeHead = mEscapeHeadMemPool.construct(
                                it->get(),
                                mOriginEdge->from(),
                                &current
                            );
                            boost::optional<Score> score = neighbourEscapeHead->urbanScore();
                            if (score) {
                                return neighbourEscapeHead;
                            }
                            openSet.push(neighbourEscapeHead->priority(), *neighbourEscapeHead);
                        }
                    }
                }
                return nullptr;
            }

            boost::object_pool<EscapeHead>           mEscapeHeadMemPool;
            const geomap::DirectedEdge::ConstHandle& mOriginEdge;
    };
}

#endif // URBANESCAPEWALKER_H
