/*************************************************************************
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * (C) 2016 Remek Zajac
 *************************************************************************/

#include "escapeScore.h"

namespace cost {
#define ADJUSTER(num, denom) SectionLengthUrbanScore::EInflateHwyAdjusters * num / denom
    const unsigned int SectionLengthUrbanScore::KHwyTypeAdjusters[24] = {
        ADJUSTER(1,10),   // "motorway",        # 0 (take 1/10th of the given length to work out its urbannes)
        ADJUSTER(1,10),   // "motorway_link",   # 1
        ADJUSTER(1,3),    // "trunk",           # 2
        ADJUSTER(1,3),    // "trunk_link",      # 3
        ADJUSTER(1,2),    // "primary",         # 4
        ADJUSTER(1,2),    // "primary_link",    # 5
        ADJUSTER(3,4),    // "secondary",       # 6
        ADJUSTER(3,4),    // "secondary_link",  # 7
        ADJUSTER(3,4),    // "tertiary",        # 8
        ADJUSTER(3,4),    // "tertiary_link",   # 9
        ADJUSTER(1,1),    // "road",            # 10
        ADJUSTER(1,1),    // "unclassified",    # 11
        ADJUSTER(1,1),    // "service",         # 12
        ADJUSTER(1,1),    // "bus_guideway",    # 13
        ADJUSTER(1,1),    // "raceway",         # 14
        ADJUSTER(1,1),    // "living_street",   # 15
        ADJUSTER(1,1),    // "residential",     # 16
        ADJUSTER(1,1),    // "cycleway",        # 17
        ADJUSTER(1,1),    // "footway",         # 18
        ADJUSTER(1,1),    // "pedestrian",      # 19
        ADJUSTER(1,1),    // "steps",           # 20
        ADJUSTER(1,1),    // "bridleway",       # 21
        ADJUSTER(1,1),    // "track",           # 22
        ADJUSTER(1,1)     // "path"             # 23
    };

    const unsigned long SectionLenghUrbanEscapeScore::EscapeHead::KInflateComparableDistance = std::pow(10,13);
    const unsigned long SectionLenghUrbanEscapeScore::EscapeHead::KMaxComparableDistanceFromOrigin =
        SectionLenghUrbanEscapeScore::EscapeHead::KInflateComparableDistance *
        geo::Point::comparableDistanceForMts(SectionLenghUrbanEscapeScore::EscapeHead::EMaxDistanceFromOriginMts);
    const geomap::Edge::EdgeNode::DirectedEdges SectionLenghUrbanEscapeScore::EscapeHead::KNoNeighbours = geomap::Edge::EdgeNode::DirectedEdges();
}
