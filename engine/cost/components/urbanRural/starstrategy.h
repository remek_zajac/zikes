/*************************************************************************
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * (C) 2016 Remek Zajac
 *************************************************************************/
#ifndef STARSTRATEGY_H
#define STARSTRATEGY_H

#include "sectionLengthScore.h"

namespace cost {

    class StarStrategy : public SectionLengthUrbanScore {
        private:
            class ScoreAccum {
                public:
                    ScoreAccum()
                    :mScore(0),
                     mContributors(0)
                    {}

                    void add(const boost::optional<Score>& aScore) {
                        if (aScore) {
                            mScore += *aScore;
                            mContributors++;
                        }
                    }

                    boost::optional<Score> finish() const {
                        if (mContributors > 0) {
                            return mScore/mContributors;
                        }
                        return boost::optional<Score>();
                    }

                private:
                    Score mScore;
                    int   mContributors;
            };

        public:
            static inline boost::optional<Score> score(const geomap::DirectedEdge& aEdge) {
                ScoreAccum score;
                if (aEdge.to().edges().size() == 2) {
                    unsigned long length         = 0;
                    geomap::Way::HighwayType hwy = 0;
                    for (auto const& edge : aEdge.to().edges()) {
                        LengthNType lt = neighbourLength(*edge);
                        length += lt.first;
                        hwy = std::max(hwy, lt.second);
                    }
                    score.add(
                        SectionLengthUrbanScore::score(length, hwy)
                    );
                } else {
                    for (auto const& edge : aEdge.to().edges()) {
                        score.add(neighbourScore(*edge));
                    }
                }
                return score.finish();
            }

            static inline boost::optional<Score> neighbourScore(const geomap::DirectedEdge& aEdge) {
                LengthNType lt = neighbourLength(aEdge);
                return SectionLengthUrbanScore::score(lt.first, lt.second);
            }

            typedef std::pair<unsigned long, geomap::Way::HighwayType> LengthNType;
            static inline LengthNType neighbourLength(const geomap::DirectedEdge& aEdge) {
                if (aEdge.to().edges().size() == 2) {
                    for (auto const& edge : aEdge.to().edges()) {
                        if (&edge->edge() != &aEdge.edge()) {
                            LengthNType followed = neighbourLength(*edge);
                            return LengthNType(
                                followed.first + aEdge.distanceMts(),
                                std::max(followed.second, aEdge.highwayType())
                            );
                        }
                    }
                }
                return LengthNType(
                    aEdge.distanceMts(),
                    aEdge.highwayType()
                );
            }
    };
}

#endif // STARSTRATEGY_H
