/*************************************************************************
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * (C) 2015 Remek Zajac
 *************************************************************************/
#ifndef ALL_H
#define ALL_H

#include "geomap/geomap.h"

#include "highwaycostfactors.h"
#include "climbfactors.h"
#include "urbanRural.h"
#include "turnpunishment.h"
#include "trafficsignals.h"
#include "circles.h"

namespace cost {

    class AllComponents {
        public:
            static CompoundCostFunctionProfile::NamedComponents construct() {
                CompoundCostFunctionProfile::NamedComponents result = {
                    { HighwayCostFactors::KNodeName,   std::make_shared<HighwayCostFactors>()},
                    { ClimbFactors::KNodeName,         std::make_shared<ClimbFactors>()},
                    { UrbanRural::KNodeName,           std::make_shared<UrbanRural>()},
                    { TurnPunishment::KNodeName,       std::make_shared<TurnPunishment>()},
                    { TrafficSignals::KNodeName,       std::make_shared<TrafficSignals>()},
                    { Circles::KNodeName,              std::make_shared<Circles>()}
                };
                return result;
            }
    };
}


#endif // ALL_H
