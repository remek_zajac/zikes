/*************************************************************************
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * (C) 2017 Remek Zajac
 *************************************************************************/
#ifndef Circle_H
#define Circle_H

#include "../CompoundCostFunctionProfile.h"

namespace cost {

    class Circles : public CompoundCostFunctionProfile::ComponentBase
    {
        public:
            const static std::string KNodeName;

            Circles()
            :CompoundCostFunctionProfile::ComponentBase(1)
            {}

            void load(const boost::property_tree::ptree &aJsonNode, const CompoundCostFunctionProfile& aParent);
            CompoundCostFunctionProfile::Calculator::ConstUniqPtr calculator() const;

        private:

            class Calculator : public CompoundCostFunctionProfile::CalculatorBase {
                public:
                    Calculator(const Circles& aCircles)
                    :CompoundCostFunctionProfile::CalculatorBase(aCircles.mSmallestFactor),
                     mCircles(aCircles)
                    {}

                    inline virtual void apply(const routing::RouteHead& aRouteHead, Multiplier& aMultiplier, routing::CostFunction::Cost& aCost) const {
                        for (auto& circle : mCircles.mCircles) {
                            circle.apply(aRouteHead, aMultiplier, aCost);
                        }
                    }

                private:
                    const Circles& mCircles;

            };

            class Circle {
                public:
                    Circle(geo::Point aCentre, unsigned long aRadiusMts, routing::CostFunction::CostFactor aPunishFactor)
                    :mCentre(aCentre),
                     mRadiusMts(aRadiusMts),
                     mPunishFactor(aPunishFactor)
                     {
                        geo::Point zeroDeg = mCentre.offsetMtsCourse(aRadiusMts, 0);
                        geo::Point ninentyDeg = mCentre.offsetMtsCourse(aRadiusMts, boost::math::constants::pi<double>()/2);
                        mMaxComparableDegreeDistance = std::max(
                            aCentre.comparableDistance(zeroDeg),
                            aCentre.comparableDistance(ninentyDeg)
                        );
                     }

                    inline virtual void apply(const routing::RouteHead& aRouteHead, Calculator::Multiplier& aMultiplier, routing::CostFunction::Cost& aCost) const {
                        const geo::Point& point = aRouteHead.edge().to();
                        if (point.comparableDistance(mCentre) < mMaxComparableDegreeDistance) {
                            if (point.distanceMts(mCentre) < mRadiusMts) {
                                aMultiplier.apply(mPunishFactor);
                            }
                        }
                    }

                private:
                    geo::Point mCentre;
                    unsigned long mRadiusMts;
                    routing::CostFunction::CostFactor mPunishFactor;
                    float mMaxComparableDegreeDistance;
            };


            std::vector<Circle> mCircles;
    };
}

#endif // Circle_H
