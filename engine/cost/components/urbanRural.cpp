/*************************************************************************
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * (C) 2016 Remek Zajac
 *************************************************************************/

#include <boost/foreach.hpp>

#include "urbanRural.h"
#include "urbanRural/starstrategy.h"

namespace cost {

    const std::string UrbanRural::KNodeName        = "urbanRural";
    const std::string             KUrbanFactor     = "urbanFactor";
    const std::string             KCapacitorCharge = "capacitorCharge";
    const std::string             KMin             = "min";
    const std::string             KMax             = "max";

    void UrbanRural::Calculator::apply(const routing::RouteHead& aRouteHead, Multiplier& aMultiplier, routing::CostFunction::Cost& aCost) const {
        const CostCapacitor* prev = aRouteHead.prev() ? static_cast<const CostCapacitor*>(aRouteHead.prev()->mUrbanScore.get()) : nullptr;
        boost::optional<StarStrategy::Score> score = StarStrategy::score(aRouteHead.edge());
        if (score) {
            aRouteHead.mUrbanScore = CostCapacitor::UniqPtr(
                new CostCapacitor(
                    mProfile.mMinMaxCapacitorCharge,
                    mProfile.mUrbanFactor,
                    aMultiplier,
                    prev,
                    StarStrategy::isUrban(*score)
                )
            );
        } else {
            aRouteHead.mUrbanScore = CostCapacitor::UniqPtr(
                new CostCapacitor(
                    mProfile.mMinMaxCapacitorCharge,
                    mProfile.mUrbanFactor,
                    aMultiplier,
                    prev
                )
            );
        }
    }

    UrbanRural::UrbanRural()
    :mMinMaxCapacitorCharge(EDeftMinCapacitorCharge, EDeftMaxCapacitorCharge)
    {}

    void UrbanRural::load(const boost::property_tree::ptree &aJsonNode, const CompoundCostFunctionProfile& aParent) {
        mUrbanFactor = float2CostFactor(aJsonNode, KUrbanFactor,KNodeName);
        mSmallestFactor = std::min(mSmallestFactor, mUrbanFactor);

        const boost::optional<const boost::property_tree::ptree&> optionalCapacitorCharges = aJsonNode.get_child_optional(KCapacitorCharge);
        if (optionalCapacitorCharges) {
            try {
                mMinMaxCapacitorCharge = CostCapacitor::MinMaxCapacitorCharge(
                    optionalCapacitorCharges->get_child(KMin).get_value<int>(EDeftMinCapacitorCharge),
                    optionalCapacitorCharges->get_child(KMax).get_value<int>(EDeftMaxCapacitorCharge)
                );
            } catch (boost::property_tree::ptree_bad_path& e) {
                throw CompoundCostFunctionProfile::Exception(
                    "Invalid parameters: "+ KNodeName + "::" + KCapacitorCharge + "::" + KMin + "/" + KMax
                );
            }
            if (!CostCapacitor::paramsValid(mMinMaxCapacitorCharge)) {
                throw CompoundCostFunctionProfile::Exception(
                    "Invalid parameters: "+ KNodeName + "::" + KCapacitorCharge + "::" + KMin + "/" + KMax
                );
            }
        }
    }

    CompoundCostFunctionProfile::Calculator::ConstUniqPtr UrbanRural::calculator() const {
        return CompoundCostFunctionProfile::Calculator::ConstUniqPtr(
            new Calculator(*this)
        );
    }
}

