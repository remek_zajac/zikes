/*************************************************************************
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * (C) 2017 Remek Zajac
 *************************************************************************/
#ifndef TrafficSignals_H
#define TrafficSignals_H

#include "../CompoundCostFunctionProfile.h"

namespace cost {

    class TrafficSignals : public CompoundCostFunctionProfile::ComponentBase
    {
        public:
            const static std::string KNodeName;
            const static std::string KPunishmentMts;

            TrafficSignals()
            :CompoundCostFunctionProfile::ComponentBase(1)
            {}

            void load(const boost::property_tree::ptree &aJsonNode, const CompoundCostFunctionProfile& aParent);
            CompoundCostFunctionProfile::Calculator::ConstUniqPtr calculator() const;

            static inline bool crossingCascade(const routing::RouteHead& aRouteHead) {
                //traffic lights form cascades and they light up green all at once, so this function
                //only returns true for the first occurence in a cascade
                return aRouteHead.edge().trafficSignals() && (aRouteHead.prev() == nullptr || !aRouteHead.prev()->edge().trafficSignals());
            }

        private:
            class Calculator : public CompoundCostFunctionProfile::CalculatorBase {
                public:
                    Calculator(const TrafficSignals& aFactors)
                    :CompoundCostFunctionProfile::CalculatorBase(aFactors.mSmallestFactor),
                     mTrafficSignals(aFactors)
                    {}

                    inline virtual void apply(const routing::RouteHead& aRouteHead, Multiplier& aMultiplier, routing::CostFunction::Cost& aCost) const {
                        if (crossingCascade(aRouteHead)) {
                            //traffic lights tend to be positioned in a cascade for a single junction and we only want to punish such junction once.
                            aCost += mTrafficSignals.mCostPunishment;
#ifndef NDEBUG
                            logSignals(aRouteHead.edge().from());
#endif
                        }
                    }

                private:
                    const TrafficSignals& mTrafficSignals;
#ifndef NDEBUG
                    void logSignals(const geo::Point& aPoint) const;
#endif

            };
            routing::CostFunction::CostFactor mCostPunishment;
    };
}

#endif // TrafficSignals_H
