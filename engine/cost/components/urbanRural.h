/*************************************************************************
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * (C) 2016 Remek Zajac
 *************************************************************************/
#ifndef UrbanRural_H
#define UrbanRural_H

#include <iostream>
#include <fstream>

#include "../CompoundCostFunctionProfile.h"
#include "urbanRural/CostCapacitor.h"

namespace cost {

    /*
     * UrbanRural is a component of the CompoundCostFunctionProfile that applies the given
     * {
     *  ...
     *  ubranFactor : x
     *  ...
     * }
     *
     * to the binary judgement it makes about the urban/rural character of the given routing::RouteHead
     */
    class UrbanRural : public CompoundCostFunctionProfile::ComponentBase {

        public:
            const static std::string KNodeName;
            enum {
                EDeftMinCapacitorCharge    = -8,
                EDeftMaxCapacitorCharge    = -EDeftMinCapacitorCharge
            };

            UrbanRural();
            typedef JunctionCostCapacitor<FF2I::NeutralPunishFactor> CostCapacitor;
            void load(const boost::property_tree::ptree &aJsonNode, const CompoundCostFunctionProfile& aParent);
            CompoundCostFunctionProfile::Calculator::ConstUniqPtr calculator() const;

        private:

            class Calculator : public CompoundCostFunctionProfile::CalculatorBase {
                public:
                    Calculator(const UrbanRural& aProfile)
                    :CompoundCostFunctionProfile::CalculatorBase(aProfile.mSmallestFactor),
                      mProfile(aProfile) {}

                    virtual ~Calculator() {}

                    virtual void apply(const routing::RouteHead& aRouteHead, Multiplier& aMultiplier, routing::CostFunction::Cost& aCost) const;

                private:
                    const UrbanRural& mProfile;
            };

            routing::CostFunction::CostFactor    mUrbanFactor;
            CostCapacitor::MinMaxCapacitorCharge mMinMaxCapacitorCharge;
    };
}


#endif // UrbanRural_H
