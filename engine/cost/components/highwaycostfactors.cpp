/*************************************************************************
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * (C) 2015 Remek Zajac
 *************************************************************************/
#include <boost/property_tree/json_parser.hpp>
#include <boost/foreach.hpp>
#include <boost/log/trivial.hpp>
#include <boost/make_shared.hpp>

#include "highwaycostfactors.h"
#include "../CompoundCostFunctionProfile.h"

#define LOG(level) BOOST_LOG_TRIVIAL(level) << "[RoutingProfile]> "

namespace cost {
    const std::string HighwayCostFactors::KNodeName = "highwayCostFactors";
    const std::string KCost                         = "cost";
    const std::string KDesignated                   = "designated";
    const std::string KForbidden                    = "forbidden";
    const std::string KForbid                       = "forbid";
    const std::string KBicycle                      = "bicycle";
    const std::string KDefault                      = "default";
    const std::string KFlatCost                     = "flatCost";


    HighwayCostFactors::HighwayCostFactors()
    :CompoundCostFunctionProfile::ComponentBase(KNeutralFactor),
     mHighways(
        geomap::Map::instance().getMeta().getHighwayTypes().size(),
        boost::make_shared<Highway>(
            HighwayCostFactors::KNeutralFactor,
            HighwayCostFactors::KNeutralFactor,
            HighwayCostFactors::KForbiddenFactor
        )
     )
    {}

    CompoundCostFunctionProfile::Calculator::ConstUniqPtr HighwayCostFactors::calculator() const {
        return CompoundCostFunctionProfile::Calculator::ConstUniqPtr(
            new HighwayCostCalculator(*this)
        );
    }

    void HighwayCostFactors::parseDefaults(
            const boost::property_tree::ptree& aNode,
            TDefaultsDictionary& aDefaults,
            const std::string& aFaultyPath
    ) {
        for (auto defaultPair : aDefaults) {
            const boost::optional<const boost::property_tree::ptree&> optionalChild = aNode.get_child_optional(defaultPair.first);
            if (optionalChild.is_initialized()) {
                boost::optional<std::string> optionalValue = optionalChild->get_value_optional<std::string>();
                if (optionalValue.is_initialized()) {
                    aDefaults[defaultPair.first] = *optionalValue;
                }
            }
        }
    }

    Highway::ShrdPtr HighwayCostFactors::parseHighwayFactorsNode(
            const boost::property_tree::ptree& aNode,
            const std::string& aFaultyPath,
            const TDefaultsDictionary& aDefaults
        ) {
        routing::CostFunction::CostFactor cost = getOptionalFactor(
            aNode,
            KCost,
            aDefaults,
            boost::optional<const TKeywordsMap>(
                {{KForbid, KForbiddenFactor}}
            ),
            aFaultyPath + "::" + KCost
        );

        routing::CostFunction::CostFactor designated = getOptionalFactor(
            aNode,
            KDesignated,
            aDefaults,
            boost::optional<const TKeywordsMap>(
                {{KForbid, KForbiddenFactor}, {KCost, cost}}
            ),
            aFaultyPath + "::" + KBicycle + "::" + KDesignated
        );

        routing::CostFunction::CostFactor forbidden = getOptionalFactor(
            aNode,
            KForbidden,
            aDefaults,
            boost::optional<const TKeywordsMap>(
                {{KForbid, KForbiddenFactor}, {KCost, cost}}
            ),
            aFaultyPath + "::" + KBicycle + "::" + KForbidden
        );
        const boost::optional<const boost::property_tree::ptree&> flatCost = aNode.get_child_optional(KFlatCost);
        if (flatCost.is_initialized()) {
            return boost::make_shared<HighwayWithFlatCost>(
                cost, designated, forbidden, flatCost->get_value<routing::CostFunction::Cost>()
            );
        }
        return boost::make_shared<Highway>(
            cost, designated, forbidden
        );
    }

    void HighwayCostFactors::load(const boost::property_tree::ptree &aJsonNode, const CompoundCostFunctionProfile& aParent) {
        mMeans = aParent.means();
        TDefaultsDictionary defaultsDictionary = {{KCost, "1"}, {KDesignated, KCost}, {KForbidden, KForbid}};
        boost::optional<const boost::property_tree::ptree&> defaults = aJsonNode.get_child_optional(KDefault);
        if (defaults.is_initialized()) {
            parseDefaults(
                *defaults,
                defaultsDictionary,
                KNodeName + "::" + KDefault
            );
            routing::CostFunction::CostFactor cost = float2CostFactor(
                defaultsDictionary[KCost],
                KNodeName + "::" + KDefault + "::" + KCost,
                boost::optional<const TKeywordsMap>(
                    {{KForbid, KForbiddenFactor}}
                )
            );
            Highway::ShrdPtr defaultHighway = boost::make_shared<Highway>(
                cost,
                float2CostFactor(
                    defaultsDictionary[KDesignated],
                    KNodeName + "::" + KDefault + "::" + KDesignated,
                    boost::optional<const TKeywordsMap>(
                        {{KCost, cost}, {KForbid, KForbiddenFactor}}
                    )
                ),
                float2CostFactor(
                    defaultsDictionary[KForbidden],
                    KNodeName + "::" + KDefault + "::" + KForbidden,
                    boost::optional<const TKeywordsMap>(
                        {{KCost,cost}, {KForbid, KForbiddenFactor}}
                    )
                )
            );
            for (unsigned int i = 0; i < geomap::Map::instance().getMeta().getHighwayTypes().size(); i++) {
                mHighways[i] = defaultHighway;
            }
        }

        BOOST_FOREACH(const boost::property_tree::ptree::value_type &v, aJsonNode.get_child("")) {
            auto knownHighwayType =  geomap::Map::instance().getMeta().getHighwayTypes().find(v.first);
            if ( knownHighwayType != geomap::Map::instance().getMeta().getHighwayTypes().end()) {
                mHighways[knownHighwayType->second] = parseHighwayFactorsNode(
                    v.second,
                    KNodeName + "::" + v.first,
                    defaultsDictionary
                );
            } else if (v.first != KDefault) {
                LOG(warning) << "Unrecognised highway type: " << v.first;
            }
        }
        mSmallestFactor = std::numeric_limits<routing::CostFunction::CostFactor>::max();
        for (const auto& hwy: mHighways) {
            mSmallestFactor = hwy->smallest(mSmallestFactor);
        }
    }
}


