/*************************************************************************
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * (C) 2016 Remek Zajac
 *************************************************************************/

#include <boost/foreach.hpp>

#include "climbfactors.h"

namespace cost {

    const std::string ClimbFactors::KNodeName = "climbCostFactors";
    const std::string KCostFactors            = "costFactors";

    void ClimbFactors::load(const boost::property_tree::ptree &aJsonNode, const CompoundCostFunctionProfile& aParent) {
        const boost::optional<const boost::property_tree::ptree&> optionalCostFactorChild = aJsonNode.get_child_optional(KCostFactors);
        if (!optionalCostFactorChild) {
            throw Exception("Missing attribute " + KNodeName + "::" + KCostFactors);
        }

        BOOST_FOREACH(const boost::property_tree::ptree::value_type &costFactor, optionalCostFactorChild->get_child("")) {
            routing::CostFunction::CostFactor factor = float2CostFactor(costFactor.second.get_value<std::string>(), KNodeName + "::" + KCostFactors);
            mFactors.push_back(factor);
            mSmallestFactor = std::min(mSmallestFactor, factor);
        }
        if (mFactors.size() == 0 || mFactors.size() % 2 == 0) {
            std::stringstream ss;
            ss << "There must be an odd number of entries in the '" + KCostFactors + "' and not " << mFactors.size();
            ss << ". The value in the middle is the factor for the 0% climb whilst single percent point steps going from it left";
            ss << "and right are cost factors for the discretelly (in/de)-creasing climbs.";
            throw Exception(ss.str());
        }
        mMinClimbPercent = -(mFactors.size() - 1)/2;
    }

    CompoundCostFunctionProfile::Calculator::ConstUniqPtr ClimbFactors::calculator() const {
        return CompoundCostFunctionProfile::Calculator::ConstUniqPtr(
            new ClimbCostCalculator(*this)
        );
    }
}



