/*************************************************************************
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * (C) 2016 Remek Zajac
 *************************************************************************/
#ifndef TURNPUNISHMENT_H
#define TURNPUNISHMENT_H

#include "../CompoundCostFunctionProfile.h"
#include "./trafficsignals.h"

namespace cost {

    class TurnPunishment : public CompoundCostFunctionProfile::ComponentBase {
            static const float KMinAngleForTurn;
        public:
            typedef std::vector<routing::CostFunction::CostFactor> TCostPerHighway;
            const static std::string KNodeName;

            TurnPunishment()
            :CompoundCostFunctionProfile::ComponentBase(1)
            {}

            void load(const boost::property_tree::ptree &aJsonNode, const CompoundCostFunctionProfile& aParent);
            CompoundCostFunctionProfile::Calculator::ConstUniqPtr calculator() const;
            inline routing::CostFunction::Cost difficult(geomap::Way::HighwayType aForHighway1, geomap::Way::HighwayType aForHighway2) const {
                return std::max(mDifficult[aForHighway1], mDifficult[aForHighway2]);
            }
            inline routing::CostFunction::Cost easy(geomap::Way::HighwayType aForHighway1, geomap::Way::HighwayType aForHighway2) const {
                return std::max(mEasy[aForHighway1], mEasy[aForHighway2]);
            }
            inline routing::CostFunction::Cost TrafficSignals(geomap::Way::HighwayType aForHighway1, geomap::Way::HighwayType aForHighway2) const {
                return std::max(mTrafficSignals[aForHighway1], mTrafficSignals[aForHighway2]);
            }


        private:
            void fillCostPerHighway(TCostPerHighway& aCosts, const boost::optional<const boost::property_tree::ptree&>& aConfigSrc, const TCostPerHighway* aTemplateCosts, const std::string& aDebugTag);

            class TurnPunishmentCalculator : public CompoundCostFunctionProfile::CalculatorBase {
                public:
                    TurnPunishmentCalculator(const TurnPunishment& aTurnPunishment)
                    :CompoundCostFunctionProfile::CalculatorBase(aTurnPunishment.mSmallestFactor),
                     mTurnPunishment(aTurnPunishment)
                    {}

                    inline virtual void apply(const routing::RouteHead& aRouteHead, Multiplier& aMultiplier, routing::CostFunction::Cost& aCost) const {                    
                        if (aRouteHead.prev() != nullptr) {
                            const routing::RouteHead& prevRouteHead       = *aRouteHead.prev();
                            const geomap::Edge::DirectedEdge& prevEdge    = prevRouteHead.edge();
                            const geomap::Edge::DirectedEdge& currentEdge = aRouteHead.edge();
                            const geomap::Edge::EdgeNode& pivot           = prevEdge.to();
                            if (pivot.edges().size() > 2 && &prevEdge.edge().way() != &currentEdge.edge().way()) {
                                //we're changing the way - this may mean many things, including that we're staying on the same road
                                //but it happens, for whatever reason, be split here.
                                routing::CostFunction::Cost punishment = mTurnPunishment.easy(prevEdge.highwayType(), aRouteHead.edge().highwayType());
                                if (punishment == 0) {
                                    //we're assuming, if there ain't easy punishment for these highways, there won't be difficult punishment either
                                    //perhaps this will change.
                                    return;
                                }

                                if (currentEdge.trafficSignals() && prevEdge.trafficSignals()) { //there are traffic lights involved, ignore turn direction then
                                    if (TrafficSignals::crossingCascade(prevRouteHead)) { //and only punish once per cascade
                                        punishment = mTurnPunishment.TrafficSignals(prevEdge.highwayType(), aRouteHead.edge().highwayType());
                                        aCost += punishment;
#ifndef NDEBUG
                                        logTurn("Traffic lights", punishment, prevEdge, currentEdge);
#endif
                                    }
                                } else {
                                    //work out the turn angle
                                    const geo::EuclidianVector fromVector(prevEdge.from(), pivot);
                                    const geo::EuclidianVector toVector(pivot, currentEdge.to());
                                    const float diamondAngle = fromVector.diamondAngle(toVector);
                                    if (diamondAngle > KMinAngleForTurn) {
                                        //right turn
                                        if (aRouteHead.edge().edge().way().leftHandSideTraffic()) {
                                            punishment = mTurnPunishment.difficult(prevEdge.highwayType(), aRouteHead.edge().highwayType());
                                        }
                                        aCost += punishment;
#ifndef NDEBUG
                                        logTurn(aRouteHead.edge().edge().way().leftHandSideTraffic() ? "Right/hard" : "Right/easy", punishment, prevEdge, currentEdge);
#endif
                                    } else if (diamondAngle < -KMinAngleForTurn) {
                                        //left turn
                                        if (!aRouteHead.edge().edge().way().leftHandSideTraffic()) {
                                            punishment = mTurnPunishment.difficult(prevEdge.highwayType(), aRouteHead.edge().highwayType());
                                        }
                                        aCost += punishment;
#ifndef NDEBUG
                                        //logTurn(aRouteHead.edge().edge().way().leftHandSideTraffic() ? "Left/easy" : "left/difficult", punishment, prevEdge, currentEdge);
#endif
                                    }
                                }
                            }
                        }
                    }

                private:
#ifndef NDEBUG
                    void logTurn(const std::string& aDescription, routing::CostFunction::Cost aPunishment, const geomap::Edge::DirectedEdge& aFrom, const geomap::Edge::DirectedEdge& aTo) const;
#endif
                    const TurnPunishment& mTurnPunishment;
            };

            //categorised (array slot) per highway type
            TCostPerHighway mEasy;           //where there is right-hand side traffic, this is right turns
            TCostPerHighway mTrafficSignals;  //where there are traffic lights
            TCostPerHighway mDifficult;      //where there is right-hand side traffic, this is left turns
    };
}


#endif // TURNPUNISHMENT_H
