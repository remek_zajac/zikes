/*************************************************************************
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * (C) 2016 Remek Zajac
 *************************************************************************/

#include <boost/log/trivial.hpp>
#include "turnpunishment.h"

#define LOG(level) BOOST_LOG_TRIVIAL(level) << "[Cost::Turns]> "

namespace cost {

    const float TurnPunishment::KMinAngleForTurn  = 0.3;
    const std::string TurnPunishment::KNodeName   = "turnPunishment";
    const std::string KEasy                       = "easy";
    const std::string KDifficult                  = "difficult";
    const std::string KTrafficSignals             = "trafficSignals";


    void TurnPunishment::fillCostPerHighway(TCostPerHighway& aCosts, const boost::optional<const boost::property_tree::ptree&>& aConfigSrc, const TCostPerHighway* aTemplateCosts, const std::string& aDebugTag) {
        if (!aConfigSrc.is_initialized()) {
            throw Exception("Can't find mandatory "+KNodeName+"::"+aDebugTag + " dictionary/map of highway names -> cost factors");
        }
        if (aConfigSrc->get_value<std::string>().size() != 0) {
            throw Exception("Unable to interpret node of "+KNodeName+"::"+aDebugTag + " as a dictionary/map of highway names -> cost factors");
        }
        for (auto highwayType : geomap::Map::instance().getMeta().getHighwayTypes()) {
            aCosts.push_back(
                aTemplateCosts != nullptr ? aTemplateCosts->at(aCosts.size()) : 0
            );
        }
        bool atLeastOneEntry = false;
        if (aConfigSrc) {
            for (auto highwayType : geomap::Map::instance().getMeta().getHighwayTypes()) {
                const boost::optional<const boost::property_tree::ptree&> optionalHighwayTypeChild = aConfigSrc->get_child_optional(highwayType.first);
                if (optionalHighwayTypeChild) {
                    try {
                        long punishmentMts = optionalHighwayTypeChild->get_value<long>();
                        if (punishmentMts < 0) {
                            throw boost::property_tree::ptree_bad_data("", optionalHighwayTypeChild->get_value<std::string>());
                        }
                        aCosts[highwayType.second] = punishmentMts;
                        atLeastOneEntry = true;
                    } catch (boost::property_tree::ptree_bad_data& e) {
                        throw Exception("Unable to interpret value of "+KNodeName+"::"+aDebugTag+"::"+ highwayType.first + " = " + optionalHighwayTypeChild->get_value<std::string>() + " as a positive integer expressing flat punishment in metres");
                    }
                }
            }
        }
        if (!atLeastOneEntry) {
            throw Exception("Unable to interpret node of "+KNodeName+"::"+aDebugTag + "a (non-empty) dictionary/map of highway names -> cost factors");
        }
    }

    void TurnPunishment::load(const boost::property_tree::ptree  &aJsonNode, const CompoundCostFunctionProfile& aParent) {
        fillCostPerHighway(mEasy,           aJsonNode.get_child_optional(KEasy),           nullptr, KEasy);
        fillCostPerHighway(mTrafficSignals, aJsonNode.get_child_optional(KTrafficSignals), nullptr, KTrafficSignals);
        fillCostPerHighway(mDifficult,      aJsonNode.get_child_optional(KDifficult),      &mEasy,  KDifficult);
    }

    CompoundCostFunctionProfile::Calculator::ConstUniqPtr TurnPunishment::calculator() const {
        return CompoundCostFunctionProfile::Calculator::ConstUniqPtr(
            new TurnPunishmentCalculator(*this)
        );
    }

#ifndef NDEBUG
    void TurnPunishment::TurnPunishmentCalculator::logTurn(const std::string& aDescription, routing::CostFunction::Cost aPunishment, const geomap::Edge::DirectedEdge& aFrom, const geomap::Edge::DirectedEdge& aTo) const {
        LOG(debug) << "{ \"description\": \"" << aDescription << "\", \"magnitude\": " << aPunishment << ", \"section\": [" << aFrom.toString() << ", " << aTo.toString() << "]}";
    }
#endif
}

