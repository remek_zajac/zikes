/*************************************************************************
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * (C) 2016 Remek Zajac
 *************************************************************************/
#ifndef CLIMBFACTORS_H
#define CLIMBFACTORS_H

#include "../CompoundCostFunctionProfile.h"

namespace cost {

    class ClimbFactors : public CompoundCostFunctionProfile::ComponentBase
    {
        public:
            const static std::string KNodeName;
            ClimbFactors()
            :CompoundCostFunctionProfile::ComponentBase(KForbiddenFactor)
            {}

            void load(const boost::property_tree::ptree &aJsonNode, const CompoundCostFunctionProfile& aParent);
            CompoundCostFunctionProfile::Calculator::ConstUniqPtr calculator() const;
            inline const routing::CostFunction::CostFactor factor(geomap::Edge::ClimbPercent aForClimbPercent) const noexcept {
                assert(mFactors.size() > 0);
                if (aForClimbPercent) {
                    int distanceFromBegininng = std::min(std::max((int)(aForClimbPercent - mMinClimbPercent), 0), (int)mFactors.size() - 1);
                    return mFactors[distanceFromBegininng];
                }
                return KNeutralFactor;
            }
        private:
            class ClimbCostCalculator : public CompoundCostFunctionProfile::CalculatorBase {
                public:
                    ClimbCostCalculator(const ClimbFactors& aFactors)
                    :CompoundCostFunctionProfile::CalculatorBase(aFactors.mSmallestFactor),
                     mClimbCostFactors(aFactors)
                    {}

                    inline virtual void apply(const routing::RouteHead& aRouteHead, Multiplier& aMultiplier, routing::CostFunction::Cost& aCost) const {
                        aMultiplier.apply(mClimbCostFactors.factor(aRouteHead.edge().climbPercent()));
                    }

                private:
                    const ClimbFactors& mClimbCostFactors;
            };

            std::vector<routing::CostFunction::CostFactor> mFactors;
            short                                          mMinClimbPercent;
    };
}



#endif // CLIMBFACTORS_H
