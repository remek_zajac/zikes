/*************************************************************************
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * (C) 2017 Remek Zajac
 *************************************************************************/
#include <boost/log/trivial.hpp>
#include <boost/foreach.hpp>

#define LOG(level) BOOST_LOG_TRIVIAL(level) << "[Cost::PointRadius]> "

#include "circles.h"


namespace cost {

    const std::string Circles::KNodeName      = "circles";
    const std::string KCentre                 = "centre";
    const std::string KRadiusMts              = "radiusMts";
    const std::string KPunishFactor           = "punishFactor";

    void Circles::load(const boost::property_tree::ptree &aJsonNode, const CompoundCostFunctionProfile& aParent) {
        BOOST_FOREACH(const boost::property_tree::ptree::value_type &v, aJsonNode.get_child("")) {
            const boost::property_tree::ptree& circle = v.second;
            boost::optional<const boost::property_tree::ptree&> optionalCentre = circle.get_child_optional(KCentre);
            if (!optionalCentre) {
                throw Exception("Missing attribute " + KNodeName + "::" + KCentre);
            }
            if (optionalCentre->count("") != 2) {
                throw Exception("Expected two [lat,lng] floats for " + KNodeName + "::" + KCentre);
            }

            auto it = optionalCentre->begin();
            float lat, lng;
            try {
                lat = it->second.get_value<float>();
                it++;
                lng = it->second.get_value<float>();
            } catch (boost::property_tree::ptree_bad_data& e) {
                throw Exception("Expected two [lat,lng] floats for " + KNodeName + "::" + KCentre);
            }

            geo::Point centre(lat,lng);
            boost::optional<const boost::property_tree::ptree&> optionalRadius = circle.get_child_optional(KRadiusMts);
            if (!optionalRadius && optionalRadius->get_value_optional<std::string>()) {
                throw Exception("Missing attribute " + KNodeName + "::" + KRadiusMts);
            }
            unsigned long radiusMts;
            try {
                radiusMts = optionalRadius->get_value<unsigned long>();
            } catch (boost::property_tree::ptree_bad_data& e) {
                throw Exception("Can't interpret' " + KNodeName + "::" + KRadiusMts + "=" + optionalRadius->get_value<std::string>() + " as a unsigned long integer");
            }

            mCircles.push_back(
                Circle(
                    centre, radiusMts, float2CostFactor(
                        circle, KPunishFactor, KNodeName + "::" + KPunishFactor
                    )
                )
            );
        }
    }

    CompoundCostFunctionProfile::Calculator::ConstUniqPtr Circles::calculator() const {
        return CompoundCostFunctionProfile::Calculator::ConstUniqPtr(
            new Calculator(*this)
        );
    }

}

