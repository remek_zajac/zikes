/*************************************************************************
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * (C) 2017 Remek Zajac
 *************************************************************************/
#include "trafficsignals.h"
#include <boost/log/trivial.hpp>

#define LOG(level) BOOST_LOG_TRIVIAL(level) << "[Cost::TrafficSignals]> "

namespace cost {

    const std::string TrafficSignals::KNodeName      = "trafficSignals";
    const std::string TrafficSignals::KPunishmentMts = "punishmentMts";

    void TrafficSignals::load(const boost::property_tree::ptree &aJsonNode, const CompoundCostFunctionProfile& aParent) {
        try {
            mCostPunishment = aJsonNode.get_child(KPunishmentMts).get_value<int>();
        } catch (boost::property_tree::ptree_bad_path& e) {
            throw CompoundCostFunctionProfile::Exception(
                "Invalid parameters: "+ KNodeName + "::" + KPunishmentMts
            );
        }
    }

    CompoundCostFunctionProfile::Calculator::ConstUniqPtr TrafficSignals::calculator() const {
        return CompoundCostFunctionProfile::Calculator::ConstUniqPtr(
            new Calculator(*this)
        );
    }

#ifndef NDEBUG
    void TrafficSignals::Calculator::logSignals(const geo::Point& aPoint) const {
        LOG(debug) << aPoint.toString();
    }
#endif
}

